//Dear emacs, this is -*- c++ -*-

/**
 * @file markertest.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Describes some tests with standard eformat markers
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE HeaderMarker Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "eformat/HeaderMarker.h"

BOOST_AUTO_TEST_CASE( marker_child )
{
  BOOST_CHECK_EQUAL(eformat::ROD, eformat::child_marker(eformat::ROB));
  BOOST_CHECK_EQUAL(eformat::ROB, eformat::child_marker(eformat::FULL_EVENT));
}

BOOST_AUTO_TEST_CASE( marker_string )
{
  BOOST_CHECK_EQUAL(eformat::marker2string(eformat::ROD), "ROD");
  BOOST_CHECK_EQUAL(eformat::marker2string(eformat::ROB), "ROB");
  BOOST_CHECK_EQUAL(eformat::marker2string(eformat::FULL_EVENT), "FULL_EVENT");
}

BOOST_AUTO_TEST_CASE( marker_string_2 )
{
  BOOST_CHECK_EQUAL(eformat::marker2string(0xee1234ee), "ROD");
  BOOST_CHECK_EQUAL(eformat::marker2string(0xdd1234dd), "ROB");
  BOOST_CHECK_EQUAL(eformat::marker2string(0xaa1234aa), "FULL_EVENT");
}

