//Dear emacs, this is -*- c++ -*-

/**
 * @file fecompressiontest.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author: vandelli $
 * $Revision: 112969 $
 * $Date: 2013-05-06 19:22:10 +0200 (Mon, 06 May 2013) $
 *
 * FullEvent fragment building and reading test for compressed events using Boost Unit Test
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FullEventFragment Compressed Memory Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "eformat/write/FullEventFragment.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/FullEventFragment.h"
#include "eformat/Status.h"
#include "eformat/HeaderMarker.h"
#include "eformat/index.h"
#include "eformat/checksum.h"
#include "eformat/compression.h"

#include <string>

static const uint32_t RUN_NUMBER = 0xababab; 
static const uint32_t RUN_TYPE = 0xddcceeff; 
static const uint16_t LUMI_BLOCK = 0xfafa;
static const uint32_t LVL1_ID = 0xcdcdcd;
static const uint16_t BC_ID = 0xfef;
static const uint8_t LVL1_TYPE = 0x12;
static const uint32_t COMPRESSION_TYPE = eformat::ZLIB;
static const uint32_t DETEV_TYPE = 0xaacdef;
static uint32_t CHECKSUM_TYPE = eformat::NO_CHECKSUM;
static const uint32_t BC_TIME_SECONDS = 0x12345678;
static const uint32_t BC_TIME_NANOSECONDS = 0x9abcdff;
static const uint64_t GLOBAL_ID = 0xFFFFABDC12344321;
static const uint32_t NL1_INFO = 2;
static const uint32_t L1_INFO[NL1_INFO] = { 0xe, 0xf };
static const uint32_t NL2_INFO = 3;
static const uint32_t L2_INFO[NL2_INFO] = { 0xa, 0xb, 0xc };
static const uint32_t NEF_INFO = 4;
static const uint32_t EF_INFO[NEF_INFO] = { 0x5, 0x6, 0x7, 0x8 };
static const uint32_t NSTREAM_TAG = 4; //4 words
static const char* STREAM_TAG = "This is a test \0"; //16 chars

/**
 * Checks for the reading of the data inside the serializer
 *
 * @param id The source identifier originally set at the writer
 * @param writer The writer used for the test
 */
void checkMetaData 
(const eformat::helper::SourceIdentifier& id, 
 const eformat::write::FullEventFragment& writer)
{
  uint32_t checksum_addon = 0;
  if (CHECKSUM_TYPE != eformat::NO_CHECKSUM) checksum_addon = 1;

  //do some checking using the writer API
  BOOST_CHECK_EQUAL(writer.source_id(), id.code());
  BOOST_CHECK_EQUAL(writer.minor_version(), eformat::DEFAULT_VERSION & 0xffff);
  BOOST_CHECK_EQUAL(writer.meta_size_word(), 23 + writer.nstatus() + 
		  writer.nstream_tag() + writer.nlvl1_trigger_info() +
      writer.nlvl2_trigger_info() +
		  writer.nevent_filter_info() + checksum_addon);
  BOOST_CHECK_EQUAL(writer.size_word(), 
      writer.meta_size_word() + checksum_addon);
  BOOST_CHECK_EQUAL(writer.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(writer.compression_type(), COMPRESSION_TYPE);
  BOOST_CHECK_EQUAL(writer.compression_level(),1);
  BOOST_CHECK_EQUAL(writer.bc_time_seconds(), BC_TIME_SECONDS);
  BOOST_CHECK_EQUAL(writer.bc_time_nanoseconds(), BC_TIME_NANOSECONDS);
  BOOST_CHECK_EQUAL(writer.global_id(), GLOBAL_ID);
  BOOST_CHECK_EQUAL(writer.run_type(), RUN_TYPE);
  BOOST_CHECK_EQUAL(writer.run_no(), RUN_NUMBER);
  BOOST_CHECK_EQUAL(writer.lumi_block(), LUMI_BLOCK);
  BOOST_CHECK_EQUAL(writer.lvl1_id(), LVL1_ID);
  BOOST_CHECK_EQUAL(writer.bc_id(), BC_ID);
  BOOST_CHECK_EQUAL(writer.lvl1_trigger_type(), LVL1_TYPE);
  BOOST_CHECK_EQUAL(writer.nlvl1_trigger_info(), NL1_INFO);
  for (unsigned int i=0; i<NL1_INFO; ++i) 
    BOOST_CHECK_EQUAL(writer.lvl1_trigger_info()[i], L1_INFO[i]);
  BOOST_CHECK_EQUAL(writer.nlvl2_trigger_info(), NL2_INFO);
  for (unsigned int i=0; i<NL2_INFO; ++i) 
    BOOST_CHECK_EQUAL(writer.lvl2_trigger_info()[i], L2_INFO[i]);
  BOOST_CHECK_EQUAL(writer.nevent_filter_info(), NEF_INFO);
  for (unsigned int i=0; i<NEF_INFO; ++i) 
    BOOST_CHECK_EQUAL(writer.event_filter_info()[i], EF_INFO[i]);
  BOOST_CHECK_EQUAL(writer.nstream_tag(), NSTREAM_TAG);
  BOOST_CHECK_EQUAL(std::string(reinterpret_cast<const char*>(writer.stream_tag())), std::string(STREAM_TAG));
}

/**
 * Tests the FullEvent writing meta data to check it corresponds to the
 * expected behaviour.
 */
BOOST_AUTO_TEST_CASE( meta_data )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS, 
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkMetaData(id, writer);
}

/**
 * Checks for the reading of a block of data that has been serialized from
 * the writer given.
 *
 * @param id The source identifier originally set at the writer
 * @param writer The writer used for the test
 */
void checkReadWrite (const eformat::helper::SourceIdentifier& id,
				    eformat::write::FullEventFragment& writer)
{

  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();
  BOOST_CHECK(top);

  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, 
				      writer.size_word()-1), (unsigned)0);

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*top, serialized, writer.size_word()),  
     writer.size_word()); //now it can go on

  eformat::FullEventFragment<const uint32_t*> reader(serialized);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::FULL_EVENT);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), id.code());
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.bc_time_seconds(), BC_TIME_SECONDS);
  BOOST_CHECK_EQUAL(reader.bc_time_nanoseconds(), BC_TIME_NANOSECONDS);
  BOOST_CHECK_EQUAL(reader.global_id(), GLOBAL_ID);
  BOOST_CHECK_EQUAL(reader.run_type(), RUN_TYPE);
  BOOST_CHECK_EQUAL(reader.run_no(), RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.lumi_block(), LUMI_BLOCK);
  BOOST_CHECK_EQUAL(reader.lvl1_id(), LVL1_ID);
  BOOST_CHECK_EQUAL(reader.bc_id(), BC_ID);
  BOOST_CHECK_EQUAL(reader.lvl1_trigger_type(), LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.compression_type(), COMPRESSION_TYPE);
  BOOST_CHECK_EQUAL(reader.nlvl1_trigger_info(), NL1_INFO);
  const uint32_t* l1_info = 0;
  reader.lvl1_trigger_info(l1_info);
  for (unsigned int i=0; i<NL1_INFO; ++i) BOOST_CHECK_EQUAL(l1_info[i], L1_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nlvl2_trigger_info(), NL2_INFO);
  const uint32_t* l2_info = 0;
  reader.lvl2_trigger_info(l2_info);
  for (unsigned int i=0; i<NL2_INFO; ++i) BOOST_CHECK_EQUAL(l2_info[i], L2_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nevent_filter_info(), NEF_INFO);
  const uint32_t* ef_info = 0;
  reader.event_filter_info(ef_info);
  for (unsigned int i=0; i<NEF_INFO; ++i) BOOST_CHECK_EQUAL(ef_info[i], EF_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nstream_tag(), NSTREAM_TAG);
  const uint32_t* stream_tag = 0;
  reader.stream_tag(stream_tag);
  BOOST_CHECK_EQUAL(std::string(reinterpret_cast<const char*>(stream_tag)),
		  std::string(STREAM_TAG));
  BOOST_CHECK_EQUAL(reader.nstatus(), (unsigned)1);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status); //makes sure we have set the status
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_status[0], 
      eformat::UNCLASSIFIED);
  const uint32_t* reader_start = 0;
  reader.start(reader_start);
  BOOST_CHECK_EQUAL(reader_start, serialized);
  BOOST_CHECK_EQUAL(reader.payload_size_word(), (unsigned)0);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + NSTREAM_TAG + NL1_INFO + NL2_INFO + NEF_INFO);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + reader.nstream_tag() + 
		 reader.nlvl1_trigger_info() + reader.nlvl2_trigger_info() + 
     reader.nevent_filter_info());
  BOOST_CHECK_EQUAL(reader.nchildren(), (unsigned)0);
  BOOST_CHECK(reader.check_tree());
  std::vector<eformat::helper::ProblemContainer> prob;
  reader.problems_tree(prob);
  BOOST_CHECK(prob.empty());
  BOOST_CHECK(reader.checksum());
  delete[] serialized;
}

/**
 * Tests the serialization of data, for an empty FullEvent fragment
 */
BOOST_AUTO_TEST_CASE( serialize )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkReadWrite(id, writer);
}

/**
 * Tests the serialization of data, for an empty FullEvent fragment
 */
BOOST_AUTO_TEST_CASE( serialize_higher_level )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_level(9);
  BOOST_CHECK_EQUAL(writer.compression_level(),9);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkReadWrite(id, writer);
}


/**
 * Checks for the reading of a block of data that has been serialized from
 * the writer given, in a more complex situation.
 *
 * @param id The source identifier originally set at the writer
 * @param writer The writer used for the test
 */
void checkReadWriteAppend 
(const eformat::helper::SourceIdentifier& id,
 eformat::write::FullEventFragment& writer)
{
  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  //Appends the ROB's
  writer.append(&rob1);
  writer.append(&rob2);
  BOOST_CHECK_EQUAL(rob1.parent(), &writer);
  BOOST_CHECK_EQUAL(rob2.parent(), &writer);
  BOOST_CHECK_EQUAL(rob1.next(), &rob2);
  BOOST_CHECK_EQUAL(rob2.next(), (eformat::write::ROBFragment*)0);
  writer.append(&rob3);
  BOOST_CHECK_EQUAL(rob3.parent(), &writer);
  BOOST_CHECK_EQUAL(rob2.next(), &rob3);

  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();
  BOOST_CHECK(top);
  
  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, 
				      writer.size_word()-1), (unsigned)0); 

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*top, serialized, writer.size_word()), 
     writer.size_word()); //now it can go on

  eformat::FullEventFragment<const uint32_t*> reader(serialized);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::FULL_EVENT);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), id.code());
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.bc_time_seconds(), BC_TIME_SECONDS);
  BOOST_CHECK_EQUAL(reader.bc_time_nanoseconds(), BC_TIME_NANOSECONDS);
  BOOST_CHECK_EQUAL(reader.global_id(), GLOBAL_ID);
  BOOST_CHECK_EQUAL(reader.run_type(), RUN_TYPE);
  BOOST_CHECK_EQUAL(reader.run_no(), RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.lumi_block(), LUMI_BLOCK);
  BOOST_CHECK_EQUAL(reader.lvl1_id(), LVL1_ID);
  BOOST_CHECK_EQUAL(reader.bc_id(), BC_ID);
  BOOST_CHECK_EQUAL(reader.lvl1_trigger_type(), LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.compression_type(), COMPRESSION_TYPE);
  BOOST_CHECK_EQUAL(reader.nlvl1_trigger_info(), NL1_INFO);
  const uint32_t* l1_info = 0;
  reader.lvl1_trigger_info(l1_info);
  for (unsigned int i=0; i<NL1_INFO; ++i) BOOST_CHECK_EQUAL(l1_info[i], L1_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nlvl2_trigger_info(), NL2_INFO);
  const uint32_t* l2_info = 0;
  reader.lvl2_trigger_info(l2_info);
  for (unsigned int i=0; i<NL2_INFO; ++i) BOOST_CHECK_EQUAL(l2_info[i], L2_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nevent_filter_info(), NEF_INFO);
  const uint32_t* ef_info = 0;
  reader.event_filter_info(ef_info);
  for (unsigned int i=0; i<NEF_INFO; ++i) BOOST_CHECK_EQUAL(ef_info[i], EF_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nstream_tag(), NSTREAM_TAG);
  const uint32_t* stream_tag = 0;
  reader.stream_tag(stream_tag);
  BOOST_CHECK_EQUAL(std::string(reinterpret_cast<const char*>(stream_tag)),
		  std::string(STREAM_TAG));
  BOOST_CHECK_EQUAL(reader.nstatus(), (unsigned)1);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status); //makes sure we have set the status
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_status[0], 
      eformat::UNCLASSIFIED);
  const uint32_t* reader_start = 0;
  reader.start(reader_start);
  BOOST_CHECK_EQUAL(reader_start, serialized);
  BOOST_CHECK_EQUAL(reader.readable_payload_size_word(), rob1.size_word() + 
		    rob2.size_word() + rob3.size_word());
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + NSTREAM_TAG + NL1_INFO + NL2_INFO + NEF_INFO);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + reader.nstream_tag() + 
		 reader.nlvl1_trigger_info() + reader.nlvl2_trigger_info() + 
     reader.nevent_filter_info());
  BOOST_CHECK_EQUAL(reader.nchildren(), (unsigned)3);
  BOOST_CHECK(reader.check_tree());
  BOOST_CHECK(reader.checksum());
  delete[] serialized;
}

/**
 * Checks for the search of ROB fragments in a Full Event
 *
 * @param id The source identifier originally set at the writer
 * @param writer The writer used for the test
 */
void checkReadSearch (eformat::write::FullEventFragment& writer)
{

  eformat::helper::SourceIdentifier rob1_id(eformat::PIXEL_DISK, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_CTP, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  //Appends the ROB's
  writer.append(&rob1);
  writer.append(&rob2);
  writer.append(&rob3);

  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  eformat::write::copy(*writer.bind(), serialized, writer.size_word()); 
  eformat::FullEventFragment<const uint32_t*> reader(serialized);

  //the base search utilities
  std::map<uint32_t, const uint32_t*> idx;
  eformat::helper::build_toc(reader, idx);
  BOOST_CHECK_EQUAL(idx.size(), (unsigned)3);
  std::map<eformat::SubDetector, std::vector<const uint32_t*> > idx2;
  eformat::helper::build_toc(reader, idx2);
  BOOST_CHECK_EQUAL(idx2.size(), (unsigned)3);
  BOOST_CHECK_EQUAL(idx2[eformat::PIXEL_DISK].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx2[eformat::TDAQ_CTP].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx2[eformat::TDAQ_LVL2].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx2[eformat::FULL_SD_EVENT].size(), (unsigned)0);
  std::map<eformat::SubDetectorGroup, std::vector<const uint32_t*> > idx3;
  eformat::helper::build_toc(reader, idx3);
  BOOST_CHECK_EQUAL(idx3.size(), (unsigned)2);
  BOOST_CHECK_EQUAL(idx3[eformat::PIXEL].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx3[eformat::TDAQ].size(), (unsigned)2);
  BOOST_CHECK_EQUAL(idx3[eformat::ANY_DETECTOR].size(), (unsigned)0);

  //make sure we can find robs
  BOOST_CHECK(eformat::helper::find_rob(reader, rob1_id.code()));
  BOOST_CHECK(eformat::helper::find_rob(reader, rob2_id.code()));
  BOOST_CHECK(eformat::helper::find_rob(reader, rob3_id.code()));
  BOOST_CHECK(!eformat::helper::find_rob(reader, 0x12345678));

  delete[] serialized;
}

/**
 * Tests the appending of ROB fragments to an existing FullEvent fragment,
 * then test serialization and readout of these entities
 */
BOOST_AUTO_TEST_CASE( serialize_complex )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkReadWriteAppend(id, writer);
}


/**
 * Checks for the reading of a block of data that has been serialized from
 * the writer given, in an even more complex situation where a few of the
 * children will get their size changed before serialization.
 *
 * @param id The source identifier originally set at the writer
 * @param writer The writer used for the test
 */
void checkReadWriteChange 
(const eformat::helper::SourceIdentifier& id,
 eformat::write::FullEventFragment& writer)
{

  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  //Appends the ROB's
  writer.append(&rob1);
  writer.append(&rob2);
  BOOST_CHECK_EQUAL(rob1.parent(), &writer);
  BOOST_CHECK_EQUAL(rob2.parent(), &writer);
  BOOST_CHECK_EQUAL(rob1.next(), &rob2);
  BOOST_CHECK_EQUAL(rob2.next(), (eformat::write::ROBFragment*)0);
  writer.append(&rob3);
  BOOST_CHECK_EQUAL(rob3.parent(), &writer);
  BOOST_CHECK_EQUAL(rob2.next(), &rob3);

  size_t old_size = writer.size_word();

  //make several changes:
  //rob1 gets its status changed (one more word introduced)
  uint32_t status[2] = { 0, 0xa };
  rob1.status(2, status);

  //rob3 gets its data changed (10 more words introduced)
  uint32_t data[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  rob3.rod_data(10, data);

  //makes sure the FullEvent fragment has taken those changes into account
  BOOST_CHECK_EQUAL(writer.size_word(), old_size + (unsigned)11);

  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();
  BOOST_CHECK(top);

  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, 
				      writer.size_word()-1), (unsigned)0); 

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*top, serialized, writer.size_word()), 
     writer.size_word()); //now it can go on

  eformat::FullEventFragment<const uint32_t*> reader(serialized);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::FULL_EVENT);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), id.code());
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.bc_time_seconds(), BC_TIME_SECONDS);
  BOOST_CHECK_EQUAL(reader.bc_time_nanoseconds(), BC_TIME_NANOSECONDS);
  BOOST_CHECK_EQUAL(reader.global_id(), GLOBAL_ID);
  BOOST_CHECK_EQUAL(reader.run_type(), RUN_TYPE);
  BOOST_CHECK_EQUAL(reader.run_no(), RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.lumi_block(), LUMI_BLOCK);
  BOOST_CHECK_EQUAL(reader.lvl1_id(), LVL1_ID);
  BOOST_CHECK_EQUAL(reader.bc_id(), BC_ID);
  BOOST_CHECK_EQUAL(reader.lvl1_trigger_type(), LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.compression_type(), COMPRESSION_TYPE);
  BOOST_CHECK_EQUAL(reader.nlvl1_trigger_info(), NL1_INFO);
  const uint32_t* l1_info = 0;
  reader.lvl1_trigger_info(l1_info);
  for (unsigned int i=0; i<NL1_INFO; ++i) BOOST_CHECK_EQUAL(l1_info[i], L1_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nlvl2_trigger_info(), NL2_INFO);
  const uint32_t* l2_info = 0;
  reader.lvl2_trigger_info(l2_info);
  for (unsigned int i=0; i<NL2_INFO; ++i) BOOST_CHECK_EQUAL(l2_info[i], L2_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nevent_filter_info(), NEF_INFO);
  const uint32_t* ef_info = 0;
  reader.event_filter_info(ef_info);
  for (unsigned int i=0; i<NEF_INFO; ++i) BOOST_CHECK_EQUAL(ef_info[i], EF_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nstream_tag(), NSTREAM_TAG);
  const uint32_t* stream_tag = 0;
  reader.stream_tag(stream_tag);
  BOOST_CHECK_EQUAL(std::string(reinterpret_cast<const char*>(stream_tag)),
		  std::string(STREAM_TAG));
  BOOST_CHECK_EQUAL(reader.nstatus(), (unsigned)1);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status); //makes sure we have set the status
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_status[0], 
      eformat::UNCLASSIFIED);
  const uint32_t* reader_start = 0;
  reader.start(reader_start);
  BOOST_CHECK_EQUAL(reader_start, serialized);
  BOOST_CHECK_EQUAL(reader.readable_payload_size_word(), rob1.size_word() + 
		 rob2.size_word() + rob3.size_word());
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + NSTREAM_TAG + NL1_INFO + NL2_INFO + NEF_INFO);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + reader.nstream_tag() + 
		 reader.nlvl1_trigger_info() + reader.nlvl2_trigger_info() + 
     reader.nevent_filter_info());
  BOOST_CHECK_EQUAL(reader.nchildren(), (unsigned)3);
  BOOST_CHECK(reader.check_tree());
  BOOST_CHECK(reader.checksum());
  delete[] serialized;
}

/**
 * Tests the appending of a ROB fragment to an existing FullEvent
 * fragment. At this point, the size of a few ROBs are changed (status, data
 * changes) and we would like to test the reflection of this in the
 * serialization of these items.
 */
BOOST_AUTO_TEST_CASE( size_change )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS, 
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkReadWriteChange(id, writer);
}

/**
 * Checks for data copying.
 */
void checkCopy (const eformat::helper::SourceIdentifier& id,
			       eformat::write::FullEventFragment& writer)
{
  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  eformat::write::FullEventFragment copy(writer);
  uint32_t* serialized = new uint32_t[copy.size_word()];
  const eformat::write::node_t* top = copy.bind();
  BOOST_CHECK(top);

  //now proceed comparing the 'copy' to the original 'writer'

  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, 
				      writer.size_word()-1), (unsigned)0); 

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*top, serialized, writer.size_word()), 
     writer.size_word()); //now it can go on

  eformat::FullEventFragment<const uint32_t*> reader(serialized);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::FULL_EVENT);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), id.code());
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.bc_time_seconds(), BC_TIME_SECONDS);
  BOOST_CHECK_EQUAL(reader.bc_time_nanoseconds(), BC_TIME_NANOSECONDS);
  BOOST_CHECK_EQUAL(reader.global_id(), GLOBAL_ID);
  BOOST_CHECK_EQUAL(reader.run_type(), RUN_TYPE);
  BOOST_CHECK_EQUAL(reader.run_no(), RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.lumi_block(), LUMI_BLOCK);
  BOOST_CHECK_EQUAL(reader.lvl1_id(), LVL1_ID);
  BOOST_CHECK_EQUAL(reader.bc_id(), BC_ID);
  BOOST_CHECK_EQUAL(reader.lvl1_trigger_type(), LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.compression_type(), COMPRESSION_TYPE);
  BOOST_CHECK_EQUAL(reader.nlvl1_trigger_info(), NL1_INFO);
  const uint32_t* l1_info = 0;
  reader.lvl1_trigger_info(l1_info);
  for (unsigned int i=0; i<NL1_INFO; ++i) BOOST_CHECK_EQUAL(l1_info[i], L1_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nlvl2_trigger_info(), NL2_INFO);
  const uint32_t* l2_info = 0;
  reader.lvl2_trigger_info(l2_info);
  for (unsigned int i=0; i<NL2_INFO; ++i) BOOST_CHECK_EQUAL(l2_info[i], L2_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nevent_filter_info(), NEF_INFO);
  const uint32_t* ef_info = 0;
  reader.event_filter_info(ef_info);
  for (unsigned int i=0; i<NEF_INFO; ++i) BOOST_CHECK_EQUAL(ef_info[i], EF_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nstream_tag(), NSTREAM_TAG);
  const uint32_t* stream_tag = 0;
  reader.stream_tag(stream_tag);
  BOOST_CHECK_EQUAL(std::string(reinterpret_cast<const char*>(stream_tag)),
		  std::string(STREAM_TAG));
  BOOST_CHECK_EQUAL(reader.nstatus(), (unsigned)1);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status); //makes sure we have set the status
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_status[0], 
      eformat::UNCLASSIFIED);
  const uint32_t* reader_start = 0;
  reader.start(reader_start);
  BOOST_CHECK_EQUAL(reader_start, serialized);
  BOOST_CHECK_EQUAL(reader.readable_payload_size_word(), (unsigned)0);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + NSTREAM_TAG + NL1_INFO + NL2_INFO + NEF_INFO);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + reader.nstream_tag() + 
		 reader.nlvl1_trigger_info() + reader.nlvl2_trigger_info() + 
     reader.nevent_filter_info());
  BOOST_CHECK_EQUAL(reader.nchildren(), (unsigned)0);
  BOOST_CHECK(reader.check_tree());
  BOOST_CHECK(reader.checksum());
  delete[] serialized;
}

/**
 * Tests the serialization of a copy of a FullEvent fragment
 */
BOOST_AUTO_TEST_CASE( copy )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkCopy(id, writer);
}

/**
 * Checks for block initialization.
 */
void checkBlock (const eformat::helper::SourceIdentifier& id,
				eformat::write::FullEventFragment& writer)
{
  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();

  BOOST_CHECK_EQUAL
    (eformat::write::copy(*top, serialized, writer.size_word()), 
     writer.size_word()); //now it can go on

  eformat::write::FullEventFragment writer2(serialized);

  //add some ROSs to this guy
  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  writer2.append(&rob1);
  writer2.append(&rob2);
  writer2.append(&rob3);

  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized2 = new uint32_t[writer2.size_word()];
  const eformat::write::node_t* top2 = writer2.bind();
  BOOST_CHECK(top2);

  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top2), writer2.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top2, serialized2, 
				      writer2.size_word()-1), (unsigned)0); 

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top2), writer2.size_word());
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*top2, serialized2, writer2.size_word()), 
     writer2.size_word()); //now it can go on

  eformat::FullEventFragment<const uint32_t*> reader(serialized2);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::FULL_EVENT);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer2.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), id.code());
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.bc_time_seconds(), BC_TIME_SECONDS);
  BOOST_CHECK_EQUAL(reader.bc_time_nanoseconds(), BC_TIME_NANOSECONDS);
  BOOST_CHECK_EQUAL(reader.global_id(), GLOBAL_ID);
  BOOST_CHECK_EQUAL(reader.run_type(), RUN_TYPE);
  BOOST_CHECK_EQUAL(reader.run_no(), RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.lumi_block(), LUMI_BLOCK);
  BOOST_CHECK_EQUAL(reader.lvl1_id(), LVL1_ID);
  BOOST_CHECK_EQUAL(reader.bc_id(), BC_ID);
  BOOST_CHECK_EQUAL(reader.lvl1_trigger_type(), LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.compression_type(), COMPRESSION_TYPE);
  BOOST_CHECK_EQUAL(reader.nlvl1_trigger_info(), NL1_INFO);
  const uint32_t* l1_info = 0;
  reader.lvl1_trigger_info(l1_info);
  for (unsigned int i=0; i<NL1_INFO; ++i) BOOST_CHECK_EQUAL(l1_info[i], L1_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nlvl2_trigger_info(), NL2_INFO);
  const uint32_t* l2_info = 0;
  reader.lvl2_trigger_info(l2_info);
  for (unsigned int i=0; i<NL2_INFO; ++i) BOOST_CHECK_EQUAL(l2_info[i], L2_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nevent_filter_info(), NEF_INFO);
  const uint32_t* ef_info = 0;
  reader.event_filter_info(ef_info);
  for (unsigned int i=0; i<NEF_INFO; ++i) BOOST_CHECK_EQUAL(ef_info[i], EF_INFO[i]);
  BOOST_CHECK_EQUAL(reader.nstream_tag(), NSTREAM_TAG);
  const uint32_t* stream_tag = 0;
  reader.stream_tag(stream_tag);
  BOOST_CHECK_EQUAL( std::string(reinterpret_cast<const char*>(stream_tag)),
		  std::string(STREAM_TAG));
  BOOST_CHECK_EQUAL(reader.nstatus(), (unsigned)1);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status); //makes sure we have set the status
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_status[0], 
      eformat::UNCLASSIFIED);
  const uint32_t* reader_start = 0;
  reader.start(reader_start);
  BOOST_CHECK_EQUAL(reader_start, serialized2);
  BOOST_CHECK_EQUAL(reader.readable_payload_size_word(), rob1.size_word() + 
		 rob2.size_word() + rob3.size_word());
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + NSTREAM_TAG + NL1_INFO + NL2_INFO + NEF_INFO);
  BOOST_CHECK_EQUAL(reader.nspecific(), 16 + reader.nstream_tag() + 
		 reader.nlvl1_trigger_info() + reader.nlvl2_trigger_info() + 
     reader.nevent_filter_info());
  BOOST_CHECK_EQUAL(reader.nchildren(), (unsigned)3);
  BOOST_CHECK(reader.check_tree());
  BOOST_CHECK(reader.checksum());
  
  delete[] serialized;
  delete[] serialized2;
}

/**
 * Tests the serialization of a FullEvent fragment started from a block of
 * serialized data.
 */
BOOST_AUTO_TEST_CASE( block )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS, 
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkBlock(id, writer);
  checkReadSearch(writer);
}

/**
 * Checks if you can bind many times to this fragment
 *
 * @param writer The writer used for the test
 */
void checkBindMany (eformat::write::FullEventFragment& writer)
{
  const eformat::write::node_t* top = writer.bind();

  //we can bind many times
  const eformat::write::node_t* test1 = writer.bind();
  const eformat::write::node_t* test2 = writer.bind();
  BOOST_CHECK_EQUAL(top, test1);
  BOOST_CHECK_EQUAL(top, test2);
  while (test1->next) {
    BOOST_CHECK_EQUAL(test1->base, test2->base);
    BOOST_CHECK_EQUAL(test1->size_word, test2->size_word);
    BOOST_CHECK_EQUAL(test1->next, test2->next);
    test1 = test1->next;
    test2 = test2->next;
  }
}

/**
 * Tests the multiple binding to the same fragment
 */
BOOST_AUTO_TEST_CASE( bind_many )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS, 
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkBindMany(writer);
}

BOOST_AUTO_TEST_CASE( checksum_without_payload )
{
  CHECKSUM_TYPE = eformat::ADLER32;

  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS, 
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkBlock(id, writer);

  CHECKSUM_TYPE = eformat::NO_CHECKSUM;
}

BOOST_AUTO_TEST_CASE( checksum_with_payload )
{
  CHECKSUM_TYPE = eformat::ADLER32;

  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS, 
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkReadWriteChange(id, writer);

  CHECKSUM_TYPE = eformat::NO_CHECKSUM;
}

/**
 * Checks for the appending of unchecked ROB fragments in a Full Event,
 * including truncated ROD fragment
 *
 * @param writer The writer used for the test
 * @param unchecked If we should use append_unchecked or not
 */
void checkAppendUnchecked (eformat::write::FullEventFragment& writer, 
    bool unchecked=true)
{

  eformat::helper::SourceIdentifier rob1_id(eformat::PIXEL_DISK, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  uint32_t* rob1_serialized = new uint32_t[rob1.size_word()];
  //to make sure all works, we screw up this ROD trailer a bit: 
  // we forget what is
  //data and what is status. The FullEvent should ignore this if every thing is
  //like it is supposed to be.
  eformat::write::copy(*rob1.bind(), rob1_serialized, rob1.size_word());
  rob1_serialized[rob1.size_word()-3] = 0;
  rob1_serialized[rob1.size_word()-2] = 0;
  rob1_serialized[rob1.size_word()-1] = 0;
  eformat::write::ROBFragment rob1_broken(rob1_serialized);
  
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_CTP, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  
  //Appends the ROB's
  if (unchecked) writer.append_unchecked(rob1_serialized);
  else 
    writer.append(&rob1_broken);
  writer.append(&rob2);
  writer.append(&rob3);

  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  eformat::write::copy(*writer.bind(), serialized, writer.size_word()); 
  eformat::FullEventFragment<const uint32_t*> reader(serialized);

  //the base search utilities
  std::map<uint32_t, const uint32_t*> idx;
  eformat::helper::build_toc(reader, idx);
  BOOST_CHECK_EQUAL(idx.size(), (unsigned)3);
  std::map<eformat::SubDetector, std::vector<const uint32_t*> > idx2;
  eformat::helper::build_toc(reader, idx2);
  BOOST_CHECK_EQUAL(idx2.size(), (unsigned)3);
  BOOST_CHECK_EQUAL(idx2[eformat::PIXEL_DISK].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx2[eformat::TDAQ_CTP].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx2[eformat::TDAQ_LVL2].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx2[eformat::FULL_SD_EVENT].size(), (unsigned)0);
  std::map<eformat::SubDetectorGroup, std::vector<const uint32_t*> > idx3;
  eformat::helper::build_toc(reader, idx3);
  BOOST_CHECK_EQUAL(idx3.size(), (unsigned)2);
  BOOST_CHECK_EQUAL(idx3[eformat::PIXEL].size(), (unsigned)1);
  BOOST_CHECK_EQUAL(idx3[eformat::TDAQ].size(), (unsigned)2);
  BOOST_CHECK_EQUAL(idx3[eformat::ANY_DETECTOR].size(), (unsigned)0);

  //make sure we can find robs
  BOOST_CHECK(eformat::helper::find_rob(reader, rob1_id.code()));
  BOOST_CHECK(eformat::helper::find_rob(reader, rob2_id.code()));
  BOOST_CHECK(eformat::helper::find_rob(reader, rob3_id.code()));
  BOOST_CHECK(!eformat::helper::find_rob(reader, 0x12345678));

  
  BOOST_CHECK(reader.check());
  //the first ROD is actually bad (truncated-like). It shall not
  // be reported
  BOOST_CHECK(reader.check_tree_noex());
  std::vector<eformat::helper::ProblemContainer> prob;
  reader.problems_tree(prob);
  BOOST_CHECK(!prob.empty());

  eformat::ROBFragment<const uint32_t*> broken(idx2[eformat::PIXEL_DISK][0]);
  BOOST_CHECK(broken.check_rob_noex());
  BOOST_CHECK(!broken.check_rod_noex());
  BOOST_CHECK(!broken.check_noex());

  delete[] serialized;
  delete[] rob1_serialized;
}


/**
 * Checks for the appending of unchecked ROB fragments in a Full Event,
 * including truncated ROD fragment
 *
 * @param writer The writer used for the test
 */
void checkAppendUncheckedRob (eformat::write::FullEventFragment& writer)
{

  eformat::helper::SourceIdentifier rob1_id(eformat::PIXEL_DISK, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  uint32_t* rob1_serialized = new uint32_t[rob1.size_word()];
  //to make sure all works, we screw up this ROB header a bit 
  //The FullEvent should ignore this if every thing is
  //like it is supposed to be.
  eformat::write::copy(*rob1.bind(), rob1_serialized, rob1.size_word());
  rob1_serialized[2] = 3;
    
  //Appends the ROB
  writer.append_unchecked(rob1_serialized);
  
  //serializes the FullEvent fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  eformat::write::copy(*writer.bind(), serialized, writer.size_word()); 
  eformat::FullEventFragment<const uint32_t*> reader(serialized);
  
  BOOST_CHECK(reader.check());
  BOOST_CHECK(!reader.check_tree_noex());
  std::vector<eformat::helper::ProblemContainer> prob;
  reader.problems_tree(prob);
  BOOST_CHECK(!prob.empty());

  delete[] serialized;
  delete[] rob1_serialized;
}


/**
 * Tests the unchecked serialization of a FullEvent fragment
 */
BOOST_AUTO_TEST_CASE( append_unchecked_rob )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkAppendUncheckedRob(writer);
}



/**
 * Tests the unchecked serialization of a FullEvent fragment
 */
BOOST_AUTO_TEST_CASE( append_unchecked )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkAppendUnchecked(writer);
}


/**
 * Tests the I-dont-care serialization of a FullEvent fragment
 */
BOOST_AUTO_TEST_CASE( append_whatever )
{
  eformat::helper::SourceIdentifier id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment writer(id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  writer.checksum_type(CHECKSUM_TYPE);
  writer.compression_type(COMPRESSION_TYPE);
  writer.lvl1_trigger_info(NL1_INFO, L1_INFO);
  writer.lvl2_trigger_info(NL2_INFO, L2_INFO);
  writer.event_filter_info(NEF_INFO, EF_INFO);
  writer.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  checkAppendUnchecked(writer, false);
}
