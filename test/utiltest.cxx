//Dear emacs, this is -*- c++ -*-

/**
 * @file utiltest.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Test the eformat base utilities (in util.h)
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Utilities Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "eformat/util.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/ROBFragment.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/Status.h"
#include "eformat/checksum.h"
#include "eformat/compression.h"

#include <iostream>

static const uint32_t RUN_NUMBER = 0xababab; 
static const uint32_t RUN_TYPE = 0xddcceeff; 
static const uint16_t LUMI_BLOCK = 0xfafa;
static const uint32_t LVL1_ID = 0xcdcdcd;
static const uint16_t BC_ID = 0xfef;
static const uint8_t  LVL1_TYPE = 0x12;
static const uint32_t DETEV_TYPE = 0xabcdef;
static const uint32_t BC_TIME_SECONDS = 0x12345678;
static const uint32_t BC_TIME_NANOSECONDS = 0x9abcdef0;
static const uint32_t GLOBAL_ID = 0x12344321;
static const uint32_t NL1_INFO = 2;
static const uint32_t NL2_INFO = 3;
static const uint32_t NEF_INFO = 4;
static const uint32_t NSTREAM_TAG = 4; //4 words
static const uint32_t L1_INFO[NL1_INFO] = { 0xe, 0xf };
static const uint32_t L2_INFO[NL2_INFO] = { 0xa, 0xb, 0xc };
static const uint32_t EF_INFO[NEF_INFO] = { 0x5, 0x6, 0x7, 0x8 };
static const char* STREAM_TAG = "This is a test\0"; //16 chars

BOOST_AUTO_TEST_CASE( util_get_robs )
{
  //prepare a test event
  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  eformat::helper::SourceIdentifier fe_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment fe(fe_id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  fe.checksum_type(eformat::NO_CHECKSUM);
  fe.lvl1_trigger_info(NL1_INFO, L1_INFO);
  fe.lvl2_trigger_info(NL2_INFO, L2_INFO);
  fe.event_filter_info(NEF_INFO, EF_INFO);
  fe.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  fe.append(&rob1);
  fe.append(&rob2);
  fe.append(&rob3);

  uint32_t* serialized = new uint32_t[fe.size_word()];
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*fe.bind(), serialized, fe.size_word()), fe.size_word());

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"


  //I have to find 3 ROBs
  const uint32_t* found[3] = { 0, 0, 0 };
  BOOST_CHECK_EQUAL(eformat::get_robs(serialized, found, 3), (uint32_t)3);
  eformat::ROBFragment<const uint32_t*> f1(found[0]);
  BOOST_CHECK(f1.check());
  eformat::ROBFragment<const uint32_t*> f2(found[1]);
  BOOST_CHECK(f2.check());
  eformat::ROBFragment<const uint32_t*> f3(found[2]);
  BOOST_CHECK(f3.check());

  delete [] serialized;

#pragma GCC diagnostic pop
}

BOOST_AUTO_TEST_CASE( util_get_robs_new )
{
  //prepare a test event
  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);

  eformat::helper::SourceIdentifier fe_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::write::FullEventFragment fe(fe_id.code(), BC_TIME_SECONDS,
      BC_TIME_NANOSECONDS, GLOBAL_ID, 
      RUN_TYPE, RUN_NUMBER, LUMI_BLOCK,
      LVL1_ID, BC_ID, LVL1_TYPE);
  fe.checksum_type(eformat::NO_CHECKSUM);
  fe.lvl1_trigger_info(NL1_INFO, L1_INFO);
  fe.lvl2_trigger_info(NL2_INFO, L2_INFO);
  fe.event_filter_info(NEF_INFO, EF_INFO);
  fe.stream_tag(NSTREAM_TAG, reinterpret_cast<const uint32_t*>(STREAM_TAG));
  fe.append(&rob1);
  fe.append(&rob2);
  fe.append(&rob3);

  uint32_t* serialized = new uint32_t[fe.size_word()];
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*fe.bind(), serialized, fe.size_word()), fe.size_word());
  
  //I have to find 3 ROBs
  std::vector<std::shared_ptr<const uint32_t>> robs;
  eformat::get_robs(serialized, robs);
  BOOST_CHECK_EQUAL(robs.size(), 3);
  eformat::ROBFragment<const uint32_t*> f1(robs[0].get());
  BOOST_CHECK(f1.check());
  eformat::ROBFragment<const uint32_t*> f2(robs[1].get());
  BOOST_CHECK(f2.check());
  eformat::ROBFragment<const uint32_t*> f3(robs[2].get());
  BOOST_CHECK(f3.check());
  
  //The pointers should be within the 'serialized' range
  for(const auto& r: robs){
    BOOST_CHECK((r.get() >= serialized && r.get() < (serialized+fe.size_word()) ));
  }		

  robs.clear();
  delete[] serialized;
  
  fe.compression_type(eformat::ZLIB);
  auto first = fe.bind();
  serialized = new uint32_t[fe.size_word()];
  
  eformat::write::copy(*first, serialized, fe.size_word());

  eformat::get_robs(serialized, robs);
  BOOST_CHECK_EQUAL(robs.size(), 3);
  eformat::ROBFragment<const uint32_t*> f1c(robs[0].get());
  BOOST_CHECK(f1c.check());
  eformat::ROBFragment<const uint32_t*> f2c(robs[1].get());
  BOOST_CHECK(f2c.check());
  eformat::ROBFragment<const uint32_t*> f3c(robs[2].get());
  BOOST_CHECK(f3c.check());
  
  //The pointers should be outside the 'serialized' range
  for(const auto& r: robs){
    BOOST_CHECK((r.get() < serialized || r.get() >= (serialized+fe.size_word()) ));
  }		
  delete[] serialized;
}

BOOST_AUTO_TEST_CASE( util_find_rods )
{
  //prepare a test set of RODs
  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  uint32_t s1_size = rob1.size_word() - rob1.rob_header_size_word();
  uint32_t s2_size = rob2.size_word() - rob2.rob_header_size_word();
  uint32_t s3_size = rob3.size_word() - rob3.rob_header_size_word();
  uint32_t total_size = s1_size + s2_size + s3_size;
  uint32_t* s = new uint32_t[total_size];
  BOOST_CHECK_EQUAL
    (eformat::write::copy(*rob1.rod_bind(), s, s1_size), s1_size);
  BOOST_CHECK_EQUAL 
    (eformat::write::copy(*rob2.rod_bind(), s+s1_size, s2_size), s2_size);
  BOOST_CHECK_EQUAL 
    (eformat::write::copy(*rob3.rod_bind(), s+s1_size+s2_size, s3_size), s3_size);
  //now we have a stream with multiple ROD's, from which we know the total size
  const uint32_t* found[3] = { 0, 0, 0 };
  uint32_t sizes[3] = { 0, 0, 0 };

  //if searching for less, can only find less...
  BOOST_CHECK_EQUAL(eformat::find_rods(s, total_size, found, sizes, 2), (unsigned)2);

  //test if the pointers are pointing to the right places
  BOOST_CHECK_EQUAL(found[0], s + s1_size + s2_size); //last ROD
  BOOST_CHECK_EQUAL(found[1], s + s1_size);

  //if searching for more can only find 3...
  BOOST_CHECK_EQUAL
    (eformat::find_rods(s, total_size, found, sizes, 4), 
     (unsigned)3);

  //test if the pointers are pointing to the right places
  BOOST_CHECK_EQUAL(found[0], s + s1_size + s2_size); //last ROD
  BOOST_CHECK_EQUAL(found[1], s + s1_size);
  BOOST_CHECK_EQUAL(found[2], s);

  /* check that we survive some forms of corruption */

  //the trailer reports a size too small, we won't find a ROD marker
  s[total_size - 2] = s[total_size - 2] - 1;
  BOOST_CHECK_THROW(eformat::find_rods(s, total_size, found, sizes, 3),
                    eformat::WrongSizeIssue);

  //the trailer reports a size too big, bigger than the buffer
  s[total_size - 2] = s[total_size - 2] + total_size + 10;
  BOOST_CHECK_THROW(eformat::find_rods(s, total_size, found, sizes, 3),
                    eformat::WrongSizeIssue);

  //the trailer reports a size so big that it wraps up
  s[total_size - 2] = s[total_size - 2] + 0xfffffffe;
  BOOST_CHECK_THROW(eformat::find_rods(s, total_size, found, sizes, 3),
                    eformat::WrongSizeIssue);

  //the size is smaller than the ROD header
  BOOST_CHECK_THROW(eformat::find_rods(s, 10, found, sizes, 3),
                    eformat::WrongSizeIssue);

}

BOOST_AUTO_TEST_CASE( util_find_fragments )
{
  //prepare a test set of ROBs
  eformat::helper::SourceIdentifier rob1_id(eformat::TDAQ_LVL2, 0x1111);
  eformat::write::ROBFragment rob1(rob1_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob2_id(eformat::TDAQ_LVL2, 0x2222);
  eformat::write::ROBFragment rob2(rob2_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  eformat::helper::SourceIdentifier rob3_id(eformat::TDAQ_LVL2, 0x3333);
  eformat::write::ROBFragment rob3(rob3_id.code(), RUN_NUMBER, LVL1_ID,
				   BC_ID, LVL1_TYPE, DETEV_TYPE,
				   0, 0, eformat::STATUS_FRONT);
  uint32_t* s = new uint32_t[rob1.size_word() + rob2.size_word() + rob3.size_word()];
  BOOST_CHECK_EQUAL 
    (eformat::write::copy(*rob1.bind(), s, rob1.size_word()), rob1.size_word());
  BOOST_CHECK_EQUAL 
    (eformat::write::copy(*rob2.bind(), s + rob1.size_word(), 
			  rob2.size_word()), rob2.size_word());
  BOOST_CHECK_EQUAL 
    (eformat::write::copy(*rob3.bind(), s + rob1.size_word() + rob2.size_word(), 
			  rob3.size_word()), rob3.size_word());

  //now we have a stream with multiple ROB's, from which we know the total size
  uint32_t* found[3] = { 0, 0, 0 };
  uint32_t total_size = rob1.size_word() + rob2.size_word() + rob3.size_word();

  //if searching for less, can only find less...
  BOOST_CHECK_EQUAL (eformat::find_fragments(eformat::ROB, s, total_size, 
					  found, 2), (unsigned)2);

  //test if the pointers are pointing to the right places
  BOOST_CHECK_EQUAL(found[0], s); //first ROD 
  BOOST_CHECK_EQUAL(found[1], s + rob1.size_word()); 

  //if searching for more can only find 3...
  BOOST_CHECK_EQUAL (eformat::find_fragments(eformat::ROB, s, total_size, 
					  found, 4), (unsigned)3);
  
  //if the blob size is too big, we emulate the case where the last ROB is too
  //small
  BOOST_CHECK_EQUAL (eformat::find_fragments(eformat::ROB, s, total_size+1, 
					  found), (unsigned)3);

  //if the blob size is too small, we emulate the case in where the last ROB is
  //too big
  BOOST_CHECK_EQUAL (eformat::find_fragments(eformat::ROB, s, total_size-1, 
					  found), (unsigned)3);
  
  
  //test if the pointers are pointing to the right places
  BOOST_CHECK_EQUAL(found[0], s); //first ROD
  eformat::ROBFragment<const uint32_t*> f1(found[0]);
  BOOST_CHECK(f1.check());
  BOOST_CHECK_EQUAL(found[1], s + rob1.size_word());
  eformat::ROBFragment<const uint32_t*> f2(found[1]);
  BOOST_CHECK(f2.check());
  BOOST_CHECK_EQUAL(found[2], s + rob1.size_word() + rob2.size_word());
  eformat::ROBFragment<const uint32_t*> f3(found[2]);
  BOOST_CHECK(f3.check());
}

