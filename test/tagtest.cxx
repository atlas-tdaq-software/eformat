//Dear emacs, this is -*- c++ -*-

/**
 * @file tagtest.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Test the eformat stream tag utilities (in StreamTag.h)
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE StreamTag Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "src/StringMap.h"
#include "eformat/StreamTag.h"

BOOST_AUTO_TEST_CASE( strmap_encode_decode_basic )
{
  std::vector<eformat::helper::StringMap> v;

  eformat::helper::StringMap smap;
  smap["key1"] = "v1";
  smap["key2"] = "v2";
  smap["key3"] = "v3";
  //total size = 18 + 6 = 24 => 6 words
  v.push_back(smap);

  uint32_t sz = eformat::helper::size_word(v);
  BOOST_CHECK_EQUAL(sz, (unsigned)6);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StringMap> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}

BOOST_AUTO_TEST_CASE( strmap_encode_decode_complex )
{
  std::vector<eformat::helper::StringMap> v;

  eformat::helper::StringMap smap;
  smap["key1"] = "value1";
  smap["key2"] = "v2";
  smap["key3"] = "1";
  smap["key4"] = "";
  smap["5"] = "0";
  smap["key6"] = "";
  //total size = 31 + 12 = 43 => 11 words
  v.push_back(smap);

  uint32_t sz = eformat::helper::size_word(v);
  BOOST_CHECK_EQUAL(sz, (unsigned)11);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StringMap> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}

BOOST_AUTO_TEST_CASE( strmap_encode_decode_many )
{
  std::vector<eformat::helper::StringMap> v;

  eformat::helper::StringMap smap1;
  smap1["key1"] = "value1";
  smap1["key2"] = "v2";
  smap1["key3"] = "1";
  smap1["key4"] = "";
  smap1["5"] = "0";
  smap1["key6"] = "";
  //total size = 31 + 12 = 43
  v.push_back(smap1);

  eformat::helper::StringMap smap2;
  smap2["key1"] = "value1";
  smap2["key2"] = "v2";
  smap2["key3"] = "1";
  smap2["key4"] = "";
  smap2["key5"] = "0";
  smap2["key67"] = "";
  //total size = 35 + 12 = 47
  v.push_back(smap2);

  eformat::helper::StringMap smap3;
  smap3["k1"] = "0101X";
  smap3["k2"] = "1010X";
  //total size = 12 + 4 = 18 => 108 characters (27 words, exact)
  v.push_back(smap3);

  uint32_t sz = eformat::helper::size_word(v);
  BOOST_CHECK_EQUAL(sz, (unsigned)27);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StringMap> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}

BOOST_AUTO_TEST_CASE( streamtag_encode_decode_single )
{
  std::vector<eformat::helper::StreamTag> v;

  eformat::helper::StreamTag tag;
  tag.name = "MyNiceName with spaces even";
  tag.type = "unknown";
  tag.obeys_lumiblock = false;
  v.push_back(tag);

  uint32_t sz = eformat::helper::size_word(v);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StreamTag> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}


BOOST_AUTO_TEST_CASE( streamtag_encode_decode_single_with_peb )
{
  std::vector<eformat::helper::StreamTag> v;

  eformat::helper::StreamTag tag;
  tag.name = "will add some PEB";
  tag.type = "unknown";
  tag.obeys_lumiblock = false;
  tag.robs.insert(0xffffffff);
  tag.robs.insert(0x10);
  tag.robs.insert(0x0);
  tag.dets.insert(eformat::TDAQ_CTP);
  v.push_back(tag);

  uint32_t sz = eformat::helper::size_word(v);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StreamTag> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}

BOOST_AUTO_TEST_CASE( streamtag_encode_decode_many )
{
  std::vector<eformat::helper::StreamTag> v;

  eformat::helper::StreamTag tag1;
  tag1.name = "MyFirstName";
  tag1.type = "physics";
  tag1.obeys_lumiblock = true;
  v.push_back(tag1);

  eformat::helper::StreamTag tag2;
  tag2.name = "MySecondName";
  tag2.type = "calibration";
  tag2.obeys_lumiblock = false;
  v.push_back(tag2);

  uint32_t sz = eformat::helper::size_word(v);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StreamTag> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}

BOOST_AUTO_TEST_CASE( streamtag_encode_decode_many_with_peb )
{
  std::vector<eformat::helper::StreamTag> v;

  eformat::helper::StreamTag tag1;
  tag1.name = "MyFirstName";
  tag1.type = "physics";
  tag1.robs.insert(0x10);
  tag1.obeys_lumiblock = true;
  v.push_back(tag1);

  eformat::helper::StreamTag tag2;
  tag2.name = "MySecondName";
  tag2.type = "calibration";
  tag2.obeys_lumiblock = false;
  tag2.dets.insert(eformat::TDAQ_CTP);
  v.push_back(tag2);

  eformat::helper::StreamTag tag3;
  tag3.name = "MySecondName";
  tag3.type = "express";
  tag3.obeys_lumiblock = false;
  v.push_back(tag3);

  eformat::helper::StreamTag tag4;
  tag4.name = "MySecondName";
  tag4.type = "monitoring";
  tag4.obeys_lumiblock = false;
  tag4.robs.insert(0x20);
  tag4.dets.insert(eformat::MUON_MDT_ENDCAP_A_SIDE);
  v.push_back(tag4);

  uint32_t sz = eformat::helper::size_word(v);
  uint32_t* encoded = new uint32_t[sz];
  eformat::helper::encode(v, sz, encoded);
  std::vector<eformat::helper::StreamTag> v2;
  eformat::helper::decode(sz, encoded, v2);
  BOOST_CHECK(v == v2);

  delete[] encoded;
}


BOOST_AUTO_TEST_CASE( streamtag_check_type )
{
  std::vector<eformat::helper::StreamTag> v;

  eformat::helper::StreamTag tag1;
  tag1.name = "MyFirstName";
  tag1.type = "physics";
  tag1.obeys_lumiblock = true;
  v.push_back(tag1);

  eformat::helper::StreamTag tag2;
  tag2.name = "MySecondName";
  tag2.type = "calibration";
  tag2.obeys_lumiblock = false;
  v.push_back(tag2);

  eformat::helper::StreamTag tag3;
  tag3.name = "MyThirdName";
  tag3.type = "monitoring";
  tag3.obeys_lumiblock = false;
  v.push_back(tag3);

  BOOST_CHECK_EQUAL(eformat::helper::contains_type(v, eformat::PHYSICS_TAG), (uint32_t)1);
  BOOST_CHECK_EQUAL(eformat::helper::contains_type(v, eformat::RESERVED_TAG), (uint32_t)0);
  BOOST_CHECK_EQUAL(eformat::helper::contains_type(v, eformat::PHYSICS_TAG|eformat::MONITORING_TAG), (uint32_t)2);
  
}
