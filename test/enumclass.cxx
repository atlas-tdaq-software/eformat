//Dear emacs, this is -*- c++ -*-

/**
 * @file enumclass.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author: rabello $
 * $Revision: 88448 $
 * $Date: 2010-01-18 14:04:59 +0100 (Mon, 18 Jan 2010) $
 *
 * eformat::helper::EnumClass test
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Enumeration Class Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "eformat/SourceIdentifier.h"

/**
 * Test all methods of EnumClass, using the Source Identifier dictionary
 */
BOOST_AUTO_TEST_CASE( enumclass_all )
{
  typedef eformat::helper::EnumClass<eformat::SubDetector> test_type;
  const test_type& test = eformat::helper::SubDetectorDictionary;

  BOOST_CHECK_EQUAL(test.begin().enumeration(), eformat::FULL_SD_EVENT);
  BOOST_CHECK_EQUAL(test.begin()->first, eformat::FULL_SD_EVENT);
  BOOST_CHECK_EQUAL(test.string(eformat::FULL_SD_EVENT), "FULL_SD_EVENT");
  BOOST_CHECK_EQUAL(test.begin().string(), "FULL_SD_EVENT");
  BOOST_CHECK_EQUAL((*test.begin()).second, "FULL_SD_EVENT");
  BOOST_CHECK_EQUAL(test[eformat::PIXEL_BARREL], "PIXEL_BARREL");
  BOOST_CHECK_EQUAL(test["LAR_HAD_ENDCAP_C_SIDE"], eformat::LAR_HAD_ENDCAP_C_SIDE);
  BOOST_CHECK_EQUAL(test.enumeration("OTHER"), eformat::OTHER);

  for (test_type::const_iterator it=test.begin(); it!=test.end(); ++it) {
    BOOST_CHECK_EQUAL(it->first, it.enumeration());
    BOOST_CHECK_EQUAL(it->second, it.string());
    BOOST_CHECK_EQUAL(it->first, test[it->second]);
    BOOST_CHECK_EQUAL(it->second, test[it->first]);
  }

  for (test_type::const_iterator it=test.begin(); it!=test.end(); ++it) {
    BOOST_CHECK_EQUAL(it->first, it.enumeration());
    BOOST_CHECK_EQUAL(it->second, it.string());
    BOOST_CHECK_EQUAL(it->first, test[it->second]);
    BOOST_CHECK_EQUAL(it->second, test[it->first]);
  }

  std::vector<eformat::SubDetector> all_sds;
  test.enumerations(all_sds);
  std::vector<std::string> all_strings;
  test.strings(all_strings);
  for (size_t it=0; it<all_sds.size(); ++it) {
    BOOST_CHECK_EQUAL(all_sds[it], test[all_strings[it]]);
    BOOST_CHECK_EQUAL(all_strings[it], test[all_sds[it]]);
  }
  BOOST_CHECK_EQUAL(test.unknown_string(), "UNKNOWN");
}
