//Dear emacs, this is -*- c++ -*-

/**
 * @file robtest.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * First test suite using CppUnit
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ROBFragment Contiguos Memory Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "eformat/write/ROBFragment.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/ROBFragment.h"
#include "eformat/Status.h"
#include "eformat/HeaderMarker.h"
#include "eformat/checksum.h"

static const uint32_t NDATA = 4096;
static const uint32_t NSTATUS = 2;
static const uint32_t ROD_NSTATUS = 32;
static const uint32_t ROD_RUN_NUMBER = 0xaaaa;
static const uint32_t ROD_LVL1_ID = 0xbbbb;
static const uint32_t ROD_BC_ID = 0xcccc;
static const uint32_t ROD_LVL1_TYPE = 0xdddd;
static const uint32_t ROD_DETEV_TYPE = 0xeeee;
static uint32_t CHECKSUM_TYPE = eformat::NO_CHECKSUM;

/**
 * Checks for the reading of a block of data that has been serialized from
 * the writer given.
 *
 * @param rob_id The source identifier originally set at the writer, for the
 * ROB 
 * @param rod_id The source identifier originally set at the writer, for the
 * ROD 
 * @param writer The writer used for the test
 */
void checkMetaData (const eformat::helper::SourceIdentifier& rob_id,
    const eformat::helper::SourceIdentifier& rod_id,
    const eformat::write::ROBFragment& writer)
{
  uint32_t checksum_addon = 0;
  if (CHECKSUM_TYPE != eformat::NO_CHECKSUM) checksum_addon = 1;

  //do some checking using the writer API
  BOOST_CHECK_EQUAL(writer.source_id(), rob_id.code());
  BOOST_CHECK_EQUAL(writer.rob_source_id(), rob_id.code());
  BOOST_CHECK_EQUAL(writer.rod_source_id(), rod_id.code());
  BOOST_CHECK_EQUAL(writer.minor_version(), eformat::DEFAULT_VERSION & 0xffff);
  BOOST_CHECK_EQUAL(writer.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(writer.rod_run_no(), ROD_RUN_NUMBER);
  BOOST_CHECK_EQUAL(writer.rod_lvl1_id(), ROD_LVL1_ID);
  BOOST_CHECK_EQUAL(writer.rod_bc_id(), ROD_BC_ID);
  BOOST_CHECK_EQUAL(writer.rod_lvl1_type(), ROD_LVL1_TYPE);
  BOOST_CHECK_EQUAL(writer.rod_detev_type(), ROD_DETEV_TYPE);
  BOOST_CHECK_EQUAL(writer.rob_header_size_word(), 7 + writer.nstatus());
  BOOST_CHECK_EQUAL(writer.meta_size_word(), 19 + writer.nstatus() + 
      writer.rod_nstatus() + checksum_addon);
  BOOST_CHECK_EQUAL(writer.size_word(), 19 + writer.nstatus() + 
      writer.rod_nstatus() + writer.rod_ndata() + checksum_addon);
}

/**
 * Checks for the reading of a block of data that has been serialized from
 * the writer given, and is using the following data blocks
 *
 * @param rob_id The source identifier originally set at the writer, for the
 * ROB 
 * @param rod_id The source identifier originally set at the writer, for the
 * ROD 
 * @param writer The writer used for the test
 * @param data The data vector originally set in the writer
 * @param status The status vector originally set in the writer (ROB-level)
 * @param rod_status The (ROD-level) status vector originally set in the
 * writer
 */
void checkReadWrite (const eformat::helper::SourceIdentifier& rob_id,
    const eformat::helper::SourceIdentifier& rod_id,
    eformat::write::ROBFragment& writer)
{
  //serializes the ROB fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();
  BOOST_CHECK(top);

  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, 
        writer.size_word()-1), (unsigned)0);

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK
    (eformat::write::copy(*top, serialized, writer.size_word()) == 
     writer.size_word()); //now it can go on

  eformat::ROBFragment<const uint32_t*> reader(serialized);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::ROB);
  BOOST_CHECK_EQUAL(reader.rod_marker(), eformat::ROD);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), rob_id.code());
  BOOST_CHECK_EQUAL(reader.rob_source_id(), rob_id.code());
  BOOST_CHECK_EQUAL(reader.rod_source_id(), rod_id.code());
  BOOST_CHECK_EQUAL(reader.nstatus(), (unsigned)1);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status);
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_status[0], 
      eformat::UNCLASSIFIED);
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.rod_run_no(), ROD_RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.rod_lvl1_id(), ROD_LVL1_ID);
  BOOST_CHECK_EQUAL(reader.rod_bc_id(), ROD_BC_ID);
  BOOST_CHECK_EQUAL(reader.rod_lvl1_trigger_type(), ROD_LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.rod_detev_type(), ROD_DETEV_TYPE);
  BOOST_CHECK_EQUAL(reader.rod_ndata(), (unsigned)0);
  BOOST_CHECK_EQUAL(reader.rod_nstatus(), (unsigned)1);
  BOOST_CHECK_EQUAL(reader.rod_header_size_word(),
      writer.rod_header_size_word());
  BOOST_CHECK_EQUAL(reader.rod_status_position(), eformat::STATUS_FRONT);
  const uint32_t* reader_rod_status = 0;
  reader.rod_status(reader_rod_status);
  BOOST_CHECK(reader_rod_status);
  BOOST_CHECK_EQUAL((eformat::GenericStatus)reader_rod_status[0], 
      eformat::UNCLASSIFIED);
  BOOST_CHECK(reader.checksum());

}

/**
 * Checks for the reading of a block of data that has been serialized from
 * the writer given.
 *
 * @param rob_id The source identifier originally set at the writer, for the
 * ROB 
 * @param rod_id The source identifier originally set at the writer, for the
 * ROD 
 * @param writer The writer used for the test
 */
void checkReadWrite (const eformat::helper::SourceIdentifier& rob_id,
    const eformat::helper::SourceIdentifier& rod_id,
    eformat::write::ROBFragment& writer,
    const uint32_t data[NDATA], 
    const uint32_t status[NSTATUS],
    const uint32_t rod_status[ROD_NSTATUS])
{
  //serializes the ROB fragment and read the same info via its read
  //counterpart, to check all values are correct
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();
  BOOST_CHECK(top);

  //makes sure we can use the node interface, with the same count
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());

  //here we force an error to make sure the library is detecting the
  //lack of space (notice the last word cannot be written given the space).
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, 
        writer.size_word()-1), (unsigned)0); 

  //once this is tested, we proceed with the "correct" serialization
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK
    (eformat::write::copy(*top, serialized, writer.size_word()) == 
     writer.size_word()); //now it can go on

  eformat::ROBFragment<const uint32_t*> reader(serialized);
  BOOST_CHECK(reader.check());
  BOOST_CHECK_EQUAL(reader.marker(), eformat::ROB);
  BOOST_CHECK_EQUAL(reader.rod_marker(), eformat::ROD);
  BOOST_CHECK_EQUAL(reader.fragment_size_word(), writer.size_word());
  BOOST_CHECK_EQUAL(reader.source_id(), rob_id.code());
  BOOST_CHECK_EQUAL(reader.rob_source_id(), rob_id.code());
  BOOST_CHECK_EQUAL(reader.rod_source_id(), rod_id.code());
  BOOST_CHECK_EQUAL(reader.nstatus(), NSTATUS);
  const uint32_t* reader_status = 0;
  reader.status(reader_status);
  BOOST_CHECK(reader_status);
  for (size_t i = 0; i < NSTATUS; ++i)
    BOOST_CHECK_EQUAL(status[i], reader_status[i]);
  BOOST_CHECK_EQUAL(reader.checksum_type(), CHECKSUM_TYPE);
  BOOST_CHECK_EQUAL(reader.rod_run_no(), ROD_RUN_NUMBER);
  BOOST_CHECK_EQUAL(reader.rod_lvl1_id(), ROD_LVL1_ID);
  BOOST_CHECK_EQUAL(reader.rod_bc_id(), ROD_BC_ID);
  BOOST_CHECK_EQUAL(reader.rod_lvl1_trigger_type(), ROD_LVL1_TYPE);
  BOOST_CHECK_EQUAL(reader.rod_detev_type(), ROD_DETEV_TYPE);
  BOOST_CHECK_EQUAL(reader.rod_ndata(), NDATA);
  const uint32_t* reader_data = 0;
  reader.rod_data(reader_data);
  BOOST_CHECK(reader_data);
  for (size_t i = 0; i < NDATA; ++i)
    BOOST_CHECK_EQUAL(reader_data[i], data[i]);
  BOOST_CHECK_EQUAL(reader.rod_nstatus(), ROD_NSTATUS);
  BOOST_CHECK_EQUAL(reader.rod_header_size_word(),
      writer.rod_header_size_word());
  BOOST_CHECK_EQUAL(reader.rod_status_position(), eformat::STATUS_FRONT);
  const uint32_t* reader_rod_status = 0;
  reader.rod_status(reader_rod_status);
  BOOST_CHECK(reader_rod_status);
  for (size_t i = 0; i < ROD_NSTATUS; ++i)
    BOOST_CHECK_EQUAL(rod_status[i], reader_rod_status[i]);
  
  BOOST_CHECK(reader.checksum());
}

/**
 * How I should prepare ("in the standard way") the data that has to be
 * changed on the tests.
 *
 * @param data The data vector, with size NDATA
 * @param status The ROB status vector, with size NSTATUS
 * @param rod_status The ROD status vector, with size ROD_NSTATUS
 */
void prepareChangeData (uint32_t* data, uint32_t* status, 
    uint32_t* rod_status)
{
  for (size_t i = 0; i < NDATA; ++i) data[i] = i;
  status[0] = eformat::helper::Status(eformat::DATA_CORRUPTION, 0xffff).code();
  status[1] = 0xdeadbeef;
  rod_status[0] = 
    eformat::helper::Status(eformat::DATA_CORRUPTION, 0xbeef).code();
  for (size_t i = 1; i < ROD_NSTATUS; ++i) rod_status[i] = i;
}

BOOST_AUTO_TEST_CASE( meta_data )
{
  eformat::helper::SourceIdentifier rob_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID, 
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);
  writer.checksum_type(CHECKSUM_TYPE);
  checkMetaData(rob_id, rod_id, writer);
}

BOOST_AUTO_TEST_CASE( serialize ) 
{
  eformat::helper::SourceIdentifier rob_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID, 
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);
  writer.checksum_type(CHECKSUM_TYPE);
  checkReadWrite(rob_id, rod_id, writer);
}

BOOST_AUTO_TEST_CASE( change_and_serialize ) 
{
  eformat::helper::SourceIdentifier rob_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID, 
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);
  writer.checksum_type(CHECKSUM_TYPE);

  //variables I need for testing
  uint32_t data[NDATA];
  uint32_t status[NSTATUS];
  uint32_t rod_status[ROD_NSTATUS];
  prepareChangeData(data, status, rod_status);

  writer.rod_data(NDATA, data);
  writer.status(NSTATUS, status);
  writer.rod_status(ROD_NSTATUS, rod_status);

  //check all non-changeable fields look ok
  checkMetaData(rob_id, rod_id, writer);

  //standard checking for serialization
  checkReadWrite(rob_id, rod_id, writer, data, status, rod_status);
}

BOOST_AUTO_TEST_CASE( serialize_from_block )
{
  eformat::helper::SourceIdentifier rob_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID, 
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);
  writer.checksum_type(CHECKSUM_TYPE);

  //write the "empty" ROB fragment to a stream of 32-bit unsigned integers
  uint32_t* serialized = new uint32_t[writer.size_word()];
  const eformat::write::node_t* top = writer.bind();
  BOOST_CHECK(top);
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), writer.size_word());
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, writer.size_word()),
      writer.size_word()); //now it can go on

  //re-start a new writer
  eformat::write::ROBFragment writer2(serialized);

  //at the new writer, change the values for data, status and ROD fragment
  //status. Remember, the writer was initialized from an existing block of
  //data, for the eformat library, this is like "cast in stone"!

  //variables I need for testing
  uint32_t data[NDATA];
  uint32_t status[NSTATUS];
  uint32_t rod_status[ROD_NSTATUS];
  prepareChangeData(data, status, rod_status);

  writer2.rod_data(NDATA, data);
  writer2.status(NSTATUS, status);
  writer2.rod_status(ROD_NSTATUS, rod_status);

  //standard checks
  checkMetaData(rob_id, rod_id, writer2);
  checkReadWrite(rob_id, rod_id, writer2, data, status, rod_status);

  //clean-up
  delete[] serialized;
}

BOOST_AUTO_TEST_CASE( serialize_from_rod_block ) 
{
  eformat::helper::SourceIdentifier rob_id(eformat::OTHER, 0xaaaa);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID, 
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);

  //write the "empty" ROB fragment to a stream of 32-bit unsigned integers
  uint32_t rod_size = writer.rod_header_size_word() + 
    writer.rod_trailer_size_word() + writer.rod_nstatus() + 
    writer.rod_ndata(); 
  uint32_t* serialized = new uint32_t[rod_size];
  const eformat::write::node_t* top = writer.rod_bind();
  BOOST_CHECK(top);
  BOOST_CHECK_EQUAL(eformat::write::count_words(*top), rod_size);
  BOOST_CHECK_EQUAL(eformat::write::copy(*top, serialized, rod_size), rod_size);

  //re-start a new writer, from the ROD block
  eformat::write::ROBFragment writer2(serialized, rod_size);
  //this should have initialized correctly, automatically
  BOOST_CHECK_EQUAL(writer2.rob_source_id(), writer2.rod_source_id());
  BOOST_CHECK_EQUAL(writer2.rob_source_id(), rod_id.code());
  //and the check sum type
  writer2.checksum_type(CHECKSUM_TYPE);

  //variables I need for testing
  uint32_t data[NDATA];
  uint32_t status[NSTATUS];
  uint32_t rod_status[ROD_NSTATUS];
  prepareChangeData(data, status, rod_status);

  writer2.rod_data(NDATA, data);
  writer2.status(NSTATUS, status);
  writer2.rod_status(ROD_NSTATUS, rod_status);

  //standard checks
  checkMetaData(rob_id, rod_id, writer2);
  checkReadWrite(rob_id, rod_id, writer2, data, status, rod_status);

  //clean-up
  delete[] serialized;
}

BOOST_AUTO_TEST_CASE( serialize_from_copy ) 
{
  eformat::helper::SourceIdentifier rob_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID, 
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);
  writer.checksum_type(CHECKSUM_TYPE);

  //variables I need for testing
  uint32_t data[NDATA];
  uint32_t status[NSTATUS];
  uint32_t rod_status[ROD_NSTATUS];
  prepareChangeData(data, status, rod_status);

  writer.rod_data(NDATA, data);
  writer.status(NSTATUS, status);
  writer.rod_status(ROD_NSTATUS, rod_status);

  eformat::write::ROBFragment writer2(writer);

  //standard checks
  checkMetaData(rob_id, rod_id, writer2);
  checkReadWrite(rob_id, rod_id, writer2, data, status, rod_status);
}

BOOST_AUTO_TEST_CASE( checksum_basic )
{
  CHECKSUM_TYPE = eformat::CRC16_CCITT;

  eformat::helper::SourceIdentifier rob_id(eformat::TDAQ_LVL2, 0xffff);
  eformat::helper::SourceIdentifier rod_id(eformat::OTHER, 0xaaaa);
  eformat::write::ROBFragment writer(rob_id.code(), rod_id.code(),
      ROD_RUN_NUMBER, ROD_LVL1_ID,
      ROD_BC_ID, ROD_LVL1_TYPE, ROD_DETEV_TYPE,
      0, 0, eformat::STATUS_FRONT);
  writer.checksum_type(CHECKSUM_TYPE);

  //variables I need for testing
  uint32_t data[NDATA];
  uint32_t status[NSTATUS];
  uint32_t rod_status[ROD_NSTATUS];
  prepareChangeData(data, status, rod_status);

  writer.rod_data(NDATA, data);
  writer.status(NSTATUS, status);
  writer.rod_status(ROD_NSTATUS, rod_status);

  //check all non-changeable fields look ok
  checkMetaData(rob_id, rod_id, writer);

  //standard checking for serialization
  checkReadWrite(rob_id, rod_id, writer, data, status, rod_status);

  CHECKSUM_TYPE = eformat::NO_CHECKSUM;
}
