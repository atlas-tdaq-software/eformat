//Dear emacs, this is -*- c++ -*-

/**
 * @file idtest.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Describes some tests with standard eformat source identifiers
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE SourceIdentifier Tests
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include "eformat/SourceIdentifier.h"
#include "eformat/DetectorMask.h"
#include "eformat/Issue.h"
#include <algorithm>
#include <iostream>

BOOST_AUTO_TEST_CASE( source_id_encoding )
{
  eformat::helper::SourceIdentifier id(eformat::FULL_SD_EVENT, 0xffff);
  BOOST_CHECK_EQUAL(id.code(), (unsigned)0x00ffff);
  eformat::helper::SourceIdentifier id2(eformat::PIXEL_BARREL, 0xfffe);
  BOOST_CHECK_EQUAL(id2.code(), (unsigned)0x11fffe);
  eformat::helper::SourceIdentifier id3(eformat::TDAQ_LVL2, 0xffef);
  BOOST_CHECK_EQUAL(id3.code(), (unsigned)0x7bffef);
}

BOOST_AUTO_TEST_CASE( source_id_decoding )
{
  eformat::helper::SourceIdentifier id(0x00ffff);
  BOOST_CHECK_EQUAL(id.subdetector_id(), eformat::FULL_SD_EVENT);
  BOOST_CHECK_EQUAL(id.module_id(), (unsigned)0xffff);
  eformat::helper::SourceIdentifier id2(0x11fffe);
  BOOST_CHECK_EQUAL(id2.subdetector_id(), eformat::PIXEL_BARREL);
  BOOST_CHECK_EQUAL(id2.module_id(), (unsigned)0xfffe);
  eformat::helper::SourceIdentifier id3(0x7bffef);
  BOOST_CHECK_EQUAL(id3.subdetector_id(), eformat::TDAQ_LVL2);
  BOOST_CHECK_EQUAL(id3.module_id(), (unsigned)0xffef);
}

BOOST_AUTO_TEST_CASE( detectormask )
{
  std::vector<eformat::SubDetector> test;
  test.push_back(eformat::OTHER);
  test.push_back(eformat::MUON_ANCILLARY_CRATE);
  test.push_back(eformat::SCT_BARREL_A_SIDE);
  test.push_back(eformat::TDAQ_CALO_PREPROC);
  // this will encode
  eformat::helper::DetectorMask mask(test);
  // this will decode and check the basic routines
  BOOST_CHECK_EQUAL(mask.is_set(eformat::OTHER), true);
  BOOST_CHECK_EQUAL(mask.is_set(eformat::MUON_ANCILLARY_CRATE), true);
  BOOST_CHECK_EQUAL(mask.is_set(eformat::SCT_BARREL_A_SIDE), true);
  BOOST_CHECK_EQUAL(mask.is_set(eformat::TDAQ_CALO_PREPROC), true);
  BOOST_CHECK_EQUAL(mask.is_set(eformat::TDAQ_CTP), false);
  try {
    BOOST_CHECK_EQUAL(mask.is_set(eformat::FULL_SD_EVENT), false);
  }
  catch (eformat::UnsupportedSubdetectorIdentifierIssue& e) {
    //expected...
  }
  std::vector<eformat::SubDetector> decoded;
  mask.sub_detectors(decoded);
  std::sort(test.begin(), test.end());
  std::sort(decoded.begin(), decoded.end());
  BOOST_CHECK_EQUAL(decoded==test, true);

  // copy constructor
  eformat::helper::DetectorMask mask2(mask);
  BOOST_CHECK_EQUAL(mask.mask(), mask2.mask());
}

BOOST_AUTO_TEST_CASE( detectormask_128 )
{
  std::bitset<128> maskvalue = 0x1234567812345678;
  maskvalue = maskvalue << 64;
  maskvalue |= 0x1234567812345678;

  eformat::helper::DetectorMask mask(maskvalue);
  BOOST_CHECK_EQUAL(mask.mask(), maskvalue);
}

BOOST_AUTO_TEST_CASE( detectormask_64_64 )
{
  uint64_t least = 0x1234567812345678LL;
  uint64_t most = 0xAABBCCDDDDCCBBAALL;
  eformat::helper::DetectorMask mask(least, most);
  std::bitset<128> checkmask(most);
  checkmask <<= 64;
  checkmask |= least;
  BOOST_CHECK_EQUAL(mask.mask(), checkmask);

  auto split = mask.serialize();
  BOOST_CHECK_EQUAL(split.first, most);
  BOOST_CHECK_EQUAL(split.second, least);
}


BOOST_AUTO_TEST_CASE( detectormask_64 )
{
  uint64_t shortmask = 0x1234567812345678LL;
  eformat::helper::DetectorMask mask(shortmask);
  BOOST_CHECK_EQUAL(mask.mask(), shortmask);
}

BOOST_AUTO_TEST_CASE( detectormask_string )
{
  std::bitset<128> maskvalue(0x1234567812345678);
  maskvalue <<= 64;
  maskvalue |= 0xAABBCCDDDDCCBBAA;
  
  BOOST_CHECK_EQUAL(maskvalue, eformat::helper::DetectorMask::from_string(eformat::helper::DetectorMask::to_string(maskvalue)));
  
  eformat::helper::DetectorMask mask(maskvalue);
  
  std::string maskstring = mask.string();
  BOOST_CHECK_EQUAL(maskstring.size(), 32);

  eformat::helper::DetectorMask mask2(maskstring);

  BOOST_CHECK_EQUAL(mask.mask(), mask2.mask());
  BOOST_CHECK_EQUAL(mask2.mask(), maskvalue);

  //Check failures of string conversion
  BOOST_CHECK_THROW(eformat::helper::DetectorMask("tooshort"), 
		    eformat::CannotDecodeMaskIssue);
  BOOST_CHECK_THROW(eformat::helper::DetectorMask("QABBCCDDAABBCCDDAABBCCDDAABBCCDD"), 
		    eformat::CannotDecodeMaskIssue);
}
