//Dear emacs, this is -*- c++ -*-

/**
 * @file SubDetectorFragment.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the SubDetector fragment writing helper class
 */

#include "eformat/write/FullEventFragment.h"
#include "eformat/FullEventFragment.h"
#include "eformat/HeaderMarker.h"
#include "eformat/Status.h"
#include "eformat/Issue.h"
#include "eformat/checksum.h"
#include "eformat/compression.h"
#include "compression/DataBuffer.h"

void eformat::write::FullEventFragment::initialize() 
{
  m_header[0] = eformat::FULL_EVENT; //marker
  m_header[1] = 24; //this header size + status size
  m_header[2] = 24; //this header size + status size
  m_header[3] = eformat::DEFAULT_VERSION; //format version
  m_header[4] = 0; //source identifier
  m_header[5] = 1; //number of status
  m_header[6] = eformat::NO_CHECKSUM; //check sum
  m_header[7] = 0; //bc time seconds
  m_header[8] = 0; //bc time nanoseconds
  m_header[9] = 0; //global identifier LS
  m_header[10] = 0; //global identifier MS
  m_header[11] = 0; //run type
  m_header[12] = 0; //run number
  m_header[13] = 0; //luminosity block number
  m_header[14] = 0; //LVL1 identifiery
  m_header[15] = 0; //bunch crossing identifier
  m_header[16] = 0; //LVL1 trigger type 
  m_header[17] = eformat::UNCOMPRESSED; //Compression type 
  m_header[18] = 0; //Uncompressed size
  m_header[19] = 0; //number of LVL1 info words
  m_header[20] = 0; //number of LVL2 info words
  m_header[21] = 0; //number of EF info words
  m_header[22] = 0; //number of Stream Tag words

  //now initialize pages
  set(m_node[0], m_header, 6, &m_node[1]);
  set(m_node[1], &eformat::DEFAULT_STATUS, 1, &m_node[2]);
  set(m_node[2], &m_header[6], 14, &m_node[3]);
  set(m_node[3], 0, 0, &m_node[4]); //no LVL1 info yet
  set(m_node[4], &m_header[20], 1, &m_node[5]); 
  set(m_node[5], 0, 0, &m_node[6]); //no LVL2 info yet
  set(m_node[6], &m_header[21], 1, &m_node[7]); 
  set(m_node[7], 0, 0, &m_node[8]); //no EF info yet
  set(m_node[8], &m_header[22], 1, &m_node[9]); 
  set(m_node[9], 0, 0, 0); //no stream tag yet

  //optional extra payload 
  set(m_node[10], 0, 0, 0);
  
  //optional checksum stuff (trailer)
  m_checksum = 0;
  set(m_node[11], &m_checksum, 1, 0);

  //allocate the initial unchecked array 
  m_unchecked_size = 2500;
  m_unchecked = new eformat::write::node_t[m_unchecked_size];

  //our unchecked data
  m_n_unchecked = 0;
  set(m_unchecked[0], 0, 0, 0);
 
  //the compressed data node
  set(m_compressed_node, 0, 0, 0);
 
  m_child = 0;
  m_last = 0;

  m_compressed = nullptr;
  m_compression_level = 1;
}

eformat::write::FullEventFragment::FullEventFragment 
(uint32_t source_id, uint32_t bc_time_secs, uint32_t bc_time_nsec,
 uint64_t global_id, uint32_t run_type,
 uint32_t run_no, uint16_t lumi_block,
 uint32_t lvl1_id, uint16_t bc_id, uint8_t lvl1_type)
{
  initialize();
  this->source_id(source_id);
  bc_time_seconds(bc_time_secs);
  bc_time_nanoseconds(bc_time_nsec);
  this->global_id(global_id);
  this->run_type(run_type);
  this->run_no(run_no);
  this->lumi_block(lumi_block);
  this->lvl1_id(lvl1_id);
  this->bc_id(bc_id);
  lvl1_trigger_type(lvl1_type);
}

eformat::write::FullEventFragment::FullEventFragment (const uint32_t* fe)
{
  initialize();
  copy_header(fe);
  eformat::FullEventFragment<const uint32_t*> read(fe);
  if (read.readable_payload_size_word() > 0) {
    m_node[9].next = &m_node[10];
    auto tmp = read.readable_payload();
    set(m_node[10], tmp, read.readable_payload_size_word(), 0);
    m_header[1] += read.readable_payload_size_word();
  }
}

eformat::write::FullEventFragment::FullEventFragment ()
{
  initialize();
}

eformat::write::FullEventFragment::FullEventFragment 
  (const eformat::write::FullEventFragment& other)
{
  *this = other;
}

eformat::write::FullEventFragment::~FullEventFragment() 
{
  delete[] m_unchecked;
}

eformat::write::FullEventFragment& 
  eformat::write::FullEventFragment::operator= 
(const eformat::write::FullEventFragment& other)
{
  if (this == &other) return *this;

  initialize();
  copy_header(other);
  //copy extra payload attached
  if (other.m_node[10].size_word) {
    m_node[9].next = &m_node[10];
    set(m_node[10], other.m_node[10].base, other.m_node[10].size_word, 0);
  }
  //copy the ROBFragments attached
  for (const eformat::write::ROBFragment* 
      it = other.m_child; it; it = it->next())
    append(const_cast<eformat::write::ROBFragment*>(it));
  //get the unchecked ROB fragments attached
  if (m_n_unchecked) {
    m_n_unchecked = other.m_n_unchecked;
    for (uint32_t i=0; i < m_n_unchecked; ++i) {
      set(m_unchecked[i], other.m_unchecked[i].base,
          other.m_unchecked[i].size_word, &m_unchecked[i+1]);
    }
    m_unchecked[m_n_unchecked-1].next = 0;
  }
  return *this;
}

void eformat::write::FullEventFragment::copy_header(const uint32_t* other)
{
  eformat::FullEventFragment<const uint32_t*> read(other);
  read.check(); //this gives us a good start-up scenario
  eformat::helper::Version fe_ver(read.version());
  minor_version(fe_ver.minor_version());
  source_id(read.source_id());
  bc_time_seconds(read.bc_time_seconds());
  bc_time_nanoseconds(read.bc_time_nanoseconds());
  global_id(read.global_id());
  run_type(read.run_type());
  run_no(read.run_no());
  lumi_block(read.lumi_block());
  lvl1_id(read.lvl1_id());
  bc_id(read.bc_id());
  lvl1_trigger_type(read.lvl1_trigger_type());
  compression_type(read.compression_type());
  status(read.nstatus(), read.status());
  lvl1_trigger_info(read.nlvl1_trigger_info(),read.lvl1_trigger_info());
  lvl2_trigger_info(read.nlvl2_trigger_info(),read.lvl2_trigger_info());
  hlt_info(read.nhlt_info(), read.hlt_info());
  stream_tag(read.nstream_tag(), read.stream_tag());
  checksum_type(read.checksum_type());
}

void eformat::write::FullEventFragment::copy_header
(const eformat::write::FullEventFragment& other)
{
  minor_version(other.minor_version());
  source_id(other.source_id());
  bc_time_seconds(other.bc_time_seconds());
  bc_time_nanoseconds(other.bc_time_nanoseconds());
  global_id(other.global_id());
  run_type(other.run_type());
  run_no(other.run_no());
  lumi_block(other.lumi_block());
  lvl1_id(other.lvl1_id());
  bc_id(other.bc_id());
  lvl1_trigger_type(other.lvl1_trigger_type());
  compression_type(other.compression_type());
  status(other.nstatus(), other.status());
  lvl1_trigger_info(other.nlvl1_trigger_info(), other.lvl1_trigger_info());
  lvl2_trigger_info(other.nlvl2_trigger_info(), other.lvl2_trigger_info());
  hlt_info(other.nhlt_info(), other.hlt_info());
  stream_tag(other.nstream_tag(), other.stream_tag());
  checksum_type(other.checksum_type());
}

void eformat::write::FullEventFragment::status 
(uint32_t n, const uint32_t* status)
{ 
  m_node[0].base[1] -= m_node[0].base[5]; //remove count from previous status
  m_node[0].base[2] -= m_node[0].base[5]; //remove count from previous status
  m_node[1].size_word = m_node[0].base[5] = n; //set new values
  m_node[0].base[1] += n;
  m_node[0].base[2] += n;
  m_node[1].base = const_cast<uint32_t*>(status);
}

void eformat::write::FullEventFragment::lvl1_trigger_info 
(uint32_t n, const uint32_t* data)
{ 
  m_node[0].base[1] -= nlvl1_trigger_info(); //remove previous count 
  m_node[0].base[2] -= nlvl1_trigger_info(); //remove previous count 
  m_node[3].size_word = m_node[2].base[13] = n; //set new values
  m_node[0].base[1] += n; //fragment size
  m_node[0].base[2] += n; //header size
  m_node[3].base = const_cast<uint32_t*>(data);
}

  void eformat::write::FullEventFragment::lvl2_trigger_info 
(uint32_t n, const uint32_t* data)
{ 
  m_node[0].base[1] -= nlvl2_trigger_info(); //remove previous count 
  m_node[0].base[2] -= nlvl2_trigger_info(); //remove previous count 
  m_node[5].size_word = m_node[4].base[0] = n; //set new values
  m_node[0].base[1] += n; //fragment size
  m_node[0].base[2] += n; //header size
  m_node[5].base = const_cast<uint32_t*>(data);
}

void eformat::write::FullEventFragment::hlt_info 
(uint32_t n, const uint32_t* data)
{ 
  m_node[0].base[1] -= nhlt_info(); //remove previous count 
  m_node[0].base[2] -= nhlt_info(); //remove previous count 
  m_node[7].size_word = m_node[6].base[0] = n; //set new values
  m_node[0].base[1] += n; //fragment size
  m_node[0].base[2] += n; //header size
  m_node[7].base = const_cast<uint32_t*>(data);
}

void eformat::write::FullEventFragment::stream_tag 
(uint32_t n, const uint32_t* data)
{ 
  m_node[0].base[1] -= nstream_tag(); //remove previous count 
  m_node[0].base[2] -= nstream_tag(); //remove previous count 
  m_node[9].size_word = m_node[8].base[0] = n; //set new values
  m_node[0].base[1] += n; //fragment size
  m_node[0].base[2] += n; //header size
  m_node[9].base = const_cast<uint32_t*>(data);
}

void eformat::write::FullEventFragment::append 
(eformat::write::ROBFragment* rob)
{
  rob->parent(this);
  rob->next(0); //reset any previous relationship
  m_node[0].base[1] += rob->size_word();

  //adjust `m_last' and `m_child' to point to the new last ROB
  if (m_last) m_last->next(rob);
  else m_child = rob;
  m_last = rob;
}

void eformat::write::FullEventFragment::append_unchecked (const uint32_t* rob)
{
  if (m_n_unchecked == m_unchecked_size){

    m_unchecked_size *= 2;
    auto tmp = new eformat::write::node_t[m_unchecked_size];
 
    // Copy the linked list updating the addresses
    for(uint32_t i = 0; i < m_n_unchecked; ++i){
      set(tmp[i], m_unchecked[i].base,
          m_unchecked[i].size_word, &tmp[i+1]);
    }

    // Set last address to 0
    tmp[m_n_unchecked-1].next = 0;

    delete[] m_unchecked;
    m_unchecked = tmp;
  }

  eformat::ROBFragment<const uint32_t*> frag(rob);
  if (m_n_unchecked) //more fragments are available before this one
    m_unchecked[m_n_unchecked-1].next = &m_unchecked[m_n_unchecked];
  set(m_unchecked[m_n_unchecked++], rob, frag.fragment_size_word(), 0);
  m_node[0].base[1] += frag.fragment_size_word();
}

const eformat::write::node_t* 
eformat::write::FullEventFragment::bind (void)
{
  //the header is already concatenated by construction
  eformat::write::node_t* last = &m_node[9];
  eformat::write::node_t* payload = last; //buffer for an instant...
  if (m_node[10].size_word) last = last->next; //advance one node
  last->next = 0; //potentially remove old checksum

  //iterate over the attached children
  for (ROBFragment* curr = m_child; curr; 
      curr = const_cast<ROBFragment*>(curr->next())) {
    last->next = const_cast<eformat::write::node_t*>(curr->bind());
    while (last->next) last = last->next; //advance until end
  }

  //make sure the last page of the last attached children points to the first
  //unchecked page, but only if we have unchecked ROB fragments appended
  if (m_n_unchecked) {
    last->next = m_unchecked;
    last = &m_unchecked[m_n_unchecked-1];
    last->next = 0; //potentially remove old checksum
  }

  //position cursor at payload start
  payload = payload->next;
   
  //set uncompressed payload size for non-compressed events
  m_header[18] = m_header[1] - m_header[2] - (checksum_type()?1:0);

  //compress the event, if needed
  if (payload && compression_type() != eformat::UNCOMPRESSED &&
      compression_type() != eformat::RESERVED) {

    //uncompressed payload size
    m_header[18] = eformat::write::count_words(*payload);
    
    //fill iovec with payload pointers
    auto iovec_size = eformat::write::count(*payload);
    auto payload_iovec = std::unique_ptr<struct iovec[]>(new struct iovec[iovec_size]);
    auto iovec_pages = eformat::write::shallow_copy(*payload, payload_iovec.get(), 
					       iovec_size);

    auto payload_size_byte = m_header[18]*sizeof(uint32_t);
    //prepare compression buffer
    m_compressed->realloc(payload_size_byte);
    
    uint32_t compressed_size_byte = 0;
    
    if(compression_type() == eformat::ZLIB){
      try{
	compression::zlibcompress(*(m_compressed.get()), 
				  compressed_size_byte, iovec_pages,
				  payload_iovec.get(), 
				  payload_size_byte, m_compression_level);
      }catch(compression::ZlibFailed& e){
	throw eformat::CompressionIssue(ERS_HERE,e);
      }
    }
    
    //fix fragment size
    auto compressed_size_word = (compressed_size_byte/sizeof(uint32_t));
    m_header[1] = m_header[2]+compressed_size_word+(checksum_type()?1:0);

    //replace uncompressed payload with compressed data
    last = &m_node[9];
    last->next = &m_compressed_node;
    m_compressed_node.base = static_cast<uint32_t*>(m_compressed->handle());
    m_compressed_node.size_word = compressed_size_word;
    m_compressed_node.next = 0;
    last = last->next;
    //Update the payload pointer for the next step
    payload = last;
  }

  //calculate the checksum if it was requested and we have data
  //in this part of the code "payload" points to 1 node before the begin
  if (checksum_type() != eformat::NO_CHECKSUM) {
    if (!payload) {
      eformat::write::node_t null;
      eformat::write::set(null, 0, 0, 0);
      m_checksum = eformat::write::checksum(checksum_type(), &null);
    } else {
      m_checksum = eformat::write::checksum(checksum_type(), payload);
    }
    last->next = &m_node[11];
  }
  
  return m_node;
}

void eformat::write::FullEventFragment::checksum_type(uint32_t s)
{ 
  if (m_node[2].base[0] == eformat::NO_CHECKSUM && s != eformat::NO_CHECKSUM) {
    //Going from no checksum to having a checksum: update sizes
    m_node[0].base[1] += 1;
  }
  else if (m_node[2].base[0] != eformat::NO_CHECKSUM && 
      s == eformat::NO_CHECKSUM) {
    //Going from having a checksum to no checksum: update sizes
    m_node[0].base[1] -= 1;
  }
  m_node[2].base[0] = s;
}

void eformat::write::FullEventFragment::compression_type(const uint32_t s)
{ 
  if(s != eformat::UNCOMPRESSED){
    m_compressed.reset(new compression::DataBuffer());
  }
  m_node[2].base[11] = s; 
}


const uint32_t* eformat::write::FullEventFragment::unchecked_fragment 
(uint32_t n) const
{
  return m_unchecked[n].base;
}
