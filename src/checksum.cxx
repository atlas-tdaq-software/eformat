/**
 * @file src/checksum.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Implements the check-sum routines
 */

#include "eformat/checksum.h"
#include <zlib.h> //for Adler-32

uint32_t eformat::helper::adler32(const uint32_t* data, uint32_t size)
{
  uint32_t boot = ::adler32(0L, Z_NULL, 0); //zlib call
  return eformat::helper::adler32(boot, data, size);
}

uint32_t eformat::helper::adler32(uint32_t init, const uint32_t* data, 
    uint32_t size)
{
  return ::adler32(init, (const Bytef*)data, size*sizeof(uint32_t)); //zlib
}

/**
 * From this point on, stuff required for the CRC16-CCITT check-sum
 * calculation.
 */
class crc16_t {

  private:

    uint32_t m_bootstrap;
    const uint32_t m_order;
    uint32_t m_xor;
    uint32_t m_poly;
    uint32_t m_mask;
    uint32_t m_lut[65536];

    /**
     * This method will update the 16-bit CRC of the 32-bit data block
     * taking into consideration one of two distinguished parts: the most
     * significative (ms) 16-bit words or the least significative. The CRC
     * calculation of both parts should be united to form the final CRC value
     * for the 32-bit data block.
     */
    uint16_t checksum16(const uint32_t* data, uint32_t size, 
        bool ms, uint32_t crc) {
      for (uint32_t i=0; i<size; ++i) {
        uint16_t val = (ms?(((data[i])>>16)&0xffff):(data[i]&0xffff));
        crc = (crc << 16) ^ m_lut[((crc >> (m_order - 16)) & 0xffff) ^ val];
      }
      crc ^= m_xor;
      crc &= m_mask;
      return (uint16_t)crc;
    }
    
  public:

    /**
     * Ctor. We initialize a few interesting values (that determine the CRC
     * type) and a fast look-up table with 64k elements.
     */
    crc16_t (): m_bootstrap(0xffff), m_order(16), m_xor(0x000), m_poly(0x1021),
                m_mask(((((uint32_t) 1 << (m_order - 1)) - 1) << 1) | 1)
    {
      //compute constant bit masks for whole CRC and CRC high bit
      uint32_t crchighbit = (uint32_t) 1 << (m_order - 1);

      //compute CRC table
      for (uint32_t i = 0; i < 65536; i++) {
        uint32_t crc = i;
        for (uint32_t j = 0; j < 16; j++) {
          uint32_t bit = crc & crchighbit;
          crc <<= 1;
          if (bit) 
            crc ^= m_poly;
        }			
        crc &= m_mask;
        m_lut[i] = crc; 
      }

      //compute missing initial CRC value
      uint32_t crc = m_bootstrap;
      for (uint32_t i = 0; i < m_order; i++) {
        uint32_t bit = crc & 1;
        if (bit) crc ^= m_poly;
        crc >>= 1;
        if (bit) crc |= crchighbit;
      }
    } 

    /**
     * This will update the 16-bit CRC check-sum for a datablock that is
     * 32-bit wide.
     */
    uint32_t checksum(uint32_t boot, const uint32_t* data, uint32_t size) {
      uint32_t retval = checksum16(data, size, true, (boot>>16)&0xffff);
      retval <<= 16;
      retval |= checksum16(data, size, false, boot&0xffff);
      return retval;
    }
    
    /**
     * This will calculate the 16-bit CRC check-sum for a datablock that is
     * 32-bit wide.
     */
    uint32_t checksum(const uint32_t* data, uint32_t size) {
      uint32_t boot = m_bootstrap;
      boot <<= 16;
      boot |= m_bootstrap;
      return this->checksum(boot, data, size);
    }
  
};

static crc16_t global_crc16_ccitt;

uint32_t eformat::helper::crc16_ccitt(const uint32_t* data, uint32_t size)
{
  return global_crc16_ccitt.checksum(data, size);
}
  
uint32_t eformat::helper::crc16_ccitt(uint32_t init, 
    const uint32_t* data, uint32_t size)
{
  return global_crc16_ccitt.checksum(init, data, size);
}

uint32_t eformat::helper::checksum(uint32_t type, uint32_t init, 
        const uint32_t* data, uint32_t size)
{
  switch ((eformat::CheckSum)type) {
    case eformat::ADLER32:
      return eformat::helper::adler32(init, data, size);
    case eformat::CRC16_CCITT:
      return crc16_ccitt(init, data, size);
    default:
      break;
  }
  return 0;
}

uint32_t eformat::helper::checksum(uint32_t type,
        const uint32_t* data, uint32_t size)
{
  switch ((eformat::CheckSum)type) {
    case eformat::ADLER32:
      return eformat::helper::adler32(data, size);
    case eformat::CRC16_CCITT:
      return crc16_ccitt(data, size);
    default:
      break;
  }
  return 0;
}

static eformat::helper::EnumClass<eformat::CheckSum> 
  make_CheckSum_dictionary()
{
  eformat::helper::EnumClass<eformat::CheckSum> d(0xf, "UNKNOWN_CHECKSUM_TYPE");
  d.add(eformat::NO_CHECKSUM, "NO_CHECKSUM");
  d.add(eformat::CRC16_CCITT, "CRC16_CCITT");
  d.add(eformat::ADLER32, "ADLER32");
  return d;
}

const eformat::helper::EnumClass<eformat::CheckSum> 
  eformat::helper::CheckSumDictionary = make_CheckSum_dictionary();
