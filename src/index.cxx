/**
 * @file src/index.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Implements eformat indexing capabilities
 */

#include "eformat/index.h"

void eformat::helper::build_toc 
(const eformat::read::FullEventFragment& event,
 std::map<uint32_t, const uint32_t*>& index, bool consider_optional) 
{
  eformat::read::FullEventFragment::child_iter_t iter = event.child_iter();
  while (const uint32_t* tmp = iter.next()) {
    eformat::read::ROBFragment rob(tmp);
    if (consider_optional) index[rob.source_id()] = tmp;
    else {
      eformat::helper::SourceIdentifier s(rob.source_id());
      index[s.simple_code()] = tmp;
    }
  }
}

void eformat::helper::build_toc 
(const eformat::read::FullEventFragment& event,
 std::map<eformat::SubDetector, std::vector<const uint32_t*> >& index)
{
  std::map<uint32_t, const uint32_t*> tmp;
  eformat::helper::build_toc(event, tmp);
  for (std::map<uint32_t, const uint32_t*>::const_iterator
        it = tmp.begin(); it != tmp.end(); ++it) {
    eformat::helper::SourceIdentifier s(it->first);
    std::map<eformat::SubDetector, std::vector<const uint32_t*> >::iterator 
      entry = index.find(s.subdetector_id());
    if (entry != index.end()) //add
      entry->second.push_back(it->second);
    else //new entry
      index[s.subdetector_id()] = std::vector<const uint32_t*>(1, it->second);
  }
}

void eformat::helper::build_toc 
(const eformat::read::FullEventFragment& event,
 std::map<eformat::SubDetectorGroup, std::vector<const uint32_t*> >& index)
{
  std::map<uint32_t, const uint32_t*> tmp;
  eformat::helper::build_toc(event, tmp);
  for (std::map<uint32_t, const uint32_t*>::const_iterator
        it = tmp.begin(); it != tmp.end(); ++it) {
    eformat::helper::SourceIdentifier s(it->first);
    std::map<eformat::SubDetectorGroup, std::vector<const uint32_t*> >::iterator entry = index.find(s.subdetector_group());
    if (entry != index.end()) //add
      entry->second.push_back(it->second);
    else //new entry
      index[s.subdetector_group()] = std::vector<const uint32_t*>(1, it->second);
  }
}

void eformat::helper::count_robs 
(const eformat::read::FullEventFragment& event,
 std::map<eformat::SubDetector, uint32_t>& index)
{
  std::map<eformat::SubDetector, std::vector<const uint32_t*> > tmp;
  eformat::helper::build_toc(event, tmp);
  for (std::map<eformat::SubDetector, std::vector<const uint32_t*> >::const_iterator it = tmp.begin(); it != tmp.end(); ++it) 
    index[it->first] = it->second.size();
}

void eformat::helper::count_robs 
(const eformat::read::FullEventFragment& event,
 std::map<eformat::SubDetectorGroup, uint32_t>& index)
{
  std::map<eformat::SubDetectorGroup, std::vector<const uint32_t*> > tmp;
  eformat::helper::build_toc(event, tmp);
  for (std::map<eformat::SubDetectorGroup, std::vector<const uint32_t*> >::const_iterator it = tmp.begin(); it != tmp.end(); ++it) 
    index[it->first] = it->second.size();
}

const uint32_t* eformat::helper::find_rob 
 (const eformat::read::FullEventFragment& event,
  uint32_t source_id, bool consider_optional)
{
  if (not consider_optional) {
    eformat::helper::SourceIdentifier s1(source_id);
    source_id = s1.simple_code();
  }

  eformat::read::FullEventFragment::child_iter_t iter = event.child_iter();
  while (const uint32_t* tmp = iter.next()) {
    eformat::read::ROBFragment rob(tmp);
    uint32_t target = rob.source_id();
    if (not consider_optional) {
      eformat::helper::SourceIdentifier s2(rob.source_id());
      target = s2.simple_code();
    }
    if (source_id == target) return tmp;
  }
  return 0;
}

