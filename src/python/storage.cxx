/**
 * @file python/storage.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Bindings to the EventStorage for reading and writing data
 */

#include <boost/python.hpp>
#include <stdexcept>
#include "ers/ers.h"
#include "eformat/python/storage.h"
#include "eformat/FullEventFragment.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/python/storage.h"
#include "eformat/python/util.h"
#include "eformat/python/container.h"
#include "EventStorage/DataWriter.h"
#include "EventStorage/DataReader.h"
#include "EventStorage/pickDataReader.h"
#include "eformat/DetectorMask.h"

using namespace boost::python;

//Convert 128bit into a python long integer
boost::python::long_
mask_python_coverter(const EventStorage::DataReader& dr)
{
  std::bitset<128> mask = dr.detectorMask();
  eformat::helper::DetectorMask dm(mask);
  return boost::python::long_(dm.string(),16);
}


BOOST_PYTHON_MODULE(libpyevent_storage)
{
   //covers std::vector<std::string> conversion to python - to be used by
   //typedefs like freeMetaDataStrings, for instance.
   to_python_converter<std::vector<std::string>, 
     eformat::python::string_vector_to_list>();

   //make sure our methods can convert a python list into a
   //std::vector<std::string> implicitly
   eformat::python::from_python_sequence<std::vector<std::string>, 
     eformat::python::variable_capacity_policy>();

   //exception handler
   register_exception_translator<std::range_error>(&eformat::python::translate_range_error);
   register_exception_translator<ers::Issue>(&eformat::python::translate_ers_issue);

   //A helper class to interpret filenames
   class_<daq::RawFileName> ("RawFileName", no_init)
     .def(init<std::string>())
     .def(init<std::string, unsigned int, std::string, 
          std::string, unsigned int, std::string, 
          optional<std::string, std::string, unsigned int, std::string> >())
     .def(init<std::string, unsigned int, optional<std::string> >())
     .def("buildFileName", &daq::RawFileName::buildFileName)
     .def("buildFileNameCore", &daq::RawFileName::buildFileNameCore)
     .def("buildFileNameTrailers", &daq::RawFileName::buildFileNameTrailers)
     .def("interpretFileName", &daq::RawFileName::interpretFileName)
     .def("print", &daq::RawFileName::print)
     
     .def("project", &daq::RawFileName::project)
     .def("runNumber", &daq::RawFileName::runNumber)
     .def("streamType", &daq::RawFileName::streamType)
     .def("streamName", &daq::RawFileName::streamName)
     .def("stream", &daq::RawFileName::stream)
     .def("lumiBlockNumber", &daq::RawFileName::lumiBlockNumber)
     .def("applicationName", &daq::RawFileName::applicationName)
     .def("productionStep", &daq::RawFileName::productionStep)
     .def("dataType", &daq::RawFileName::dataType)
     .def("fileSequenceNumber", &daq::RawFileName::fileSequenceNumber)
     .def("extension", &daq::RawFileName::extension)
     .def("fileNameCore", &daq::RawFileName::fileNameCore)
     .def("fileName", &daq::RawFileName::fileName)
     .def("hasValidCore", &daq::RawFileName::hasValidCore)
     .def("hasValidTrailer", &daq::RawFileName::hasValidTrailer)
     .def("datasetName", &daq::RawFileName::datasetName)

     .def("setDefaults", &daq::RawFileName::setDefaults)
     .def("setTrailers", &daq::RawFileName::setTailers)
     .def("setFileSequenceNumber", &daq::RawFileName::setFileSequenceNumber)
     .def("setExtension", &daq::RawFileName::setExtension);

   //The data reader itself (abstract class)
   class_<EventStorage::DataReader, 
     boost::shared_ptr<EventStorage::DataReader>, 
     boost::noncopyable> ("DataReader", no_init)
     .def("enableSequenceReading", 
         &EventStorage::DataReader::enableSequenceReading)
     .def("good", &EventStorage::DataReader::good)
     .def("fileName", &EventStorage::DataReader::fileName)
     .def("fileStartDate", &EventStorage::DataReader::fileStartDate)
     .def("fileStartTime", &EventStorage::DataReader::fileStartTime)
     .def("fileSizeLimitInDataBlocks", 
         &EventStorage::DataReader::fileSizeLimitInDataBlocks)
     .def("fileSizeLimitInMB", &EventStorage::DataReader::fileSizeLimitInMB)
     .def("appName", &EventStorage::DataReader::appName)
     .def("fileNameCore", &EventStorage::DataReader::fileNameCore)
     .def("runNumber", &EventStorage::DataReader::runNumber)
     .def("maxEvents", &EventStorage::DataReader::maxEvents)
     .def("recEnable", &EventStorage::DataReader::recEnable)
     .def("triggerType", &EventStorage::DataReader::triggerType)
     .def("detectorMask", &mask_python_coverter)
     .def("beamType", &EventStorage::DataReader::beamType)
     .def("beamEnergy", &EventStorage::DataReader::beamEnergy)
     .def("freeMetaDataStrings", 
         &EventStorage::DataReader::freeMetaDataStrings)
     .def("compression",&EventStorage::DataReader::compression)
     .def("GUID", &EventStorage::DataReader::GUID)
     .def("fileEndDate", &EventStorage::DataReader::fileEndDate)
     .def("fileEndTime", &EventStorage::DataReader::fileEndTime)
     .def("eventsInFile", &EventStorage::DataReader::eventsInFile)
     .def("dataMB_InFile", &EventStorage::DataReader::dataMB_InFile)
     .def("eventsInFileSequence", 
         &EventStorage::DataReader::eventsInFileSequence)
     .def("dataMB_InFileSequence", 
         &EventStorage::DataReader::dataMB_InFileSequence)
     .def("getData", &eformat::python::read_u32list)
     .def("getPosition", &EventStorage::DataReader::getPosition)
     .def("lumiblockNumber", &EventStorage::DataReader::lumiblockNumber)
     .def("stream",  &EventStorage::DataReader::stream)
     .def("projectTag", &EventStorage::DataReader::projectTag)
     ;

  //allows us to get the data reader
  def("pickDataReader", &eformat::python::make_reader_simple);

  //Auxiliary stuff for the data writer
  enum_<EventStorage::DWError>("DWError")
    .value("DWOK", EventStorage::DWOK)
    .value("DWNOOK", EventStorage::DWNOOK)
    ;

  enum_<EventStorage::FileStatus>("FileStatus")
    .value("FINISHED", EventStorage::FINISHED)
    .value("UNFINISHED", EventStorage::UNFINISHED)
    ;

  class_<EventStorage::run_parameters_record>("run_parameters_record")
    .def_readwrite("marker", 
        &EventStorage::run_parameters_record::marker)
    .def_readwrite("record_size", 
        &EventStorage::run_parameters_record::record_size)
    .def_readwrite("run_number", 
        &EventStorage::run_parameters_record::run_number)
    .def_readwrite("max_events",
        &EventStorage::run_parameters_record::max_events)
    .def_readwrite("rec_enable",
        &EventStorage::run_parameters_record::rec_enable)
    .def_readwrite("trigger_type",
        &EventStorage::run_parameters_record::trigger_type)
    .def_readwrite("detector_mask_LS",
        &EventStorage::run_parameters_record::detector_mask_LS)
    .def_readwrite("detector_mask_MS",
	&EventStorage::run_parameters_record::detector_mask_MS)
    .def_readwrite("beam_type", 
        &EventStorage::run_parameters_record::beam_type)
    .def_readwrite("beam_energy", 
        &EventStorage::run_parameters_record::beam_energy)
    ;

  //The data writer is easier to wrap (no abstract classes)
  class_<EventStorage::DataWriter, boost::noncopyable>("DataWriter", 
      init<const std::string, 
	   const std::string,
           const EventStorage::run_parameters_record,
           const EventStorage::freeMetaDataStrings, unsigned int,
           const EventStorage::CompressionType,
	   const unsigned int>())
    .def("setMaxFileNE",
        &EventStorage::DataWriter::setMaxFileNE)
    .def("setMaxFileMB",
        &EventStorage::DataWriter::setMaxFileMB)
    .def("good", 
        &EventStorage::DataWriter::good)
    .def("cd", 
        &EventStorage::DataWriter::cd)
    .def("inTransition",
        &EventStorage::DataWriter::inTransition)
    .def("putData", &eformat::python::write_blob<eformat::helper::u32list>)
    .def("putData", &eformat::python::write_blob<eformat::helper::u32slice>)
    .def("putData", &eformat::python::write_readonly<eformat::read::FullEventFragment>)
    .def("putData", &eformat::python::write_readwrite<eformat::write::FullEventFragment>)
    .def("closeFile",
        &EventStorage::DataWriter::closeFile)
    .def("nextFile",
        &EventStorage::DataWriter::nextFile)
    .def("nameFile",
        &EventStorage::DataWriter::nameFile)
    .def("getPosition",
        &EventStorage::DataWriter::getPosition)
    .def("eventsInFile", 
        &EventStorage::DataWriter::eventsInFile)
    .def("eventsInFileSequence", 
        &EventStorage::DataWriter::eventsInFileSequence)
    .def("dataMB_InFile", 
        &EventStorage::DataWriter::dataMB_InFile)
    .def("dataMB_InFileSequence", 
        &EventStorage::DataWriter::dataMB_InFileSequence)
    .def("setGuid", 
        &EventStorage::DataWriter::setGuid)
    ;
  
  enum_<EventStorage::CompressionType>("CompressionType")
    .value("NONE", EventStorage::NONE)
    .value("RESERVED", EventStorage::RESERVED)
    .value("UNKNOWN", EventStorage::UNKNOWN)
    .value("ZLIB", EventStorage::ZLIB)
    ;

}
