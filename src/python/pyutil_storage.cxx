/**
 * @file src/python/util_storage.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Utilities for the EventStorage python bindings
 */

#include "eformat/python/storage.h"
#include "ers/ers.h"
#include "EventStorage/pickDataReader.h"

//wrapping function to handle the EventStorage error codes
eformat::helper::u32list eformat::python::read_u32list
  (EventStorage::DataReader& dr) 
{
  char* data;
  unsigned int size = 0;
  int error_code = dr.getData(size, &data);
  //copied from Szymons readData program:
  while (error_code == DRWAIT) {
    usleep(500000);
    ERS_INFO("[EventStorage] Waiting for more data.");
    error_code = dr.getData(size, &data);
  }
  if (error_code == DRNOOK) {
    ERS_INFO("[EventStorage] Reading of data NOT OK!");
  }
  if (error_code == DROK) {
    ERS_DEBUG(1, "[EventStorage] Event OK");
  }
  return eformat::helper::u32list(reinterpret_cast<uint32_t*>(data), 0, 
      size/sizeof(uint32_t));
}

boost::shared_ptr<EventStorage::DataReader>
eformat::python::make_reader_simple(const std::string& s)
{
  return boost::shared_ptr<EventStorage::DataReader>(pickDataReader(s));
}

