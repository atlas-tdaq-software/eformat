/**
 * @file python/helper.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Python bindings to types and enums in the helper namespace
 */

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "eformat/SourceIdentifier.h"
#include "eformat/Status.h"
#include "eformat/Version.h"
#include "eformat/HeaderMarker.h"
#include "eformat/RunType.h"
#include "eformat/StreamTag.h"
#include "eformat/Problem.h"
#include "eformat/checksum.h"
#include "eformat/DetectorMask.h"
#include "eformat/python/util.h"
#include "eformat/compression.h"

using namespace boost::python;

//Convert 128bit into a python long integer
boost::python::long_
mask_python_coverter(const eformat::helper::DetectorMask& mask)
{
  return boost::python::long_(mask.string(),16);
}


BOOST_PYTHON_MODULE(libpyeformat_helper)
{
  //Fragment problems
  enum_<eformat::FragmentProblem>("FragmentProblem")
    .value("NO_PROBLEM", eformat::NO_PROBLEM)
    .value("WRONG_MARKER", eformat::WRONG_MARKER)
    .value("WRONG_ROD_MARKER", eformat::WRONG_ROD_MARKER)
    .value("UNSUPPORTED_VERSION", eformat::UNSUPPORTED_VERSION)
    .value("WRONG_FRAGMENT_SIZE", eformat::WRONG_FRAGMENT_SIZE)
    .value("UNSUPPORTED_ROD_VERSION", eformat::UNSUPPORTED_ROD_VERSION)
    .value("WRONG_ROD_HEADER_SIZE", eformat::WRONG_ROD_HEADER_SIZE)
    .value("WRONG_ROD_FRAGMENT_SIZE", eformat::WRONG_ROD_FRAGMENT_SIZE)
    ;

  //Source identifier
  enum_<eformat::SubDetectorGroup>("SubDetectorGroup")
    .value("ANY_DETECTOR", eformat::ANY_DETECTOR)
    .value("PIXEL", eformat::PIXEL)
    .value("SCT", eformat::SCT)
    .value("TRT", eformat::TRT)
    .value("LAR", eformat::LAR)
    .value("TILECAL", eformat::TILECAL)
    .value("MUON", eformat::MUON)
    .value("TDAQ", eformat::TDAQ)
    .value("FORWARD", eformat::FORWARD)
    .value("OTHER_DETECTORS", eformat::OTHER_DETECTORS)
    ;

  enum_<eformat::SubDetector>("SubDetector")
    .value("FULL_SD_EVENT", eformat::FULL_SD_EVENT)
    .value("OFFLINE", eformat::OFFLINE)
    .value("PIXEL_BARREL", eformat::PIXEL_BARREL)
    .value("PIXEL_DISK_SIDE", eformat::PIXEL_DISK)
    .value("PIXEL_B_LAYER", eformat::PIXEL_B_LAYER)
    .value("PIXEL_IBL", eformat::PIXEL_IBL)
    .value("PIXEL_DBM", eformat::PIXEL_DBM)
    .value("SCT_BARREL_A_SIDE", eformat::SCT_BARREL_A_SIDE)
    .value("SCT_BARREL_C_SIDE", eformat::SCT_BARREL_C_SIDE)
    .value("SCT_ENDCAP_A_SIDE", eformat::SCT_ENDCAP_A_SIDE)
    .value("SCT_ENDCAP_C_SIDE", eformat::SCT_ENDCAP_C_SIDE)
    .value("TRT_ANCILLARY_CRATE", eformat::TRT_ANCILLARY_CRATE)
    .value("TRT_BARREL_A_SIDE", eformat::TRT_BARREL_A_SIDE)
    .value("TRT_BARREL_C_SIDE", eformat::TRT_BARREL_C_SIDE)
    .value("TRT_ENDCAP_A_SIDE", eformat::TRT_ENDCAP_A_SIDE)
    .value("TRT_ENDCAP_C_SIDE", eformat::TRT_ENDCAP_C_SIDE)
    .value("LAR_EM_BARREL_A_SIDE", eformat::LAR_EM_BARREL_A_SIDE)
    .value("LAR_EM_BARREL_C_SIDE", eformat::LAR_EM_BARREL_C_SIDE)
    .value("LAR_EM_ENDCAP_A_SIDE", eformat::LAR_EM_ENDCAP_A_SIDE)
    .value("LAR_EM_ENDCAP_C_SIDE", eformat::LAR_EM_ENDCAP_C_SIDE)
    .value("LAR_HAD_ENDCAP_A_SIDE", eformat::LAR_HAD_ENDCAP_A_SIDE)
    .value("LAR_HAD_ENDCAP_C_SIDE", eformat::LAR_HAD_ENDCAP_C_SIDE)
    .value("LAR_FCAL_A_SIDE", eformat::LAR_FCAL_A_SIDE)
    .value("LAR_FCAL_C_SIDE", eformat::LAR_FCAL_C_SIDE)
    .value("LAR_EM_BARREL_ENDCAP_A_SIDE", eformat::LAR_EM_BARREL_ENDCAP_A_SIDE)
    .value("LAR_EM_BARREL_ENDCAP_C_SIDE", eformat::LAR_EM_BARREL_ENDCAP_C_SIDE)
    .value("LAR_EM_HAD_ENDCAP_A_SIDE", eformat::LAR_EM_HAD_ENDCAP_A_SIDE)
    .value("LAR_EM_HAD_ENDCAP_C_SIDE", eformat::LAR_EM_HAD_ENDCAP_C_SIDE)
    .value("TILECAL_LASER_CRATE", eformat::TILECAL_LASER_CRATE)
    .value("TILECAL_BARREL_A_SIDE", eformat::TILECAL_BARREL_A_SIDE)
    .value("TILECAL_BARREL_C_SIDE", eformat::TILECAL_BARREL_C_SIDE)
    .value("TILECAL_EXT_A_SIDE", eformat::TILECAL_EXT_A_SIDE)
    .value("TILECAL_EXT_C_SIDE", eformat::TILECAL_EXT_C_SIDE)
    .value("MUON_ANCILLARY_CRATE", eformat::MUON_ANCILLARY_CRATE)
    .value("MUON_MDT_BARREL_A_SIDE", eformat::MUON_MDT_BARREL_A_SIDE)
    .value("MUON_MDT_BARREL_C_SIDE", eformat::MUON_MDT_BARREL_C_SIDE)
    .value("MUON_MDT_ENDCAP_A_SIDE", eformat::MUON_MDT_ENDCAP_A_SIDE)
    .value("MUON_MDT_ENDCAP_C_SIDE", eformat::MUON_MDT_ENDCAP_C_SIDE)
    .value("MUON_RPC_BARREL_A_SIDE", eformat::MUON_RPC_BARREL_A_SIDE)
    .value("MUON_RPC_BARREL_C_SIDE", eformat::MUON_RPC_BARREL_C_SIDE)
    .value("MUON_TGC_ENDCAP_A_SIDE", eformat::MUON_TGC_ENDCAP_A_SIDE)
    .value("MUON_TGC_ENDCAP_C_SIDE", eformat::MUON_TGC_ENDCAP_C_SIDE)
    .value("MUON_CSC_ENDCAP_A_SIDE", eformat::MUON_CSC_ENDCAP_A_SIDE)
    .value("MUON_CSC_ENDCAP_C_SIDE", eformat::MUON_CSC_ENDCAP_C_SIDE)
    .value("MUON_MMEGA_ENDCAP_A_SIDE", eformat::MUON_MMEGA_ENDCAP_A_SIDE)
    .value("MUON_MMEGA_ENDCAP_C_SIDE", eformat::MUON_MMEGA_ENDCAP_C_SIDE)
    .value("MUON_STGC_ENDCAP_A_SIDE", eformat::MUON_STGC_ENDCAP_A_SIDE)
    .value("MUON_STGC_ENDCAP_C_SIDE", eformat::MUON_STGC_ENDCAP_C_SIDE)
    .value("TDAQ_BEAM_CRATE", eformat::TDAQ_BEAM_CRATE)
    .value("TDAQ_CALO_PREPROC", eformat::TDAQ_CALO_PREPROC)
    .value("TDAQ_CALO_CLUSTER_PROC_DAQ", eformat::TDAQ_CALO_CLUSTER_PROC_DAQ)
    .value("TDAQ_CALO_CLUSTER_PROC_ROI", eformat::TDAQ_CALO_CLUSTER_PROC_ROI)
    .value("TDAQ_CALO_JET_PROC_DAQ", eformat::TDAQ_CALO_JET_PROC_DAQ)
    .value("TDAQ_CALO_JET_PROC_ROI", eformat::TDAQ_CALO_JET_PROC_ROI)
    .value("TDAQ_MUON_CTP_INTERFACE", eformat::TDAQ_MUON_CTP_INTERFACE)
    .value("TDAQ_CTP", eformat::TDAQ_CTP)
    .value("TDAQ_L2SV", eformat::TDAQ_L2SV)
    .value("TDAQ_SFI", eformat::TDAQ_SFI)
    .value("TDAQ_SFO", eformat::TDAQ_SFO)
    .value("TDAQ_LVL2", eformat::TDAQ_LVL2)
    .value("TDAQ_EVENT_FILTER", eformat::TDAQ_EVENT_FILTER)
    .value("TDAQ_HLT", eformat::TDAQ_HLT)
    .value("TDAQ_LAR_MET", eformat::TDAQ_LAR_MET)
    .value("TDAQ_TILE_MET", eformat::TDAQ_TILE_MET)
    .value("TDAQ_FTK", eformat::TDAQ_FTK)
    .value("FORWARD_BCM", eformat::FORWARD_BCM)
    .value("FORWARD_LUCID", eformat::FORWARD_LUCID)
    .value("FORWARD_ZDC", eformat::FORWARD_ZDC)
    .value("FORWARD_ALPHA", eformat::FORWARD_ALPHA)
    .value("FORWARD_AFP", eformat::FORWARD_AFP)
    .value("TDAQ_CALO_TOPO_PROC", eformat::TDAQ_CALO_TOPO_PROC)
    .value("TDAQ_CALO_DIGITAL_PROC", eformat::TDAQ_CALO_DIGITAL_PROC)
    .value("TDAQ_CALO_FEAT_EXTRACT_DAQ", eformat::TDAQ_CALO_FEAT_EXTRACT_DAQ)
    .value("TDAQ_CALO_FEAT_EXTRACT_ROI", eformat::TDAQ_CALO_FEAT_EXTRACT_ROI)
    .value("OTHER", eformat::OTHER)
    ;
  
  class_<eformat::helper::SourceIdentifier>("SourceIdentifier",  
                                            init<uint32_t>())
    .def(init<eformat::SubDetector, uint16_t, optional<uint8_t> >())
    .def("subdetector_id", &eformat::helper::SourceIdentifier::subdetector_id)
    .def("subdetector_group", &eformat::helper::SourceIdentifier::subdetector_group)
    .def("module_id", &eformat::helper::SourceIdentifier::module_id)
    .def("optional_field", &eformat::helper::SourceIdentifier::optional_field)
    .def("code", &eformat::helper::SourceIdentifier::code)
    .def("simple_code", &eformat::helper::SourceIdentifier::simple_code)
    .def("__long__", &eformat::helper::SourceIdentifier::code)
    .def("__int__", &eformat::helper::SourceIdentifier::code)
    .def("__hash__", &eformat::helper::SourceIdentifier::code)
    .def("human", &eformat::helper::SourceIdentifier::human)
    .def("human_detector", &eformat::helper::SourceIdentifier::human_detector)
    .def("human_group", &eformat::helper::SourceIdentifier::human_group)
    ;

  //Status
  enum_<eformat::GenericStatus>("GenericStatus")
    .value("UNCLASSIFIED", eformat::UNCLASSIFIED)
    .value("BCID_CHECK_FAIL", eformat::BCID_CHECK_FAIL)
    .value("LVL1ID_CHECK_FAIL", eformat::LVL1ID_CHECK_FAIL)
    .value("TIMEOUT", eformat::TIMEOUT)
    .value("DATA_CORRUPTION", eformat::DATA_CORRUPTION)
    .value("INTERNAL_OVERFLOW", eformat::INTERNAL_OVERFLOW)
    .value("DUMMY_FRAGMENT", eformat::DUMMY_FRAGMENT)
    ;

  enum_<eformat::RobinStatus>("RobinStatus")
    .value("TRIGGER_TYPE_SYNC_ERROR", eformat::TRIGGER_TYPE_SYNC_ERROR)
    .value("DATAFLOW_DUMMY", eformat::DATAFLOW_DUMMY)
    .value("FRAGMENT_SIZE_ERROR", eformat::FRAGMENT_SIZE_ERROR)
    .value("DATABLOCK_ERROR", eformat::DATABLOCK_ERROR)
    .value("CTRL_WORD_ERROR", eformat::CTRL_WORD_ERROR)
    .value("MISSING_BOF", eformat::MISSING_BOF)
    .value("MISSING_EOF", eformat::MISSING_EOF)
    .value("INVALID_HEADER_MARKER", eformat::INVALID_HEADER_MARKER)
    .value("FORMAT_ERROR", eformat::FORMAT_ERROR)
    .value("DUPLICATE_EVENT", eformat::DUPLICATE_EVENT)
    .value("SEQUENCE_ERROR", eformat::SEQUENCE_ERROR)
    .value("TRANSMISSION_ERROR", eformat::TRANSMISSION_ERROR)
    .value("TRUNCATION", eformat::TRUNCATION)
    .value("SHORT_FRAGMENT", eformat::SHORT_FRAGMENT)
    .value("FRAGMENT_LOST", eformat::FRAGMENT_LOST)
    .value("FRAGMENT_PENDING", eformat::FRAGMENT_PENDING)
    .value("ROBIN_DISCARD_MODE", eformat::ROBIN_DISCARD_MODE)
   ;
  
  enum_<eformat::DummyROBStatus>("DummyROBStatus")
    .value("INCOMPLETE_ROI", eformat::INCOMPLETE_ROI)
    .value("COLLECTION_TIMEOUT", eformat::COLLECTION_TIMEOUT)
   ;

  enum_<eformat::FullEventStatus>("FullEventStatus")
    .value("L2_PROCESSING_TIMEOUT", eformat::L2_PROCESSING_TIMEOUT)
    .value("L2PU_PROCESSING_TIMEOUT", eformat::L2PU_PROCESSING_TIMEOUT)
    .value("SFI_DUPLICATION_WARNING", eformat::SFI_DUPLICATION_WARNING)
    .value("DFM_DUPLICATION_WARNING", eformat::DFM_DUPLICATION_WARNING)
    .value("HLTSV_DUPLICATION_WARNING", eformat::HLTSV_DUPLICATION_WARNING)
    .value("L2PSC_PROBLEM", eformat::L2PSC_PROBLEM)
    .value("EF_PROCESSING_TIMEOUT", eformat::EF_PROCESSING_TIMEOUT)
    .value("DCM_PROCESSING_TIMEOUT", eformat::DCM_PROCESSING_TIMEOUT)
    .value("PT_PROCESSING_TIMEOUT", eformat::PT_PROCESSING_TIMEOUT)
    .value("HLTPU_PROCESSING_TIMEOUT", eformat::HLTPU_PROCESSING_TIMEOUT)
    .value("SFO_DUPLICATION_WARNING", eformat::SFO_DUPLICATION_WARNING)
    .value("DCM_DUPLICATION_WARNING", eformat::DCM_DUPLICATION_WARNING)
    .value("EFD_RECOVERED_EVENT", eformat::EFD_RECOVERED_EVENT)
    .value("DCM_RECOVERED_EVENT", eformat::DCM_RECOVERED_EVENT)
    .value("EFPSC_PROBLEM", eformat::EFPSC_PROBLEM)
    .value("PSC_PROBLEM", eformat::PSC_PROBLEM)
    .value("EFD_FORCED_ACCEPT", eformat::EFD_FORCED_ACCEPT)
    .value("DCM_FORCED_ACCEPT", eformat::DCM_FORCED_ACCEPT)
    .value("PARTIAL_EVENT", eformat::PARTIAL_EVENT)
    ;

  scope().attr("STATUS_FRONT") = eformat::STATUS_FRONT;
  scope().attr("STATUS_BACK") = eformat::STATUS_BACK;
  scope().attr("DEFAULT_STATUS") = eformat::DEFAULT_STATUS;
  
  class_<eformat::helper::Status>("Status", init<uint32_t>())
    .def(init<eformat::GenericStatus, uint16_t>())
    .def(init<eformat::GenericStatus, uint8_t, uint8_t>())
    .def("generic", &eformat::helper::Status::generic)
    .def("specific", &eformat::helper::Status::specific)
    .def("specific_msb", &eformat::helper::Status::specific_msb)
    .def("specific_lsb", &eformat::helper::Status::specific_lsb)
    .def("code", &eformat::helper::Status::code)
    .def("__long__", &eformat::helper::Status::code)
    .def("__int__", &eformat::helper::Status::code)
    .def("__hash__", &eformat::helper::Status::code)
    ;

  //Header markers (too big to fit into enum_<>, since it converts stuff
  //to signed ints...
  enum_<eformat::HeaderMarker>("HeaderMarker")
    .value("ROD", eformat::ROD)
    .value("ROB", eformat::ROB)
    .value("FULL_EVENT", eformat::FULL_EVENT)
    ;
    
  def("child_marker", &eformat::child_marker);
  def("marker2string", (std::string (*)(uint32_t))&eformat::marker2string);
  def("marker2string", (std::string (*)(const eformat::HeaderMarker&))&eformat::marker2string);

  //Version numbers
  scope().attr("DEFAULT_VERSION") = eformat::DEFAULT_VERSION;
  scope().attr("MAJOR_DEFAULT_VERSION") = eformat::MAJOR_DEFAULT_VERSION;
  scope().attr("MAJOR_V30_VERSION") = eformat::MAJOR_V30_VERSION;
  scope().attr("MAJOR_V24_VERSION") = eformat::MAJOR_V24_VERSION;

  class_<eformat::helper::Version>("Version", init<optional<uint32_t> >())
    //.def(init<uint16_t, optional<uint16_t> >()) //confuses
    .def("major_version", &eformat::helper::Version::major_version)
    .def("minor_version", &eformat::helper::Version::minor_version)
    .def("code", &eformat::helper::Version::code)
    .def("__long__", &eformat::helper::Version::code)
    .def("__hash__", &eformat::helper::Version::code)
    .def("__int__", &eformat::helper::Version::code)
    .def("human_major", &eformat::helper::Version::human_major)
    .def("human", &eformat::helper::Version::human)
    ;

  //Run type
  enum_<eformat::RunType>("RunType")
    .value("PHYSICS", eformat::PHYSICS)
    .value("CALIBRATION", eformat::CALIBRATION)
    .value("COSMICS", eformat::COSMICS)
    .value("TEST", eformat::TEST)
    ;

  def("run_type2string", (std::string (*)(const eformat::RunType&))&eformat::run_type2string);
  def("run_type2string", (std::string (*)(uint32_t))&eformat::run_type2string);

  //Run type
  enum_<eformat::TagType>("TagType")
    .value("PHYSICS_TAG", eformat::PHYSICS_TAG)
    .value("CALIBRATION_TAG", eformat::CALIBRATION_TAG)
    .value("RESERVED_TAG", eformat::RESERVED_TAG)
    .value("DEBUG_TAG", eformat::DEBUG_TAG)
    .value("UNKNOWN_TAG", eformat::UNKNOWN_TAG)
    .value("EXPRESS_TAG", eformat::EXPRESS_TAG)
    .value("MONITORING_TAG", eformat::MONITORING_TAG)
    ;


  //So we can return std::set
  eformat::python::set_wrapper<uint32_t>::wrap("u32_set");
  eformat::python::set_wrapper<eformat::SubDetector>::wrap("SubDetector_set");


  //Stream tags
  class_<eformat::helper::StreamTag>("StreamTag")
    .def(init<const std::string&, const eformat::TagType&, bool, 
	 optional<const std::set<uint32_t>&,
	 const std::set<eformat::SubDetector>& > >())
    .def(init<const std::string&, const std::string&, bool,
	 optional<const std::set<uint32_t>&,
	 const std::set<eformat::SubDetector>& > >())
    .def_readwrite("name", &eformat::helper::StreamTag::name)
    .def_readwrite("type", &eformat::helper::StreamTag::type)
    .def_readwrite("obeys_lumiblock", &eformat::helper::StreamTag::obeys_lumiblock)
    .def_readwrite("robs", &eformat::helper::StreamTag::robs)
    .def_readwrite("dets", &eformat::helper::StreamTag::dets)
    .def(self == self)
    .def(self != self)
    .def(self < self)
    ;

  def("tagtype_to_string", &eformat::helper::tagtype_to_string);
  def("string_to_tagtype", &eformat::helper::string_to_tagtype);

  //so we can return a pythonic version of stream tag vectors
  class_<std::vector<eformat::helper::StreamTag> >("StreamTags")
    .def(vector_indexing_suite<std::vector<eformat::helper::StreamTag> >())
    .def("__raw__", &eformat::python::stream_tag_encode)
    .def("__fromraw__", &eformat::python::stream_tag_decode)
    .def("contains", &eformat::helper::contains_type)
    ;

  //Check sum
  enum_<eformat::CheckSum>("CheckSum")
    .value("NO_CHECKSUM", eformat::NO_CHECKSUM)
    .value("ADLER32", eformat::ADLER32)
    .value("CRC16_CCITT", eformat::CRC16_CCITT)
    ;

  enum_<eformat::Compression>("Compression")
    .value("UNCOMPRESSED", eformat::UNCOMPRESSED)
    .value("ZLIB", eformat::ZLIB)
    .value("RESERVED", eformat::RESERVED)
    ;

  def("checksum", (uint32_t (*)(const eformat::CheckSum&, 
    const eformat::helper::u32list&))&eformat::python::checksum);
  def("checksum", (uint32_t (*)(const eformat::CheckSum&, 
    const eformat::helper::u32slice&))&eformat::python::checksum);
  def("checksum", (uint32_t (*)(const eformat::CheckSum&, 
    const boost::python::list&))&eformat::python::checksum);

  //Detector masks
  class_<eformat::helper::DetectorMask>("DetectorMask")
    .def(init<>())
    .def(init<uint64_t>())
    .def(init<uint64_t, uint64_t>())
    .def(init<std::string>())
    .def(init<eformat::helper::DetectorMask>())
    .def("__init__", make_constructor(&eformat::python::detmask_from_list))
    .def("mask", &mask_python_coverter)
    .def("sub_detectors", &eformat::python::detmask_decode)
    .def("set", (void (eformat::helper::DetectorMask::*)(eformat::SubDetector))&eformat::helper::DetectorMask::set)
    .def("set", &eformat::python::detmask_set_list)
    .def("unset", (void (eformat::helper::DetectorMask::*)(eformat::SubDetector))&eformat::helper::DetectorMask::unset)
    .def("unset", &eformat::python::detmask_unset_list)
    .def("reset", &eformat::helper::DetectorMask::reset) 
    .def("is_set", &eformat::helper::DetectorMask::is_set)
    .def("string", &eformat::helper::DetectorMask::string)
    ;
}
