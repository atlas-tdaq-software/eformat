/**
 * @file pyutil.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Generic eformat-python utilities implementation
 */

#include "eformat/python/util.h"
#include "eformat/old/util.h"
#include "eformat/index.h"
#include "eformat/Problem.h"
#include <boost/format.hpp>
#include <sstream>

PyObject* eformat::python::string_vector_to_list::convert
  (const std::vector<std::string>& v) 
{
  boost::python::list l;
  for (unsigned i=0; i<v.size(); ++i) l.append(boost::python::str(v[i]));
  PyObject* retval = l.ptr();
  Py_INCREF(retval);
  return retval;
}

void eformat::python::translate_range_error(std::range_error const& e) 
{
  PyErr_SetString(PyExc_IndexError, e.what());
}

/** 
 * Does the dirty work of contatenating exceptions
 */
static std::string make_ers_message(ers::Issue const& e) 
{
  const ers::Context& curr = e.context();
  std::ostringstream oss;
  oss << "[" << curr.package_name() << "]" 
      << curr.function_name() << "@" << curr.file_name() << "+"
      << curr.line_number() << ": " << e.message();
  if (e.cause()) oss << " caused by " << make_ers_message(*e.cause());
  return oss.str();
}

void eformat::python::translate_ers_issue(ers::Issue const& e) 
{
  PyErr_SetString(PyExc_RuntimeError, make_ers_message(e).c_str());
}
 
eformat::helper::u32list eformat::python::stream_tag_encode
  (const std::vector<eformat::helper::StreamTag>& v) 
{
  uint32_t size = eformat::helper::size_word(v);
  uint32_t* blob = new uint32_t[size];
  eformat::helper::encode(v, size, blob);
  return eformat::helper::u32list(blob, 0, size);
}

void eformat::python::stream_tag_decode
(std::vector<eformat::helper::StreamTag>& v, 
 const eformat::helper::u32list& blob) 
{
  v.clear();
  eformat::helper::decode(blob.length(), blob.ptr(), v);
}

boost::shared_ptr<eformat::helper::u32list> eformat::python::from_py_list
  (const boost::python::list& l) 
{
  uint32_t* data = new uint32_t[PyList_Size(l.ptr())];
  size_t length = 0;
  for (long i=0; i<PyList_Size(l.ptr()); ++i) { 
    PyObject* item = PyList_GetItem(l.ptr(), i);
#if PY_MAJOR_VERSION > 2
    if (PyLong_Check(item)) {
#else
    if (PyInt_Check(item) || PyLong_Check(item)) {
#endif // PY_MAJOR_VERSION > 2
      data[i] = PyLong_AsUnsignedLongMask(item);
      ++length;
    }
  }
  return boost::shared_ptr<eformat::helper::u32list>
    (new eformat::helper::u32list(data, 0, length));
}

eformat::helper::u32slice eformat::python::getslice
  (const eformat::helper::u32list& l, const boost::python::slice& pos)
{
  boost::python::slice::range<const uint32_t*> bounds;
  const uint32_t* p = l.ptr() + l.start();
  try {
    bounds = pos.get_indices<>(p, p+l.length());
  }
  catch (std::invalid_argument& ) {
    return eformat::helper::u32slice(p, 0);
  }
  return eformat::helper::u32slice(bounds.start, bounds.stop-bounds.start+1);
}

boost::python::list eformat::python::copy(const eformat::helper::u32list& orig, 
    const boost::python::slice& pos) 
{
  eformat::helper::u32slice l(getslice(orig, pos));
  boost::python::list retval;
  for (unsigned i=0; i<l.length(); ++i) 
    retval.append(boost::python::long_(l.getitem(i)));
  return retval;
}

eformat::helper::u32slice eformat::python::getslice
  (const eformat::helper::u32slice& s, const boost::python::slice& pos) 
{
  boost::python::slice::range<const uint32_t*> bounds;
  try {
    bounds = pos.get_indices<>(s.ptr(), s.ptr()+s.length());
  }
  catch (std::invalid_argument& ) {
    return eformat::helper::u32slice(s.ptr(), 0);
  }
  return eformat::helper::u32slice
    (bounds.start, bounds.stop-bounds.start+1);
}

boost::python::list eformat::python::copy(const eformat::helper::u32slice& s,
    const boost::python::slice& pos)
{
  eformat::helper::u32slice l(getslice(s, pos));
  boost::python::list retval;
  for (unsigned i=0; i<l.length(); ++i)
    retval.append(boost::python::long_(l.getitem(i)));
  return retval;
}

//This one we cannot make work as an injected constructor, since the
//custodian_and_ward policies seem to give problems
//boost::shared_ptr<eformat::read::FullEventFragment > 
//  eformat::python::make_event(const u32list& blob) {
//  return boost::shared_ptr<eformat::read::FullEventFragment >
//    (new eformat::read::FullEventFragment(blob.ptr()));
//}

eformat::helper::u32slice eformat::python::get_blob
  (const eformat::Header& f) 
{
  const uint32_t* p = 0; f.start(p);
  return eformat::helper::u32slice(p, f.fragment_size_word());
}

eformat::helper::u32slice eformat::python::get_status
  (const eformat::Header& f) 
{
  const uint32_t* s = 0; f.status(s);
  return eformat::helper::u32slice(s, f.nstatus());
}

eformat::helper::u32slice eformat::python::get_specific_header
  (const eformat::Header& f) 
{
  const uint32_t* s = 0; f.specific_header(s);
  return eformat::helper::u32slice(s, f.nspecific());
}

eformat::helper::u32slice eformat::python::get_payload
  (const eformat::Header& f) 
{
  const uint32_t* s = 0; f.payload(s);
  return eformat::helper::u32slice(s, f.payload_size_word());
}

eformat::helper::u32slice eformat::python::get_header
  (const eformat::Header& f) 
{
  const uint32_t* s = 0; f.start(s);
  return eformat::helper::u32slice(s, f.header_size_word());
}

eformat::helper::SourceIdentifier eformat::python::get_source_id
  (const eformat::Header& f)
{
  eformat::helper::SourceIdentifier retval(f.source_id());
  return retval;
}

eformat::helper::Version eformat::python::get_version
  (const eformat::Header& f)
{
  eformat::helper::Version retval(f.version());
  return retval;
}

uint16_t eformat::python::get_minor_version (const eformat::Header& f) {
  eformat::helper::Version retval(f.version());
  return retval.minor_version();
}

eformat::helper::u32slice eformat::python::get_readable_payload
  (const eformat::read::FullEventFragment& f) 
{
  const uint32_t* s = f.readable_payload();
  return eformat::helper::u32slice(s, f.readable_payload_size_word());
}

eformat::helper::u32slice eformat::python::get_lvl1_trigger_info
  (const eformat::read::FullEventFragment& f) 
{
  const uint32_t* s = 0; f.lvl1_trigger_info(s);
  return eformat::helper::u32slice(s, f.nlvl1_trigger_info());
}

eformat::helper::u32slice eformat::python::get_lvl2_trigger_info
  (const eformat::read::FullEventFragment& f) 
{
  const uint32_t* s = 0; f.lvl2_trigger_info(s);
  return eformat::helper::u32slice(s, f.nlvl2_trigger_info());
}

eformat::helper::u32slice eformat::python::get_event_filter_info
  (const eformat::read::FullEventFragment& f) 
{
  const uint32_t* s = 0; f.event_filter_info(s);
  return eformat::helper::u32slice(s, f.nevent_filter_info());
}

eformat::helper::u32slice eformat::python::get_raw_stream_tag
  (const eformat::read::FullEventFragment& f) 
{
  const uint32_t* s = 0; f.stream_tag(s);
  return eformat::helper::u32slice(s, f.nstream_tag());
}

std::vector<eformat::helper::StreamTag> eformat::python::get_stream_tag
  (const eformat::read::FullEventFragment& f) 
{
  const uint32_t* s = 0; f.stream_tag(s);
  std::vector<eformat::helper::StreamTag> decoded;
  eformat::helper::decode(f.nstream_tag(), s, decoded);
  return decoded;
}

eformat::RunType eformat::python::get_run_type
  (const eformat::read::FullEventFragment& f) 
{
  return eformat::RunType(f.run_type());
}

eformat::helper::SourceIdentifier eformat::python::get_rod_source_id
  (const eformat::read::ROBFragment& f)
{
  eformat::helper::SourceIdentifier retval(f.rod_source_id());
  return retval;
}

eformat::helper::Version eformat::python::get_rod_version
  (const eformat::read::ROBFragment& f)
{
  eformat::helper::Version retval(f.rod_version());
  return retval;
}

uint16_t eformat::python::get_rod_minor_version 
  (const eformat::read::ROBFragment& f)
{
  eformat::helper::Version retval(f.rod_version());
  return retval.minor_version();
}

eformat::helper::u32slice eformat::python::get_rod_status
  (const eformat::read::ROBFragment& f) 
{
  const uint32_t* s = 0; f.rod_status(s);
  return eformat::helper::u32slice(s, f.rod_nstatus());
}

eformat::helper::u32slice eformat::python::get_rod_data
  (const eformat::read::ROBFragment& f) 
{
  const uint32_t* s = 0; f.rod_data(s);
  return eformat::helper::u32slice(s, f.rod_ndata());
}

eformat::helper::u32slice eformat::python::get_rod_header
  (const eformat::read::ROBFragment& f) 
{
  const uint32_t* s = 0; f.rod_start(s);
  return eformat::helper::u32slice(s, f.rod_header_size_word());
}

eformat::helper::u32slice eformat::python::get_rod
  (const eformat::read::ROBFragment& f) 
{
  const uint32_t* s = 0; f.rod_start(s);
  return eformat::helper::u32slice(s, f.rod_fragment_size_word());
}

eformat::helper::u32list eformat::python::convert_old
  (const eformat::helper::u32list& old,
   const eformat::CheckSum& event_checksum,
   const eformat::CheckSum& rob_checksum) 
{
  size_t new_size = old.length();
  new_size += old.length()>>3; //gives 1.125 of its original size
  uint32_t* new_blob = new uint32_t[new_size];

  //convert and check
  uint32_t result = eformat::old::convert(old.ptr(), new_blob, new_size,
      event_checksum, rob_checksum);
  if (not result) 
    throw std::overflow_error("Failed to convert from old to new format");

  //if you get here, return as expected
  return eformat::helper::u32list(new_blob, 0, result);
}

eformat::helper::u32slice eformat::python::write_get_lvl1_trigger_info 
  (const eformat::write::FullEventFragment& fragment) {
  return eformat::helper::u32slice(fragment.lvl1_trigger_info(), 
      fragment.nlvl1_trigger_info());
}

void eformat::python::write_set_lvl1_trigger_info 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32slice& s)
{
  fragment.lvl1_trigger_info(s.length(), s.ptr());
}

void eformat::python::write_set_lvl1_trigger_info 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32list& l)
{
  fragment.lvl1_trigger_info(l.length(), l.ptr());
}

eformat::helper::u32slice eformat::python::write_get_lvl2_trigger_info 
  (const eformat::write::FullEventFragment& fragment) 
{
  return eformat::helper::u32slice(fragment.lvl2_trigger_info(), 
    fragment.nlvl2_trigger_info());
}

void eformat::python::write_set_lvl2_trigger_info 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32slice& s) 
{
  fragment.lvl2_trigger_info(s.length(), s.ptr());
}

void eformat::python::write_set_lvl2_trigger_info 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32list& l) 
{
  fragment.lvl2_trigger_info(l.length(), l.ptr());
}

eformat::helper::u32slice eformat::python::write_get_event_filter_info 
  (const eformat::write::FullEventFragment& fragment) 
{
  return eformat::helper::u32slice(fragment.event_filter_info(), 
      fragment.nevent_filter_info());
}

void eformat::python::write_set_event_filter_info 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32slice& s) 
{
  fragment.event_filter_info(s.length(), s.ptr());
}

void eformat::python::write_set_event_filter_info 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32list& l) 
{
  fragment.event_filter_info(l.length(), l.ptr());
}

eformat::helper::u32slice eformat::python::write_get_raw_stream_tag 
  (const eformat::write::FullEventFragment& fragment) 
{
  return eformat::helper::u32slice(fragment.stream_tag(), 
      fragment.nstream_tag());
}

std::vector<eformat::helper::StreamTag> eformat::python::write_get_stream_tag 
  (const eformat::write::FullEventFragment& f) 
{
  std::vector<eformat::helper::StreamTag> decoded;
  eformat::helper::decode(f.nstream_tag(), f.stream_tag(), decoded);
  return decoded;
}

void eformat::python::write_set_stream_tag 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32slice& s) 
{
  fragment.stream_tag(s.length(), s.ptr());
}

void eformat::python::write_set_stream_tag 
  (eformat::write::FullEventFragment& fragment, eformat::helper::u32list& l) 
{
  fragment.stream_tag(l.length(), l.ptr());
}

eformat::helper::u32slice eformat::python::write_get_rod_status 
  (const eformat::write::ROBFragment& fragment) 
{
  return eformat::helper::u32slice(fragment.rod_status(), 
      fragment.rod_nstatus());
}

void eformat::python::write_set_rod_status 
  (eformat::write::ROBFragment& fragment, eformat::helper::u32slice& s) 
{
    fragment.rod_status(s.length(), s.ptr());
}

void eformat::python::write_set_rod_status 
  (eformat::write::ROBFragment& fragment, eformat::helper::u32list& s) 
{
    fragment.rod_status(s.length(), s.ptr());
}

eformat::helper::u32slice eformat::python::write_get_rod_data 
  (const eformat::write::ROBFragment& fragment) 
{
  return eformat::helper::u32slice(fragment.rod_data(), fragment.rod_ndata());
}

void eformat::python::write_set_rod_data 
  (eformat::write::ROBFragment& fragment, eformat::helper::u32slice& s) 
{
    fragment.rod_data(s.length(), s.ptr());
}

void eformat::python::write_set_rod_data 
  (eformat::write::ROBFragment& fragment, eformat::helper::u32list& s) 
{
    fragment.rod_data(s.length(), s.ptr());
}

void eformat::python::write_set_rod_source 
  (eformat::write::ROBFragment& fragment, eformat::helper::SourceIdentifier& s)
{
  fragment.rod_source_id(s.code());
}

eformat::helper::SourceIdentifier eformat::python::write_get_rod_source 
  (const eformat::write::ROBFragment& fragment) 
{
  return fragment.rod_source_id();
}

uint32_t eformat::python::checksum(const eformat::CheckSum& type, 
    const eformat::helper::u32list& s)
{
  return eformat::helper::checksum(type, s.ptr(), s.length()); 
}

uint32_t eformat::python::checksum(const eformat::CheckSum& type, 
    const eformat::helper::u32slice& s)
{
  return eformat::helper::checksum(type, s.ptr(), s.length()); 
}

uint32_t eformat::python::checksum(const eformat::CheckSum& type, 
    const boost::python::list& l)
{
  boost::shared_ptr<eformat::helper::u32list> s = 
    eformat::python::from_py_list(l);
  return eformat::python::checksum(type, *s);
}

std::map<eformat::helper::SourceIdentifier, 
  eformat::read::ROBFragment>* eformat::python::build_toc
  (const eformat::read::FullEventFragment& event) 
{
  return eformat::python::build_toc_opt(event, false);
}

std::map<eformat::helper::SourceIdentifier, 
  eformat::read::ROBFragment>* eformat::python::build_toc_opt
  (const eformat::read::FullEventFragment& event,
   bool consider_optional) 
{
  std::map<uint32_t, const uint32_t*> index;
  eformat::helper::build_toc(event, index, consider_optional);
  std::map<eformat::helper::SourceIdentifier, 
    eformat::read::ROBFragment>* retval =
    new std::map<eformat::helper::SourceIdentifier, 
      eformat::read::ROBFragment>();
  for (std::map<uint32_t, const uint32_t*>::iterator
        it = index.begin(); it != index.end(); ++it)
    (*retval)[it->first] = it->second; 
  return retval;
}

eformat::read::ROBFragment eformat::python::find_rob 
(const eformat::read::FullEventFragment& event,
   const eformat::helper::SourceIdentifier& source_id) {
  return eformat::python::find_rob_opt(event, source_id, false);
}

eformat::read::ROBFragment eformat::python::find_rob_opt
(const eformat::read::FullEventFragment& event,
   const eformat::helper::SourceIdentifier& source_id,
   bool consider_optional) {
  return eformat::helper::find_rob(event, source_id.code(), consider_optional);
}

void eformat::python::write_append_unchecked
(eformat::write::FullEventFragment& event, eformat::helper::u32list& s)
{
  event.append_unchecked(s.ptr());
}

void eformat::python::write_append_unchecked
(eformat::write::FullEventFragment& event, eformat::helper::u32slice& s)
{
  event.append_unchecked(s.ptr());
}

void eformat::python::write_append_unchecked
(eformat::write::FullEventFragment& event, 
 eformat::read::ROBFragment& f)
{
  const uint32_t* start = 0;
  f.start(start);
  event.append_unchecked(start);
}

eformat::read::ROBFragment eformat::python::unchecked_fragment
(eformat::write::FullEventFragment& event,  uint32_t n) 
{
  if (n >= event.nunchecked_fragments()) {
    boost::format msg("Cannot retrieve unchecked ROBFragment %d since there are only %d fragments in this FullEventFragment.");
    msg % n % event.nunchecked_fragments();
    throw std::out_of_range(msg.str());
  }
  return event.unchecked_fragment(n);
}

boost::shared_ptr<eformat::helper::DetectorMask> eformat::python::detmask_from_list(const boost::python::list& ids) 
{
  std::vector<eformat::SubDetector> vec;
  for (int i=0; i<PyList_Size(ids.ptr()); ++i)
    vec.push_back(boost::python::extract<eformat::SubDetector>(ids[i]));
  return boost::shared_ptr<eformat::helper::DetectorMask>(new eformat::helper::DetectorMask(vec)); 
}

void eformat::python::detmask_set_list(eformat::helper::DetectorMask& mask,
    const boost::python::list& ids)
{
  for (int i=0; i<PyList_Size(ids.ptr()); ++i)
    mask.set(boost::python::extract<eformat::SubDetector>(ids[i]));
}

void eformat::python::detmask_unset_list(eformat::helper::DetectorMask& mask, 
    const boost::python::list& ids)
{
  for (int i=0; i<PyList_Size(ids.ptr()); ++i)
    mask.unset(boost::python::extract<eformat::SubDetector>(ids[i]));
}

boost::python::list eformat::python::detmask_decode(eformat::helper::DetectorMask& mask)
{
  std::vector<eformat::SubDetector> vec;
  mask.sub_detectors(vec);
  boost::python::list retval;
  for (unsigned i=0; i<vec.size(); ++i) retval.append(vec[i]);
  return retval; 
}

boost::python::list eformat::python::get_fe_problems
(const eformat::read::FullEventFragment& event)
{
  std::vector<eformat::FragmentProblem> vec;
  event.problems(vec);
  boost::python::list retval;
  for (unsigned i=0; i<vec.size(); ++i) retval.append(vec[i]);
  return retval; 
}

boost::python::list eformat::python::get_all_problems
(const eformat::read::ROBFragment& rob)
{
  std::vector<eformat::FragmentProblem> vec;
  rob.problems(vec);
  boost::python::list retval;
  for (unsigned i=0; i<vec.size(); ++i) retval.append(vec[i]);
  return retval; 
}

boost::python::list eformat::python::get_rob_problems
(const eformat::read::ROBFragment& rob)
{
  std::vector<eformat::FragmentProblem> vec;
  rob.rob_problems(vec);
  boost::python::list retval;
  for (unsigned i=0; i<vec.size(); ++i) retval.append(vec[i]);
  return retval; 
}

boost::python::list eformat::python::get_rod_problems
(const eformat::read::ROBFragment& rob)
{
  std::vector<eformat::FragmentProblem> vec;
  rob.rod_problems(vec);
  boost::python::list retval;
  for (unsigned i=0; i<vec.size(); ++i) retval.append(vec[i]);
  return retval; 
}

