/**
 * @file python/eformat.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief  Python bindings to the eformat reading constructions
 */

#include <boost/python.hpp> 
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/slice.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <stdexcept>
#include "eformat/Header.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/RunType.h"
#include "eformat/Version.h"
#include "eformat/StreamTag.h"
#include "eformat/FullEventFragment.h"
#include "eformat/ROBFragment.h"
#include "eformat/python/util.h"
#include "eformat/old/util.h"

using namespace boost::python;

BOOST_PYTHON_FUNCTION_OVERLOADS(convert_old_overloads, \
    eformat::python::convert_old, 1, 3)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(full_event_check_overloads, \
  eformat::read::FullEventFragment::check, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(full_event_check_tree_overloads, \
  eformat::read::FullEventFragment::check_tree, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(full_event_check_noex_overloads, \
  eformat::read::FullEventFragment::check_noex, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(full_event_check_tree_noex_overloads, \
  eformat::read::FullEventFragment::check_tree_noex, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(rob_check_overloads, \
  eformat::read::ROBFragment::check, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(rob_check_rob_overloads, \
  eformat::read::ROBFragment::check_rob, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(rob_check_rod_overloads, \
  eformat::read::ROBFragment::check_rod, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(rob_check_noex_overloads, \
  eformat::read::ROBFragment::check_noex, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(rob_check_rob_noex_overloads, \
  eformat::read::ROBFragment::check_rob_noex, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(rob_check_rod_noex_overloads, \
  eformat::read::ROBFragment::check_rod_noex, 0, 1)
  
// helper method which converts rob vectors into lists
boost::python::tuple
FullEventFragment_robs(const eformat::read::FullEventFragment& f)
{
  std::vector<eformat::read::ROBFragment> good, bad;
  f.robs(good, bad);

  list pygood, pybad;
  BOOST_FOREACH(const eformat::read::ROBFragment& rob, good) {
    pygood.append(rob);
  }
  BOOST_FOREACH(const eformat::read::ROBFragment& rob, bad) {
    pybad.append(rob);
  }

  return make_tuple(pygood, pybad);
}

// Special iterator class for ROBS inside full event fragment
struct FullEventFragment_rob_iterator {

  FullEventFragment_rob_iterator(const eformat::read::FullEventFragment& f) : m_iter(f.child_iter()) {}

  eformat::read::ROBFragment next() {
    const uint32_t* p = m_iter.next();
    if (not p) {
      // done iterating
      PyErr_SetString(PyExc_StopIteration, "No more data.");
      boost::python::throw_error_already_set();
      // it should not come to this
    }
    return p ? eformat::read::ROBFragment(p) : eformat::read::ROBFragment();
  }

  // factory method for iterator
  static FullEventFragment_rob_iterator
  make_rob_iterator(const eformat::read::FullEventFragment& f)
  {
    return FullEventFragment_rob_iterator(f);
  }

  static object pass_through(object const& o) { return o; }

  eformat::read::FullEventFragment::child_iter_t m_iter;
};


BOOST_PYTHON_MODULE(libpyeformat)
{
   //exception handler
   register_exception_translator<std::range_error>(&eformat::python::translate_range_error);
   register_exception_translator<ers::Issue>(&eformat::python::translate_ers_issue);
   
   //defines the 32-bit data blob we need for passing stuff from the storage to
   //the eformat library.
   class_<eformat::helper::u32list>("u32list", init<const eformat::helper::u32list&>())
     .def("__init__", make_constructor(&eformat::python::from_py_list))
     .def("__len__", &eformat::helper::u32list::length)
     .def("__setitem__", &eformat::helper::u32list::setitem)
     .def("__getitem__", &eformat::helper::u32list::getitem)
     .def("__getitem__", (eformat::helper::u32slice(*)(const eformat::helper::u32list&, const boost::python::slice&))&eformat::python::getslice, with_custodian_and_ward_postcall<0, 1>())
     .def("copy", (boost::python::list(*)(const eformat::helper::u32list&, const boost::python::slice&))&eformat::python::copy)
     ;
  
  //u32list functionality, but w/o memory management
  class_<eformat::helper::u32slice>("u32slice", init<const eformat::helper::u32slice&>())
    .def(init<const eformat::helper::u32list&>()[with_custodian_and_ward<1,2>()])
    .def("__len__", &eformat::helper::u32slice::length)
    .def("__getitem__", &eformat::helper::u32slice::getitem)
    .def("__getitem__", (eformat::helper::u32slice(*)(const eformat::helper::u32slice&, const boost::python::slice&))&eformat::python::getslice, with_custodian_and_ward_postcall<0, 1>())
    .def("copy", (boost::python::list(*)(const eformat::helper::u32slice&, const boost::python::slice&))&eformat::python::copy)
    ;

  //for the Full Event Fragment TOC building
  class_<std::map<eformat::helper::SourceIdentifier, 
    eformat::read::ROBFragment > >("toc_table")
    .def(map_indexing_suite<std::map<eformat::helper::SourceIdentifier, 
	 eformat::read::ROBFragment > >())
    ;
  
  class_<eformat::Header, 
         boost::noncopyable>("Header", no_init)
    .def("marker",
        &eformat::Header::marker)
    .def("__len__",
        &eformat::Header::fragment_size_word)
    .def("fragment_size_word",
        &eformat::Header::fragment_size_word)
    .def("header_size_word",
        &eformat::Header::header_size_word)
    .def("payload_size_word",
        &eformat::Header::payload_size_word)
    .def("version", &eformat::python::get_version)
    .def("minor_version", &eformat::python::get_minor_version)
    .def("source_id", &eformat::python::get_source_id)
    .def("status", 
        &eformat::python::get_status, with_custodian_and_ward_postcall<0, 1>())
    .def("payload", 
        &eformat::python::get_payload, with_custodian_and_ward_postcall<0, 1>())
    .def("header", 
        &eformat::python::get_header, with_custodian_and_ward_postcall<0, 1>())
    .def("checksum_type",
        &eformat::Header::checksum_type)
    .def("checksum_value",
        &eformat::Header::checksum_value)
    .def("eval_checksum",
        &eformat::Header::eval_checksum)
    .def("checksum",
        &eformat::Header::checksum)
    .def("specific_header", 
        &eformat::python::get_specific_header, with_custodian_and_ward_postcall<0, 1>())
    .def("__raw__", &eformat::python::get_blob, with_custodian_and_ward_postcall<0, 1>())
    ;

  // wrapper for iterator class
  class_<FullEventFragment_rob_iterator>("FullEventFragment_rob_iterator", no_init)
    .def("__iter__", &FullEventFragment_rob_iterator::pass_through)
    .def("next", &FullEventFragment_rob_iterator::next, with_custodian_and_ward_postcall<0, 1>())
    .def("__next__", &FullEventFragment_rob_iterator::next, with_custodian_and_ward_postcall<0, 1>())
    ;

  class_<eformat::read::FullEventFragment, 
     boost::shared_ptr<eformat::read::FullEventFragment >, 
     bases<eformat::Header > >("FullEventFragment", no_init)
    .def(init<const eformat::helper::u32list&>()[with_custodian_and_ward<1,2>()])
    .def(init<const eformat::helper::u32slice&>()[with_custodian_and_ward<1,2>()])
    .def("readable_payload_size_word",&eformat::read::FullEventFragment::readable_payload_size_word)
    .def("readable_payload", 
	 &eformat::python::get_readable_payload, 
	 with_custodian_and_ward_postcall<0, 1>())
    .def("check",
        &eformat::read::FullEventFragment::check, 
        full_event_check_overloads())
    .def("check_tree",
        &eformat::read::FullEventFragment::check_tree, 
        full_event_check_tree_overloads())
    .def("check_noex",
        &eformat::read::FullEventFragment::check_noex, 
        full_event_check_noex_overloads())
    .def("check_tree_noex",
        &eformat::read::FullEventFragment::check_tree_noex, 
        full_event_check_tree_noex_overloads())
    .def("problems", &eformat::python::get_fe_problems)
    .def("bc_time_seconds", 
        &eformat::read::FullEventFragment::bc_time_seconds)
    .def("bc_time_nanoseconds", 
        &eformat::read::FullEventFragment::bc_time_nanoseconds)
    .def("global_id", 
        &eformat::read::FullEventFragment::global_id)
    .def("run_type", &eformat::python::get_run_type)
    .def("run_no", 
        &eformat::read::FullEventFragment::run_no)
    .def("lumi_block", 
        &eformat::read::FullEventFragment::lumi_block)
    .def("lvl1_id", 
        &eformat::read::FullEventFragment::lvl1_id)
    .def("bc_id", 
        &eformat::read::FullEventFragment::bc_id)
    .def("lvl1_trigger_type", 
        &eformat::read::FullEventFragment::lvl1_trigger_type)
    .def("compression_type", 
	 &eformat::read::FullEventFragment::compression_type)
    .def("uncompressed_payload_size", 
	 &eformat::read::FullEventFragment::readable_payload_size_word)
    .def("lvl1_trigger_info", 
        &eformat::python::get_lvl1_trigger_info, with_custodian_and_ward_postcall<0, 1>())
    .def("lvl2_trigger_info", 
        &eformat::python::get_lvl2_trigger_info, with_custodian_and_ward_postcall<0, 1>())
    .def("event_filter_info", 
        &eformat::python::get_event_filter_info, with_custodian_and_ward_postcall<0, 1>())
    .def("stream_tag", &eformat::python::get_stream_tag)
    .def("raw_stream_tag", &eformat::python::get_raw_stream_tag)
    .def("__getitem__", &eformat::python::get_child<eformat::read::ROBFragment, eformat::read::FullEventFragment >, with_custodian_and_ward_postcall<0, 1>())
    .def("__iter__", &FullEventFragment_rob_iterator::make_rob_iterator, with_custodian_and_ward_postcall<0, 1>())
    .def("children", &FullEventFragment_rob_iterator::make_rob_iterator, with_custodian_and_ward_postcall<0, 1>())
    .def("nchildren", &eformat::read::FullEventFragment::nchildren)
    .def("robs", &FullEventFragment_robs)
    .def("toc", eformat::python::build_toc,
         return_value_policy<manage_new_object, 
         with_custodian_and_ward_postcall<0, 1> >())
    .def("toc", eformat::python::build_toc_opt,
         return_value_policy<manage_new_object, 
         with_custodian_and_ward_postcall<0, 1> >())
    .def("find", eformat::python::find_rob, 
         with_custodian_and_ward_postcall<0, 1>())
    .def("find", eformat::python::find_rob_opt, 
         with_custodian_and_ward_postcall<0, 1>())
    ;
  
  class_<eformat::read::ROBFragment, 
    bases<eformat::Header> >("ROBFragment")
    .def(init<const eformat::helper::u32list&>()[with_custodian_and_ward<1,2>()])
    .def(init<const eformat::helper::u32slice&>()[with_custodian_and_ward<1,2>()])
    .def("check", &eformat::read::ROBFragment::check,
        rob_check_overloads())
    .def("check_rob", &eformat::read::ROBFragment::check_rob,
        rob_check_rob_overloads())
    .def("check_rod", &eformat::read::ROBFragment::check_rod,
        rob_check_rod_overloads())
    .def("check_noex", &eformat::read::ROBFragment::check_noex,
        rob_check_noex_overloads())
    .def("check_rob_noex", &eformat::read::ROBFragment::check_rob_noex,
        rob_check_rob_noex_overloads())
    .def("check_rod_noex", &eformat::read::ROBFragment::check_rod_noex,
        rob_check_rod_noex_overloads())
    .def("problems", &eformat::python::get_all_problems)
    .def("rob_problems", &eformat::python::get_rob_problems)
    .def("rod_problems", &eformat::python::get_rod_problems)
    .def("rob_source_id", &eformat::python::get_source_id)
    .def("rod_marker",
        &eformat::read::ROBFragment::rod_marker)
    .def("rod_version", &eformat::python::get_rod_version)
    .def("rod_minor_version", &eformat::python::get_rod_minor_version)
    .def("rod_source_id", &eformat::python::get_rod_source_id)
    .def("rod_run_no", &eformat::read::ROBFragment::rod_run_no)
    .def("rod_lvl1_id", &eformat::read::ROBFragment::rod_lvl1_id)
    .def("rod_bc_id", &eformat::read::ROBFragment::rod_bc_id)
    .def("rod_lvl1_trigger_type", 
        &eformat::read::ROBFragment::rod_lvl1_trigger_type)
    .def("rod_detev_type", 
        &eformat::read::ROBFragment::rod_detev_type)
    .def("rod_status", 
        &eformat::python::get_rod_status, with_custodian_and_ward_postcall<0, 1>())
    .def("rod_data", 
        &eformat::python::get_rod_data, with_custodian_and_ward_postcall<0, 1>())
    .def("status_position",
        &eformat::read::ROBFragment::rod_status_position)
    .def("rod",
        &eformat::python::get_rod, with_custodian_and_ward_postcall<0, 1>())
    .def("rod_header",
        &eformat::python::get_rod_header, with_custodian_and_ward_postcall<0, 1>())
    ;

  def("convert_old", (eformat::helper::u32list(*)(const eformat::helper::u32list&, const eformat::CheckSum&, const eformat::CheckSum&))0, convert_old_overloads());
 
}
