//Dear emacs, this is -*- c++ -*-

/**
 * @file Status.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the Status helper class.
 */

#include "eformat/Status.h"

eformat::helper::Status::Status (uint32_t v)
  : m_gen(static_cast<eformat::GenericStatus>(v&0xffff)),
    m_spec((v>>16)&0xffff)
{
}

uint32_t eformat::helper::Status::code (void) const 
{
  uint32_t retval = m_spec;
  retval <<= 16;
  retval |= m_gen;
  return retval;
}

static eformat::helper::EnumClass<eformat::GenericStatus> 
  make_GenericStatus_dictionary()
{
  eformat::helper::EnumClass<eformat::GenericStatus> d(0xff, "UNKNOWN_GENERIC_STATUS");
  d.add(eformat::UNCLASSIFIED, "UNCLASSIFIED");
  d.add(eformat::BCID_CHECK_FAIL, "BCID_CHECK_FAIL");
  d.add(eformat::LVL1ID_CHECK_FAIL, "LVL1ID_CHECK_FAIL");
  d.add(eformat::TIMEOUT, "TIMEOUT");
  d.add(eformat::DATA_CORRUPTION, "DATA_CORRUPTION");
  d.add(eformat::INTERNAL_OVERFLOW, "INTERNAL_OVERFLOW");
  d.add(eformat::DUMMY_FRAGMENT, "DUMMY_FRAGMENT");
  return d;
}

const eformat::helper::EnumClass<eformat::GenericStatus> 
  eformat::helper::GenericStatusDictionary = make_GenericStatus_dictionary();

static eformat::helper::EnumClass<eformat::RobinStatus> 
  make_RobinStatus_dictionary()
{
  eformat::helper::EnumClass<eformat::RobinStatus> d(0xffff, "UNKNOWN_ROBIN_STATUS");
  d.add(eformat::TRIGGER_TYPE_SYNC_ERROR, "TRIGGER_TYPE_SYNC_ERROR");
  d.add(eformat::DATAFLOW_DUMMY, "DATAFLOW_DUMMY");
  d.add(eformat::FRAGMENT_SIZE_ERROR, "FRAGMENT_SIZE_ERROR");
  d.add(eformat::DATABLOCK_ERROR, "DATABLOCK_ERROR");
  d.add(eformat::CTRL_WORD_ERROR, "CTRL_WORD_ERROR");
  d.add(eformat::MISSING_BOF, "MISSING_BOF");
  d.add(eformat::MISSING_EOF, "MISSING_EOF");
  d.add(eformat::INVALID_HEADER_MARKER, "INVALID_HEADER_MARKER");
  d.add(eformat::FORMAT_ERROR, "FORMAT_ERROR");
  d.add(eformat::DUPLICATE_EVENT, "DUPLICATE_EVENT");
  d.add(eformat::SEQUENCE_ERROR, "SEQUENCE_ERROR");
  d.add(eformat::TRANSMISSION_ERROR, "TRANSMISSION_ERROR");
  d.add(eformat::TRUNCATION, "TRUNCATION");
  d.add(eformat::SHORT_FRAGMENT, "SHORT_FRAGMENT");
  d.add(eformat::FRAGMENT_LOST, "FRAGMENT_LOST");
  d.add(eformat::FRAGMENT_PENDING, "FRAGMENT_PENDING");
  d.add(eformat::ROBIN_DISCARD_MODE, "ROBIN_DISCARD_MODE");
  return d;
}

const eformat::helper::EnumClass<eformat::RobinStatus> 
  eformat::helper::RobinStatusDictionary = make_RobinStatus_dictionary();


static eformat::helper::EnumClass<eformat::DummyROBStatus> 
  make_DummyROBStatus_dictionary()
{
  eformat::helper::EnumClass<eformat::DummyROBStatus> d(0xffff, "UNKNOWN_DUMMYROB_STATUS");
  d.add(eformat::INCOMPLETE_ROI, "INCOMPLETE_ROI");
  d.add(eformat::COLLECTION_TIMEOUT, "COLLECTION_TIMEOUT");
  return d;
}

const eformat::helper::EnumClass<eformat::DummyROBStatus> 
  eformat::helper::DummyROBStatusDictionary = make_DummyROBStatus_dictionary();

static eformat::helper::EnumClass<eformat::FullEventStatus> 
  make_FullEventStatusV40_dictionary()
{
  eformat::helper::EnumClass<eformat::FullEventStatus> d(0xffff, "UNKNOWN_FULLEVENT_STATUS");
  d.add(eformat::L2_PROCESSING_TIMEOUT, "L2_PROCESSING_TIMEOUT");
  d.add(eformat::L2PU_PROCESSING_TIMEOUT, "L2PU_PROCESSING_TIMEOUT");
  d.add(eformat::SFI_DUPLICATION_WARNING, "SFI_DUPLICATION_WARNING");
  d.add(eformat::DFM_DUPLICATION_WARNING, "DFM_DUPLICATION_WARNING");
  d.add(eformat::L2PSC_PROBLEM, "L2PSC_PROBLEM");
  //bit 21 is reserved, 0x20
  //bit 22 is reserved, 0x40
  //bit 23 is reserved, 0x80
  d.add(eformat::EF_PROCESSING_TIMEOUT, "EF_PROCESSING_TIMEOUT");
  d.add(eformat::PT_PROCESSING_TIMEOUT, "PT_PROCESSING_TIMEOUT");
  d.add(eformat::SFO_DUPLICATION_WARNING, "SFO_DUPLICATION_WARNING");
  d.add(eformat::EFD_RECOVERED_EVENT, "EFD_RECOVERED_EVENT");
  d.add(eformat::EFPSC_PROBLEM, "EFPSC_PROBLEM");
  d.add(eformat::EFD_FORCED_ACCEPT, "EFD_FORCED_ACCEPT");
  //bit 30 is reserved, 0x4000
  d.add(eformat::PARTIAL_EVENT, "PARTIAL_EVENT");
  return d;
}

const eformat::helper::EnumClass<eformat::FullEventStatus> 
eformat::helper::FullEventStatusDictionaryV40 = make_FullEventStatusV40_dictionary();


static eformat::helper::EnumClass<eformat::FullEventStatus> 
  make_FullEventStatus_dictionary()
{
  eformat::helper::EnumClass<eformat::FullEventStatus> d(0xffff, "UNKNOWN_FULLEVENT_STATUS");
  d.add(eformat::HLTSV_DUPLICATION_WARNING, "HTLSV_DUPLICATION_WARNING");
  d.add(eformat::DCM_PROCESSING_TIMEOUT, "DCM_PROCESSING_TIMEOUT");
  d.add(eformat::HLTPU_PROCESSING_TIMEOUT, "HLTPU_PROCESSING_TIMEOUT");
  d.add(eformat::DCM_DUPLICATION_WARNING, "DCM_DUPLICATION_WARNING");
  d.add(eformat::DCM_RECOVERED_EVENT, "DCM_RECOVERED_EVENT");
  d.add(eformat::PSC_PROBLEM, "PSC_PROBLEM");
  d.add(eformat::DCM_FORCED_ACCEPT, "DCM_FORCED_ACCEPT");
  d.add(eformat::PARTIAL_EVENT, "PARTIAL_EVENT");
  return d;
}

const eformat::helper::EnumClass<eformat::FullEventStatus> 
eformat::helper::FullEventStatusDictionary = make_FullEventStatus_dictionary();
