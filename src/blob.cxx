/**
 * @file blob.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief  
 */

#include "eformat/blob.h"

eformat::helper::u32list::u32list(uint32_t* data, size_t start, size_t size)
  : m_data(data),
    m_start(start),
    m_size(size)
{
}

eformat::helper::u32list::~u32list()
{
}

uint32_t eformat::helper::u32list::setitem(long pos, uint32_t v) 
{
  long comp = m_size;
  if (pos >= comp || pos < -comp) {
    throw std::range_error("Index is out of range");
  }
  return (m_data[m_start+(pos<0?(m_size+pos):pos)] = v);
}

uint32_t eformat::helper::u32list::getitem(long pos) const
{
  long comp = m_size;
  if (pos >= comp || pos < -comp) {
    throw std::range_error("Index is out of range");
  }
  return m_data[m_start+(pos<0?(m_size+pos):pos)];
}

eformat::helper::u32slice::u32slice(const uint32_t* data, size_t size)
  : m_data(data),
    m_size(size)
{
}

eformat::helper::u32slice::u32slice(const u32list& other)
  : m_data(other.ptr()+other.start()),
    m_size(other.length())
{
}

eformat::helper::u32slice::~u32slice()
{
}

uint32_t eformat::helper::u32slice::getitem(long pos) const
{
  long comp = m_size;
  if (pos >= comp || pos < -comp) {
    throw std::range_error("Index is out of range");
  }
  return m_data[(pos<0?(m_size+pos):pos)];
}

