/**
 * @file DetectorMask.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 * $Author:$
 *
 * @brief Defines the behavior of Detector Masks
 */

#include "eformat/DetectorMask.h"
#include "eformat/Issue.h"

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <stdlib.h>
#include <errno.h>

static const std::vector<eformat::SubDetector>& mask_order() 
{
  static std::vector<eformat::SubDetector> MASK_ORDER;
  if (MASK_ORDER.size()) return MASK_ORDER;

  //initialize the mask order
  //note: the order in this vector is VERY relevant to the decoding of the
  //Detector Mask. Please do NOT modify it and only add new components in the
  //end of the vector.
  MASK_ORDER.reserve(128);
  MASK_ORDER.push_back(eformat::PIXEL_BARREL);
  MASK_ORDER.push_back(eformat::PIXEL_DISK);
  MASK_ORDER.push_back(eformat::PIXEL_B_LAYER);
  MASK_ORDER.push_back(eformat::OTHER); // Removed Pix layer
  MASK_ORDER.push_back(eformat::SCT_BARREL_A_SIDE);
  MASK_ORDER.push_back(eformat::SCT_BARREL_C_SIDE);
  MASK_ORDER.push_back(eformat::SCT_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::SCT_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::TRT_BARREL_A_SIDE);
  MASK_ORDER.push_back(eformat::TRT_BARREL_C_SIDE);
  MASK_ORDER.push_back(eformat::TRT_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::TRT_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_BARREL_A_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_BARREL_C_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::LAR_HAD_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::LAR_HAD_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::LAR_FCAL_A_SIDE);
  MASK_ORDER.push_back(eformat::LAR_FCAL_C_SIDE);
  MASK_ORDER.push_back(eformat::TILECAL_BARREL_A_SIDE);
  MASK_ORDER.push_back(eformat::TILECAL_BARREL_C_SIDE);
  MASK_ORDER.push_back(eformat::TILECAL_EXT_A_SIDE);
  MASK_ORDER.push_back(eformat::TILECAL_EXT_C_SIDE);
  MASK_ORDER.push_back(eformat::MUON_MDT_BARREL_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_MDT_BARREL_C_SIDE);
  MASK_ORDER.push_back(eformat::MUON_MDT_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_MDT_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::MUON_RPC_BARREL_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_RPC_BARREL_C_SIDE);
  MASK_ORDER.push_back(eformat::MUON_TGC_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_TGC_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::MUON_CSC_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_CSC_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_PREPROC);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_CLUSTER_PROC_DAQ);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_CLUSTER_PROC_ROI);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_JET_PROC_DAQ);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_JET_PROC_ROI);
  MASK_ORDER.push_back(eformat::TDAQ_MUON_CTP_INTERFACE);
  MASK_ORDER.push_back(eformat::TDAQ_CTP);
  MASK_ORDER.push_back(eformat::TDAQ_L2SV);
  MASK_ORDER.push_back(eformat::TDAQ_SFI);
  MASK_ORDER.push_back(eformat::TDAQ_SFO);
  MASK_ORDER.push_back(eformat::TDAQ_LVL2);
  MASK_ORDER.push_back(eformat::TDAQ_HLT);
  MASK_ORDER.push_back(eformat::FORWARD_BCM);
  MASK_ORDER.push_back(eformat::FORWARD_LUCID);
  MASK_ORDER.push_back(eformat::FORWARD_ZDC);
  MASK_ORDER.push_back(eformat::FORWARD_ALPHA);
  MASK_ORDER.push_back(eformat::TRT_ANCILLARY_CRATE);
  MASK_ORDER.push_back(eformat::TILECAL_LASER_CRATE);
  MASK_ORDER.push_back(eformat::MUON_ANCILLARY_CRATE);
  MASK_ORDER.push_back(eformat::TDAQ_BEAM_CRATE);
  MASK_ORDER.push_back(eformat::TDAQ_FTK);
  MASK_ORDER.push_back(eformat::OFFLINE);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_TOPO_PROC);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_DIGITAL_PROC);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_FEAT_EXTRACT_DAQ);
  MASK_ORDER.push_back(eformat::TDAQ_CALO_FEAT_EXTRACT_ROI);
  MASK_ORDER.push_back(eformat::MUON_MMEGA_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_MMEGA_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::PIXEL_IBL);
  MASK_ORDER.push_back(eformat::PIXEL_DBM);
  MASK_ORDER.push_back(eformat::FORWARD_AFP);
  MASK_ORDER.push_back(eformat::LAR_EM_BARREL_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_BARREL_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_HAD_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::LAR_EM_HAD_ENDCAP_C_SIDE);
  MASK_ORDER.push_back(eformat::MUON_STGC_ENDCAP_A_SIDE);
  MASK_ORDER.push_back(eformat::MUON_STGC_ENDCAP_C_SIDE);
  return MASK_ORDER;
}

static unsigned index(eformat::SubDetector id) {
  static const std::vector<eformat::SubDetector>& order = mask_order();
  std::vector<eformat::SubDetector>::const_iterator obj = 
    std::find(order.begin(), order.end(), id);
  if (obj == order.end()) { 
    if (id != eformat::FULL_SD_EVENT) throw EFORMAT_UNKNOWN_SUBDETECTOR(id);
    else throw EFORMAT_UNSUPPORTED_SUBDETECTOR(id);
  }
  return obj-order.begin();
}

static void mask_set(std::bitset<128>& mask, eformat::SubDetector id, bool val) {
  std::bitset<128> add = 1;
  add <<= index(id);
  if (val) mask |= add; //if we have to set, we OR the mask 
  else mask &= ~add; //otherwise, we have to AND the inverse bit-pattern
}

std::string
eformat::helper::DetectorMask::to_string(const std::bitset<128>& mask) {

  //32bit encoded in hex + '\0' --> 9 chars
  char repr[9];
  std::string str("");
  for(int i=3; i>=0; --i){
    uint32_t slice = ((mask >> 32*i) & std::bitset<128>(0xffffffff)).to_ulong();
    ::snprintf(repr, 9, "%08x", slice);
    str += std::string(repr);
  }
  return str;
}

std::string
eformat::helper::DetectorMask::to_string(const uint64_t least, 
					 const uint64_t most) {

  std::bitset<128> fullmask(most);
  fullmask <<= 64;
  fullmask |= least;
  return eformat::helper::DetectorMask::to_string(fullmask);
}

std::bitset<128> 
eformat::helper::DetectorMask::from_string(const std::string& mask) {
  
  //4 "words" of 8 chars each
  unsigned int expected = 32;
  if (mask.size() != expected) {
    std::string reason("string is the wrong size (" + 
		       boost::lexical_cast<std::string>(mask.size()) +
		       " instead " + 
		       boost::lexical_cast<std::string>(expected));
    throw eformat::CannotDecodeMaskIssue(ERS_HERE,mask,reason);
  }

  std::bitset<128> decoded = 0;

  for(int i=0; i<2; ++i){
    
    std::string partialstr = mask.substr(i*16,16);
    
    errno = 0;
    char * end = NULL;
    unsigned long long int partial = ::strtoull(partialstr.c_str(),&end,16);
   
    if(errno || end == partialstr.c_str() || *end != '\0'){
      std::string reason("string to number conversion failed ("+
			 std::string(::strerror(errno)) + ")");
      throw eformat::CannotDecodeMaskIssue(ERS_HERE,mask,reason);
    }else{
      std::bitset<128> tmp = partial;
      decoded |= tmp << (64*(1-i));
    }
  }
  return decoded;
}

eformat::helper::DetectorMask::DetectorMask()
  : m_mask(0)
{
}

eformat::helper::DetectorMask::DetectorMask(const std::bitset<128>& mask)
  : m_mask(mask)
{
}

eformat::helper::DetectorMask::DetectorMask(const uint64_t least, const uint64_t most)
  : m_mask(most)
{
  m_mask <<= 64;
  m_mask |= least;
}

eformat::helper::DetectorMask::DetectorMask(const std::string& mask)
  : m_mask(eformat::helper::DetectorMask::from_string(mask))
{
}

eformat::helper::DetectorMask::DetectorMask(const std::vector<eformat::SubDetector>& ids)
  : m_mask(0)
{
  for (size_t i=0; i<ids.size(); ++i) mask_set(m_mask, ids[i], true);
}

eformat::helper::DetectorMask::~DetectorMask()
{
}

void eformat::helper::DetectorMask::sub_detectors(std::vector<eformat::SubDetector>& ids) const
{
  static const std::vector<eformat::SubDetector>& order = mask_order();
  for (unsigned i=0; i<order.size(); ++i)
    if (m_mask.test(i)) ids.push_back(order[i]);
}

std::bitset<128> eformat::helper::DetectorMask::mask(void) const
{
  return m_mask;
}

void eformat::helper::DetectorMask::set(eformat::SubDetector id) {
  mask_set(m_mask, id, true);
}

void eformat::helper::DetectorMask::set(const std::vector<eformat::SubDetector>& ids) {
  for (size_t i=0; i<ids.size(); ++i) mask_set(m_mask, ids[i], true);
}

void eformat::helper::DetectorMask::unset(eformat::SubDetector id) {
  mask_set(m_mask, id, false);
}

void eformat::helper::DetectorMask::unset(const std::vector<eformat::SubDetector>& ids) {
  for (size_t i=0; i<ids.size(); ++i) mask_set(m_mask, ids[i], false);
}

void eformat::helper::DetectorMask::reset ()
{
  m_mask = 0; 
}

bool eformat::helper::DetectorMask::is_set(eformat::SubDetector id) const
{
  return m_mask.test(index(id));
}

std::string eformat::helper::DetectorMask::string() const
{
  return eformat::helper::DetectorMask::to_string(m_mask);
}

std::pair<uint64_t, uint64_t> 
eformat::helper::DetectorMask::serialize() const
{
  uint64_t most = (m_mask >> 64).to_ullong();
  uint64_t least = (m_mask & std::bitset<128>(0xffffffffffffffff)).to_ullong();
  return std::make_pair(most, least);
}
