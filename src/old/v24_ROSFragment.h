//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v24_ROSFragment.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Describes the ROS fragment as defined in the Event Format note,
 * version 2.4
 */

#ifndef EFORMAT_V24_ROSFRAGMENT_H
#define EFORMAT_V24_ROSFRAGMENT_H

#include "src/old/v24_Header.h"

namespace eformat {

  namespace v24 {

    /**
     * Describes how to access the contents of a subdetector fragment, as
     * prescribed by the event format note.
     */
    class ROSFragment : public eformat::v24::Header {

    public: //interface

      /**
       * To build a fragment given the containing buffer. I need to know
       * where the fragment starts in order to do that.
       *
       * @param it The exact position where this fragment should start.
       */
      ROSFragment (const uint32_t* it);

      /**
       * Destructor virtualisation
       */
      virtual ~ROSFragment() {}

      /**
       * Says if the the fragment is valid. This may throw exceptions.
       */
      virtual bool check () const;

      /**
       * Says if the the fragment is valid. This may throw exceptions.
       */
      bool check_tree () const;

      /**
       * Returns the lvl1 identifier
       */
      inline uint32_t lvl1_id() const { return m_start[0]; }

      /**
       * Returns the bunch crossing identifier
       */
      inline uint32_t bc_id() const { return m_start[1]; }

    private: //representation

      const uint32_t* m_start; ///< my start word

    };

  }

}

#endif /* EFORMAT_V24_ROSFRAGMENT_H */
