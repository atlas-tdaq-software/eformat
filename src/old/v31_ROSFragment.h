//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v31_ROSFragment.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Describes the ROS fragment as defined in the Event Format note.
 */

#ifndef EFORMAT_V31_ROSFRAGMENT_H
#define EFORMAT_V31_ROSFRAGMENT_H

#include "src/old/v31_Header.h"
#include "src/old/v31_HeaderMarker.h"
#include "src/old/v31_ROBFragment.h"
#include "eformat/Issue.h"

namespace eformat { namespace v31 {

  /**
   * Describes how to access the contents of a subdetector fragment, as
   * prescribed by the event format note.
   */
  class ROSFragment : public eformat::v31::Header {

  public: //interface
    
    /**
     * To build a fragment given the containing buffer. I need to know
     * where the fragment starts in order to do that.
     *
     * @param it The exact position where this fragment should start.
     */
    ROSFragment (const uint32_t* it);

    /**
     * Builds an empty, otherwise useless ROSFragment
     */
    ROSFragment () : Header(), m_start() {}

    /**
     * Copy constructor
     *
     * @param other The fragment to be copied
     */
    ROSFragment (const ROSFragment& other) 
      : Header(other), m_start(other.m_start) {}

    /**
     * Destructor virtualisation
     */
    virtual ~ROSFragment() {}

    /**
     * Copy operator
     *
     * @param other The fragment to be copied
     */
    ROSFragment& operator= (const ROSFragment& other) 
    { Header::operator=(other); m_start=other.m_start; return *this; }

    /**
     * Manual re-assignment
     *
     * @param it The position pointing the first word of this fragment
     */
    ROSFragment& assign (const uint32_t* it);

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    virtual bool check (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    bool check_tree (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Returns the run number
     */
    inline uint32_t run_no() const { return m_start[0]; }

    /**
     * Returns the lvl1 identifier
     */
    inline uint32_t lvl1_id() const { return m_start[1]; }

    /**
     * Returns the bunch crossing identifier
     */
    inline uint16_t bc_id() const { return m_start[2]; }

  private: //static stuff

    static const uint32_t NSPECIFIC;

  private: //representation

    const uint32_t* m_start; ///< my start word

  };

} }

#endif /* EFORMAT_V31_ROSFRAGMENT_H */
