/**
 * @file v31_FullEventFragment.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief  
 */

#include "v31_FullEventFragment.h"

eformat::v31::FullEventFragment::FullEventFragment (const uint32_t* it)
  : eformat::v31::Header(it, eformat::v31::FULL_EVENT),
    m_start(eformat::v31::Header::specific_header())
{
}

 eformat::v31::FullEventFragment& 
eformat::v31::FullEventFragment::assign (const uint32_t* it)
{
  eformat::v31::Header::assign(it, eformat::v31::FULL_EVENT);
  m_start = eformat::v31::Header::specific_header();
  return *this;
}

bool eformat::v31::FullEventFragment::check (const uint16_t to_check) const
{
  eformat::v31::Header::check(to_check); //< first do a generic check
  if (eformat::v31::Header::nspecific() != 
      (14 + nlvl1_trigger_info() + nlvl2_trigger_info() +
       nevent_filter_info() + nstream_tag())) {
    throw EFORMAT_SIZE_CHECK((14 + nlvl1_trigger_info() +
                              nlvl2_trigger_info() +
                  			      nevent_filter_info() +
                              nstream_tag()),
			     eformat::v31::Header::nspecific());
  }
  return true;
}

bool eformat::v31::FullEventFragment::check_tree (const uint16_t to_check) const
{
  check(to_check); // check myself
  uint32_t total = eformat::v31::Header::nchildren();
  for (size_t i=0; i<total; ++i) {
    eformat::v31::SubDetectorFragment f(eformat::v31::Header::child(i));
    f.check_tree(to_check);
  }
  return true;
}

