//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v31_Header.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andre Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$ 
 *
 * @brief Defines the Header entity. The definition is based on the
 * update of ATL-DAQ-98-129, by D.Francis et al.
 */

#ifndef EFORMAT_V31_HEADER_H
#define EFORMAT_V31_HEADER_H

#include <stdint.h>
#include "eformat/Version.h"

namespace eformat { namespace v31 {

  /**
   * Contains the information on the Header of a fragment as described
   * by the original note. The header is a composite entity, build from
   * two parts:
   *
   * -# The generic part, containing the header type, size and version
   * information;
   * -# The specific part, containing data that is specific for a
   * particular fragment.
   */
  class Header {

  public:
   
   /**
     * To build a header given the containing buffer. I need to know
     * where the header starts in order to do that.
     *
     * @param it The exact position where this header should start.
     * @param match The word that this header should match.
     */
    Header (const uint32_t* it, uint32_t match);

    /**
     * Builds an empty, otherwise useless Header
     */
    Header () : m_start() {}

    /**
     * Copy constructor
     *
     * @param other The other header to construct from
     */
    Header (const Header& other) : m_start(other.m_start) {}

    /**
     * Destructor virtualisation
     */
    virtual ~Header() {}

    /**
     * Assigment operator
     *
     * @param other The other header to assign from
     */
    Header& operator= (const Header& other)
    { m_start = other.m_start; return *this; }

    /**
     * Reassign this header
     *
     * @param it The exact position where this header should start.
     * @param match The word that this header should match.
     */
    Header& assign (const uint32_t* it, uint32_t match);

    /**
     * Says if the generic part of the header is valid. This may throw
     * exceptions.
     */
    virtual bool check (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Returns the fragment type.
     */
    inline uint32_t marker() const { return m_start[0]; }

    /**
     * Returns the size, in words, of the current fragment.
     */
    inline uint32_t fragment_size_word() const { return m_start[1]; }

    /**
     * Returns the size, in words, of the current header. That does include
     * the specific part of the header.
     */
    inline uint32_t header_size_word() const { return m_start[2]; }

    /**
     * Returns the formatting version.
     */
    inline uint32_t version() const { return m_start[3]; }

    /**
     * Returns the full source identifier.
     */
    inline uint32_t source_id() const { return m_start[4]; }

    /**
     * Returns the number of status words available
     */
    inline uint32_t nstatus () const { return m_start[5]; }

    /**
     * Sets the pointer to my start
     *
     * @param it The pointer to set
     */
    inline const uint32_t* start () const { return m_start; }

    /**
     * Sets the pointer to where the payload starts
     *
     * @param it The pointer to set
     */
    inline const uint32_t* payload () const 
    { return m_start + header_size_word(); }

    /**
     * Sets the pointer to one-past my end position
     *
     * @param it The pointer to set
     */
    inline const uint32_t* end () const 
    { return m_start + fragment_size_word(); }

    /**
     * Returns the payload size
     */
    inline uint32_t payload_size_word (void) const
    { return fragment_size_word() - header_size_word(); }

    /**
     * Returns the status words, as an iterator to the status words available.
     *
     * @param it An <em>updateable</em> iterator you should provide.
     */
    inline const uint32_t* status () const { return m_start + 6; }


    /**
     * Returns the number of specific words available in the specific header
     * part 
     */
    inline uint32_t nspecific () const { return m_start[6 + nstatus()]; }

    /**
     * Returns an iterator to the start of the specific header part (this
     * does not include the number of specific header fragments)
     *
     * @param it An <em>updateable</em> iterator you should provide.
     */
    inline const uint32_t* specific_header () const
    { return m_start + 7 + nstatus(); }

    /**
     * Returns the number of children available.
     */
    virtual uint32_t nchildren () const;

    /**
     * Returns the nth child fragment. If the nth fragment doesn't exist, the
     * behaviour is undefined.
     *
     * @param n The fragment position, starting at zero, of the child fragment
     * you would like to get.
     */
    virtual const uint32_t* child (size_t n) const;

    /**
     * Returns the nth child fragment. If the nth fragment doesn't exist, an
     * exception is thrown. 
     *
     * @warning Use this method with care, it is slower than the equivalent
     * method that doesn't check.
     *
     * @param n The fragment position, starting at zero, of the child fragment
     * you would like to get.
     */
    virtual const uint32_t* child_check (size_t n) const;

    /**
     * Returns all the children of this fragment. The input to this method is a
     * valid set of iterators to existing, pre-allocated pointers
     *
     * @param p A pointer to a set of preallocated TPointer's to set to the
     * position of the children of this fragment.
     * @param max The maximum number of children, p can point to.
     *
     * @return The number of children found on this fragment
     */
    virtual uint32_t children (const uint32_t** p, size_t max) const;

  private: ///< representation

    const uint32_t* m_start; ///< my start word

  };

} }

#endif //EFORMAT_HEADER_H
