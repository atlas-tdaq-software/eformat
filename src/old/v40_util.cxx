//Dear emacs, this is -*- c++ -*-

/**
 * @file util.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author: rabello $
 * $Revision: 48966 $
 * $Date: 2008-04-25 12:52:53 +0200 (Fri, 25 Apr 2008) $
 *
 * Implements the utilities described in the equivalent header.
 */

#include "src/old/old_Issue.h"
#include "src/old/v40_util.h"
#include "src/old/v40_FullEventFragment.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/write/ROBFragment.h"
#include "eformat/ROBFragment.h"
#include "ers/ers.h"

uint32_t eformat::v40::convert(const uint32_t* src, uint32_t* dest,
    uint32_t max, const eformat::CheckSum& event_checksum,
    const eformat::CheckSum& rob_checksum)
{
  if (src[0] != eformat::v40::FULL_EVENT) {
    throw EFORMAT_WRONG_MARKER(src[0], eformat::v40::FULL_EVENT);
  }

  //check version
  helper::Version version(src[3]);
  if (version.major_version() == MAJOR_DEFAULT_VERSION) {
    memcpy(dest, src, sizeof(uint32_t)*src[1]);
    return src[1];
  }
  if (version.major_version() != MAJOR_V40_VERSION)
    throw EFORMAT_BAD_VERSION(version.major_version(), MAJOR_V40_VERSION);

  //this is from the old major version of eformat, proceed with conversion
  eformat::v40::FullEventFragment fe(src);
  fe.check(); //this may throw

  //create the base FullEvent
  eformat::write::FullEventFragment nfe(fe.source_id(), fe.bc_time_seconds(), 
					fe.bc_time_nanoseconds(), 
					fe.global_id(), fe.run_type(), 
					fe.run_no(), fe.lumi_block(), 
					fe.lvl1_id(), fe.bc_id(), 
					fe.lvl1_trigger_type());

  nfe.lvl1_trigger_info(fe.nlvl1_trigger_info(), fe.lvl1_trigger_info());
  nfe.lvl2_trigger_info(fe.nlvl2_trigger_info(), fe.lvl2_trigger_info());
  nfe.event_filter_info(fe.nevent_filter_info(), fe.event_filter_info());
  nfe.stream_tag(fe.nstream_tag(), fe.stream_tag());
  nfe.status(fe.nstatus(), fe.status());
  nfe.minor_version(version.minor_version());
  nfe.checksum_type(event_checksum);

  std::vector<eformat::write::ROBFragment*> acc_rob;
  for (size_t i=0; i<fe.nchildren(); ++i) {
    try {
      eformat::read::ROBFragment rob(fe.child(i));
      rob.check(MAJOR_V40_VERSION);
      eformat::write::ROBFragment* nrob = 
	new eformat::write::ROBFragment(rob.source_id(), 
					rob.rod_run_no(), 
					rob.rod_lvl1_id(), rob.rod_bc_id(), 
					rob.rod_lvl1_trigger_type(), 
					rob.rod_detev_type(), 
					rob.rod_ndata(), rob.rod_data(), 
					rob.rod_status_position());
      nrob->status(rob.nstatus(), rob.status());
      nrob->rod_status(rob.rod_nstatus(), rob.rod_status());
      helper::Version rob_version(rob.rod_version());
      nrob->minor_version(rob_version.minor_version());
      helper::Version rod_version(rob.rod_version());
      nrob->rod_minor_version(rod_version.minor_version());
      nrob->checksum_type(rob_checksum);

      //make this new ROB part of the new ROS
      nfe.append(nrob);
      //make sure we don't forget to delete this guy
      acc_rob.push_back(nrob);
    }
    catch (eformat::Issue& e) {
      ers::warning(e);
      ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROB","FULL_EVENT",0)); 
      //we skip this fragment, but do not loose the whole event
      continue;
    }
  }

  //now the FullEvent is in `nfe', bind
  const eformat::write::node_t* top = nfe.bind();
  //memcpy the list of pages into contiguous memory
  uint32_t retval = eformat::write::copy(*top, dest, max);

  //delete the allocated stuff
  for (size_t i=0; i<acc_rob.size(); ++i) delete acc_rob[i];

  return retval;
}

