//Dear emacs, this is -*- c++ -*-

/**
 * @file old/v24_Header.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the old Header class
 */

#include "src/old/v24_Header.h"
#include "eformat/HeaderMarker.h"
#include "eformat/Version.h"
#include "eformat/Issue.h"

eformat::v24::Header::Header (const uint32_t* it, uint32_t match)
  : m_start(it)
{
  //get endiannesso
  uint32_t m = marker();
  if (m != match) throw EFORMAT_WRONG_MARKER(m, match);
}

bool eformat::v24::Header::check () const
{
  if ( version() >> 16 != eformat::MAJOR_V24_VERSION )
    throw EFORMAT_BAD_VERSION(version() >> 16, eformat::MAJOR_V24_VERSION);
  if ( header_size_word() != (9 + nstatus() + noffset() + nspecific()) )
    throw EFORMAT_SIZE_CHECK(header_size_word(),
			     (9 + nstatus() + noffset() + nspecific()));
  return true;
}

const uint32_t* eformat::v24::Header::child (uint32_t n) const
{
  if (n >= noffset()) throw EFORMAT_NO_SUCH_CHILD(n, noffset());
  return &m_start[0xffffff & offset()[n]];
}




