/**
 * @file v31_ROBFragment.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief  
 */

#include "v31_ROBFragment.h"

const uint32_t eformat::v31::ROBFragment::NSPECIFIC = 0;

eformat::v31::ROBFragment::ROBFragment (const uint32_t* it)
  : eformat::v31::Header(it, eformat::v31::ROB),
    m_start(eformat::v31::Header::specific_header())
{
  if (rod_marker() != eformat::v31::ROD) 
    throw EFORMAT_WRONG_MARKER(rod_marker(), eformat::v31::ROD);
}

eformat::v31::ROBFragment&
eformat::v31::ROBFragment::assign (const uint32_t* it)
{
  eformat::v31::Header::assign(it, eformat::v31::ROB);
  m_start = eformat::v31::Header::specific_header();
  if (rod_marker() != eformat::v31::ROD) 
    throw EFORMAT_WRONG_MARKER(rod_marker(), eformat::v31::ROD);
  return *this;
}

bool eformat::v31::ROBFragment::check (const uint16_t to_check) const
{
  //ROB checking
  eformat::v31::Header::check(to_check); //< first do a generic check
  if (eformat::v31::Header::nspecific() != NSPECIFIC)
    throw EFORMAT_SIZE_CHECK(NSPECIFIC, 
			     eformat::v31::Header::nspecific());

  //ROD checking
  if ( rod_version() >> 16 != to_check )
    throw EFORMAT_BAD_VERSION(rod_version() >> 16, to_check);
  if ( rod_header_size_word() != 9 )
    throw EFORMAT_SIZE_CHECK(9, rod_header_size_word());
  if ( rod_fragment_size_word() != 12 + rod_nstatus() + rod_ndata() )
    throw EFORMAT_SIZE_CHECK(rod_fragment_size_word(), 
			     (12 + rod_nstatus() + rod_ndata()));
  return true;
}

uint32_t eformat::v31::ROBFragment::children (const uint32_t** p, size_t max) 
  const
{
  if (max == 0) return 0;
  p[0] = m_start;
  return 1;
}

