//Dear emacs, this is -*- c++ -*-

/**
 * @file old/v24_ROBFragment.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the old eformat 2.4 ROB fragment
 */

#include "src/old/v24_ROBFragment.h"
#include "src/old/v24_RODFragment.h"
#include "eformat/Issue.h"
#include "eformat/HeaderMarker.h"

eformat::v24::ROBFragment::ROBFragment (const uint32_t* it)
  : eformat::v24::Header(it, eformat::ROB),
    m_start(specific_header())
{
}

bool eformat::v24::ROBFragment::check () const
{
  eformat::v24::Header::check(); //< first do a generic check
  if (nspecific() != 4) throw EFORMAT_SIZE_CHECK(4, nspecific());
  return true;
}

bool eformat::v24::ROBFragment::check_tree () const
{
  check(); // check myself
  for (unsigned int i=0; i<noffset(); ++i) {
    unsigned int rod_size = 0;
    //typical
    if (noffset() == 1) rod_size = fragment_size_word() - header_size_word();
    else { //more atypical, have to calculate
      if (i != noffset() - 1) rod_size = (0xffffff & offset()[i+1]);
      else rod_size = fragment_size_word();
      rod_size -= (0xffffff & offset()[i]);
    }
    eformat::v24::RODFragment f(child(i), rod_size);
    f.check();
  }
  return true;
}

eformat::v24::RODFragment eformat::v24::ROBFragment::rod (uint32_t n) const
{
  check();
  size_t rod_size = 0;
  //typical
  if (noffset() == 1) 
    rod_size = fragment_size_word() - header_size_word();
  else { //more atypical, have to calculate
    if (n != noffset() - 1) rod_size = (0xffffff & offset()[n+1]);
    else rod_size = fragment_size_word();
    rod_size -= (0xffffff & offset()[n]);
  }
  eformat::v24::RODFragment f(child(n), rod_size);
  f.check();
  return f;
}



