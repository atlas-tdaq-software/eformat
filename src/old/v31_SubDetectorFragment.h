//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v31_SubDetectorFragment.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines the subdetector fragment entity. The definition is based on
 * the update of ATL-DAQ-98-129, by D.Francis et al.
 */

#ifndef EFORMAT_V31_SUBDETECTORFRAGMENT_H
#define EFORMAT_V31_SUBDETECTORFRAGMENT_H

#include "src/old/v31_Header.h"
#include "src/old/v31_HeaderMarker.h"
#include "src/old/v31_ROSFragment.h"
#include "eformat/Issue.h"

namespace eformat { namespace v31 {

  /**
   * Describes how to access the contents of a subdetector fragment, as
   * prescribed by the event format note.
   */
  
  class SubDetectorFragment : public Header {

  public: //interface

   /**
     * To build a fragment given the containing buffer. I need to know
     * where the fragment starts in order to do that.
     *
     * @param it The exact position where this fragment should start.
     */
    SubDetectorFragment (const uint32_t* it);

    /**
     * Builds an empty, otherwise useless SubDetectorFragment
     */
    SubDetectorFragment () : Header(), m_start() {}

    /**
     * Copy constructor
     *
     * @param other The fragment to be copied
     */
    SubDetectorFragment (const SubDetectorFragment& other) 
      : Header(other), m_start(other.m_start) {}

    /**
     * Destructor virtualisation
     */
    virtual ~SubDetectorFragment() {}

    /**
     * Copy operator
     *
     * @param other The fragment to be copied
     */
    SubDetectorFragment& operator= (const SubDetectorFragment& other) 
    { Header::operator=(other); m_start=other.m_start; return *this; }

    /**
     * Manual re-assignment
     *
     * @param it The position pointing the first word of this fragment
     */
    SubDetectorFragment& assign (const uint32_t* it);

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    virtual bool check (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    bool check_tree (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

  private: //static stuff

    static const uint32_t NSPECIFIC;

  private: //representation

    const uint32_t* m_start; ///< my start word

  };

} }

 
#endif /* EFORMAT_V31_SUBDETECTORFRAGMENT_H */
