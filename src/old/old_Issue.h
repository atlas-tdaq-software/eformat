/**
 * @file old_Issue.h
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Issues that can be used when converting data from old to new format.
 */

#ifndef EFORMAT_OLD_ISSUE_H 
#define EFORMAT_OLD_ISSUE_H

#include "eformat/Issue.h"

/**
 * This exception is supposed to be thrown when I decide to skip a fragment 
 */
ERS_DECLARE_ISSUE_BASE(eformat, SkippedFragmentIssue, eformat::Issue,
		       "Skipping " << ftype << " fragment in " << ptype
           << " fragment with identifier 0x" 
           << EFORMAT_HEX(source_id), ERS_EMPTY, 
           ((std::string) ftype) ((std::string) ptype) ((uint32_t) source_id))

/**
 * Simplifies the use of this Issue
 *
 * @param ftype A string representing the fragment type
 * @param sid The source identifier for the skipped fragment
 */
#define EFORMAT_SKIPPED_FRAGMENT(ftype,ptype,sid) \
  eformat::SkippedFragmentIssue(ERS_HERE, ftype, ptype, sid)


#endif /* EFORMAT_OLD_ISSUE_H */

