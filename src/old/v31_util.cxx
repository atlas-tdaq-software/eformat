//Dear emacs, this is -*- c++ -*-

/**
 * @file util.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the utilities described in the equivalent header.
 */

#include "src/old/old_Issue.h"
#include "src/old/v31_util.h"
#include "src/old/v31_FullEventFragment.h"
#include "src/old/v31_SubDetectorFragment.h"
#include "src/old/v31_ROSFragment.h"
#include "src/old/v31_ROBFragment.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/write/ROBFragment.h"
#include "ers/ers.h"

uint32_t eformat::v31::convert_source (uint32_t v31_id)
{
  uint8_t sd = (v31_id >> 16) & 0xff;
  if (sd == 0x81) sd = 0xff; //OTHER
  uint32_t retval = sd;
  retval <<= 16;
  retval |= (v31_id & 0xffff);
  ERS_DEBUG(3, "Source identifier " << EFORMAT_HEX(v31_id) 
      << "  [v3.1] was converted to " << EFORMAT_HEX(retval) 
      << " [v4.0].");
  return retval;
}

/**
 * Converts a ROS fragment, from the old to new format, using the space of
 * contiguous memory storage area given. If the event given is already on v4.0
 * format, no conversion takes place.
 *
 * @param src A pointer to the first word of the fragment, lying in a @b
 * contiguous area of memory.
 * @param dest The destination area of memory, preallocated
 * @param max The maximum number of words that fit in the preallocated
 * memory area "dest".
 * @param event_checksum What type of checksum to deploy for full events
 * @param rob_checksum What type of checksum to deploy for rob fragments
 *
 * @return A counter, for the number of words copied from the source to the
 * destination. If that number is zero, something wrong happened.
 */
static uint32_t convert_ros(const uint32_t* src, uint32_t* dest, uint32_t max,
        const eformat::CheckSum& event_checksum=eformat::NO_CHECKSUM,
        const eformat::CheckSum& rob_checksum=eformat::NO_CHECKSUM)
{
  using namespace eformat;

  if (src[0] != eformat::v31::ROS) {
    throw EFORMAT_WRONG_MARKER(src[0], eformat::v31::ROS);
  }

  //check version
  helper::Version version(src[3]);
  if (version.major_version() == MAJOR_DEFAULT_VERSION) {
    memcpy(dest, src, sizeof(uint32_t)*src[1]);
    return src[1];
  }

  if (version.major_version() != MAJOR_V31_VERSION)
    throw EFORMAT_BAD_VERSION(version.major_version(), MAJOR_V31_VERSION);

  //this is from the old major version of eformat, proceed with conversion
  eformat::v31::ROSFragment ros(src);
  ros.check(); //this may throw

  //create the base FullEvent
  eformat::write::FullEventFragment nfe(eformat::v31::convert_source(ros.source_id()), 0, 0, 0, 0, ros.run_no(), 0, ros.lvl1_id(), ros.bc_id(), 0);
  nfe.status(ros.nstatus(), ros.status());
  nfe.minor_version(version.minor_version());
  nfe.checksum_type(event_checksum);

  std::vector<eformat::write::ROBFragment*> acc_rob;
  for (size_t k=0; k<ros.nchildren(); ++k) {
    try {
      eformat::v31::ROBFragment rob(ros.child(k));
      rob.check(); //this may throw
      eformat::write::ROBFragment* nrob = 
        new eformat::write::ROBFragment(eformat::v31::convert_source(rob.source_id()), rob.rod_run_no(), 
          rob.rod_lvl1_id(), rob.rod_bc_id(), rob.rod_lvl1_trigger_type(), 
          rob.rod_detev_type(), rob.rod_ndata(), rob.rod_data(), 
          rob.rod_status_position()); 
      nrob->status(rob.nstatus(), rob.status());
      nrob->rod_status(rob.rod_nstatus(), rob.rod_status());
      helper::Version rob_version(rob.rod_version());
      nrob->minor_version(rob_version.minor_version());
      helper::Version rod_version(rob.rod_version());
      nrob->rod_minor_version(rod_version.minor_version());
      nrob->checksum_type(rob_checksum);
      //make this new ROB part of the new full event 
      nfe.append(nrob);
      //make sure we don't forget to delete this guy
      acc_rob.push_back(nrob);
    }
    catch (eformat::Issue& e) {
      ers::warning(e);
      ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROB", "ROS", ros.source_id())); 
      //we skip this fragment, but do not loose the whole event
      continue;
    }
  }

  //now the FullEvent is in `nfe', bind
  const eformat::write::node_t* top = nfe.bind();
  //memcpy the list of pages into contiguous memory
  uint32_t retval = eformat::write::copy(*top, dest, max);

  //delete the allocated stuff
  for (size_t i=0; i<acc_rob.size(); ++i) delete acc_rob[i];

  return retval;
}

uint32_t eformat::v31::convert(const uint32_t* src, uint32_t* dest,
    uint32_t max, const eformat::CheckSum& event_checksum,
    const eformat::CheckSum& rob_checksum)
{
  if (src[0] != eformat::v31::FULL_EVENT) {
    if (src[0] != eformat::v31::ROS) {
      throw EFORMAT_WRONG_MARKER(src[0], eformat::v31::FULL_EVENT);
    }
    //call a specialized function
    return convert_ros(src, dest, max, event_checksum, rob_checksum);
  }

  //check version
  helper::Version version(src[3]);
  if (version.major_version() == MAJOR_DEFAULT_VERSION) {
    memcpy(dest, src, sizeof(uint32_t)*src[1]);
    return src[1];
  }
  if (version.major_version() != MAJOR_V31_VERSION)
    throw EFORMAT_BAD_VERSION(version.major_version(), MAJOR_V31_VERSION);

  //this is from the old major version of eformat, proceed with conversion
  eformat::v31::FullEventFragment fe(src);
  fe.check(); //this may throw

  //create the base FullEvent
  eformat::write::FullEventFragment nfe(eformat::v31::convert_source(fe.source_id()), fe.bc_time_seconds(), 
      fe.bc_time_nanoseconds(), fe.global_id(), fe.run_type(), fe.run_no(), 
      fe.lumi_block(), fe.lvl1_id(), fe.bc_id(), fe.lvl1_trigger_type());
  nfe.lvl1_trigger_info(fe.nlvl1_trigger_info(), fe.lvl1_trigger_info());
  nfe.lvl2_trigger_info(fe.nlvl2_trigger_info(), fe.lvl2_trigger_info());
  nfe.event_filter_info(fe.nevent_filter_info(), fe.event_filter_info());
  nfe.stream_tag(fe.nstream_tag(), fe.stream_tag());
  nfe.status(fe.nstatus(), fe.status());
  nfe.minor_version(version.minor_version());
  nfe.checksum_type(event_checksum);

  std::vector<eformat::write::ROBFragment*> acc_rob;
  for (size_t i=0; i<fe.nchildren(); ++i) {
    try {
      eformat::v31::SubDetectorFragment sd(fe.child(i));
      sd.check();
      for (size_t j=0; j<sd.nchildren(); ++j) {
        try {
          eformat::v31::ROSFragment ros(sd.child(j));
          ros.check();
          for (size_t k=0; k<ros.nchildren(); ++k) {
            try {
              eformat::v31::ROBFragment rob(ros.child(k));
              rob.check();
              eformat::write::ROBFragment* nrob = 
                new eformat::write::ROBFragment(eformat::v31::convert_source(rob.source_id()), 
                      rob.rod_run_no(), 
                      rob.rod_lvl1_id(), rob.rod_bc_id(), 
                      rob.rod_lvl1_trigger_type(), 
                      rob.rod_detev_type(), rob.rod_ndata(), rob.rod_data(), 
                      rob.rod_status_position()); 
              nrob->status(rob.nstatus(), rob.status());
              nrob->rod_status(rob.rod_nstatus(), rob.rod_status());
              helper::Version rob_version(rob.rod_version());
              nrob->minor_version(rob_version.minor_version());
              helper::Version rod_version(rob.rod_version());
              nrob->rod_minor_version(rod_version.minor_version());
              nrob->checksum_type(rob_checksum);

              //make this new ROB part of the new ROS
              nfe.append(nrob);
              //make sure we don't forget to delete this guy
              acc_rob.push_back(nrob);
            }
            catch (eformat::Issue& e) {
              ers::warning(e);
              ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROB", "ROS", ros.source_id())); 
              //we skip this fragment, but do not loose the whole event
              continue;
            }
          }
        }
        catch (eformat::Issue& e) {
          ers::warning(e);
          ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROS", "SUBDETECTOR", sd.source_id())); 
          //we skip this fragment, but do not loose the whole event
          continue;
        }
      }
    }
    catch (eformat::Issue& e) {
      ers::warning(e);
      ers::warning(EFORMAT_SKIPPED_FRAGMENT("SUBDETECTOR", "FULLEVENT", fe.lvl1_id())); 
      //we skip this fragment, but do not loose the whole event
      continue;
    }
  }

  //now the FullEvent is in `nfe', bind
  const eformat::write::node_t* top = nfe.bind();
  //memcpy the list of pages into contiguous memory
  uint32_t retval = eformat::write::copy(*top, dest, max);

  //delete the allocated stuff
  for (size_t i=0; i<acc_rob.size(); ++i) delete acc_rob[i];

  return retval;
}

