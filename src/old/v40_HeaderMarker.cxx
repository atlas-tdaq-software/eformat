//Dear emacs, this is -*- c++ -*-

/**
 * @file HeaderMarker.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author: rabello $
 * $Revision: 44797 $
 * $Date: 2008-02-01 17:18:10 +0100 (Fri, 01 Feb 2008) $
 *
 * Implements a few functionality around the header marker
 */

#include "v40_HeaderMarker.h"

eformat::v40::HeaderMarker eformat::v40::child_marker(eformat::v40::HeaderMarker e)
{
  using namespace eformat::v40;
  switch (e) {
  case FULL_EVENT:
    return ROB;
  case ROB:
    return ROD;
  default:
    break;
  }
  return FULL_EVENT;
}

std::string eformat::v40::marker2string (const eformat::v40::HeaderMarker& e)
{
  using namespace eformat::v40;
  switch (e) {
  case ROD:
    return "ROD";
  case ROB:
    return "ROB";
  case FULL_EVENT:
    return "FULL_EVENT";
  }
  return "UNKNOWN_MARKER";
}

std::string eformat::v40::marker2string (uint32_t e)
{
  return eformat::v40::marker2string((eformat::v40::HeaderMarker)e);
}
