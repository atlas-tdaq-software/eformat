//Dear emacs, this is -*- c++ -*-

/**
 * @file old/v24_SubDetectorFragment.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the old SubDetector fragment interface
 */

#include "src/old/v24_SubDetectorFragment.h"
#include "src/old/v24_ROSFragment.h"
#include "eformat/Issue.h"
#include "src/old/v31_HeaderMarker.h"

eformat::v24::SubDetectorFragment::SubDetectorFragment (const uint32_t* it)
  : eformat::v24::Header(it, eformat::v31::SUB_DETECTOR),
    m_start(specific_header())
{ 
}

bool eformat::v24::SubDetectorFragment::check () const
{
  eformat::v24::Header::check(); //< first do a generic check
  if (nspecific() != 1) throw EFORMAT_SIZE_CHECK(1, nspecific());
  return true;
}

bool eformat::v24::SubDetectorFragment::check_tree () const
{
  check(); // check myself
  for (size_t i=0; i<noffset(); ++i) {
    eformat::v24::ROSFragment ros(child(i));
    ros.check_tree();
  }
  return true;
}



