//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/util.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andre Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Implements a set of utilities for event conversion.
 */

#include "eformat/old/util.h"
#include "eformat/util.h"
#include "eformat/Version.h"
#include "eformat/Issue.h"

//for the conversion
#include "src/old/v31_HeaderMarker.h"
#include "src/old/v24_util.h"
#include "src/old/v30_util.h"
#include "src/old/v31_util.h"
#include "src/old/v40_util.h"

uint32_t eformat::old::convert(const uint32_t* src, uint32_t* dest, 
    uint32_t max, const eformat::CheckSum& event_checksum,
    const eformat::CheckSum& rob_checksum) {
  /**
   * The conversion procedure can be executed in phases or in a single
   * step. In either case they are transparent to the user. The difference 
   * relies on the fact that conversion may take multiple conversion steps 
   * from the original fragment format into some intermediate format(s) and 
   * then to the current valid format. If the upgrade from the previous 
   * version to the current version was "easy", then we can be straight 
   * forward. 
   **/
  switch ((eformat::v31::HeaderMarker)eformat::peek_type(src)) {
    case eformat::v31::FULL_EVENT:
    case eformat::v31::ROS:
      switch (eformat::peek_major_version(src)) {
        case eformat::MAJOR_V40_VERSION:
          return eformat::v40::convert(src, dest, max, event_checksum, rob_checksum);
        case eformat::MAJOR_V31_VERSION:
          return eformat::v31::convert(src, dest, max, event_checksum, rob_checksum);
          break;
        case eformat::MAJOR_V30_VERSION:
          return eformat::v30::convert(src, dest, max, event_checksum, rob_checksum);
          break;
        case eformat::MAJOR_V24_VERSION:
          return eformat::v24::convert(src, dest, max, event_checksum, rob_checksum);
          break;
        default:
          throw EFORMAT_BAD_VERSION(eformat::peek_major_version(src), 
              eformat::MAJOR_DEFAULT_VERSION);
          break;
      }
    default:
      throw EFORMAT_UNSUPPORTED_OPERATION("convert from old format", src[0]);
  }
  return 0; //this should not occur...
}
