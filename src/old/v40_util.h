//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v31_util.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author: rabello $
 * $Revision: 44971 $
 * $Date: 2008-02-05 22:47:15 +0100 (Tue, 05 Feb 2008) $
 *
 * @brief A set of utilities to convert old eformat (v4.0) tags into new ones.
 */

#ifndef EFORMAT_V40_UTIL_H
#define EFORMAT_V40_UTIL_H

#include <stdint.h>
#include "eformat/checksum.h"

namespace eformat {

  namespace v40 {

    
    /**
     * Gets an old source identitifier (v4.0) and transforms it into a new one
     *
     * @param old_id The old source identifier
     */
    uint32_t convert_source (uint32_t old_id);

    /**
     * Converts a full event fragment, from the old to new format, using the
     * space of contiguous memory storage area given. If the event given is
     * already on the current format, no conversion takes place.
     *
     * @param src A pointer to the first word of the event, lying in a @b
     * contiguous area of memory.
     * @param dest The destination area of memory, preallocated
     * @param max The maximum number of words that fit in the preallocated
     * memory area "dest".
     * @param event_checksum What type of checksum to deploy for full events
     * @param rob_checksum What type of checksum to deploy for rob fragments
     *
     * @return A counter, for the number of words copied from the source to the
     * destination. If that number is zero, something wrong happened.
     */
    uint32_t convert(const uint32_t* src, uint32_t* dest, uint32_t max,
        const eformat::CheckSum& event_checksum=eformat::NO_CHECKSUM,
        const eformat::CheckSum& rob_checksum=eformat::NO_CHECKSUM);

  }

}

#endif /* EFORMAT_V40_UTIL_H */
