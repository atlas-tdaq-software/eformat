//Dear emacs, this is -*- c++ -*-

/**
 * @file eformat/old/v24_Issue.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief All issues from the eformat::v24 namespace are here.
 */

#ifndef EFORMAT_V24_ISSUE_H
#define EFORMAT_V24_ISSUE_H

#include "eformat/Issue.h"

/**
 * This exception is supposed to be thrown when I find a v24 source
 * identifier I cannot map to a new one
 */
ERS_DECLARE_ISSUE_BASE(eformat, UnboundSourceIdentifierIssue, eformat::Issue,
		       "Unbound source identifier 0x" << EFORMAT_HEX(source_id), ERS_EMPTY, 
           ((uint32_t) source_id))

/**
 * Simplifies the use of this Issue
 *
 * @param sid The source identifier I cannot map
 */
#define EFORMAT_UNBOUND_SOURCE_IDENTIFIER(sid) \
  eformat::UnboundSourceIdentifierIssue(ERS_HERE, sid)

#endif /* EFORMAT_V24_ISSUE_H */
