//Dear emacs, this is -*- c++ -*-

/**
 * @file old/v24_FullEventFragment.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the old FullEventFragment class
 */

#include "src/old/v24_FullEventFragment.h"
#include "src/old/v24_SubDetectorFragment.h"
#include "eformat/Issue.h"
#include "eformat/HeaderMarker.h"

eformat::v24::FullEventFragment::FullEventFragment (const uint32_t* it)
  : eformat::v24::Header(it, eformat::FULL_EVENT),
    m_start(specific_header())
{
}

bool eformat::v24::FullEventFragment::check () const
{
  eformat::v24::Header::check(); //< first do a generic check
  if (nspecific() != 10) { //@warning THIS IS WRONG!!! It should be 9
    throw EFORMAT_SIZE_CHECK(10, nspecific());
  }
  return true;
}

bool eformat::v24::FullEventFragment::check_tree () const
{
  check(); // check myself
  for (size_t i=0; i<noffset(); ++i) {
    eformat::v24::SubDetectorFragment sd(child(i));
    sd.check_tree();
  }
  return true;
}



