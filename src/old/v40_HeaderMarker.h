//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v40/HeaderMarker.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andre DOS ANJOS</a>
 * $Author: rabello $
 * $Revision: 44797 $
 * $Date: 2008-02-01 17:18:10 +0100 (Fri, 01 Feb 2008) $ 
 *
 * @brief Defines the constants used by Event Fragments.
 */

#ifndef EFORMAT_V40_HEADERMARKER_H
#define EFORMAT_V40_HEADERMARKER_H

#include <stdint.h>
#include <string>

namespace eformat { namespace v40 {

  /**
   * The types of header markers available. They are all 32 bit
   * numbers, with 1234 in the middle for the identification of byte
   * ordering (endianness).
   */
  enum HeaderMarker { 
          ROD = 0xee1234ee, ///< The ROD marker
	  ROB = 0xdd1234dd, ///< The ROB marker
	  FULL_EVENT = 0xaa1234aa ///< The event marker
  };
 
  /**
   * Returns the child marker of a given parent marker
   *
   * @param e The marker from which you would like to the get child fragment
   * type from
   */
  HeaderMarker child_marker(HeaderMarker e);

  /**
   * Returns a string that represents the string of the equivalent marker
   *
   * @param e The enumeration value
   */
  std::string marker2string (const HeaderMarker& e);

  /**
   * Returns a string that represents the string of the equivalent marker
   *
   * @param e The enumeration value
   */
  std::string marker2string (uint32_t e);

} }

#endif //EFORMAT_V40_HEADERMARKER_H
