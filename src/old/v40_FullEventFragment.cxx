/**
 * @file FullEventFragmentNoTemplates.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Implements full event fragments without using templates to ease
 * maintenance (patching).
 */

#include "v40_FullEventFragment.h"

eformat::v40::FullEventFragment::FullEventFragment (const uint32_t* it)
  : eformat::v40::Header(it,eformat::v40::FULL_EVENT), 
    m_start(eformat::v40::Header::specific_header())
{
}

eformat::v40::FullEventFragment::FullEventFragment () 
  : Header(), m_start() 
{
}

eformat::v40::FullEventFragment::~FullEventFragment ()
{
}

eformat::v40::FullEventFragment& eformat::v40::FullEventFragment::assign 
(const uint32_t* it)
{
  eformat::v40::Header::assign(it, eformat::v40::FULL_EVENT);
  eformat::v40::Header::specific_header();
  return *this;
}


bool 
eformat::v40::FullEventFragment::check(const uint16_t version) const
{
  
  bool result = true;

  //Fragment checking, only this header
  if (eformat::v40::Header::marker() != eformat::FULL_EVENT) {
    throw EFORMAT_WRONG_MARKER(marker(), eformat::FULL_EVENT);
  }
  
  if (eformat::v40::Header::version() >> 16 != version) {
    throw EFORMAT_BAD_VERSION(eformat::v40::Header::version() >> 16, version);
  }
  
  uint32_t calculated = 20 + eformat::v40::Header::nstatus() + 
    nlvl1_trigger_info() + nlvl2_trigger_info() + nevent_filter_info() + 
    nstream_tag();
  if (eformat::v40::Header::header_size_word() != calculated) {
    throw EFORMAT_SIZE_CHECK(calculated,  eformat::v40::Header::header_size_word());
  }
  return result;
}

bool 
eformat::v40::FullEventFragment::check_tree(const uint16_t version, const uint16_t rod_version) const
{

  if (!check(version)) return false; // check myself
  eformat::v40::FullEventFragment::child_iter_t iter = child_iter();
  while (const uint32_t* fp = iter.next()) {
    eformat::read::ROBFragment f(fp);
    bool rob_check;
    try{
      rob_check = f.check(version, rod_version);
    }catch(eformat::RODSizeCheckIssue &ex){
      //In case of a truncated ROD fragment, we ignore and continue
      //Anyway the ROB is good, hence we can safely navigate the event
      continue;
    }    
    if (!rob_check) return false;
  }
  return true;
}

uint32_t eformat::v40::FullEventFragment::bc_time_seconds() const 
{ 
  return m_start[0]; 
}

uint32_t eformat::v40::FullEventFragment::bc_time_nanoseconds() const 
{ 
  return m_start[1]; 
}

uint32_t eformat::v40::FullEventFragment::global_id() const 
{ 
  return m_start[2]; 
}

uint32_t eformat::v40::FullEventFragment::run_type() const 
{ 
  return m_start[3]; 
}

uint32_t eformat::v40::FullEventFragment::run_no() const 
{ 
  return m_start[4]; 
}

uint16_t eformat::v40::FullEventFragment::lumi_block() const 
{ 
  return m_start[5]; 
}

uint32_t eformat::v40::FullEventFragment::lvl1_id() const
{ 
  return m_start[6]; 
}

uint16_t eformat::v40::FullEventFragment::bc_id() const
{ 
  return m_start[7]; 
}

uint8_t eformat::v40::FullEventFragment::lvl1_trigger_type() const
{ 
  return m_start[8]; 
}

uint32_t eformat::v40::FullEventFragment::nlvl1_trigger_info() const
{ 
  return m_start[9]; 
}

const uint32_t* eformat::v40::FullEventFragment::lvl1_trigger_info () const 
{
  return m_start + 10;
}

uint32_t eformat::v40::FullEventFragment::nlvl2_trigger_info() const
{ 
  return m_start[10+nlvl1_trigger_info()]; 
}

const uint32_t* eformat::v40::FullEventFragment::lvl2_trigger_info () const
{
  return m_start + 11 + nlvl1_trigger_info();
}

uint32_t eformat::v40::FullEventFragment::nevent_filter_info () const
{ 
  return m_start[11+nlvl1_trigger_info()+nlvl2_trigger_info()]; 
}

const uint32_t* eformat::v40::FullEventFragment::event_filter_info () const 
{
  return m_start + 12 + nlvl1_trigger_info() + nlvl2_trigger_info();
}

uint32_t eformat::v40::FullEventFragment::nstream_tag () const
{ 
  return m_start[12+nlvl1_trigger_info() + 
    nlvl2_trigger_info() + nevent_filter_info()]; 
}

const uint32_t* eformat::v40::FullEventFragment::stream_tag () const 
{
  return m_start + 13 + nlvl1_trigger_info() + nlvl2_trigger_info() + 
    nevent_filter_info();
}

