//Dear emacs, this is -*- c++ -*-

/**
 * @file old/v24_ROSFragment.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the old ROS fragment, as defined in eformat 2.4
 */

#include "src/old/v24_ROSFragment.h"
#include "src/old/v24_ROBFragment.h"
#include "eformat/Issue.h"
#include "src/old/v31_HeaderMarker.h"

eformat::v24::ROSFragment::ROSFragment (const uint32_t* it)
  : eformat::v24::Header(it, eformat::v31::ROS),
    m_start(specific_header())
{
}

bool eformat::v24::ROSFragment::check () const
{
  eformat::v24::Header::check(); //< first do a generic check
  if (nspecific() != 2) {
    throw EFORMAT_SIZE_CHECK(2, nspecific());
  }
  return true;
}

bool eformat::v24::ROSFragment::check_tree () const
{
  check(); // check myself
  for (size_t i=0; i<noffset(); ++i) {
    eformat::v24::ROBFragment rob(child(i));
    rob.check_tree();
  }
  return true;
}




