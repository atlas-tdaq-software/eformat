//Dear emacs, this is -*- c++ -*-

/**
 * @file eformat/FullEventFragment.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author: rabello $
 * $Revision: 88361 $
 * $Date: 2010-01-14 18:28:09 +0100 (Thu, 14 Jan 2010) $
 *
 * @brief Defines the Event Fragment entity. The definition is based on the
 * update of ATL-DAQ-98-129, by D.Francis et al.
 */


#ifndef EFORMAT_V40_FULLEVENTFRAGMENT_H
#define EFORMAT_V40_FULLEVENTFRAGMENT_H

#include "v40_Header.h"
#include "v40_HeaderMarker.h"
#include "eformat/ROBFragment.h"
#include "eformat/Issue.h"

namespace eformat { namespace v40 {

    namespace detail {
      /**
       *  Iterator for child fragments
       */
      struct FullEventFragment_child_iter {
	FullEventFragment_child_iter(const uint32_t* begin, const uint32_t* end) : m_cur(begin), m_end(end) {}
	
	// return pointer to next fragment, 0 when done
	const uint32_t* next() {
	  const uint32_t* p = 0;
	  if (m_cur < m_end && (*m_cur) == eformat::ROB) {
	    p = m_cur;
	    // advance current position for next iteration
	    m_cur += m_cur[1];
	  }
	  return p;
	}
      private:
	const uint32_t* m_cur;
	const uint32_t* m_end;
      };
      
    } // namespace detail
    
    
    /**
     * Describes how to access the contents of an event fragment, as prescribed
     * by the event format note.
     */
    class FullEventFragment : public eformat::v40::Header {
      
    public: //interface

      typedef detail::FullEventFragment_child_iter child_iter_t;

      /**
       * To build a fragment given the containing buffer. I need to know
       * where the fragment starts in order to do that.
       *
       * @param it The exact position where this fragment should start.
       */
      FullEventFragment (const uint32_t* it);

      /**
       * Builds an empty, otherwise useless FullEventFragment
       */
      FullEventFragment ();

      /**
       * Copy constructor
       *
       * @param other The fragment to be copied
       */
      FullEventFragment (const FullEventFragment& other) = default; 
 
      /**
       * Destructor virtualisation
       */
      virtual ~FullEventFragment();

      /**
       * Copy operator
       *
       * @param other The fragment to be copied
       */
      FullEventFragment& operator= (const FullEventFragment& other) = default;

      /**
       * Manual re-assignment
       *
       * @param it The position pointing the first word of this fragment
       */
      FullEventFragment& assign (const uint32_t* it);


      /**
       *  Returns iterator object for all fragments contained inside this full event fragment.
       */
      child_iter_t child_iter() const {
	const uint32_t* begin = payload();
	return child_iter_t(begin, begin+payload_size_word());
      }
 
      /**
       * Says if the the fragment is valid. This method throw exceptions.
       */
      bool check (const uint16_t version=eformat::MAJOR_V40_VERSION) const;

    
      /**
       * Says if the Fragment and all children are valid. This throws exceptions.
       */
      bool check_tree (const uint16_t version=eformat::MAJOR_V40_VERSION,
		       const uint16_t rod_version=eformat::MAJOR_V31_VERSION) const;
      
      /**
       * Returns amount of seconds since the (unix) origin, for the bunch
       * crossing time.
       */
      uint32_t bc_time_seconds() const; 

      /**
       * Returns amount of nanoseconds aditionally to the amount of seconds
       * returned by bc_time_seconds, for the bunch crossing time.
       */
      uint32_t bc_time_nanoseconds() const; 

      /**
       * Returns the global identifier
       */
      uint32_t global_id() const;

      /**
       * Returns the run type
       */
      uint32_t run_type() const;

      /**
       * Returns the run number
       */
      uint32_t run_no() const;

      /**
       * Returns the luminosity block number
       */
      uint16_t lumi_block() const;

      /**
       * Returns the lvl1 identifier
       */
      uint32_t lvl1_id() const;

      /**
       * Returns the bunch crossing identifier
       */
      uint16_t bc_id() const;

      /**
       * Returns the lvl1 trigger type
       */
      uint8_t lvl1_trigger_type() const;

      /**
       * Returns the number of lvl1 trigger info words
       */
      uint32_t nlvl1_trigger_info() const;

      /**
       * Returns an iterator to the first of the LVL1 trigger info words
       *
       * @param it An <em>updateable</em> iterator you should provide.
       */
      const uint32_t* lvl1_trigger_info() const;
      
      /**
       * Returns the number of lvl2 trigger info words
       */
      uint32_t nlvl2_trigger_info() const;

      /**
       * Returns an iterator to the first of the LVL2 trigger info words
       *
       * @param it An <em>updateable</em> iterator you should provide.
       */
      const uint32_t* lvl2_trigger_info() const;
      
      /**
       * Returns the number of Event Filter words
       */
      uint32_t nevent_filter_info () const;

      /**
       * Returns an iterator to the first of the EF info words
       *
       * @param it An <em>updateable</em> iterator you should provide.
       */
      const uint32_t* event_filter_info() const;
      
      /**
       * Returns the number of stream tag words
       */
      uint32_t nstream_tag () const;

      /**
       * Returns an iterator to the first of the stream tag words
       *
       * @param it An <em>updateable</em> iterator you should provide.
       */
      const uint32_t* stream_tag() const;
      inline void stream_tag (const uint32_t*& it) const //legacy
      { it = stream_tag(); }
   
    private: //representation

      const uint32_t* m_start; ///< my start word

    };

  }
}

#endif /* EFORMAT_V40_FULLEVENTFRAGMENT_H */
