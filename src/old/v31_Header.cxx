/**
 * @file v31_Header.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 */

#include "eformat/util.h"
#include "eformat/Issue.h"
#include "eformat/SourceIdentifier.h"
#include "ers/ers.h"
#include "v31_Header.h"
#include "v31_HeaderMarker.h"

eformat::v31::Header::Header (const uint32_t* it, uint32_t match)
  : m_start(it)
{
  if (marker() != match) {
    throw EFORMAT_WRONG_MARKER(marker(), match);
  }
}

eformat::v31::Header& eformat::v31::Header::assign (const uint32_t* it, uint32_t match)
{
  m_start = it;
  if (marker() != match) {
    throw EFORMAT_WRONG_MARKER(marker(), match);
  }
  return *this;
}

bool eformat::v31::Header::check (const uint16_t to_check) const
{
  if ( version() >> 16 != to_check )
    throw EFORMAT_BAD_VERSION(version() >> 16, to_check);
  if ( header_size_word() != (7 + nstatus() + nspecific()) )
    throw EFORMAT_SIZE_CHECK(header_size_word(),
			     (7 + nstatus() + nspecific()));
  return true;
}

uint32_t eformat::v31::Header::nchildren () const
{
  return children(0,0);
}

const uint32_t* eformat::v31::Header::child (size_t n) const
{
  const uint32_t* next = payload();
  for (size_t i=0; i<n; ++i) next += next[1];
  return next;
}

const uint32_t* eformat::v31::Header::child_check (size_t n) const
{
  uint32_t total = nchildren();
  if (n >= total) throw EFORMAT_NO_SUCH_CHILD(n, total);
  return child(n);
}

uint32_t eformat::v31::Header::children (const uint32_t** p, size_t max) const
{
  const uint32_t* payload_start = payload();
  try {
    return eformat::find_fragments((uint32_t)eformat::v31::child_marker((eformat::v31::HeaderMarker)marker()),
			  payload_start, payload_size_word(), p, max);
  }
  catch (WrongMarkerIssue& ex) {
    //This normally means the fragment size is wrong...
    throw EFORMAT_WRONG_SIZE(fragment_size_word());
  }
  return 0;
}

