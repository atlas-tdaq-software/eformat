//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v31_ROBFragment.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines the ROB fragment entity as described in the Event Format
 * note.
 */

#ifndef EFORMAT_V31_ROBFRAGMENT_H
#define EFORMAT_V31_ROBFRAGMENT_H

#include "src/old/v31_Header.h"
#include "src/old/v31_HeaderMarker.h"
#include "eformat/Issue.h"

namespace eformat { namespace v31 {

  /**
   * Describes how to access the contents of a subdetector fragment, as
   * prescribed by the event format note.
   */
  class ROBFragment : public eformat::v31::Header {

  public: //interface

   /**
     * To build a fragment given the containing buffer. I need to know
     * where the fragment starts in order to do that.
     *
     * @param it The exact position where this fragment should start.
     */
    ROBFragment (const uint32_t* it);

    /**
     * Copy constructor
     *
     * @param other The fragment to be copied
     */
    ROBFragment (const ROBFragment& other) 
      : Header(other), m_start(other.m_start) {}

    /**
     * Builds an empty, otherwise useless ROBFragment
     */
    ROBFragment () : Header(), m_start() {}

    /**
     * Destructor virtualisation
     */
    virtual ~ROBFragment() {}

    /**
     * Assignment
     *
     * @param other The fragment to be copied
     */
    ROBFragment& operator= (const ROBFragment& other)
    { Header::operator=(other); m_start=other.m_start; return *this; }

    /**
     * Manual re-assignment
     *
     * @param it The position pointing the first word of this fragment
     */
    ROBFragment& assign (const uint32_t* it);

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    virtual bool check (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    inline bool check_tree (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const 
    { check(to_check); return true; }

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    inline uint32_t rob_source_id () const 
    { return Header::source_id(); }

    /**
     * Sets the given pointer to the ROD fragment start
     */
    inline const uint32_t* rod_start (void) const { return m_start; }

    /**
     * Returns the fragment type.
     */
    inline uint32_t rod_marker() const { return m_start[0]; }

    /**
     * Returns the total fragment size
     */
    inline uint32_t rod_fragment_size_word() const 
    { return eformat::v31::Header::payload_size_word(); }

    /**
     * Returns the size, in words, of the current header. That does @b
     * not include the trailer.
     */
    inline uint32_t rod_header_size_word() const { return m_start[1]; }

    /**
     * Returns the size, in words, of the trailer
     */
    inline uint32_t rod_trailer_size_word() const { return 3; }

    /**
     * Returns the formatting version.
     */
    inline uint32_t rod_version() const { return m_start[2]; }

    /**
     * Returns the source identifier of the ROD fragment.
     */
    inline uint32_t rod_source_id() const { return m_start[3]; }

    /**
     * Returns the current run number.
     */
    inline uint32_t rod_run_no() const { return m_start[4]; }

    /**
     * Returns the lvl1 identifier
     */
    inline uint32_t rod_lvl1_id() const { return m_start[5]; }

    /**
     * Returns the bunch crossing identifier
     */
    inline uint32_t rod_bc_id() const { return m_start[6]; }

    /**
     * Returns the lvl1 trigger type
     */
    inline uint32_t rod_lvl1_trigger_type() const { return m_start[7]; }

    /**
     * Returns the detector event type
     */
    inline uint32_t rod_detev_type() const { return m_start[8]; }

    /**
     * Returns the number of status words available
     */
    inline uint32_t rod_nstatus () const
    { return m_start[eformat::v31::Header::payload_size_word()-3]; }
    
    /**
     * Returns the status words, as an iterator to the status words
     * available.
     *
     * @param it An <em>updateable</em> iterator you should provide.
     */
    inline const uint32_t* rod_status (void) const 
    { return m_start + 9 + (rod_status_position()?rod_ndata():0); }

    /**
     * Returns the number of data words available
     */
    inline uint32_t rod_ndata () const 
    { return m_start[eformat::v31::Header::payload_size_word()-2]; }

    /**
     * Returns a pointer to the first data word
     */
    inline const uint32_t* rod_data () const
    { return m_start + 9 + (rod_status_position()?0:rod_nstatus()); }

    /**
     * Returns the status block position. A value of <tt>zero</tt> indicates
     * that the status block preceeds the data block. A value of <tt>one</tt>
     * means the contrary.
     */
    inline uint32_t rod_status_position () const 
    { return m_start[eformat::v31::Header::payload_size_word()-1]; }

    /**
     * Returns the number of children available.
     */
    virtual inline uint32_t nchildren () const { return 1; }

    /**
     * Returns the nth child fragment. If the nth fragment doesn't exist, the
     * behaviour is undefined.
     *
     * @param n The fragment position, starting at zero, of the child fragment
     * you would like to get.
     */
    virtual const uint32_t* child (size_t) const
    { return m_start; }

    /**
     * Returns all the children of this fragment. The input to this method is a
     * valid set of iterators to existing, pre-allocated pointers
     *
     * @param p A pointer to a set of preallocated const uint32_t*'s to set to the
     * position of the children of this fragment.
     * @param max The maximum number of children, p can point to.
     *
     * @return The number of children found on this fragment
     */
    virtual uint32_t children (const uint32_t** p, size_t max) const;

  private: //static stuff

    static const uint32_t NSPECIFIC;

  private: //representation

    const uint32_t* m_start; ///< my one-after-the-last word

  };

} }

#endif /* EFORMAT_V31_ROBFRAGMENT_H */
