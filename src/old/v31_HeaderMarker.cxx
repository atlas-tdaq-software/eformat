//Dear emacs, this is -*- c++ -*-

/**
 * @file HeaderMarker.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements a few functionality around the header marker
 */

#include "v31_HeaderMarker.h"

eformat::v31::HeaderMarker eformat::v31::child_marker(eformat::v31::HeaderMarker e)
{
  using namespace eformat::v31;
  switch (e) {
  case FULL_EVENT:
    return SUB_DETECTOR;
  case SUB_DETECTOR:
    return ROS;
  case ROS:
    return ROB;
  case ROB:
    return ROD;
  default:
    break;
  }
  return FULL_EVENT;
}

std::string eformat::v31::marker2string (const eformat::v31::HeaderMarker& e)
{
  using namespace eformat::v31;
  switch (e) {
  case ROD:
    return "ROD";
  case ROB:
    return "ROB";
  case ROS:
    return "ROS";
  case SUB_DETECTOR:
    return "SUB_DETECTOR";
  case FULL_EVENT:
    return "FULL_EVENT";
  }
  return "UNKNOWN_MARKER";
}

std::string eformat::v31::marker2string (uint32_t e)
{
  return eformat::v31::marker2string((eformat::v31::HeaderMarker)e);
}
