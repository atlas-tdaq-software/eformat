//Dear emacs, this is -*- c++ -*-

/**
 * @file src/old/v31_FullEventFragment.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines the Event Fragment entity. The definition is based on the
 * update of ATL-DAQ-98-129, by D.Francis et al.
 */

#ifndef EFORMAT_V31_FULLEVENTFRAGMENT_H
#define EFORMAT_V31_FULLEVENTFRAGMENT_H

#include "src/old/v31_Header.h"
#include "src/old/v31_HeaderMarker.h"
#include "src/old/v31_SubDetectorFragment.h"
#include "eformat/Issue.h"

namespace eformat { namespace v31 {

  /**
   * Describes how to access the contents of an event fragment, as prescribed
   * by the event format note.
   */
  class FullEventFragment : public eformat::v31::Header {

  public: //interface

   /**
     * To build a fragment given the containing buffer. I need to know
     * where the fragment starts in order to do that.
     *
     * @param it The exact position where this fragment should start.
     */
    FullEventFragment (const uint32_t* it);

    /**
     * Builds an empty, otherwise useless FullEventFragment
     */
    FullEventFragment () : Header(), m_start() {}

    /**
     * Copy constructor
     *
     * @param other The fragment to be copied
     */
    FullEventFragment (const FullEventFragment& other) 
      : Header(other), m_start(other.m_start) {}

    /**
     * Destructor virtualisation
     */
    virtual ~FullEventFragment() {}

    /**
     * Copy operator
     *
     * @param other The fragment to be copied
     */
    FullEventFragment& operator= (const FullEventFragment& other) 
    { Header::operator=(other); m_start=other.m_start; return *this; }

    /**
     * Manual re-assignment
     *
     * @param it The position pointing the first word of this fragment
     */
    FullEventFragment& assign (const uint32_t* it);

    /**
     * Says if the the fragment is valid. This may throw exceptions.
     */
    virtual bool check (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Says if the Fragment and all children are valid.
     */
    bool check_tree (const uint16_t to_check=eformat::MAJOR_V31_VERSION) const;

    /**
     * Returns amount of seconds since the (unix) origin, for the bunch
     * crossing time.
     */
    inline uint32_t bc_time_seconds() const 
    { return m_start[0]; }

    /**
     * Returns amount of nanoseconds aditionally to the amount of seconds
     * returned by bc_time_seconds, for the bunch crossing time.
     */
    inline uint32_t bc_time_nanoseconds() const 
    { return m_start[1]; }

    /**
     * Returns the global identifier
     */
    inline uint32_t global_id() const 
    { return m_start[2]; }

    /**
     * Returns the run type
     */
    inline uint32_t run_type() const 
    { return m_start[3]; }

    /**
     * Returns the run number
     */
    inline uint32_t run_no() const 
    { return m_start[4]; }

    /**
     * Returns the luminosity block number
     */
    inline uint16_t lumi_block() const 
    { return m_start[5]; }

    /**
     * Returns the lvl1 identifier
     */
    inline uint32_t lvl1_id() const
    { return m_start[6]; }

    /**
     * Returns the bunch crossing identifier
     */
    inline uint16_t bc_id() const
    { return m_start[7]; }

    /**
     * Returns the detector mask. This method is marked obsolete. 
     */
    inline uint64_t detector_mask() const 
    { return *reinterpret_cast<const uint64_t*>(&m_start[8]); }

    /**
     * Returns the lvl1 trigger type
     */
    inline uint8_t lvl1_trigger_type() const
    { return m_start[10]; }

    /**
     * Returns the number of lvl1 trigger info words
     */
    inline uint32_t nlvl1_trigger_info() const
    { return m_start[11]; }

    /**
     * Returns an iterator to the first of the LVL1 trigger info words
     */
    inline const uint32_t* lvl1_trigger_info () const 
    { return m_start + 12; }

    /**
     * Returns the number of lvl2 trigger info words
     */
    inline uint32_t nlvl2_trigger_info() const
    { return m_start[12+nlvl1_trigger_info()]; }

    /**
     * Returns an iterator to the first of the LVL2 trigger info words
     */
    inline const uint32_t* lvl2_trigger_info () const 
    { return m_start + 13 + nlvl1_trigger_info(); }

    /**
     * Returns the number of Event Filter words
     */
    inline uint32_t nevent_filter_info () const
    { return m_start[13+nlvl1_trigger_info()+nlvl2_trigger_info()]; }

    /**
     * Returns an iterator to the first of the EF info words
     */
    const uint32_t* event_filter_info () const
    { return m_start + 14 + nlvl1_trigger_info() + nlvl2_trigger_info(); }

    /**
     * Returns the number of stream tag words
     */
    inline uint32_t nstream_tag () const
    { return this->nspecific() -
      (14+nlvl1_trigger_info()+nlvl2_trigger_info()+nevent_filter_info()); }

    /**
     * Returns an iterator to the first of the stream tag words
     */
    const uint32_t* stream_tag (void) const
    { return m_start + 14 + nlvl1_trigger_info() + nlvl2_trigger_info() +
      nevent_filter_info(); }

  private: //representation

    const uint32_t* m_start; ///< my start word

  };

} }

#endif /* EFORMAT_V31_FULLEVENTFRAGMENT_H */
