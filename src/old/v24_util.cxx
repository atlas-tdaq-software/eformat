//Dear emacs, this is -*- c++ -*-

/**
 * @file util.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the utilities described in the equivalent header.
 */

#include "src/old/v24_util.h"
#include "src/old/v24_FullEventFragment.h"
#include "src/old/v24_SubDetectorFragment.h"
#include "src/old/v24_ROSFragment.h"
#include "src/old/v24_ROBFragment.h"
#include "src/old/v24_RODFragment.h"
#include "src/old/v24_Issue.h"
#include "src/old/v31_HeaderMarker.h"

#include "eformat/SourceIdentifier.h"
#include "src/old/old_Issue.h"
#include "eformat/Version.h"

#include "eformat/write/FullEventFragment.h"

#include "ers/ers.h"

uint32_t eformat::v24::convert_source (uint32_t v24_id)
{
  using namespace eformat::v31;

  uint16_t id = v24_id & 0x00ff;
  uint8_t sd = ( v24_id >> 8 ) & 0xff;
  uint8_t md = ( v24_id >> 16 ) & 0xff;
  uint32_t retval = 0;

  //The pixel detectors, that have been re-shuffled
  if (sd == 0x13) sd = 0x12;
  if (sd == 0x14) sd = 0x13;

  switch ((SubDetector)sd) {
    case eformat::FULL_SD_EVENT:
    case eformat::PIXEL_BARREL:
    case eformat::PIXEL_DISK:
    case eformat::PIXEL_B_LAYER:
    case eformat::SCT_BARREL_A_SIDE:
    case eformat::SCT_BARREL_C_SIDE:
    case eformat::SCT_ENDCAP_A_SIDE:
    case eformat::SCT_ENDCAP_C_SIDE:
    case eformat::TRT_ANCILLARY_CRATE:
    case eformat::TRT_BARREL_A_SIDE:
    case eformat::TRT_BARREL_C_SIDE:
    case eformat::TRT_ENDCAP_A_SIDE:
    case eformat::TRT_ENDCAP_C_SIDE:
    case eformat::LAR_EM_BARREL_A_SIDE:
    case eformat::LAR_EM_BARREL_C_SIDE:
    case eformat::LAR_EM_ENDCAP_A_SIDE:
    case eformat::LAR_EM_ENDCAP_C_SIDE:
    case eformat::LAR_HAD_ENDCAP_A_SIDE:
    case eformat::LAR_HAD_ENDCAP_C_SIDE:
    case eformat::LAR_FCAL_A_SIDE:
    case eformat::LAR_FCAL_C_SIDE:
    case eformat::TILECAL_LASER_CRATE:
    case eformat::TILECAL_BARREL_A_SIDE:
    case eformat::TILECAL_BARREL_C_SIDE:
    case eformat::TILECAL_EXT_A_SIDE:
    case eformat::TILECAL_EXT_C_SIDE:
    case eformat::MUON_ANCILLARY_CRATE:
    case eformat::MUON_MDT_BARREL_A_SIDE:
    case eformat::MUON_MDT_BARREL_C_SIDE:
    case eformat::MUON_MDT_ENDCAP_A_SIDE:
    case eformat::MUON_MDT_ENDCAP_C_SIDE:
    case eformat::MUON_RPC_BARREL_A_SIDE:
    case eformat::MUON_RPC_BARREL_C_SIDE:
    case eformat::MUON_TGC_ENDCAP_A_SIDE:
    case eformat::MUON_TGC_ENDCAP_C_SIDE:
    case eformat::MUON_CSC_ENDCAP_A_SIDE:
    case eformat::MUON_CSC_ENDCAP_C_SIDE:
    case eformat::TDAQ_BEAM_CRATE:
    case eformat::TDAQ_CALO_PREPROC:
      retval = sd;
      break;
    case eformat::TDAQ_CALO_CLUSTER_PROC_DAQ: //v24 TDAQ_CALO_CLUSTER_PROC
      retval = 0x73;
      break;
    case eformat::OTHER:
      retval = sd;
      break;
    case eformat::TDAQ_CALO_CLUSTER_PROC_ROI: //v24 TDAQ_CALO_JET
      retval = 0x75;
      break;
    case eformat::TDAQ_CALO_JET_PROC_DAQ: //v24 TDAQ_CTP
      retval = 0x77;
      id = 1; //forces id == 1 so simulation is happy
      break;
    case eformat::TDAQ_CALO_JET_PROC_ROI: //v24 TDAQ_MUON_INTERFACE
      retval = 0x76;
      id = 1; //forces id == 1 so simulation is happy
      break;
    case eformat::TDAQ_MUON_CTP_INTERFACE: //v24 TDAQ_DATAFLOW
      switch (md) {
        case 0x02: //LVL1 ROSes
        case 0x04: //v24 SUPERVISOR
          retval = 0x78; //new L2SV
          break;
        case 0x06: //v24 SFI
          retval = 0x79; //new SFI
          break;
        case 0x07: //v24 SFO
          retval = 0x7a; //new SFO
          break;
        case 0x0a: //v24 OTHER_MODULE
          retval = OTHER; //new OTHER
          break;
        default:
          ers::warning(EFORMAT_UNBOUND_SOURCE_IDENTIFIER(v24_id));
      } 
      break;
    case eformat::TDAQ_CTP: //v24 TDAQ_LVL2
      switch (md) {
        case 0x01: //v24 L2PU->PROS
        case 0x02: //v24 L2PU->PROS
        case 0x05: //v24 HLT_PROCESSOR
          retval = 0x7b; //new LVL2
          break;
        case 0x04: //v24 SUPERVISOR
          retval = 0x78; //new L2SV
          break;
        case 0x06: //v24 SFI marker for PROS data
          retval = 0x79; //new SFI
          break;
        default:
          ers::warning(EFORMAT_UNBOUND_SOURCE_IDENTIFIER(v24_id));
      }
      break;
    case eformat::TDAQ_L2SV: //v24 TDAQ_EVENT_FILTER
      switch (md) {
        case 0x05: //v24 HLT_PROCESSOR
          retval = 0x7b; //new LVL2
        default:
          ers::warning(EFORMAT_UNBOUND_SOURCE_IDENTIFIER(v24_id));
      }
      break;
    case eformat::TDAQ_SFI:
    case eformat::TDAQ_SFO:
    case eformat::TDAQ_LVL2:
    case eformat::TDAQ_EVENT_FILTER:
    default:
      ers::warning(EFORMAT_UNBOUND_SOURCE_IDENTIFIER(v24_id));
      break;
  }
  retval <<= 16;
  retval |= id;
  ERS_DEBUG(3, "Source identifier " << EFORMAT_HEX(v24_id) 
      << "  [v2.4] was converted to " << EFORMAT_HEX(retval) << " [v4.0].");
  return retval;
}

/**
 * Converts a ROS fragment, from the v24 to new format, using the space of
 * contiguous memory storage area given. If the event given is already on v3.0
 * format, no conversion takes place.
 *
 * @param src A pointer to the first word of the fragment, lying in a @b
 * contiguous area of memory.
 * @param dest The destination area of memory, preallocated
 * @param max The maximum number of words that fit in the preallocated
 * memory area "dest".
 * @param event_checksum What type of checksum to deploy for full events
 * @param rob_checksum What type of checksum to deploy for rob fragments
 *
 * @return A counter, for the number of words copied from the source to the
 * destination. If that number is zero, something wrong happened.
 */
static uint32_t convert_ros(const uint32_t* src, uint32_t* dest, uint32_t max,
        const eformat::CheckSum& event_checksum=eformat::NO_CHECKSUM,
        const eformat::CheckSum& rob_checksum=eformat::NO_CHECKSUM)
{
  using namespace eformat;

  if (src[0] != v31::ROS) {
    throw EFORMAT_WRONG_MARKER(src[0], v31::ROS);
  }

  //check version
  helper::Version version(src[3]);
  if (version.major_version() == MAJOR_DEFAULT_VERSION) {
    memcpy(dest, src, sizeof(uint32_t)*src[1]);
    return src[1];
  }
  if (version.major_version() != MAJOR_V24_VERSION)
    throw EFORMAT_BAD_VERSION(version.major_version(), MAJOR_V24_VERSION);

  //this is from the v24 major version of eformat, proceed with conversion
  v24::ROSFragment ros(src);
  ros.check(); //this may throw

  //create the base FullEvent
  eformat::write::FullEventFragment nfe(eformat::v24::convert_source(ros.source_id()), 0, 0, 0, 0, 0, 0, ros.lvl1_id(), ros.bc_id(), 0);
  nfe.status(ros.nstatus(), ros.status());
  nfe.minor_version(version.minor_version());
  nfe.checksum_type(event_checksum);

  std::vector<eformat::write::ROBFragment*> acc_rob;
  for (size_t k=0; k<ros.noffset(); ++k) {
    try {
      v24::ROBFragment rob(ros.child(k));
      rob.check();
      uint32_t source_id = rob.source_id();

      for (size_t l=0; l<rob.noffset(); ++l) {
        try {
          v24::RODFragment rod(rob.rod(l));
          rod.check();
          if (rob.noffset() != 1) source_id = rod.source_id();
          eformat::write::ROBFragment* nrob =
            new eformat::write::ROBFragment(eformat::v24::convert_source(source_id), 
              rod.run_no(), rod.lvl1_id(), rod.bc_id(), 
              rod.lvl1_trigger_type(), rod.detev_type(), 
              rod.ndata(), rod.data(), rod.status_position());
          nrob->status(rob.nstatus(), rob.status());
          nrob->rod_status(rod.nstatus(), rod.status());
          helper::Version rob_version(rob.version());
          nrob->minor_version(rob_version.minor_version());
          helper::Version rod_version(rod.version());
          nrob->rod_minor_version(rod_version.minor_version());
          nrob->checksum_type(rob_checksum);
          //make this new ROB part of the new ROS
          nfe.append(nrob);
          //make sure we don't forget to delete this guy
          acc_rob.push_back(nrob);
        }
        catch (eformat::Issue& e) {
          ers::warning(e);
          ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROD", "ROB", source_id)); 
          //we skip this fragment, but do not loose the whole event
          continue;
        }
      }
    }
    catch (eformat::Issue& e) {
      ers::warning(e);
      ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROB", "ROS", ros.source_id())); 
      //we skip this fragment, but do not loose the whole event
      continue;
    }
  }

  //now the FullEvent is in `nfe', bind
  const eformat::write::node_t* top = nfe.bind();
  //memcpy the list of pages into contiguous memory
  uint32_t retval = eformat::write::copy(*top, dest, max);

  //delete the allocated stuff
  for (size_t i=0; i<acc_rob.size(); ++i) delete acc_rob[i];

  return retval;
}

uint32_t eformat::v24::convert(const uint32_t* src, uint32_t* dest,
    uint32_t max, const eformat::CheckSum& event_checksum,
    const eformat::CheckSum& rob_checksum)
{
  using namespace eformat;

  if (src[0] != v31::FULL_EVENT) {
    if (src[0] != v31::ROS) {
      throw EFORMAT_WRONG_MARKER(src[0], v31::FULL_EVENT);
    }
    return convert_ros(src, dest, max, event_checksum, rob_checksum);
  }

  //check version
  helper::Version version(src[3]);
  if (version.major_version() == MAJOR_DEFAULT_VERSION) {
    memcpy(dest, src, sizeof(uint32_t)*src[1]);
    return src[1];
  }
  if (version.major_version() != MAJOR_V24_VERSION)
    throw EFORMAT_BAD_VERSION(version.major_version(), MAJOR_V24_VERSION);

  //this is from the v24 major version of eformat, proceed with conversion
  v24::FullEventFragment fe(src);
  fe.check(); //this may throw

  //create the base FullEvent
  eformat::write::FullEventFragment nfe(eformat::v24::convert_source(fe.source_id()), 
      fe.date(), 0, 
      fe.global_id(), 0, fe.run_no(), 0,
      fe.lvl1_id(), 0, fe.lvl1_trigger_type());
  const uint32_t lvl2_info = fe.lvl2_trigger_info();
  nfe.lvl2_trigger_info(1, &lvl2_info);
  nfe.event_filter_info(fe.nevent_filter_info(), fe.event_filter_info());
  nfe.status(fe.nstatus(), fe.status());
  nfe.minor_version(version.minor_version());
  nfe.checksum_type(event_checksum);

  nfe.status(fe.nstatus(), fe.status());
  nfe.minor_version(version.minor_version());

  std::vector<eformat::write::ROBFragment*> acc_rob;
  for (size_t i=0; i<fe.noffset(); ++i) {
    try {
      v24::SubDetectorFragment sd(fe.child(i));
      sd.check();
      for (size_t j=0; j<sd.noffset(); ++j) {
        try {
          v24::ROSFragment ros(sd.child(j));
          ros.check();
          for (size_t k=0; k<ros.noffset(); ++k) {
            try {
              v24::ROBFragment rob(ros.child(k));
              rob.check();
              uint32_t source_id = rob.source_id();
              for (size_t l=0; l<rob.noffset(); ++l) {
                try {
                  v24::RODFragment rod(rob.rod(l));
                  rod.check();
                  if (rob.noffset() != 1) source_id = rod.source_id();
                  eformat::write::ROBFragment* nrob = new write::ROBFragment
                    (eformat::v24::convert_source(source_id), rod.run_no(), rod.lvl1_id(),
                     rod.bc_id(), rod.lvl1_trigger_type(), rod.detev_type(), 
                     rod.ndata(), rod.data(), rod.status_position());
                  nrob->status(rob.nstatus(), rob.status());
                  nrob->rod_status(rod.nstatus(), rod.status());
                  helper::Version rob_version(rob.version());
                  nrob->minor_version(rob_version.minor_version());
                  helper::Version rod_version(rod.version());
                  nrob->rod_minor_version(rod_version.minor_version());
                  nrob->checksum_type(rob_checksum);

                  //make sure we don't forget to delete this guy
                  acc_rob.push_back(nrob);
                  //make this new ROB part of the new FE
                  nfe.append(nrob);
                }
                catch (eformat::Issue& e) {
                  ers::warning(e);
                  ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROD", "ROB", 
                        source_id));
                  //we skip this fragment, but do not loose the whole event
                  continue;
                }
              }
            }
            catch (eformat::Issue& e) {
              ers::warning(e);
              ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROB", "ROS", 
                    ros.source_id())); 
              //we skip this fragment, but do not loose the whole event
              continue;
            }
          }
        }
        catch (eformat::Issue& e) {
          ers::warning(e);
          ers::warning(EFORMAT_SKIPPED_FRAGMENT("ROS", "SUBDETECTOR", 
                sd.source_id())); 
          //we skip this fragment, but do not loose the whole event
          continue;
        }
      }
    }
    catch (eformat::Issue& e) {
      ers::warning(e);
      ers::warning(EFORMAT_SKIPPED_FRAGMENT("SUBDETECTOR", "FULLEVENT", 
            fe.lvl1_id())); 
      //we skip this fragment, but do not loose the whole event
      continue;
    }
  }

  //now the FullEvent is in `nfe', bind
  const eformat::write::node_t* top = nfe.bind();
  //memcpy the list of pages into contiguous memory
  uint32_t retval = eformat::write::copy(*top, dest, max);

  //delete the allocated stuff
  for (size_t i=0; i<acc_rob.size(); ++i) delete acc_rob[i];

  return retval;
}
