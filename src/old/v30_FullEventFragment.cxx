//Dear emacs, this is -*- c++ -*-

/**
 * @file FullEventFragment30.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the old FullEventFragment description
 */

#include "src/old/v30_FullEventFragment.h"
#include "src/old/v31_HeaderMarker.h"
#include "src/old/v31_SubDetectorFragment.h"

const uint32_t eformat::v30::FullEventFragment::NSPECIFIC = 10;

eformat::v30::FullEventFragment::FullEventFragment (const uint32_t* it)
  : eformat::v31::Header(it, eformat::v31::FULL_EVENT),
    m_start(eformat::v31::Header::specific_header())
{
}

eformat::v30::FullEventFragment& 
eformat::v30::FullEventFragment::assign (const uint32_t* it)
{
  eformat::v31::Header::assign(it, eformat::v31::FULL_EVENT);
  m_start = eformat::v31::Header::specific_header();
  return *this;
}

bool eformat::v30::FullEventFragment::check (const uint16_t to_check) const
{
  eformat::v31::Header::check(to_check); //< first do a generic check
  if (eformat::v31::Header::nspecific() != NSPECIFIC) { 
    throw EFORMAT_SIZE_CHECK(NSPECIFIC, 
			     eformat::v31::Header::nspecific());
  }
  return true;
}

bool eformat::v30::FullEventFragment::check_tree (const uint16_t to_check) const
{
  check(to_check); // check myself
  uint32_t total = eformat::v31::Header::nchildren();
  for (size_t i=0; i<total; ++i) {
    eformat::v31::SubDetectorFragment f(eformat::v31::Header::child(i));
    f.check_tree(to_check);
  }
  return true;
}

