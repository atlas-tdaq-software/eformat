/**
 * @file v31_ROSFragment.cxx
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief  
 */

#include "v31_ROSFragment.h"

const uint32_t eformat::v31::ROSFragment::NSPECIFIC = 3;

eformat::v31::ROSFragment::ROSFragment (const uint32_t* it)
  : eformat::v31::Header(it, eformat::v31::ROS),
    m_start(eformat::v31::Header::specific_header())
{
}

 eformat::v31::ROSFragment& 
eformat::v31::ROSFragment::assign (const uint32_t* it)
{
  eformat::v31::Header::assign(it, eformat::v31::ROS);
  m_start = eformat::v31::Header::specific_header();
  return *this;
}

bool eformat::v31::ROSFragment::check (const uint16_t to_check) const
{
  eformat::v31::Header::check(to_check); //< first do a generic check
  if (eformat::v31::Header::nspecific() != NSPECIFIC)
    throw EFORMAT_SIZE_CHECK(NSPECIFIC, 
			     eformat::v31::Header::nspecific());
  return true;
}

bool eformat::v31::ROSFragment::check_tree (const uint16_t to_check) const
{
  check(to_check); // check myself
  uint32_t total = eformat::v31::Header::nchildren();
  for (size_t i=0; i<total; ++i) {
    eformat::v31::ROBFragment f(eformat::v31::Header::child(i));
    f.check_tree(to_check);
  }
  return true;
}

