/**
 * @file FullEventFragmentNoTemplates.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Implements full event fragments without using templates to ease
 * maintenance (patching).
 */

#include "eformat/FullEventFragmentNoTemplates.h"
#include "eformat/HeaderMarker.h"
#include "eformat/util.h"
#include "eformat/compression.h"
#include "compression/DataBuffer.h"

#include <cstring>

void eformat::read::FullEventFragment::initialize()
{
  if(!this->compression_type()) {
    m_payload = eformat::Header::payload();
  }
}

eformat::read::FullEventFragment::FullEventFragment (const uint32_t* it)
  : eformat::Header(it),
    m_start(), m_uncompressed(nullptr), m_payload(0) 
{
  m_start = eformat::Header::specific_header();
  initialize();
}

eformat::read::FullEventFragment::FullEventFragment () 
  : Header(), m_start(), m_uncompressed(nullptr), m_payload(0) 
{
}

eformat::read::FullEventFragment::FullEventFragment 
(const eformat::read::FullEventFragment& other)
  : Header(other)
{
  *this=other;
}

eformat::read::FullEventFragment::FullEventFragment 
  (const eformat::helper::u32list& blob)
  : eformat::Header(blob.ptr()),
    m_start(), m_uncompressed(nullptr), m_payload(0) 
{
  m_start = eformat::Header::specific_header();
  initialize();
}

eformat::read::FullEventFragment::FullEventFragment 
  (const eformat::helper::u32slice& blob)
  : eformat::Header(blob.ptr()),
    m_start(), m_uncompressed(nullptr), m_payload(0) 
{
  m_start = eformat::Header::specific_header();
  initialize();
}

eformat::read::FullEventFragment::~FullEventFragment ()
{
}

eformat::read::FullEventFragment& eformat::read::FullEventFragment::operator= 
(const eformat::read::FullEventFragment& other) 
{ 
  if (this == &other) return *this;
  
  eformat::Header::operator=(other); 
  m_start=other.m_start; 
  m_uncompressed = nullptr;
  m_payload = 0;
  initialize();

  if(this->compression_type() && other.m_payload){
    //Already uncompressed. We make a copy
    auto payload_size_byte = (this->readable_payload_size_word())*sizeof(uint32_t);
    m_uncompressed->realloc(payload_size_byte);
    std::memcpy(m_uncompressed->handle(),
		other.m_uncompressed->handle(), payload_size_byte);
    m_payload = static_cast<uint32_t*>(m_uncompressed->handle());
  }

  return *this; 
}

eformat::read::FullEventFragment& eformat::read::FullEventFragment::assign 
(const uint32_t* it)
{
  eformat::Header::assign(it);
  m_start = eformat::Header::specific_header();
  initialize();
  return *this;
}

const uint32_t* eformat::read::FullEventFragment::lvl1_trigger_info () const 
{
  return m_start + 13;
}

const uint32_t* eformat::read::FullEventFragment::lvl2_trigger_info () const
{
  return m_start + 14 + nlvl1_trigger_info();
}

const uint32_t* eformat::read::FullEventFragment::event_filter_info () const 
{
  return this->hlt_info();
}

const uint32_t* eformat::read::FullEventFragment::hlt_info () const 
{
  return m_start + 15 + nlvl1_trigger_info() + nlvl2_trigger_info();
}

const uint32_t* eformat::read::FullEventFragment::stream_tag () const 
{
  return m_start + 16 + nlvl1_trigger_info() + nlvl2_trigger_info() + 
    nhlt_info();
}

void eformat::read::FullEventFragment::uncompress() const
{
  if(!m_payload && this->readable_payload_size_word()){
    uint32_t payload_size;
    
    m_uncompressed.reset(new compression::DataBuffer(this->readable_payload_size_word()*sizeof(uint32_t)));

    if(compression_type() == eformat::ZLIB){
      try{
      compression::zlibuncompress(*(m_uncompressed.get()),
				  payload_size,
				  eformat::Header::payload(),
				  eformat::Header::payload_size_word()*sizeof(uint32_t));
      }catch(compression::ZlibFailed& e){
	throw eformat::CompressionIssue(ERS_HERE,e);
      }
      
      //Update the payload pointer
      m_payload = static_cast<uint32_t *>(m_uncompressed->handle());

    } else if (compression_type() == eformat::UNCOMPRESSED ||
               compression_type() == eformat::RESERVED){
      m_payload = eformat::Header::payload();
      payload_size = eformat::Header::payload_size_word()*sizeof(uint32_t);
    } else {
      // This is an unknown compression type!
      throw EFORMAT_UNKNOWN_COMPRESSION_TYPE(compression_type());
    }
    
    //Check the uncompressed size matches the expected value from the header
    auto expect_payload_size =
      this->readable_payload_size_word()*sizeof(uint32_t);

    if (payload_size != expect_payload_size){
      throw EFORMAT_WRONG_UNCOMPRESSED_SIZE(expect_payload_size, payload_size);
    }
  }
}


bool 
eformat::read::FullEventFragment::check_impl 
(const uint16_t version,
 const bool exc) 
  const
{
  
  bool result = true;

  //Fragment checking, only this header
  try{
    if (eformat::Header::marker() != eformat::FULL_EVENT) {
      throw EFORMAT_WRONG_MARKER(marker(), eformat::FULL_EVENT);
    }
    
    if (eformat::Header::version() >> 16 != version) {
      throw EFORMAT_BAD_VERSION(eformat::Header::version() >> 16, version);
    }
    
    uint32_t calculated = 23 + eformat::Header::nstatus() + 
      nlvl1_trigger_info() + nlvl2_trigger_info() + nhlt_info() + 
      nstream_tag();
    if (eformat::Header::header_size_word() != calculated) {
      throw EFORMAT_SIZE_CHECK(calculated,  eformat::Header::header_size_word());
    }
  }catch(eformat::WrongMarkerIssue &ex){
    if(exc) throw ex;
    result = false;
  }catch(eformat::BadVersionIssue &ex){
    if(exc) throw ex;
    result = false;
  }catch(eformat::SizeCheckIssue &ex){
    if(exc) throw ex;
    result = false;
  }
      
  return result;
}


bool eformat::read::FullEventFragment::check (const uint16_t version) const
{
  return this->check_impl(version, true);
}

bool eformat::read::FullEventFragment::check_noex (const uint16_t version) const
{
  return this->check_impl(version, false);
}

bool 
eformat::read::FullEventFragment::check_tree_impl 
(const uint16_t version, const uint16_t rod_version, const bool exc)
  const
{

  if (!check_impl(version, exc)) return false; // check myself
  eformat::read::FullEventFragment::child_iter_t iter = child_iter();
  while (const uint32_t* fp = iter.next()) {
    eformat::read::ROBFragment f(fp);
    bool rob_check;
    try{
      rob_check = f.check(version, rod_version);
    }catch(eformat::RODSizeCheckIssue &ex){
      //In case of a truncated ROD fragment, we ignore and continue
      //Anyway the ROB is good, hence we can safely navigate the event
      continue;
    }catch(eformat::WrongMarkerIssue &ex){
      if(exc) throw ex;
      rob_check = false;
    }catch(eformat::BadVersionIssue &ex){
      if(exc) throw ex;
      rob_check = false;
    }catch(eformat::SizeCheckIssue &ex){
      if(exc) throw ex;
      rob_check = false;
    }catch(eformat::BadRodVersionIssue &ex){
      if(exc) throw ex;
      rob_check = false;
    }catch(eformat::Issue &ex){
      if(exc) throw ex;
      rob_check = false;
    }
    
    if (!rob_check) return false;
  }
  return true;
}

bool eformat::read::FullEventFragment::check_tree (const uint16_t version,
    const uint16_t rod_version) 
  const
{
  return this->check_tree_impl(version, rod_version, true);
}

bool eformat::read::FullEventFragment::check_tree_noex (const uint16_t version,
    const uint16_t rod_version)
  const
{
  return this->check_tree_impl(version, rod_version, false);
}

void eformat::read::FullEventFragment::problems 
  (std::vector<eformat::FragmentProblem>& p, const uint16_t version) const {
  //Fragment checking, only this header
  if (eformat::Header::marker() != eformat::FULL_EVENT)
    p.push_back(eformat::WRONG_MARKER);
  if ( eformat::Header::version() >> 16 != version )
    p.push_back(eformat::UNSUPPORTED_VERSION);
  uint32_t calculated = 23 + eformat::Header::nstatus() + 
    nlvl1_trigger_info() + nlvl2_trigger_info() + nhlt_info() + 
    nstream_tag();
  if (eformat::Header::header_size_word() != calculated)
    p.push_back(eformat::WRONG_FRAGMENT_SIZE);
}

void 
eformat::read::FullEventFragment::problems_tree
(std::vector<eformat::helper::ProblemContainer>& p,
 const uint16_t version,
 const uint16_t rod_version) 
  const
{
  std::vector<eformat::FragmentProblem> tmp; 
  this->problems(tmp, version);
  
  if (!tmp.empty()){
    eformat::helper::ProblemContainer pc(this->source_id(), tmp);
    p.push_back(pc);
    return;
  }
  
  eformat::read::FullEventFragment::child_iter_t iter = child_iter();
  while (const uint32_t* fp = iter.next()) {
    eformat::read::ROBFragment f(fp);
    std::vector<eformat::FragmentProblem> tmp; 
    f.problems(tmp, version, rod_version);
    eformat::helper::ProblemContainer pc(f.source_id(), tmp);
    p.push_back(pc);
  }
}

void eformat::read::FullEventFragment::robs
(std::vector<eformat::read::ROBFragment>& r) const
{
  uint32_t total = nchildren();
  r.reserve(total);
  eformat::read::FullEventFragment::child_iter_t iter = child_iter();
  while (const uint32_t* fp = iter.next()) {
    r.push_back(eformat::read::ROBFragment(fp));
  }
}

void eformat::read::FullEventFragment::robs
(std::vector<eformat::read::ROBFragment>& good,
 std::vector<eformat::read::ROBFragment>& bad,
  const uint16_t version, const uint16_t rod_version) const
{
  eformat::read::FullEventFragment::child_iter_t iter = child_iter();
  while (const uint32_t* fp = iter.next()) {
    eformat::read::ROBFragment f(fp);
    if (f.check_noex(version, rod_version)) good.push_back(f);
    else bad.push_back(f);
  }
}

uint32_t eformat::read::FullEventFragment::nchildren () const
{
  return children(0,0);
}

const uint32_t* eformat::read::FullEventFragment::child (size_t n) const
{
  uncompress();
  const uint32_t* next = m_payload;
  for (size_t i=0; i<n; ++i) next += next[1];
  return next;
}

const uint32_t* eformat::read::FullEventFragment::child_check (size_t n) const
{
  uint32_t total = nchildren();
  if (n >= total) throw EFORMAT_NO_SUCH_CHILD(n, total);
  return child(n);
}

uint32_t eformat::read::FullEventFragment::children (const uint32_t** p, size_t max) const
{
  uncompress();
  return find_fragments(child_marker(static_cast<HeaderMarker>(eformat::Header::marker())), m_payload, readable_payload_size_word(), p, max);
}

uint32_t eformat::read::FullEventFragment::bc_time_seconds() const 
{ 
  return m_start[0]; 
}

uint32_t eformat::read::FullEventFragment::bc_time_nanoseconds() const 
{ 
  return m_start[1]; 
}

uint64_t eformat::read::FullEventFragment::global_id() const 
{ 
  uint64_t gid = m_start[3];
  gid <<= 32;
  gid |= m_start[2] ;
  return gid; 
}

uint32_t eformat::read::FullEventFragment::run_type() const 
{ 
  return m_start[4]; 
}

uint32_t eformat::read::FullEventFragment::run_no() const 
{ 
  return m_start[5]; 
}

uint16_t eformat::read::FullEventFragment::lumi_block() const 
{ 
  return m_start[6]; 
}

uint32_t eformat::read::FullEventFragment::lvl1_id() const
{ 
  return m_start[7]; 
}

uint16_t eformat::read::FullEventFragment::bc_id() const
{ 
  return m_start[8]; 
}

uint8_t eformat::read::FullEventFragment::lvl1_trigger_type() const
{ 
  return m_start[9]; 
}

uint32_t eformat::read::FullEventFragment::compression_type() const
{
  return m_start[10];
}

uint32_t eformat::read::FullEventFragment::readable_payload_size_word() const
{
  return m_start[11];
}

uint32_t eformat::read::FullEventFragment::nlvl1_trigger_info() const
{ 
  return m_start[12]; 
}

uint32_t eformat::read::FullEventFragment::nlvl2_trigger_info() const
{ 
  return m_start[13+nlvl1_trigger_info()]; 
}

uint32_t eformat::read::FullEventFragment::nevent_filter_info () const
{ 
  return this->nhlt_info();
}

uint32_t eformat::read::FullEventFragment::nhlt_info () const
{ 
  return m_start[14+nlvl1_trigger_info()+nlvl2_trigger_info()]; 
}

uint32_t eformat::read::FullEventFragment::nstream_tag () const
{ 
  return m_start[15+nlvl1_trigger_info() + 
    nlvl2_trigger_info() + nhlt_info()]; 
}

const uint32_t* eformat::read::FullEventFragment::readable_payload() const
{
  uncompress();
  return m_payload;
}
