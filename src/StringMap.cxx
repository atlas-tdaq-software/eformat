//Dear emacs, this is -*- c++ -*-

/**
 * @file StringMap.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the functionality prototyped in StringMap.h
 */

#include "eformat/Issue.h"
#include "StringMap.h"

/**
 * Decodes a stream input string into a tag. The implementation looks simple,
 * but handles most of the conditions.
 * 
 * @param input The encode tag, in 'key1=value1;key2=value2;...' format
 * @param strmap The tag object to fill with the decoded result
 */
void decode_map (const std::string& input, 
		 eformat::helper::StringMap& strmap) 
{
  std::string::size_type begin = 0;
  std::string::size_type end = 0;
  while (end != std::string::npos) {
    end = input.find('=', end);
    if (end == std::string::npos) break;
    else {
      while (input[begin] == ';') ++begin;
      std::string key(input, begin, end-begin);
      begin = end + 1;
      end = input.find(';', begin);
      std::string value(input, begin, end-begin);
      strmap[key] = value;
      if (end != std::string::npos) {
	begin = end + 1;
	end = begin;
      }
    }
  }
}

void eformat::helper::decode (const uint32_t& szword, const uint32_t* encoded,
			      std::vector<eformat::helper::StringMap>& decoded)
{
  const char* multistring = reinterpret_cast<const char*>(encoded);
  uint32_t remains = sizeof(uint32_t) * szword;
  std::vector<std::string> stringvector;
  while (remains) {
    while (!multistring[0]) { //checks if I'm not on a \0-only region
      ++multistring;
      --remains;
      if (!remains) break;
    }
    if (!remains) break;
    std::string current(multistring);
    remains -= (current.size() + 1); //accounts for \0 in the end!
    multistring += (current.size() + 1);
    stringvector.push_back(current); //this buffers current results
  }

  //from this point, we have the strings for each tag, in stringvector
  for (std::vector<std::string>::iterator i=stringvector.begin(); 
       i!=stringvector.end(); ++i) {
    eformat::helper::StringMap strmap;
    decode_map(*i, strmap);
    decoded.push_back(strmap);
  }
}

uint32_t eformat::helper::size_word 
(const std::vector<eformat::helper::StringMap>& decoded)
{
  uint32_t total = 0;
  for (std::vector<eformat::helper::StringMap>::const_iterator 
	 i=decoded.begin(); i!=decoded.end(); ++i) {
    for (eformat::helper::StringMap::const_iterator 
	   j=i->begin(); j!=i->end(); ++j) {
      total += (j->first.size() + j->second.size() + 2);
    }
  }
  return ((total / sizeof(uint32_t)) + ((total % sizeof(uint32_t))?1:0));
}

void eformat::helper::encode 
(const std::vector<eformat::helper::StringMap>& decoded,
 uint32_t& szword, uint32_t* encoded)
{
  if (szword < size_word(decoded))
    throw EFORMAT_BLOCK_SIZE_TOO_SMALL(size_word(decoded), szword);

  char* dest = reinterpret_cast<char*>(encoded);
  for (std::vector<eformat::helper::StringMap>::const_iterator 
	 i=decoded.begin(); i!=decoded.end(); ++i) {
    for (eformat::helper::StringMap::const_iterator 
	   j=i->begin(); j!=i->end(); ++j) {
      std::copy((j->first).begin(), (j->first).end(), dest); 
      dest += j->first.size();
      (*dest++) = '=';
      std::copy((j->second).begin(), (j->second).end(), dest); 
      dest += j->second.size();
      (*dest++) = ';';
    }
    *(dest-1) = 0; //removes the last ';', it is not needed
  }

  //fill the remaining gaps
  size_t to_fill = (sizeof(uint32_t)*szword) - 
    (dest - reinterpret_cast<char*>(encoded));
  while (to_fill--) dest[to_fill] = 0;
}
