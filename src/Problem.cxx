/**
 * @file src/FragmentProblem.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Dictionaries
 */

#include "eformat/Problem.h"

static eformat::helper::EnumClass<eformat::FragmentProblem> 
  make_FragmentProblem_dictionary()
{
  eformat::helper::EnumClass<eformat::FragmentProblem> d(16, "UNKNOWN");
  d.add(eformat::NO_PROBLEM, "NO_PROBLEM");
  d.add(eformat::WRONG_MARKER, "WRONG_MARKER");
  d.add(eformat::WRONG_ROD_MARKER, "WRONG_ROD_MARKER");
  d.add(eformat::UNSUPPORTED_VERSION, "UNSUPPORTED_VERSION");
  d.add(eformat::WRONG_FRAGMENT_SIZE, "WRONG_FRAGMENT_SIZE");
  d.add(eformat::UNSUPPORTED_ROD_VERSION, "UNSUPPORTED_ROD_VERSION");
  d.add(eformat::WRONG_ROD_HEADER_SIZE, "WRONG_ROD_HEADER_SIZE");
  d.add(eformat::WRONG_ROD_FRAGMENT_SIZE, "WRONG_ROD_FRAGMENT_SIZE");
  return d;
}

const eformat::helper::EnumClass<eformat::FragmentProblem> 
  eformat::helper::FragmentProblemDictionary = make_FragmentProblem_dictionary();
