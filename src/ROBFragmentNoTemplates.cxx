/**
 * @file ROBFragmentNoTemplates.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Implements untemplated read-only ROB fragments. 
 */

#include "eformat/ROBFragmentNoTemplates.h"
#include "eformat/HeaderMarker.h"
#include "eformat/Issue.h"

eformat::read::ROBFragment::ROBFragment (const uint32_t* it)
  : eformat::Header(it),
    m_start()
{
  eformat::Header::specific_header(m_start);
}

eformat::read::ROBFragment::ROBFragment () 
  : Header(), m_start()
{
}

eformat::read::ROBFragment::ROBFragment (const eformat::helper::u32list& blob)
  : eformat::Header(blob.ptr()),
    m_start()
{
  eformat::Header::specific_header(m_start);
}

eformat::read::ROBFragment::ROBFragment (const eformat::helper::u32slice& blob)
  : eformat::Header(blob.ptr()),
    m_start()
{
  eformat::Header::specific_header(m_start);
}

eformat::read::ROBFragment::~ROBFragment ()
{
}

eformat::read::ROBFragment& 
eformat::read::ROBFragment::assign (const uint32_t* it)
{
  eformat::Header::assign(it);
  eformat::Header::specific_header(m_start);
  return *this;
}

bool 
eformat::read::ROBFragment::check_rob_impl (const uint16_t version,
					    const bool exc) 
  const
{
  bool result = true;

  try{
    if (eformat::Header::marker() != eformat::ROB) {
      throw EFORMAT_WRONG_MARKER(marker(), eformat::ROB);
    }
    if (eformat::Header::version() >> 16 != version) {
      throw EFORMAT_BAD_VERSION(eformat::Header::version() >> 16, version);
    }
    uint32_t calculated = 7 + eformat::Header::nstatus();
    if (eformat::Header::header_size_word() != calculated) {
      throw EFORMAT_SIZE_CHECK(calculated, eformat::Header::header_size_word());
    }
  }catch(eformat::WrongMarkerIssue &ex){
    if(exc) throw ex;
    result = false;
  }catch(eformat::SizeCheckIssue &ex){
    if(exc) throw ex;
    result = false;
  }catch(eformat::BadVersionIssue &ex){
    if(exc) throw ex;
    result = false;
  }
  return result;
}

bool eformat::read::ROBFragment::check_rob_noex (const uint16_t version) const
{
  return this->check_rob_impl(version, false);
}

bool eformat::read::ROBFragment::check_rob (const uint16_t version) const
{
    return this->check_rob_impl(version, true);
}


bool 
eformat::read::ROBFragment::check_rod_impl (const uint16_t version, 
					    const bool exc) 
  const
{
  bool result = true;

  try{

    //First check there at least enough ROD data to check the next field
    if ( payload_size_word() < 2){
      throw EFORMAT_ROD_SIZE_CHECK(2, payload_size_word());
    }

    if ( rod_header_size_word() != 9 ) {
      throw EFORMAT_SIZE_CHECK(9, rod_header_size_word());
    }
    if ( rod_version() >> 16 != version ) {
      throw EFORMAT_BAD_ROD_VERSION(rod_version() >> 16, version);
    }
    if ( rod_fragment_size_word() != 12 + rod_nstatus() + rod_ndata() ) {
      throw EFORMAT_ROD_SIZE_CHECK(rod_fragment_size_word(),
				   (12 + rod_nstatus() + rod_ndata()));
    }
  }catch(eformat::RODSizeCheckIssue &ex){
    if(exc) throw ex;
    result = false;
  }catch(eformat::SizeCheckIssue &ex){
    if(exc) throw ex;
    result = false;
  }catch(eformat::BadRodVersionIssue &ex){
    if(exc) throw ex;
    result = false;
  }

  return result;
}

bool eformat::read::ROBFragment::check_rod_noex (const uint16_t version) const
{
  return this->check_rod_impl(version, false);
}

bool eformat::read::ROBFragment::check_rod (const uint16_t version) const
{
  return this->check_rod_impl(version, true);
}


bool 
eformat::read::ROBFragment::check_impl(const uint16_t version, 
				       const uint16_t rod_version,
				       const bool exc) 
  const
{
  return this->check_rob_impl(version, exc) && 
    this->check_rod_impl(rod_version, exc);
}

bool 
eformat::read::ROBFragment::check_noex (const uint16_t version,
					const uint16_t rod_version) const
{
  return this->check_impl(version, rod_version, false);
}

bool 
eformat::read::ROBFragment::check (const uint16_t version,
				   const uint16_t rod_version) const
{
  return this->check_impl(version, rod_version, true);
}

void eformat::read::ROBFragment::rob_problems 
  (std::vector<eformat::FragmentProblem>& p, const uint16_t version) const 
{
  //ROB checking
  if (eformat::Header::marker() != eformat::ROB)
    p.push_back(eformat::WRONG_MARKER);
  if ( eformat::Header::version() >> 16 != version )
    p.push_back(eformat::UNSUPPORTED_VERSION);
  uint32_t calculated = 7 + eformat::Header::nstatus();
  if (eformat::Header::header_size_word() != calculated)
    p.push_back(eformat::WRONG_FRAGMENT_SIZE);
}

void eformat::read::ROBFragment::rod_problems 
  (std::vector<eformat::FragmentProblem>& p, const uint16_t version) const 
{
  //ROD stuff
  if(payload_size_word() < 3) {
    // The ROD fragment is smaller than the header size, it is not safe to
    // try to access the fields below, we skip the following checks
    p.push_back(eformat::WRONG_ROD_FRAGMENT_SIZE);
    return;
  }

  if (rod_marker() != eformat::ROD) p.push_back(eformat::WRONG_ROD_MARKER); 
  if (rod_version() >> 16 != version)
    p.push_back(eformat::UNSUPPORTED_ROD_VERSION);
  if (rod_header_size_word() != 9) p.push_back(eformat::WRONG_ROD_HEADER_SIZE);
  if (!check_rod_size())
    p.push_back(eformat::WRONG_ROD_FRAGMENT_SIZE);
}

void eformat::read::ROBFragment::problems
(std::vector<eformat::FragmentProblem>& p,
 const uint16_t version, const uint16_t rod_version) const
{
  rob_problems(p, version);
  rod_problems(p, rod_version);
}

bool eformat::read::ROBFragment::check_rod_size() const
{
  if (rod_fragment_size_word() < 12) {
    return false;
  }

  auto ndata = m_start[eformat::Header::payload_size_word()-2];
  auto nstatus = m_start[eformat::Header::payload_size_word()-3];
  return (rod_fragment_size_word() == 12 + nstatus + ndata);
}

const uint32_t* eformat::read::ROBFragment::rod_status () const
{
  if(!check_rod_size()) {
    // The fragment is most probably truncated. We return no status words
    return m_start + rod_fragment_size_word() - 9;
  }
  
  if (rod_status_position()) return m_start + 9 + rod_ndata();
  return m_start + 9;
}

const uint32_t* eformat::read::ROBFragment::rod_data () const
{
  if(!check_rod_size()) {
    // The fragment is most probably truncated. We return the whole payload
    // as data
    return m_start + 9;
  }
    
  if (!rod_status_position()) return m_start + 9 + rod_nstatus();
  return m_start + 9;
}

const uint32_t* eformat::read::ROBFragment::child (size_t n) const
{
  if (n == 0) return m_start;
  return 0;
}

uint32_t eformat::read::ROBFragment::children (const uint32_t** p, size_t max) 
  const
{
  if (max == 0) return 0;
  p[0] = m_start;
  return 1;
}

uint32_t eformat::read::ROBFragment::rod_nstatus () const
{ 
  if(!check_rod_size()){
    // The fragment is most probably truncated. We return zero status words
    return 0;
  }

  return m_start[eformat::Header::payload_size_word()-3]; 
}

uint32_t eformat::read::ROBFragment::rod_ndata () const 
{ 
  if(!check_rod_size()){
    // The fragment is most probably truncated. We return the whole payload
    // as data. We have to make sure there are data beyond the ROD header
    const uint32_t rod_header_size = 9;
    return rod_fragment_size_word() >= rod_header_size ?
      rod_fragment_size_word() - rod_header_size : 0;
  }

  return m_start[eformat::Header::payload_size_word()-2];
}

uint32_t eformat::read::ROBFragment::rod_status_position () const
{ 
  if(!check_rod_size()){
    // The fragment is most probably truncated. We return one
    // (status after data)
    return 1;
  }

  return m_start[eformat::Header::payload_size_word()-1];
}

uint32_t eformat::read::ROBFragment::rob_source_id () const 
{ 
  return Header::source_id(); 
}

const uint32_t* eformat::read::ROBFragment::rod_start () const 
{ 
  return m_start; 
}

uint32_t eformat::read::ROBFragment::rod_marker() const 
{ 
  return m_start[0]; 
}

uint32_t eformat::read::ROBFragment::rod_fragment_size_word() const 
{ 
  return eformat::Header::payload_size_word(); 
}

uint32_t eformat::read::ROBFragment::rod_header_size_word() const 
{ 
  return m_start[1]; 
}

uint32_t eformat::read::ROBFragment::rod_trailer_size_word() const 
{ 
  return 3; 
}

uint32_t eformat::read::ROBFragment::rod_version() const 
{ 
  return m_start[2]; 
}

uint32_t eformat::read::ROBFragment::rod_source_id() const 
{ 
  return m_start[3]; 
}

uint32_t eformat::read::ROBFragment::rod_run_no() const 
{ 
  return m_start[4]; 
}

uint32_t eformat::read::ROBFragment::rod_lvl1_id() const 
{ 
  return m_start[5]; 
}

uint32_t eformat::read::ROBFragment::rod_bc_id() const 
{ 
  return m_start[6]; 
}

uint32_t eformat::read::ROBFragment::rod_lvl1_trigger_type() const 
{ 
  return m_start[7]; 
}

uint32_t eformat::read::ROBFragment::rod_detev_type() const 
{ 
  return m_start[8]; 
}

uint32_t eformat::read::ROBFragment::nchildren () const 
{ 
  return 1; 
}
