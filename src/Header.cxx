/**
 * @file HeaderNoTemplates.cxx 
 * @author <a href="mailto:andre.dos.anjos@cern.ch">Andre Anjos</a> 
 *
 * @brief Implementation of the eformat Header w/o templates
 */

#include "eformat/Header.h"
#include "eformat/checksum.h"

eformat::Header::Header (const uint32_t* it)
  : m_top(it)
{
}

eformat::Header::Header () 
  : m_top()
{
}

eformat::Header::~Header() 
{
}

uint32_t eformat::Header::marker() const 
{ 
  return m_top[0]; 
}

uint32_t eformat::Header::fragment_size_word() const 
{ 
  return m_top[1]; 
}

uint32_t eformat::Header::header_size_word() const 
{ 
  return m_top[2]; 
}

uint32_t eformat::Header::version() const 
{ 
  return m_top[3]; 
}

uint32_t eformat::Header::source_id() const 
{ 
  return m_top[4]; 
}

uint32_t eformat::Header::nstatus () const 
{ 
  return m_top[5]; 
}

const uint32_t* eformat::Header::start () const 
{ 
  return m_top;
}

const uint32_t* eformat::Header::payload () const
{ 
  return m_top + header_size_word();
}

const uint32_t* eformat::Header::end () const 
{ 
  return m_top + fragment_size_word();
}

uint32_t eformat::Header::payload_size_word (void) const
{ 
  return fragment_size_word() - (header_size_word() + (checksum_type()?1:0)); 
}

const uint32_t* eformat::Header::status () const 
{ 
  return m_top + 6;
}

uint32_t eformat::Header::checksum_type () const 
{ 
  return m_top[6 + nstatus()]; 
}

uint32_t eformat::Header::nspecific () const 
{ 
  return header_size_word() - ( 7 + nstatus() );
}
    
const uint32_t* eformat::Header::specific_header () const
{ 
  return m_top + 7 + nstatus(); 
}

uint32_t eformat::Header::checksum_value (void) const 
{ 
  return (checksum_type()?m_top[fragment_size_word()-1]:0); 
}
    
eformat::Header& eformat::Header::assign (const uint32_t* it)
{
  m_top = it;
  return *this;
}

uint32_t eformat::Header::eval_checksum (void) const
{
  return eformat::helper::checksum(checksum_type(), payload(), payload_size_word());
}

bool eformat::Header::checksum (void) const
{
  return (eval_checksum() == checksum_value());
}
