//Dear emacs, this is -*- c++ -*-

/**
 * @file StringMap.h
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre Anjos</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Describes a generic way to read/write string maps to a string of
 * 32-bit words, in a way that the result is 32-bit null padded.
 */

#ifndef EFORMAT_STRINGMAP_H
#define EFORMAT_STRINGMAP_H

#include <string>
#include <vector>
#include <stdint.h>
#include <map>

namespace eformat {

  namespace helper {

    /**
     * Defines a generic string-to-string map that is used as a starting point
     * to encode/decode the fields
     */
    typedef std::map<std::string, std::string> StringMap;
    
    /**
     * Defines how to decode the information on the event. Multiple Tags can
     * exist in a event. They are separated by null characters ('\0'). The
     * total amount of tags is calculated on-the-fly by taking into
     * consideration the total tag size. In order to be 32-bit padded, it is
     * possible that the last few bytes are all null.
     *
     * @param szword The size, in 32-bit words, of the encoded tag vector
     * @param encoded A bytestream of tags encoded in 32-bit words
     * @param decoded A vector of Tag objets, decoded
     */
    void decode (const uint32_t& szword, const uint32_t* encoded,
		 std::vector<StringMap>& decoded);
    
    /**
     * Determines the number of words which are necessary for a stream tag
     * vector to be encoded.
     *
     * @param decoded A vector of string map objets, in decoded form
     */
    uint32_t size_word (const std::vector<StringMap>& decoded);
    
    /**
     * Defines how to encode the information on the event. Multiple Tags can
     * exist in a event. They are separated by null characters ('\0'). The
     * total amount of tags is calculated on-the-fly by taking into
     * consideration the total tag size. In order to be 32-bit padded, it is
     * possible that the last few bytes are all null.
     *
     * @param decoded A vector of map objets, in decoded form
     * @param szword The size, in 32-bit words, of the encoded tag vector
     * @param encoded A bytestream of tags encoded in 32-bit words
     */
    void encode (const std::vector<StringMap>& decoded,
		 uint32_t& szword, uint32_t* encoded);

  }

}

#endif /* EFORMAT_STRINGMAP_H */
