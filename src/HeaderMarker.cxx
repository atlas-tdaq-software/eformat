//Dear emacs, this is -*- c++ -*-

/**
 * @file HeaderMarker.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements a few functionality around the header marker
 */

#include "eformat/HeaderMarker.h"

eformat::HeaderMarker eformat::child_marker(eformat::HeaderMarker e)
{
  using namespace eformat;
  switch (e) {
  case FULL_EVENT:
    return ROB;
  case ROB:
    return ROD;
  default:
    break;
  }
  return FULL_EVENT;
}

std::string eformat::marker2string (const eformat::HeaderMarker& e)
{
  return eformat::helper::HeaderMarkerDictionary.string(e);
}

std::string eformat::marker2string (uint32_t e)
{
  return marker2string((eformat::HeaderMarker)e);
}

static eformat::helper::EnumClass<eformat::HeaderMarker> 
  make_HeaderMarker_dictionary()
{
  eformat::helper::EnumClass<eformat::HeaderMarker> d(0xffffffff, "UNKNOWN_MARKER");
  d.add(eformat::ROD, "ROD");
  d.add(eformat::ROB, "ROB");
  d.add(eformat::FULL_EVENT, "FULL_EVENT");
  return d;
}

const eformat::helper::EnumClass<eformat::HeaderMarker> 
  eformat::helper::HeaderMarkerDictionary = make_HeaderMarker_dictionary();
