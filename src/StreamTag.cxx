//Dear emacs, this is -*- c++ -*-

/**
 * @file StreamTag.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements the stream tag functionality
 */

#include <boost/algorithm/string/case_conv.hpp>

#include "eformat/StreamTag.h"
#include "eformat/Issue.h"
#include "ers/ers.h"
#include "StringMap.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/**
 * This exception is supposed to be thrown when version problems are found
 */
ERS_DECLARE_ISSUE_BASE(eformat, BadTagTypeIssue, eformat::Issue, 
  "StreamTag type \"" << unsup << "\" is not supported."
  << " Understanding this tag type as \"unknown\"", ERS_EMPTY,
  ((std::string)unsup))

/**
 * This exception is supposed to be thrown when string-->ID conversion fails
 */
ERS_DECLARE_ISSUE_BASE(eformat, IDConversionIssue, eformat::Issue, 
		       "Conversion of \"" << str 
		       << "\" into a numeric ID failed. Reason (errno "
		       << err_no << "): " 
		       << ::strerror(err_no), ERS_EMPTY,
		       ((std::string)str) ((uint32_t)err_no))

/**
 * Simplifies the use of this Issue
 *
 * @param unsup The name of the unsupported tag type
 */
#define EFORMAT_BAD_TAGTYPE(unsup) eformat::BadTagTypeIssue(ERS_HERE, unsup)

eformat::TagType eformat::helper::string_to_tagtype(const std::string& orig) {
  std::string t = boost::to_lower_copy(orig);
  if (t == "physics") return eformat::PHYSICS_TAG;
  else if (t == "calibration") return eformat::CALIBRATION_TAG;
  else if (t == "reserved") return eformat::RESERVED_TAG;
  else if (t == "debug") return eformat::DEBUG_TAG;
  else if (t == "unknown") return eformat::UNKNOWN_TAG;
  else if (t == "express") return eformat::EXPRESS_TAG;
  else if (t == "monitoring") return eformat::MONITORING_TAG;
  else ers::warning(EFORMAT_BAD_TAGTYPE(t));
  return eformat::UNKNOWN_TAG; 
}

/**
 * Faster streamlining of tag types. Only used internally.
 */
static void check_tagtype(std::string& t, bool with_warning=true) {
  boost::to_lower(t);
  if (t != "physics" && t != "calibration" && 
      t != "debug" && t != "unknown" &&
      t != "reserved" && t != "express" && t != "monitoring") {
    if (with_warning) ers::warning(EFORMAT_BAD_TAGTYPE(t));
    t = "unknown";
  }
}

std::string eformat::helper::tagtype_to_string(const eformat::TagType& t) {
  switch (t) {
    case eformat::PHYSICS_TAG:
      return "physics";
      break;
    case eformat::CALIBRATION_TAG:
      return "calibration";
      break;
    case eformat::RESERVED_TAG:
      return "reserved";
      break;
    case eformat::DEBUG_TAG:
      return "debug";
      break;
    case eformat::EXPRESS_TAG:
      return "express";
      break;
    case eformat::MONITORING_TAG:
      return "monitoring";
      break;
    default:
      break;
  }
  return "unknown";
}


/**
 * Encodes a tag,value pair into a preallocated buffer.
 * Each pair is translated into a "tag=value;" c-string
 * 
 * @param tag A string representing the tag for this pair
 * @param value A string representing the value for this pair
 * @param dest A pre-allocated buffer where the IDs will end-up
 *
 * @result Pointer to the location in the buffer following the IDs
 */
char* encode_tag(const std::string& tag, const std::string& value, 
		 char* dest){
  
  std::copy(tag.begin(), tag.end(), dest); 
  dest += tag.size();
  (*dest++) = '=';
  std::copy(value.begin(),value.end(), dest); 
  dest += value.size();
  (*dest++) = ';';

  return dest;
}

/**
 * Encodes a list of numeric IDs into a preallocated buffer.
 * Each ID translated in a 8 chars c-string
 * 
 * @param tag A string representing the tag for the IDs
 * @param ids List of numeric IDs to be encoded
 * @param dest A pre-allocated buffer where the IDs will end-up
 *
 * @result Pointer to the location in the buffer following the IDs
 */
template <typename T>
char* encode_id_list(const std::string& tag, const std::set<T> &ids, char * dest){

  //Add the tag
  std::copy(tag.begin(), tag.end(), dest);
  dest += tag.size();
  (*dest++)= '=';

  //Serialize the IDs
  for(typename std::set<T>::const_iterator i = ids.begin();
      i != ids.end(); ++i){
    ::snprintf(dest, 9, "%08x", *i);
    //Only add 8, since we want to overwrite the '\0'
    dest += 8;
  }
  
  //Close the list with ';'
  (*dest++) = ';';

  return dest;
}

/**
 * Decodes a stream input string into list of numeric IDs. Each ID is 
 * supposed to use 8 chars.
 * 
 * @param ids A container that will be filled with the numeric IDs
 * @param encoded A string of IDs in encoded form
 */
template <typename T>
void decode_id_list(std::set<T> &ids, const std::string& encoded){
  
  if(encoded.size()%8){
    //This should not happen
  }
  
  for(uint32_t i=0; i<encoded.size(); i+=8){
    
    std::string tmp = encoded.substr(i,8);

    errno = 0;
    char * end = NULL;
    unsigned long int id = ::strtoul(tmp.c_str(), &end, 16);
    if(errno || end == tmp.c_str() || *end != '\0'){
      ers::warning(eformat::IDConversionIssue(ERS_HERE, tmp, errno));
    }else{
      ids.insert(static_cast<T>(id));
    }
  }
}


eformat::helper::StreamTag::StreamTag(const std::string& n, 
                                      const eformat::TagType& t, bool ol,
				      const std::set<uint32_t>& r,
				      const std::set<eformat::SubDetector>& d)
  : name(n), type(tagtype_to_string(t)), obeys_lumiblock(ol),robs(r), dets(d)
    
{
}

eformat::helper::StreamTag::StreamTag(const std::string& n, 
                                      const std::string& t, bool ol,
				      const std::set<uint32_t>& r,
				      const std::set<eformat::SubDetector>& d)
  : name(n), type(t), obeys_lumiblock(ol),robs(r), dets(d)
{
  check_tagtype(type);
}

bool eformat::helper::StreamTag::operator== 
(const eformat::helper::StreamTag& other) const
{
  if (this->obeys_lumiblock != other.obeys_lumiblock) return false;
  if (this->name != other.name) return false;
  if (this->type != other.type) return false;
  if (this->dets != other.dets) return false;
  if (this->robs != other.robs) return false;
  return true;
}

bool eformat::helper::StreamTag::operator!= 
(const eformat::helper::StreamTag& other) const
{
  return !((*this) == other);
}

bool eformat::helper::StreamTag::operator< 
(const eformat::helper::StreamTag& other) const
{
  return ((this->name+this->type) < (other.name+other.type));
}


void eformat::helper::decode 
(const uint32_t& szword, const uint32_t* encoded,
 std::vector<eformat::helper::StreamTag>& decoded)
{
  std::vector<eformat::helper::StringMap> to_decode;
  eformat::helper::decode(szword, encoded, to_decode);
  for (std::vector<eformat::helper::StringMap>::iterator 
	 i=to_decode.begin(); i!=to_decode.end(); ++i) {
    StreamTag t;
    //this should correctly throw if any of the keys are not found
    //WV the above is not true!
    t.name = (*i)["name"];
    t.type = (*i)["type"];
    check_tagtype(t.type, false); //potentially fixes problems, w/o warnings!
    t.obeys_lumiblock = true;
    if ((*i)["lumi"] == "0") t.obeys_lumiblock = false;

    eformat::helper::StringMap::const_iterator it = (*i).find("robs");
    if (it != (*i).end()){
      decode_id_list(t.robs, it->second);
    }

    it = (*i).find("dets");
    if (it != (*i).end()){
      decode_id_list(t.dets, it->second);
    }

    decoded.push_back(t);
  }
}

void eformat::helper::encode 
(const std::vector<eformat::helper::StreamTag>& decoded,
 uint32_t& szword, uint32_t* encoded)
{

  if (szword < size_word(decoded))
    throw EFORMAT_BLOCK_SIZE_TOO_SMALL(size_word(decoded), szword);

  char * dest = reinterpret_cast<char*>(encoded);

  std::vector<eformat::helper::StringMap> to_encode;
  for (std::vector<eformat::helper::StreamTag>::const_iterator 
	 i=decoded.begin(); i!=decoded.end(); ++i) {
    eformat::helper::StringMap m;
    
    dest = encode_tag("name", i->name, dest);

    std::string type(i->type);
    check_tagtype(type); //potentially fixes problems
    dest = encode_tag("type",type,dest);

    std::string obl = (i->obeys_lumiblock)?"1":"0";
    dest = encode_tag("lumi", obl, dest);

    if(!(i->robs).empty()){
      dest = encode_id_list("robs", i->robs, dest);
    }
    
    if(!(i->dets).empty()){
      dest = encode_id_list("dets", i->dets, dest);
    }

    //Replace last ';' with a '\0'
    *(dest-1) = 0;
  }

  //Fill remaing space with '\0'
  size_t to_fill = (sizeof(uint32_t)*szword) - 
    (dest - reinterpret_cast<char*>(encoded));
  ::bzero(dest, to_fill);
}
    
uint32_t eformat::helper::size_word 
(const std::vector<eformat::helper::StreamTag>& decoded)
{
  uint32_t total = 0;
  for (std::vector<eformat::helper::StreamTag>::const_iterator
      i=decoded.begin(); i!=decoded.end(); ++i) {
    std::string type(i->type);
    check_tagtype(type); //potentially fixes problems
    //how to count:
    //"name" field => 4 chars + size of "name" field + 2 (delimiters)
    //"type" => 4 chars + size of "type" field + 2
    //"lumi" => 4 chars + 1 byte (true/false, 0/1) + 2
    //"robs" => 4 chars + 8 chars * #robs + 2
    //"dets" => 4 chars + 8 chars * #dets + 2
    total += (19 + i->name.size() + type.size()); //name+type+lumi
    
    if(!(i->robs).empty()){
      total += 6 + 8*i->robs.size();
    }
    
    if(!(i->dets).empty()){
      total += 6 + 8*i->dets.size();
    }
  }
  return ((total / sizeof(uint32_t)) + ((total % sizeof(uint32_t))?1:0));
}

uint32_t eformat::helper::contains_type (const std::vector<StreamTag>& v,
                                         uint32_t type)
{
  uint32_t retval = 0;
  for (size_t i=0; i<v.size(); ++i) 
    if (type & string_to_tagtype(v[i].type)) ++retval;
  return retval;
}

static eformat::helper::EnumClass<eformat::TagType> 
  make_TagType_dictionary()
{
  eformat::helper::EnumClass<eformat::TagType> d(0xff, "UNKNOWN");
  d.add(eformat::PHYSICS_TAG, "PHYSICS_TAG");
  d.add(eformat::CALIBRATION_TAG, "CALIBRATION_TAG");
  d.add(eformat::RESERVED_TAG, "RESERVED_TAG");
  d.add(eformat::DEBUG_TAG, "DEBUG_TAG");
  d.add(eformat::UNKNOWN_TAG, "UNKNOWN_TAG");
  d.add(eformat::EXPRESS_TAG, "EXPRESS_TAG");
  d.add(eformat::MONITORING_TAG, "MONITORING_TAG");
  return d;
}

const eformat::helper::EnumClass<eformat::TagType> 
  eformat::helper::TagTypeDictionary = make_TagType_dictionary();

