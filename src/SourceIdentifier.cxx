//Dear emacs, this is -*- c++ -*-

/**
 * @file SourceIdentifier.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andre Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines a type that can perform the conversion between source
 * identifier components and the its 32-bit version.
 */

#include "eformat/SourceIdentifier.h"
#include <sstream>

eformat::helper::SourceIdentifier::SourceIdentifier 
(eformat::SubDetector subdet, uint16_t id, uint8_t optional)
  : m_sd(subdet),
    m_id(id),
    m_opt(optional)
{
}

eformat::helper::SourceIdentifier::SourceIdentifier (uint32_t sid)
  : m_sd(static_cast<eformat::SubDetector>((sid >> 16) & 0xff)),
    m_id(static_cast<uint16_t>(0xffff & sid)),
    m_opt(static_cast<uint8_t>(0xff & (sid >> 24)))
{
}

uint32_t eformat::helper::SourceIdentifier::code (void) const
{
  uint32_t retval = (0xff) & m_opt;
  retval <<= 8;
  retval |= (0xff) & m_sd;
  retval <<= 16;
  retval |= m_id;
  return retval;
}

uint32_t eformat::helper::SourceIdentifier::simple_code (void) const
{
  uint32_t retval = 0; 
  retval |= (0xff) & m_sd;
  retval <<= 16;
  retval |= m_id;
  return retval;
}

std::string eformat::helper::SourceIdentifier::human (void) const
{
  std::ostringstream oss;
  oss << this->human_detector() << ", module=" << (unsigned int)m_id;
  oss << " (opt=" << (unsigned int)m_opt << ")";
  return oss.str();
}

std::string eformat::helper::SourceIdentifier::human_detector (void) const
{
  return eformat::helper::SubDetectorDictionary.string(subdetector_id());
}

std::string eformat::helper::SourceIdentifier::human_group (void) const
{
  return eformat::helper::SubDetectorGroupDictionary.string(subdetector_group());
}

static eformat::helper::EnumClass<eformat::SubDetectorGroup> 
  make_SubDetectorGroup_dictionary()
{
  eformat::helper::EnumClass<eformat::SubDetectorGroup> d(0x9, "UNKNOWN_SUBDETECTOR_GROUP");
  d.add(eformat::ANY_DETECTOR, "ANY_DETECTOR");
  d.add(eformat::PIXEL, "PIXEL");
  d.add(eformat::SCT, "SCT");
  d.add(eformat::TRT, "TRT");
  d.add(eformat::LAR, "LAR");
  d.add(eformat::TILECAL, "TILECAL");
  d.add(eformat::MUON, "MUON");
  d.add(eformat::TDAQ, "TDAQ");
  d.add(eformat::FORWARD, "FORWARD");
  d.add(eformat::OTHER_DETECTORS, "OTHER_DETECTORS");
  return d;
}

const eformat::helper::EnumClass<eformat::SubDetectorGroup> 
  eformat::helper::SubDetectorGroupDictionary = 
  make_SubDetectorGroup_dictionary();

static eformat::helper::EnumClass<eformat::SubDetector> 
  make_SubDetector_dictionary()
{
  eformat::helper::EnumClass<eformat::SubDetector> d(0xf0, "UNKNOWN");
  d.add(eformat::FULL_SD_EVENT, "FULL_SD_EVENT");
  d.add(eformat::PIXEL_BARREL, "PIXEL_BARREL");
  d.add(eformat::PIXEL_DISK, "PIXEL_DISK");
  d.add(eformat::PIXEL_B_LAYER, "PIXEL_B_LAYER");
  d.add(eformat::PIXEL_IBL, "PIXEL_IBL");
  d.add(eformat::PIXEL_DBM, "PIXEL_DBM");
  d.add(eformat::SCT_BARREL_A_SIDE, "SCT_BARREL_A_SIDE");
  d.add(eformat::SCT_BARREL_C_SIDE, "SCT_BARREL_C_SIDE");
  d.add(eformat::SCT_ENDCAP_A_SIDE, "SCT_ENDCAP_A_SIDE");
  d.add(eformat::SCT_ENDCAP_C_SIDE, "SCT_ENDCAP_C_SIDE");
  d.add(eformat::TRT_ANCILLARY_CRATE, "TRT_ANCILLARY_CRATE");
  d.add(eformat::TRT_BARREL_A_SIDE, "TRT_BARREL_A_SIDE");
  d.add(eformat::TRT_BARREL_C_SIDE, "TRT_BARREL_C_SIDE");
  d.add(eformat::TRT_ENDCAP_A_SIDE, "TRT_ENDCAP_A_SIDE");
  d.add(eformat::TRT_ENDCAP_C_SIDE, "TRT_ENDCAP_C_SIDE");
  d.add(eformat::LAR_EM_BARREL_A_SIDE, "LAR_EM_BARREL_A_SIDE");
  d.add(eformat::LAR_EM_BARREL_C_SIDE, "LAR_EM_BARREL_C_SIDE");
  d.add(eformat::LAR_EM_ENDCAP_A_SIDE, "LAR_EM_ENDCAP_A_SIDE");
  d.add(eformat::LAR_EM_ENDCAP_C_SIDE, "LAR_EM_ENDCAP_C_SIDE");
  d.add(eformat::LAR_HAD_ENDCAP_A_SIDE, "LAR_HAD_ENDCAP_A_SIDE");
  d.add(eformat::LAR_HAD_ENDCAP_C_SIDE, "LAR_HAD_ENDCAP_C_SIDE");
  d.add(eformat::LAR_FCAL_A_SIDE, "LAR_FCAL_A_SIDE");
  d.add(eformat::LAR_FCAL_C_SIDE, "LAR_FCAL_C_SIDE");
  d.add(eformat::LAR_EM_BARREL_ENDCAP_A_SIDE, "LAR_EM_BARREL_ENDCAP_A_SIDE");
  d.add(eformat::LAR_EM_BARREL_ENDCAP_C_SIDE, "LAR_EM_BARREL_ENDCAP_C_SIDE");
  d.add(eformat::LAR_EM_HAD_ENDCAP_A_SIDE, "LAR_EM_HAD_ENDCAP_A_SIDE");
  d.add(eformat::LAR_EM_HAD_ENDCAP_C_SIDE, "LAR_EM_HAD_ENDCAP_C_SIDE");
  d.add(eformat::TILECAL_LASER_CRATE, "TILECAL_LASER_CRATE");
  d.add(eformat::TILECAL_BARREL_A_SIDE, "TILECAL_BARREL_A_SIDE");
  d.add(eformat::TILECAL_BARREL_C_SIDE, "TILECAL_BARREL_C_SIDE");
  d.add(eformat::TILECAL_EXT_A_SIDE, "TILECAL_EXT_A_SIDE");
  d.add(eformat::TILECAL_EXT_C_SIDE, "TILECAL_EXT_C_SIDE");
  d.add(eformat::MUON_ANCILLARY_CRATE, "MUON_ANCILLARY_CRATE");
  d.add(eformat::MUON_MDT_BARREL_A_SIDE, "MUON_MDT_BARREL_A_SIDE");
  d.add(eformat::MUON_MDT_BARREL_C_SIDE, "MUON_MDT_BARREL_C_SIDE");
  d.add(eformat::MUON_MDT_ENDCAP_A_SIDE, "MUON_MDT_ENDCAP_A_SIDE");
  d.add(eformat::MUON_MDT_ENDCAP_C_SIDE, "MUON_MDT_ENDCAP_C_SIDE");
  d.add(eformat::MUON_RPC_BARREL_A_SIDE, "MUON_RPC_BARREL_A_SIDE");
  d.add(eformat::MUON_RPC_BARREL_C_SIDE, "MUON_RPC_BARREL_C_SIDE");
  d.add(eformat::MUON_TGC_ENDCAP_A_SIDE, "MUON_TGC_ENDCAP_A_SIDE");
  d.add(eformat::MUON_TGC_ENDCAP_C_SIDE, "MUON_TGC_ENDCAP_C_SIDE");
  d.add(eformat::MUON_CSC_ENDCAP_A_SIDE, "MUON_CSC_ENDCAP_A_SIDE");
  d.add(eformat::MUON_CSC_ENDCAP_C_SIDE, "MUON_CSC_ENDCAP_C_SIDE");
  d.add(eformat::MUON_MMEGA_ENDCAP_A_SIDE, "MUON_MMEGA_ENDCAP_A_SIDE");
  d.add(eformat::MUON_MMEGA_ENDCAP_C_SIDE, "MUON_MMEGA_ENDCAP_C_SIDE");
  d.add(eformat::MUON_STGC_ENDCAP_A_SIDE, "MUON_STGC_ENDCAP_A_SIDE");
  d.add(eformat::MUON_STGC_ENDCAP_C_SIDE, "MUON_STGC_ENDCAP_C_SIDE");
  d.add(eformat::TDAQ_BEAM_CRATE, "TDAQ_BEAM_CRATE");
  d.add(eformat::TDAQ_CALO_PREPROC, "TDAQ_CALO_PREPROC");
  d.add(eformat::TDAQ_CALO_CLUSTER_PROC_DAQ, "TDAQ_CALO_CLUSTER_PROC_DAQ");
  d.add(eformat::TDAQ_CALO_CLUSTER_PROC_ROI, "TDAQ_CALO_CLUSTER_PROC_ROI");
  d.add(eformat::TDAQ_CALO_JET_PROC_DAQ, "TDAQ_CALO_JET_PROC_DAQ");
  d.add(eformat::TDAQ_CALO_JET_PROC_ROI, "TDAQ_CALO_JET_PROC_ROI");
  d.add(eformat::TDAQ_MUON_CTP_INTERFACE, "TDAQ_MUON_CTP_INTERFACE");
  d.add(eformat::TDAQ_CTP, "TDAQ_CTP");
  d.add(eformat::TDAQ_L2SV, "TDAQ_L2SV");
  d.add(eformat::TDAQ_SFI, "TDAQ_SFI");
  d.add(eformat::TDAQ_SFO, "TDAQ_SFO");
  d.add(eformat::TDAQ_LVL2, "TDAQ_LVL2");
  d.add(eformat::TDAQ_HLT, "TDAQ_HLT");
  d.add(eformat::TDAQ_LAR_MET, "TDAQ_LAR_MET");
  d.add(eformat::TDAQ_TILE_MET, "TDAQ_TILE_MET");
  d.add(eformat::TDAQ_FTK, "TDAQ_FTK");
  d.add(eformat::FORWARD_BCM, "FORWARD_BCM");
  d.add(eformat::FORWARD_LUCID, "FORWARD_LUCID");
  d.add(eformat::FORWARD_ZDC, "FORWARD_ZDC");
  d.add(eformat::FORWARD_ALPHA, "FORWARD_ALPHA");
  d.add(eformat::FORWARD_AFP, "FORWARD_AFP");
  d.add(eformat::TDAQ_CALO_TOPO_PROC, "TDAQ_CALO_TOPO_PROC");
  d.add(eformat::TDAQ_CALO_DIGITAL_PROC, "TDAQ_CALO_DIGITAL_PROC");
  d.add(eformat::TDAQ_CALO_FEAT_EXTRACT_DAQ, "TDAQ_CALO_FEAT_EXTRACT_DAQ");
  d.add(eformat::TDAQ_CALO_FEAT_EXTRACT_ROI, "TDAQ_CALO_FEAT_EXTRACT_ROI");
  d.add(eformat::OTHER, "OTHER");
  return d;
}

const eformat::helper::EnumClass<eformat::SubDetector> 
  eformat::helper::SubDetectorDictionary = make_SubDetector_dictionary();

