//Dear emacs, this is -*- c++ -*-

/**
 * @file RunType.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Implements RunType to string to conversion functions
 */

#include "eformat/RunType.h"

std::string eformat::run_type2string (const eformat::RunType& e)
{
  using namespace eformat;
  std::string retval = "UNKNOWN_RUNTYPE";
  switch (e & 0x7fffffff) {
  case PHYSICS:
    retval = "PHYSICS";
    break;
  case CALIBRATION:
    retval = "CALIBRATION";
    break;
  case COSMICS:
    retval = "COSMICS";
    break;
  case TEST:
    retval = "TEST";
    break;
  }
  if (e & eformat::SIMULATION) retval += " (SIMULATION)";
  return retval; 
}

std::string eformat::run_type2string (uint32_t e)
{
  return run_type2string((eformat::RunType)e);
}

static eformat::helper::EnumClass<eformat::RunType> 
  make_RunType_dictionary()
{
  eformat::helper::EnumClass<eformat::RunType> d(0xffffffff, "UNKNOWN_RUNTYPE");
  d.add(eformat::PHYSICS, "PHYSICS");
  d.add(eformat::CALIBRATION, "CALIBRATION");
  d.add(eformat::COSMICS, "COSMICS");
  d.add(eformat::SIMULATION, "SIMULATION");
  d.add(eformat::TEST, "TEST");
  return d;
}

const eformat::helper::EnumClass<eformat::RunType> 
  eformat::helper::RunTypeDictionary = make_RunType_dictionary();


