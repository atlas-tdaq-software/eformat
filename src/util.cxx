//Dear emacs, this is -*- c++ -*-

/**
 * @file util.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andre Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Implements a set of utilities common to many event operations.
 */

#include "eformat/util.h"
#include "eformat/ROBFragment.h"
#include "eformat/FullEventFragment.h"
#include "eformat/Version.h"
#include "eformat/compression.h"
#include "ers/ers.h"

//for the conversion
#include "src/old/v24_util.h"
#include "src/old/v30_util.h"

#include <boost/format.hpp>
#include <boost/core/null_deleter.hpp>
#include <memory>
#include <cstring>

#include <iostream>

uint32_t* eformat::next_fragment (std::fstream& fs, uint32_t* addr,
				  size_t size)
{
  using namespace eformat;

  off_t offset = fs.tellg();
  uint32_t data[2];
  if (fs && fs.good() && ! fs.eof()) {
    //soft check, so we don't make mistakes
    fs.read((char*)data, 8);
    if (!fs.good() || fs.eof()) return 0; // end-of-file 
    switch((HeaderMarker)data[0]) {
    case FULL_EVENT:
    case ROB:
    case ROD:
      break;
    default:
      return 0;
    }
  }
  else return 0; //file is over

  //data[1] is the fragment size, in words. Take it and read the fragment
  fs.seekg(offset);
  if (addr && (size >= (data[1]*4))) {
    //space is preallocated and checked
    fs.read((char*)addr, data[1]*4);
    return addr;
  }
  else if (addr) {
    return 0;
  }

  //if I get here, is because I have to allocate space myself
  uint32_t* retval = new uint32_t[data[1]];
  fs.read((char*)retval, data[1]*4);
  return retval;
}

size_t eformat::find_rods (const uint32_t* block_start, size_t block_size,
			   const uint32_t** rod, uint32_t* rod_size,
			   size_t max_count)
{
  const uint32_t* block_end = block_start + block_size;
  size_t counter = 0;
  while (block_end > block_start) {
    uint32_t curr_rod_size = 12; // base size for a ROD as eformat 2.4
    const uint32_t status_size = *(block_end-3);
    const uint32_t data_size = *(block_end-2);
    curr_rod_size += status_size;
    curr_rod_size += data_size;

    const uint32_t available_size = block_end - block_start;

    // We are going to be extra careful here, since we cannot trust the
    // data we receive. In particular each addition operation can roll over
    // if one of the elements is big enough.

    if (data_size > available_size ||
        status_size > available_size ||
        curr_rod_size > available_size) {
      // Something went wrong, most probably the ROD fragment
      // is corrupted (or it is not a ROD fragment at all)
      throw EFORMAT_WRONG_SIZE(curr_rod_size);
    }

    block_end -= curr_rod_size;

    if (rod && counter < max_count) {
      if (block_end[0] != eformat::ROD) 
        throw EFORMAT_WRONG_MARKER(block_end[0], eformat::ROD);
      rod_size[counter] = curr_rod_size;
      rod[counter] = block_end;
    }
    else if (max_count) break;
    ++counter;
  }
  return counter;
}

size_t eformat::find_fragments (uint32_t marker, 
    const uint32_t* block_start, size_t block_size,
    const uint32_t** frag, size_t max_count)
{
  uint32_t counter = 0;
  const uint32_t* next = block_start;
  const uint32_t* endp = block_start + block_size;
  while (next < endp) {
    if (next[0] != marker) {
      if (counter > 0) return counter;
      else return 1;
    }
    if (frag && counter < max_count) frag[counter] = next;
    else if (max_count) break;
    ++counter;
    next += next[1];
  }
  return counter;
}

size_t eformat::find_fragments (uint32_t marker, 
    uint32_t* block_start, size_t block_size, uint32_t** frag, size_t max_count)
{
  return eformat::find_fragments((uint32_t)marker, block_start, 
      block_size, (const uint32_t**)frag, max_count);
}

size_t eformat::find_fragments (eformat::HeaderMarker marker, 
    const uint32_t* block_start, size_t block_size,
    const uint32_t** frag, size_t max_count)
{
  return eformat::find_fragments((uint32_t)marker, block_start, 
      block_size, frag, max_count);
}

size_t eformat::find_fragments (eformat::HeaderMarker marker, 
    uint32_t* block_start, size_t block_size, uint32_t** frag, size_t max_count)
{
  return eformat::find_fragments((uint32_t)marker, block_start, 
      block_size, (const uint32_t**)frag, max_count);
}

size_t eformat::get_robs (const uint32_t* fragment, const uint32_t** rob,
                          size_t max_count)
{
  using namespace eformat;

  size_t counter = 0;
  switch ((eformat::HeaderMarker)(fragment[0])) {
    case ROD:
      return 0;
    case ROB:
      {
        if ( max_count > 0 ) {
          rob[0] = fragment;
          ++counter;
        }
      }
      break;
    case FULL_EVENT:
      {
        eformat::FullEventFragment<const uint32_t*> fe(fragment);
        counter = fe.children(rob, max_count);
      }
      break;
  }

  return counter;
}

void eformat::get_robs  (const uint32_t* fragment,
			 std::vector<std::shared_ptr<const uint32_t>>& robs)
{

  switch ((eformat::HeaderMarker)(fragment[0])) {
    case ROD:
      return;
    case ROB:
      {
	robs.emplace_back(fragment, boost::null_deleter());
      }
      break;
    case FULL_EVENT:
      {
	eformat::read::FullEventFragment fe(fragment);
	bool compressed = (fe.compression_type() > eformat::UNCOMPRESSED);
	eformat::read::FullEventFragment::child_iter_t iter = fe.child_iter();
	
	while (const uint32_t* fp = iter.next()) {
	  
	  if (compressed) {
	    eformat::read::ROBFragment f(fp);
	    auto size = f.fragment_size_word();
	    robs.emplace_back(new uint32_t[size], 
			     [](const uint32_t* p) { delete[] p; } );
	    std::memcpy(const_cast<uint32_t *>(robs.back().get()), 
			fp, sizeof(uint32_t)*size);
	  } else {
	    robs.emplace_back(fp, boost::null_deleter());
	  }
	}
      }
      break;
  }

  return;
}

uint16_t eformat::peek_major_version (const uint32_t* fragment) {
  uint32_t value = 0;
  switch (eformat::peek_type(fragment)) {
  case eformat::ROD:
    value = fragment[2];
    break;
  default:
    value = fragment[3];
    break;
  }
  eformat::helper::Version v(value);
  return v.major_version();
}
