#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to eformat fragment reading."""
from __future__ import division
from __future__ import print_function

from past.utils import old_div
import unittest
import eformat
import time
INPUT= 'test.data'
ESCOMP='comp.data'
EFCOMP='eformatcomp.data'

dataset = [INPUT,ESCOMP,EFCOMP]

class ZBenchMarker(unittest.TestCase):
  """Tests if we can read data with the python eformat interface."""

  def test01_data_read(self):
    
    for f in dataset:
      cpu_time_used = 0
      elements_read = 0
      robs_read = 0
      for e in eformat.istream(f):
        tmp = 0
        start = time.time()
        for r in e:
          data = r.rod_data()
          tmp += sum(data)
          elements_read += len(data)
          robs_read += 1
        cpu_time_used += time.time() - start
      print()
      print(" Input: %s" % f)
      print(" Statistics for (python) ROB data access:") 
      print(" ----------------------------------------")
      print("  - Total reading time: %.2e seconds" % cpu_time_used)
      print("  - Reading time per ROB (%d): %.2e millisecs" % \
          (robs_read, old_div(1000*cpu_time_used,robs_read)))
      print("  - Reading time per data word in a ROB (%d): %.2e microsecs" % \
          (elements_read, old_div(1000000*cpu_time_used,elements_read)))
      print()

  def test02_header_access(self):

    for f in dataset:
      cpu_time_used = 0
      validation = 0
      events_read = 0
      robs_read = 0
      for e in eformat.istream(INPUT):
        tmp = 0
        start = time.time()
        for r in e:
          tmp += r.rod_lvl1_id()
          robs_read += 1
        cpu_time_used += time.time() - start
        start = time.time()
        e.check_tree()
        validation += time.time() - start
        events_read += 1
      cpu_time_used *= 1000
      print()
      print(" Input: %s" % f)
      print(" Statistics for (python) ROB header access:") 
      print(" ------------------------------------------")
      print("  - Total reading time: %.2e millisecs" % cpu_time_used)
      print("  - Reading time per ROB (%d): %.2e microsecs" % \
          (robs_read, old_div(1000*cpu_time_used,robs_read)))
      print("  - Validation per event (after header access): %2.e millisecs" % \
          (old_div(1000*validation,events_read)))
      print()

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
