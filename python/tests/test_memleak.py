#!/usr/bin/env tdaq_python

"""This program should never create a memory leakage.

Because of the way we tie together the lifetimes of the fragments and
contiguous blocks of data (like e.g. status blocks), the act of "resetting" a
block will not delete the previous allocated block before the actual eformat
fragment holding that block is deleted.

This happens because we use the "with_custodian_and_ward" Boost.Python policy
and are not wrapping directly the pointers, but u32{slice,list}'s to achieve
the effect of pointer passing. This artifact make's it difficult to release the
memory occupied by those, unless the holding eformat fragment is deleted.

To test for that, you can move the declaration of the eformat fragments out of
the loop bellow and let the program reset the block data forever on those. You
will be able to observe that the amount of memory occupied by the program will
increase forever.

Under normal circumstances of usage, this should not present a problem, since
rarely somebody will reset so many times a single fragment, preffering to
create new ones. This behaviour will then, trigger the correct memory
management for the objects.
"""
import random
import eformat
import sys

print("Testing for memory leaks... press ctrl + c to finish.")

while True:
  
  fullFrag = eformat.write.FullEventFragment()
  subFrag = eformat.write.SubDetectorFragment()
  rosFrag = eformat.write.ROSFragment()
  robFrag = eformat.write.ROBFragment()

  #Creating the event
  rodData = list(range(90000));
  rodStatus = list(range(10000))
  lvl2Info = list(range(20000))
  efInfo = list(range(50000))
  status = list(range(40000))
  stream = [];
  for i in range(30000):
    a = eformat.helper.StreamTag();
    a.name = 'Name-%d' % i;
    a.type = 'Type-%d' % i;
    stream.append(a);

  fullFrag.lvl2_trigger_info(lvl2Info)
  fullFrag.event_filter_info(efInfo)
  fullFrag.status(status)
  fullFrag.stream_tag(stream)

  a = fullFrag.lvl2_trigger_info()
  b = fullFrag.event_filter_info()
  c = fullFrag.status()
  d = fullFrag.stream_tag()

  subFrag.status(status)
  e = subFrag.status()
  
  rosFrag.status(status)
  f = rosFrag.status()
  
  robFrag.status(status)
  robFrag.rod_status(rodStatus)
  robFrag.rod_data(rodData)
  
  g = robFrag.status()
  i = robFrag.rod_status()
  j = robFrag.rod_data()
  sys.stdout.write('.')  
  sys.stdout.flush()
