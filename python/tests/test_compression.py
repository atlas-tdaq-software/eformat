#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to the EventStorage."""

import unittest
import eformat
import os

INPUT = 'test.data'
OUTPUT = 'comp.data'


class FileCompression(unittest.TestCase):

  def test01_WriteCompressedFile(self):
    ins = eformat.istream(INPUT)
    out = eformat.compstream(compression=eformat.EventStorage.CompressionType.ZLIB)
    for e in ins:
      out.write(e)

    fname = out.last_filename()
    del out
    os.rename(fname,OUTPUT)

  def test02_ReadCompressedFile(self):
    ins = eformat.istream(OUTPUT)

    for e in ins:
      _ = len(e)

  def test03_CheckEventInCompressedFile(self):
    raw = eformat.istream(INPUT)
    comp = eformat.istream(OUTPUT)

    return all([a==b for a,b in zip(raw,comp)])
    

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  eformat.dummy.make_file(INPUT, 10, verbose=True)
  unittest.main()
  
