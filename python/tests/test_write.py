#!/usr/bin/env tdaq_python

"""Unit test for libpyeformat_write"""

import unittest
import sys
import eformat

class ROBFragmentTestCase(unittest.TestCase):
  """
  Tests the ROBFragment class.
  """
  
  def testGetAndSetAttributes(self):
    frag = eformat.write.ROBFragment()
    status = [1,2,3]
    rodStatus = [4,5,6,7,8]
    rodData = [9,10,11,12,13,14,15,16,17,18,19,20]
    
    sid = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)
    sid_rod = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x2)

    frag.status(status)
    for f,t in zip(frag.status(), status): self.assertEqual(f, t)
    frag.minor_version(1)
    self.assertEqual(frag.minor_version(), 1)
    frag.rod_status(rodStatus)
    for f,t in zip(frag.rod_status(), rodStatus): self.assertEqual(f, t)
    frag.status_position(0)
    self.assertEqual(frag.status_position(), 0)
    frag.minor_version(2)
    self.assertEqual(frag.minor_version(), 2)
    frag.rod_data(rodData)
    for f,t in zip(frag.rod_data(), rodData): self.assertEqual(f, t)
    frag.rod_source_id(sid_rod)
    frag.rob_source_id(sid)
    self.assertEqual(frag.rob_source_id(), sid)
    frag.rod_source_id(sid_rod)
    self.assertEqual(frag.rod_source_id(), sid_rod)

    frag.rod_run_no(5)
    self.assertEqual(frag.rod_run_no(), 5)
    frag.rod_lvl1_id(6)
    self.assertEqual(frag.rod_lvl1_id(), 6)
    frag.rod_bc_id(7)
    self.assertEqual(frag.rod_bc_id(), 7)
    frag.rod_lvl1_trigger_type(8)
    self.assertEqual(frag.rod_lvl1_trigger_type(), 8)
    frag.rod_detev_type(9)
    self.assertEqual(frag.rod_detev_type(), 9)
    
    frag.source_id(sid)
    self.assertEqual(frag.rob_source_id(), sid)
    self.assertEqual(frag.rod_source_id(), sid)

class FullEventFragmentTestCase(unittest.TestCase):
  """
  Tests the FullEventFragment class.
  """
  
  def testGetAndSetAttributes(self):
    frag = eformat.write.FullEventFragment()
    status = [13748, 59697, 748596, 23433, 234345, 5456]
    L2Data = [75869]
    EFData = [89,4567,3245,3456,78543,5678,432,4657,6789,534,6345,567,5231,4567,312]
    streamData = []
    for i in range(10):
      a = eformat.helper.StreamTag('Name-%d' % i, 'physics', True)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      streamData.append(a)
    
    sid = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)
    frag.source_id(sid)
    self.assertEqual(frag.source_id(), sid)
    frag.bc_time_seconds(0x12345678)
    self.assertEqual(frag.bc_time_seconds(), 0x12345678)
    frag.bc_time_nanoseconds(0xabcdef01)
    self.assertEqual(frag.bc_time_nanoseconds(), 0xabcdef01)
    frag.global_id(0xABCD1234ffff5678)
    self.assertEqual(frag.global_id(), 0xABCD1234ffff5678)
    frag.run_type(76)
    self.assertEqual(frag.run_type(), 76)
    frag.run_no(9876)
    self.assertEqual(frag.run_no(), 9876)
    frag.lumi_block(8950)
    self.assertEqual(frag.lumi_block(), 8950)
    frag.lvl1_id(813593)
    self.assertEqual(frag.lvl1_id(), 813593)
    frag.bc_id(45)
    self.assertEqual(frag.bc_id(), 45)
    frag.checksum_type(eformat.helper.CheckSum.NO_CHECKSUM)
    self.assertEqual(frag.checksum_type(), eformat.helper.CheckSum.NO_CHECKSUM)
    
    frag.lvl1_trigger_type(35)
    self.assertEqual(frag.lvl1_trigger_type(), 35)

    frag.compression_type(eformat.helper.Compression.ZLIB)
    self.assertEqual(frag.compression_type(), eformat.helper.Compression.ZLIB)
    frag.compression_type(eformat.helper.Compression.UNCOMPRESSED)
    self.assertEqual(frag.compression_type(), eformat.helper.Compression.UNCOMPRESSED)
    
    frag.status(status)
    self.assertEqual(len(status), len(frag.status()))
    for f,t in zip(frag.status(), status): self.assertEqual(f, t)
    
    frag.lvl2_trigger_info(L2Data)
    self.assertEqual(len(L2Data), len(frag.lvl2_trigger_info()))
    for f,t in zip(frag.lvl2_trigger_info(), L2Data): self.assertEqual(f, t)
    
    frag.event_filter_info(EFData)
    self.assertEqual(len(EFData), len(frag.event_filter_info()))
    for f,t in zip(frag.event_filter_info(), EFData): self.assertEqual(f, t)
    
    frag.stream_tag(streamData)
    self.assertEqual(len(streamData), len(frag.stream_tag()))
    for f,t in zip(frag.stream_tag(), streamData): self.assertEqual(f, t)

  def testGetAndSetAttributesCompressed(self):
    frag = eformat.write.FullEventFragment()
    frag.compression_type(eformat.helper.Compression.ZLIB)
    status = [13748, 59697, 748596, 23433, 234345, 5456]
    L2Data = [75869]
    EFData = [89,4567,3245,3456,78543,5678,432,4657,6789,534,6345,567,5231,4567,312]
    streamData = []
    for i in range(10):
      a = eformat.helper.StreamTag('Name-%d' % i, 'physics', True)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      streamData.append(a)
    
    sid = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)
    frag.source_id(sid)
    self.assertEqual(frag.source_id(), sid)
    frag.bc_time_seconds(0x12345678)
    self.assertEqual(frag.bc_time_seconds(), 0x12345678)
    frag.bc_time_nanoseconds(0xabcdef01)
    self.assertEqual(frag.bc_time_nanoseconds(), 0xabcdef01)
    frag.global_id(0xABCD1234ffff5678)
    self.assertEqual(frag.global_id(), 0xABCD1234ffff5678)
    frag.run_type(76)
    self.assertEqual(frag.run_type(), 76)
    frag.run_no(9876)
    self.assertEqual(frag.run_no(), 9876)
    frag.lumi_block(8950)
    self.assertEqual(frag.lumi_block(), 8950)
    frag.lvl1_id(813593)
    self.assertEqual(frag.lvl1_id(), 813593)
    frag.bc_id(45)
    self.assertEqual(frag.bc_id(), 45)
    frag.checksum_type(eformat.helper.CheckSum.NO_CHECKSUM)
    self.assertEqual(frag.checksum_type(), eformat.helper.CheckSum.NO_CHECKSUM)
    
    frag.lvl1_trigger_type(35)
    self.assertEqual(frag.lvl1_trigger_type(), 35)

    frag.compression_type(eformat.helper.Compression.ZLIB)
    self.assertEqual(frag.compression_type(), eformat.helper.Compression.ZLIB)
    frag.compression_type(eformat.helper.Compression.UNCOMPRESSED)
    self.assertEqual(frag.compression_type(), eformat.helper.Compression.UNCOMPRESSED)
    
    frag.status(status)
    self.assertEqual(len(status), len(frag.status()))
    for f,t in zip(frag.status(), status): self.assertEqual(f, t)
    
    frag.lvl2_trigger_info(L2Data)
    self.assertEqual(len(L2Data), len(frag.lvl2_trigger_info()))
    for f,t in zip(frag.lvl2_trigger_info(), L2Data): self.assertEqual(f, t)
    
    frag.event_filter_info(EFData)
    self.assertEqual(len(EFData), len(frag.event_filter_info()))
    for f,t in zip(frag.event_filter_info(), EFData): self.assertEqual(f, t)
    
    frag.stream_tag(streamData)
    self.assertEqual(len(streamData), len(frag.stream_tag()))
    for f,t in zip(frag.stream_tag(), streamData): self.assertEqual(f, t)

class GlobalTestCase(unittest.TestCase):
  """
  Tests the package as a whole.
  """

  def testIsEqual(self):
    #Creating the event
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    lvl2Info = [16, 17]
    efInfo = [18]
    status = [19,20,21]
    stream = []
    for i in range(5):
      a = eformat.helper.StreamTag('Name-%d' % i, 'debug', True)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      stream.append(a)


    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    robFrag = eformat.write.ROBFragment()
    robFrag.source_id(sid_rob)
    robFrag.rod_status(rodStatus)
    robFrag.rod_data(rodData)

    fullFrag = eformat.write.FullEventFragment()
    fullFrag.append(robFrag)
    fullFrag.lvl2_trigger_info(lvl2Info)
    fullFrag.event_filter_info(efInfo)
    fullFrag.status(status)
    fullFrag.stream_tag(stream)
    
    #Build a reader out of a writer
    fullFrag2 = eformat.FullEventFragment(fullFrag.__raw__())
    self.assertEqual(len(fullFrag2), len(fullFrag))
    self.assertEqual(fullFrag2, fullFrag)
    
    #Build a writer out of a reader
    fullFrag3 = eformat.write.FullEventFragment(fullFrag2)
    self.assertEqual(len(fullFrag3), len(fullFrag2))
    self.assertEqual(fullFrag3, fullFrag)

    #Build a writer out of *another writer
    fullFrag4 = eformat.write.FullEventFragment(fullFrag)
    self.assertEqual(len(fullFrag4), len(fullFrag))
    self.assertEqual(len(fullFrag4), len(fullFrag3))
    self.assertEqual(fullFrag3, fullFrag)

    #Test regex
    import re
    fullFrag5 = eformat.write.FullEventFragment(fullFrag, re.compile('0x00%2x.*' % eformat.helper.SubDetector.TDAQ_LVL2.real))
    self.assertNotEqual(len(fullFrag5), len(fullFrag))


  def testIsEqualCompressedZlib(self):
    #Creating the event
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    lvl2Info = [16, 17]
    efInfo = [18]
    status = [19,20,21]
    stream = []
    for i in range(5):
      a = eformat.helper.StreamTag('Name-%d' % i, 'debug', True)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      stream.append(a)


    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    robFrag = eformat.write.ROBFragment()
    robFrag.source_id(sid_rob)
    robFrag.rod_status(rodStatus)
    robFrag.rod_data(rodData)

    fullFrag = eformat.write.FullEventFragment()
    fullFrag.append(robFrag)
    fullFrag.lvl2_trigger_info(lvl2Info)
    fullFrag.event_filter_info(efInfo)
    fullFrag.status(status)
    fullFrag.stream_tag(stream)
    fullFrag.compression_type(eformat.helper.Compression.ZLIB)
    
    #Build a reader out of a writer
    fullFrag_raw_size = len(fullFrag)
    fullFrag2 = eformat.FullEventFragment(fullFrag.__raw__())
    fullFrag_comp_size = len(fullFrag)
    self.assertEqual(len(fullFrag2), fullFrag_comp_size)
    self.assertEqual(fullFrag2, fullFrag)
    
    #Build a writer out of a reader
    fullFrag3 = eformat.write.FullEventFragment(fullFrag2)
    self.assertEqual(len(fullFrag3), fullFrag_raw_size)
    self.assertEqual(fullFrag3, fullFrag)

    #Build a writer out of *another writer
    fullFrag4 = eformat.write.FullEventFragment(fullFrag)
    self.assertEqual(len(fullFrag4), fullFrag_raw_size)
    _ = fullFrag4.__raw__()
    self.assertEqual(len(fullFrag4), len(fullFrag3))
    self.assertEqual(len(fullFrag4), fullFrag_comp_size)
    self.assertEqual(fullFrag3, fullFrag)

    #Test regex
    import re
    fullFrag5 = eformat.write.FullEventFragment(fullFrag, re.compile('0x00%2x.*' % eformat.helper.SubDetector.TDAQ_LVL2.real))
    self.assertNotEqual(len(fullFrag5), len(fullFrag))


  def testIsEqualCompressedReserved(self):
    #Creating the event
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    lvl2Info = [16, 17]
    efInfo = [18]
    status = [19,20,21]
    stream = []
    for i in range(5):
      a = eformat.helper.StreamTag('Name-%d' % i, 'debug', True)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      stream.append(a)


    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    robFrag = eformat.write.ROBFragment()
    robFrag.source_id(sid_rob)
    robFrag.rod_status(rodStatus)
    robFrag.rod_data(rodData)

    fullFrag = eformat.write.FullEventFragment()
    fullFrag.append(robFrag)
    fullFrag.lvl2_trigger_info(lvl2Info)
    fullFrag.event_filter_info(efInfo)
    fullFrag.status(status)
    fullFrag.stream_tag(stream)
    fullFrag.compression_type(eformat.helper.Compression.RESERVED)

    #Build a reader out of a writer
    fullFrag2 = eformat.FullEventFragment(fullFrag.__raw__())
    self.assertEqual(len(fullFrag2), len(fullFrag))
    self.assertEqual(fullFrag2, fullFrag)
    
    #Build a writer out of a reader
    fullFrag3 = eformat.write.FullEventFragment(fullFrag2)
    self.assertEqual(len(fullFrag3), len(fullFrag2))
    self.assertEqual(fullFrag3, fullFrag)

    #Build a writer out of *another writer
    fullFrag4 = eformat.write.FullEventFragment(fullFrag)
    self.assertEqual(len(fullFrag4), len(fullFrag))
    self.assertEqual(len(fullFrag4), len(fullFrag3))
    self.assertEqual(fullFrag3, fullFrag)

    
if __name__ == "__main__":
        #Adding the be verbose parameter.
        sys.argv.append('-v')
        unittest.main()
