#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 16 Jun 2009 09:26:33 AM CEST 

"""Test a few DetectorMask python features
"""

import unittest
import eformat

class DetectorMask(unittest.TestCase):

  notinmask = (eformat.helper.SubDetector.FULL_SD_EVENT,
               eformat.helper.SubDetector.TDAQ_LAR_MET,
               eformat.helper.SubDetector.TDAQ_TILE_MET)

  def test_create_detmask(self):
    x = eformat.helper.DetectorMask(0xffffffffffffffff,0xffffffffffffffff)
    for i in list(eformat.helper.SubDetector.values.values()):
      if i in DetectorMask.notinmask: continue
      self.assertEqual(x.is_set(i), True)
    y = eformat.helper.DetectorMask()
    for i in list(eformat.helper.SubDetector.values.values()):
      if i in DetectorMask.notinmask: continue
      self.assertEqual(y.is_set(i), False)

  def test_encode_decode(self):
    s = [
         eformat.helper.SubDetector.TDAQ_CTP, 
         eformat.helper.SubDetector.OTHER,
         eformat.helper.SubDetector.TDAQ_BEAM_CRATE,
        ]
    x = eformat.helper.DetectorMask(s)
    dec = x.sub_detectors()
    self.assertEqual(len(s), len(dec))
    for k in s: self.assertEqual(k in dec, True)

  def test_set_unset(self):
    s = [
         eformat.helper.SubDetector.TDAQ_CTP, 
         eformat.helper.SubDetector.OTHER,
         eformat.helper.SubDetector.TDAQ_BEAM_CRATE,
        ]
    x = eformat.helper.DetectorMask(s)
    for k in s:
      self.assertEqual(x.is_set(k), True)
    x.unset(eformat.helper.SubDetector.OTHER)
    self.assertEqual(x.is_set(eformat.helper.SubDetector.OTHER), False)
    x.set(eformat.helper.SubDetector.PIXEL_BARREL)
    self.assertEqual(x.is_set(eformat.helper.SubDetector.PIXEL_BARREL), True)
    x.reset()
    for i in list(eformat.helper.SubDetector.values.values()):
      if i in DetectorMask.notinmask: continue
      self.assertEqual(x.is_set(i), False)
    self.assertEqual(x.mask(), 0)

  def test_split(self):
    x = eformat.helper.DetectorMask(0xffffffffffffffff, 0xffffffffffffffff)
    for i in list(eformat.helper.SubDetector.values.values()):
      if i in DetectorMask.notinmask: continue
      self.assertEqual(x.is_set(i), True)

  def test_string(self):
    m = "ffffffffffffffffffffffffffffffff"
    x = eformat.helper.DetectorMask(m)
    self.assertEqual(x.string(), m)
    self.assertEqual(int(m,16), x.mask())
    

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  

