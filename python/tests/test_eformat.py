#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to eformat fragment reading."""

import unittest
import os.path
import eformat

# Here are standard files used for testing. If they are
# not visible, then we will complain.
INPUT= 'test.data'

class EformatTester(unittest.TestCase):
  """Tests if we can read data with the python eformat interface."""

  def test01_CanCheckEvent(self):
    input = eformat.istream(INPUT)
    for event in input: event.check()
    
  def test02_CanCheckTreeEvent(self):
    input = eformat.istream(INPUT)
    input = eformat.istream(INPUT)
    for event in input: event.check_tree()

  def test03_CanRetrieveSimpleFields(self):
    input = eformat.istream(INPUT)
    for event in input:
      lvl1_id = event.lvl1_id()
      checksum_type = event.checksum_type()
      header_size = event.header_size_word()
      self.assertEqual(type(event.source_id()), eformat.helper.SourceIdentifier)
      self.assertEqual(type(event.run_type()), eformat.helper.RunType)
      self.assertEqual(type(event.version()), eformat.helper.Version)

  def test04_CanRetrieveSliceFields(self):
    input = eformat.istream(INPUT)
    for event in input:
      status = event.status()
      ef_info = event.event_filter_info()
      stream_tag = event.stream_tag()
      del event
      self.assertEqual(type(status), eformat.u32slice)
  
  def test05_CanRetrieveStreamTags(self):
    input = eformat.istream(INPUT)
    for event in input:
      tag = event.stream_tag()
      
  def test06_CanGetROBsFromEvent(self):
    input = eformat.istream(INPUT)
    for event in input:
      for i in range(event.nchildren()):
        rob = event[i]
        self.assertEqual(type(rob), eformat.ROBFragment)
      
  #def test07_CanGetTOC(self):
  #  input = eformat.istream(INPUT)
  #  for event in input:
  #    toc = event.toc()
  #    print toc

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
