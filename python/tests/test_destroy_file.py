#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to the EventStorage."""

import unittest
import eformat
import os

dataset = ['test.data','comp.data','eformatcomp.data']

class FileDestroyer(unittest.TestCase):

  def test_DeleteDummyFile(self):
    import os
    for f in dataset:
      os.unlink(f)

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
