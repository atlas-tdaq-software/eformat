#!/usr/bin/env tdaq_python

"""Test a few RunType python features
"""

import unittest
import eformat

class RunType(unittest.TestCase):

  def test_encode_decode(self):

    for k in eformat.helper.RunType.values:
      st = eformat.helper.run_type2string(eformat.helper.RunType.values[k])
      self.assertEqual(eformat.helper.RunType.names[st], \
                       eformat.helper.RunType.values[k])
      
    for k in eformat.helper.RunType.values:
      st = eformat.helper.run_type2string(k)
      self.assertEqual(eformat.helper.RunType.names[st], \
                       eformat.helper.RunType.values[k])

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  

