#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre ANJOS <Andre.dos.Anjos@cern.ch>, 26-Mar-2007

"""Unit tests for the combined reading and writing parts of the 
   eformat python bindings. This suite should be run after the
   all the others since it combines the funcionality of them all."""

import unittest
import eformat
import os

class FileReadingAndWriting(unittest.TestCase):
  """Performs various combined read/write tests on eformat and the
     writing extensions."""
  
  def test01_WriteReadOne(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    lvl2Info = [16, 17]
    efInfo = [18]
    status = [19,20,21]
    stream = []
    
    for i in range(5):
      a = eformat.helper.StreamTag()
      a.name = 'Name-%d' % i
      a.type = 'calibration'
      a.obeys_lumiblock = bool(i % 3)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      
      stream.append(a)

    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    event = eformat.write.FullEventFragment()
    event.append(rob)
    event.lvl2_trigger_info(lvl2Info)
    event.event_filter_info(efInfo)
    event.status(status)
    event.stream_tag(stream)
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    
    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(event, read)
    self.assertEqual(read.checksum(), True)
    
    os.unlink(outfile)

  def test02_WriteReadMany(self):

    # For this test we use a trick from the library.
    # We write each time the same event with changed
    # L1 identifiers only.
    
    out = eformat.ostream()
    
    event = eformat.write.FullEventFragment()
    event.lvl1_id(0x53)

    out.write(event)
    event.lvl1_id(0x7)
    out.write(event)
    event.lvl1_id(0x22)
    out.write(event)
    outfile = out.last_filename()
    del out
    
    input = eformat.istream(outfile)
    self.assertEqual(3, len(input))
    read = []
    for e in input: read.append(e)
    self.assertEqual(read[0].lvl1_id(), 0x53)
    self.assertEqual(read[1].lvl1_id(), 0x7)
    self.assertEqual(read[2].lvl1_id(), 0x22)
    self.assertEqual(read[0].checksum(), True)
    self.assertEqual(read[1].checksum(), True)
    self.assertEqual(read[2].checksum(), True)
    
    os.unlink(outfile)
  
  def test03_WriteReadBroken(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we do some screw-up on these data
    serial_rob = rob.__raw__()
    serial_rob[-1] = 0
    serial_rob[-2] = 0
    serial_rob[-3] = 0
    test_this = eformat.ROBFragment(serial_rob)

    # if it pass this point, we have a damaged ROB
    self.assertRaises(RuntimeError, test_this.check)
    self.assertRaises(RuntimeError, test_this.check_rod)
    self.assertEqual(test_this.check_rob(), True)

    event = eformat.write.FullEventFragment()
    event.append_unchecked(test_this) #appends unchecked ROB
    event.append(rob) #appends checked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    
    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(read.checksum(), True)
    self.assertEqual(event, read)

    #check_tree should not fail for a truncated ROD fragment
    self.assertEqual(read.check_tree(), True)
    self.assertEqual(read.check_tree_noex(), True)

    # make sure the ROB is still bad
    self.assertRaises(RuntimeError, read[1].check) # bad ROB
    self.assertEqual(read[1].check_noex(), False) # bad ROB
    self.assertRaises(RuntimeError, read[1].check_rod) # bad ROB
    self.assertEqual(read[1].check_rod_noex(), False) # bad ROB
    self.assertEqual(read[1].check_rob_noex(), True) # bad ROB
    
    # compare the damaged ROB and the one serialized word after word
    self.assertEqual(serial_rob, read[1].__raw__())
    
    os.unlink(outfile)

  def test04_WriteReadBrokenNormally(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we do some screw-up on these data
    serial_rob = rob.__raw__()
    serial_rob[-1] = 0
    serial_rob[-2] = 0
    serial_rob[-3] = 0
    for k in range(len(serial_rob)):
      if serial_rob[k] == eformat.helper.HeaderMarker.ROD: serial_rob[k] = 0
    test_this = eformat.write.ROBFragment(eformat.ROBFragment(serial_rob))

    event = eformat.write.FullEventFragment()
    event.append(test_this) #appends unchecked ROB
    event.append(rob) #appends checked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)

    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(read.nchildren(), 2)
    self.assertEqual(len(read), len(event))
    self.assertEqual(read.checksum(), True)
    self.assertEqual(event, read)

    # truncated ROD shall not cause a failure
    self.assertEqual(read.check_tree_noex(), True)

    # make sure the ROB is still bad
    self.assertEqual(read[0].check_noex(), False) # bad ROB
    self.assertEqual(read[0].check_rod_noex(), False) # bad ROB
    self.assertEqual(read[0].check_rob_noex(), True) # bad ROB

    # retrieve ROB problems
    self.assertEqual(read[0].problems(),
        [eformat.helper.FragmentProblem.WRONG_ROD_MARKER, eformat.helper.FragmentProblem.WRONG_ROD_FRAGMENT_SIZE])

    # compare the damaged ROB and the one serialized word after word
    self.assertEqual(serial_rob, read[0].__raw__())
    
    os.unlink(outfile)

  def test05_WriteReadBrokenROB(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we do some screw-up on these data
    serial_rob = rob.__raw__()
    serial_rob[2] = 3

    test_this = eformat.ROBFragment(serial_rob)

    # if it pass this point, we have a damaged ROB
    self.assertRaises(RuntimeError, test_this.check)
    self.assertEqual(test_this.check_rob_noex(), False)

    event = eformat.write.FullEventFragment()
    event.append_unchecked(serial_rob) #appends unchecked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    
    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(read.checksum(), True)
    self.assertEqual(event, read)

    #check_tree should not fail for a truncated ROD fragment
    self.assertRaises(RuntimeError, read.check_tree)
    self.assertEqual(read.check_tree_noex(), False)

    # make sure the ROB is still bad
    self.assertRaises(RuntimeError, read[0].check) # bad ROB
    self.assertEqual(read[0].check_noex(), False) # bad ROB
    self.assertRaises(RuntimeError, read[0].check_rod) # bad ROB
    self.assertEqual(read[0].check_rob_noex(), False) # bad ROB
    
    # compare the damaged ROB and the one serialized word after word
    self.assertEqual(serial_rob, read[0].__raw__())

    os.unlink(outfile)

  def test06_WriteReadShort(self):

    rodData = [1,1,1,1,1,1]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we cut this rob short
    serial_rob = rob.__raw__()
    serial_rob[1] = 15 #Trucanted ROD of 7 words + ROB header

    event = eformat.write.FullEventFragment()
    event.append_unchecked(serial_rob) #appends unchecked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)

    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one

    #The ROB fragment is shorter then the minimum size
    self.assertEqual(0, len(read[0].rod_data()))

    os.unlink(outfile)

  def test06_WriteReadTruncated(self):

    rodData = [1,1,1,1,1,1]
    rodStatus = [7,8,9,10,11,12,13,14,15]

    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we cut this rob short
    serial_rob = rob.__raw__()
    serial_rob[-3] = 0
    serial_rob[-2] = 0
    serial_rob[-1] = 0

    event = eformat.write.FullEventFragment()
    event.append_unchecked(serial_rob) #appends unchecked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)

    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one

    #The ROB fragment is truncated, all payload is returned as data
    self.assertEqual(len(rodData) + len(rodStatus) + 3, len(read[0].rod_data()))
    #The ROB fragment is truncated, no status words are returned
    self.assertEqual(0, len(read[0].rod_status()))

    os.unlink(outfile)

  def test06_CloseFile(self):

    out = eformat.ostream()

    event = eformat.write.FullEventFragment()
    event.lvl1_id(0x53)

    out.write(event)
    outfile = out.last_filename()
    out.close()

    try:
      os.unlink(outfile)
    except FileNotFoundError:
      self.fail("File was not closed")

  def test06_ContextManagerFile(self):

    with eformat.ostream() as out:
      event = eformat.write.FullEventFragment()
      event.lvl1_id(0x53)
      out.write(event)
      outfile = out.last_filename()

    try:
      os.unlink(outfile)
    except FileNotFoundError:
      self.fail("File was not closed")


class FileReadingAndWritingCompressedROB(unittest.TestCase):
  """Performs various combined read/write tests on eformat and the
     writing extensions."""
  
  def test01_WriteReadOne(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    lvl2Info = [16, 17]
    efInfo = [18]
    status = [19,20,21]
    stream = []
    
    for i in range(5):
      a = eformat.helper.StreamTag()
      a.name = 'Name-%d' % i
      a.type = 'calibration'
      a.obeys_lumiblock = bool(i % 3)
      if i==3:
        a.robs.append(0xff)
      if i==5:
        a.dets.append(eformat.helper.SubDetector.TDAQ_BEAM_CRATE)
      
      stream.append(a)

    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    event = eformat.write.FullEventFragment()
    event.append(rob)
    event.lvl2_trigger_info(lvl2Info)
    event.event_filter_info(efInfo)
    event.status(status)
    event.stream_tag(stream)
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    event.compression_type(eformat.helper.Compression.ZLIB)
    
    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(event, read)
    self.assertEqual(read.checksum(), True)
    
    os.unlink(outfile)

  def test02_WriteReadMany(self):

    # For this test we use a trick from the library.
    # We write each time the same event with changed
    # L1 identifiers only.
    
    out = eformat.ostream()
    
    event = eformat.write.FullEventFragment()
    event.compression_type(eformat.helper.Compression.ZLIB)
    event.lvl1_id(0x53)

    out.write(event)
    event.lvl1_id(0x7)
    out.write(event)
    event.lvl1_id(0x22)
    out.write(event)
    outfile = out.last_filename()
    del out
    
    input = eformat.istream(outfile)
    self.assertEqual(3, len(input))
    read = []
    for e in input: read.append(e)
    self.assertEqual(read[0].lvl1_id(), 0x53)
    self.assertEqual(read[1].lvl1_id(), 0x7)
    self.assertEqual(read[2].lvl1_id(), 0x22)
    self.assertEqual(read[0].checksum(), True)
    self.assertEqual(read[1].checksum(), True)
    self.assertEqual(read[2].checksum(), True)
    
    os.unlink(outfile)
  
  def test03_WriteReadBroken(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we do some screw-up on these data
    serial_rob = rob.__raw__()
    serial_rob[-1] = 0
    serial_rob[-2] = 0
    serial_rob[-3] = 0
    test_this = eformat.ROBFragment(serial_rob)

    # if it pass this point, we have a damaged ROB
    self.assertRaises(RuntimeError, test_this.check)
    self.assertRaises(RuntimeError, test_this.check_rod)
    self.assertEqual(test_this.check_rob(), True)

    event = eformat.write.FullEventFragment()
    event.compression_type(eformat.helper.Compression.ZLIB)
    event.append_unchecked(test_this) #appends unchecked ROB
    event.append(rob) #appends checked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    
    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(read.checksum(), True)
    self.assertEqual(event, read)

    #check_tree should not fail for a truncated ROD fragment
    self.assertEqual(read.check_tree(), True)
    self.assertEqual(read.check_tree_noex(), True)

    # make sure the ROB is still bad
    self.assertRaises(RuntimeError, read[1].check) # bad ROB
    self.assertEqual(read[1].check_noex(), False) # bad ROB
    self.assertRaises(RuntimeError, read[1].check_rod) # bad ROB
    self.assertEqual(read[1].check_rod_noex(), False) # bad ROB
    self.assertEqual(read[1].check_rob_noex(), True) # bad ROB
    
    # compare the damaged ROB and the one serialized word after word
    self.assertEqual(serial_rob, read[1].__raw__())
    
    os.unlink(outfile)

  def test04_WriteReadBrokenNormally(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we do some screw-up on these data
    serial_rob = rob.__raw__()
    serial_rob[-1] = 0
    serial_rob[-2] = 0
    serial_rob[-3] = 0
    for k in range(len(serial_rob)):
      if serial_rob[k] == eformat.helper.HeaderMarker.ROD: serial_rob[k] = 0
    test_this = eformat.write.ROBFragment(eformat.ROBFragment(serial_rob))

    event = eformat.write.FullEventFragment()
    event.append(test_this) #appends unchecked ROB
    event.append(rob) #appends checked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    event.compression_type(eformat.helper.Compression.ZLIB)

    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(read.nchildren(), 2)
    self.assertEqual(len(read), len(event))
    self.assertEqual(read.checksum(), True)
    self.assertEqual(event, read)

    # truncated ROD shall not cause a failure
    self.assertEqual(read.check_tree_noex(), True)

    # make sure the ROB is still bad
    self.assertEqual(read[0].check_noex(), False) # bad ROB
    self.assertEqual(read[0].check_rod_noex(), False) # bad ROB
    self.assertEqual(read[0].check_rob_noex(), True) # bad ROB

    # retrieve ROB problems
    self.assertEqual(read[0].problems(),
        [eformat.helper.FragmentProblem.WRONG_ROD_MARKER, eformat.helper.FragmentProblem.WRONG_ROD_FRAGMENT_SIZE])

    # compare the damaged ROB and the one serialized word after word
    self.assertEqual(serial_rob, read[0].__raw__())
    
    os.unlink(outfile)

  def test05_WriteReadBrokenROB(self):
  
    rodData = [1,2,3,4,5,6]
    rodStatus = [7,8,9,10,11,12,13,14,15]
    
    sid_rob = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2, 0x1)

    rob = eformat.write.ROBFragment()
    rob.source_id(sid_rob)
    rob.rod_status(rodStatus)
    rob.rod_data(rodData)

    # now we do some screw-up on these data
    serial_rob = rob.__raw__()
    serial_rob[2] = 3

    test_this = eformat.ROBFragment(serial_rob)

    # if it pass this point, we have a damaged ROB
    self.assertRaises(RuntimeError, test_this.check)
    self.assertEqual(test_this.check_rob_noex(), False)

    event = eformat.write.FullEventFragment()
    event.append_unchecked(serial_rob) #appends unchecked ROB
    event.checksum_type(eformat.helper.CheckSum.ADLER32)
    event.compression_type(eformat.helper.Compression.ZLIB)
    
    out = eformat.ostream()
    out.write(event)
    outfile = out.last_filename()
    del out #so we close the file

    input = eformat.istream(outfile)
    self.assertEqual(1, len(input))
    read = input[0] #there should be only one
    self.assertEqual(read.checksum(), True)
    self.assertEqual(event, read)

    #check_tree should not fail for a truncated ROD fragment
    self.assertRaises(RuntimeError, read.check_tree)
    self.assertEqual(read.check_tree_noex(), False)

    # make sure the ROB is still bad
    self.assertRaises(RuntimeError, read[0].check) # bad ROB
    self.assertEqual(read[0].check_noex(), False) # bad ROB
    self.assertRaises(RuntimeError, read[0].check_rod) # bad ROB
    self.assertEqual(read[0].check_rob_noex(), False) # bad ROB
    
    # compare the damaged ROB and the one serialized word after word
    self.assertEqual(serial_rob, read[0].__raw__())

    
    os.unlink(outfile)


if __name__ == '__main__':
  import sys
  sys.argv.append('-v')
  unittest.main()

