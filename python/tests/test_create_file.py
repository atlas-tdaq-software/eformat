#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to the EventStorage."""

import unittest
import eformat

INPUT = 'test.data'

class FileCreator(unittest.TestCase):

  def test_CanCreateDummyFile(self):
    eformat.dummy.make_file(INPUT, 10, verbose=True)

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
