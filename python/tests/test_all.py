#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 26-Mar-2007

# This python file runs most of the tests we need for the EventApps
# python bindings. It combines read/write-to-file tests, writing
# API exercises and, finally a whole combined test, with reading
# and writing.

from test_helper import *
from test_storage import *
from test_eformat import *
from test_write import *
from test_combined import *
from test_detmask import *
from bench import *
from test_compression import *
from test_runtype import *
from test_eformatcompression import *

if __name__ == '__main__':
  import sys
  import unittest
  sys.argv.append('-v')
  unittest.main()
  
