#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to the EventStorage."""

import unittest
import eformat
import libpyevent_storage
import os.path

INPUT = 'test.data'

class SimpleReader(unittest.TestCase):
  """Tests if we can read data from an EventStorage file, in a simple way."""

  def test01_CanAddressBlobs(self):
    input = libpyevent_storage.pickDataReader(INPUT)
    blob = input.getData()
    for i in range(len(blob)): k = blob[i]
    self.assertEqual(blob[len(blob)-1], blob[-1])

  def test02_CanSliceBlobs(self):
    input = libpyevent_storage.pickDataReader(INPUT)
    blob = input.getData()
    x = blob[1:-1]
    self.assertEqual(type(x), eformat.u32slice)
    self.assertEqual(len(x), len(blob)-2)

  def test03_CanCopyBlobs(self):
    input = libpyevent_storage.pickDataReader(INPUT)
    blob = input.getData()
    x = blob.copy(slice(1,-1))
    self.assertEqual(type(x), list)
    self.assertEqual(len(x), len(blob)-2)

  def test04_ConvertFromBlobs(self):
    input = libpyevent_storage.pickDataReader(INPUT)
    while input.good():
      blob = input.getData()
      event = eformat.FullEventFragment(blob)
      self.assertEqual(type(event), eformat.FullEventFragment)

  def test05_CanModifyBlobs(self):
    input = libpyevent_storage.pickDataReader(INPUT)
    while input.good():
      blob = input.getData()
      blob[0] = 1
      self.assertEqual(blob[0], 1)
      blob[-1] = 3
      self.assertEqual(blob[-1], 3)
      
  def test06_CanOpenAndReadAdvanced(self):
    input = eformat.istream(INPUT)
    counter = 0
    for i in input.iter_raw(): counter += 1
    self.assertEqual(counter, len(input))

  def test07_CanReadFragmentsAdvanced(self):
    input = eformat.istream(INPUT)
    counter = 0
    for i in input: counter += 1
    self.assertEqual(counter, len(input))

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
