#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to eformat fragment reading."""

import unittest
import eformat

class HelperTester(unittest.TestCase):
  """Tests if we can read data with the python eformat interface."""

  def test01_SimpleSourceIdentifier(self):
    t = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2,
        0x1736)
    self.assertEqual(t.subdetector_id(), eformat.helper.SubDetector.TDAQ_LVL2)
    self.assertEqual(t.module_id(), 0x1736)
    self.assertEqual(t.optional_field(), 0) #has to be zero to conform
    self.assertEqual(t.code(), 0x7b1736)

  def test02_OptionalSourceIdentifier(self):
    t = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_LVL2,
        0x1736, 0xfa)
    self.assertEqual(t.subdetector_id(), eformat.helper.SubDetector.TDAQ_LVL2)
    self.assertEqual(t.module_id(), 0x1736)
    self.assertEqual(t.optional_field(), 0xfa)
    self.assertEqual(t.code(), 0xfa7b1736)

  def test03_CheckSumNoCheckSum(self):
    self.assertEqual(0,
        eformat.helper.checksum(eformat.helper.CheckSum.NO_CHECKSUM, []))
    self.assertEqual(0,
        eformat.helper.checksum(eformat.helper.CheckSum.NO_CHECKSUM, [1, 2]))

  def test04_CheckSumAdler32(self):
    self.assertEqual(0x00000001,
        eformat.helper.checksum(eformat.helper.CheckSum.ADLER32, []))
    self.assertEqual(0x00180004,
        eformat.helper.checksum(eformat.helper.CheckSum.ADLER32, [1, 2]))
    self.assertEqual(0x23d407f1,
        eformat.helper.checksum(eformat.helper.CheckSum.ADLER32, 
          [0xffffffff, 0xfff7ffff]))
    self.assertEqual(0x23d407f1,
        eformat.helper.checksum(eformat.helper.CheckSum.ADLER32, 
          eformat.u32list([0xffffffff, 0xfff7ffff])))
    # calling it a second time should give the same result
    self.assertEqual(0x23d407f1,
        eformat.helper.checksum(eformat.helper.CheckSum.ADLER32, 
          eformat.u32list([0xffffffff, 0xfff7ffff])))

  def test05_CheckSumCrc16Ccitt(self):
    self.assertEqual(0xffffffff,
        eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT, []))
    self.assertEqual(0x84c093b2,
        eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT, [1, 2]))
    self.assertEqual(0x9c071d0f,
        eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT, 
          [0xffffffff, 0xfff7ffff]))
    self.assertEqual(0x9c071d0f,
        eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT, 
          eformat.u32list([0xffffffff, 0xfff7ffff])))
    # calling it a second time should give the same result
    self.assertEqual(0x9c071d0f,
        eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT, 
          eformat.u32list([0xffffffff, 0xfff7ffff])))

    # symmetry should be respected for symmetric entries
    tmp = eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT,
          eformat.u32list([0x0, 0x0]))
    self.assertEqual(tmp & 0xffff, tmp >> 16)
    tmp = eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT,
          eformat.u32list([0xffffffff, 0xffffffff]))
    self.assertEqual(tmp & 0xffff, tmp >> 16)

  def test06_CheckSumCrc16CcittOfficial(self):
    data = [0xee1234ee, 0x9, 0x3010000, 0x51000a, 0x0, 0x9, 0x0, 0x0, 0x0, 0x0,
        0x0, 0x0]
    crc = eformat.helper.checksum(eformat.helper.CheckSum.CRC16_CCITT, data)
    self.assertEqual(0x4ae5659b, crc)

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
