#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to the EventStorage."""

import unittest
import eformat
import os

INPUT = 'test.data'
OUTPUT = 'eformatcomp.data'


class EformatCompression(unittest.TestCase):

  def test01_WriteCompressedEvents(self):
    ins = eformat.istream(INPUT)
    out = eformat.ostream()
    for e in ins:
      fw = eformat.write.FullEventFragment(e)
      fw.compression_type(eformat.helper.Compression.ZLIB)
      out.write(fw)

    fname = out.last_filename()
    del out
    os.rename(fname,OUTPUT)

  def test02_ReadCompressedEvents(self):
    ins = eformat.istream(OUTPUT)

    for e in ins:
      self.assertEqual(e.compression_type(),eformat.helper.Compression.ZLIB)
      _ = len(e)
      _ = e.nchildren()
      
  def test03_CheckEventInCompressedFile(self):
    raw = eformat.istream(INPUT)
    comp = eformat.istream(OUTPUT)

    return all([a==b for a,b in zip(raw,comp)])
    

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  eformat.dummy.make_file(INPUT, 10, verbose=True)
  unittest.main()
  
