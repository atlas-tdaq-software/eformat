#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 22-Nov-2006

# Replaces the dump-eformat-in-data C++ application with a python script. Just
# an example of the potential of these bindings.

import sys
import os

"""
Using default signal handler for SIGPIPE, so that one can pipe it into, e.g., head
"""
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

def my_dump(argv):
  """Runs the dumping routines"""

  import EventApps.myopt as myopt
  import eformat, logging
  import eformat.dump

  option = {}
  option['start-event'] = {'short': 'a', 'arg': True,
                           'default': 0,
                           'description': 'The number of events I should skip from the begin'}
  option['number-of-events'] = {'short': 'n', 'arg': True,
                               'default': 0,
                               'description': 'The number of events to dump/analyze (0 means all)'}
  option['output'] = {'short': 'o', 'arg': True,
                      'default': '',
                      'description': 'The output file to use'}
  option['verbosity'] = {'short': 'v', 'arg': True,
                         'default': 2,
                         'description': 'Up to which level to dump (0, 1, 2)'}
  option['debug'] = {'short': 'd', 'arg': True,
                     'default': logging.INFO,
                     'description': 'Up to which level to print debug messages (0, 1, 2, 3)'}
  
  parser = myopt.Parser(extra_args=True)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])
  
  if len(sys.argv) == 1:
    print(parser.usage('global "%s" options:' % sys.argv[0]))
    sys.exit(1)

  #process the global options
  (kwargs, extra) = parser.parse(sys.argv[1:], prefix='global "%s" options:' % sys.argv[0])

  #now the things which require global defaults
  logging.getLogger('').setLevel(kwargs['debug'])
  #os.environ['TDAQ_ERS_DEBUG_LEVEL'] = str(kwargs['debug'])

  stream = eformat.istream(extra)
  
  if kwargs['verbosity'] > 0:
    eformat.dump.event_callback.append(('.+', eformat.dump.fullevent_handler))
  if kwargs['verbosity'] > 1:
    eformat.dump.rob_callback.append(('.+', eformat.dump.rob_handler))

  eformat.dump.dump(stream, kwargs['start-event'], kwargs['number-of-events'])
  sys.exit(0)

if __name__ == "__main__":
  my_dump(sys.argv)
