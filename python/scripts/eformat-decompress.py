#!/usr/bin/env tdaq_python

import sys
import eformat
import os.path


if __name__ == '__main__':

    if len(sys.argv) < 3:
        print('Usage: %s <infile> <outfile>' % os.path.basename(sys.argv[0]))
        sys.exit(1)

    ins = eformat.istream(sys.argv[1])

    directory = os.path.abspath(os.path.dirname(sys.argv[2]))
    out = eformat.ostream(directory=directory,
                          core_name=os.path.basename(sys.argv[2]))

    for e in ins:
        new_ev = eformat.write.FullEventFragment(e)
        new_ev.compression_type(0)
        out.write(new_ev.__raw__())
