#!/usr/bin/env tdaq_python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 15-Nov-2006

import eformat, logging 
logging.getLogger('').setLevel(logging.INFO)
import sys
import os

version = eformat.helper.Version().human_major()

if len(sys.argv) == 1:
  print("Converts files from any older supported format to v%s" % version) 
  print("Files are stored in the current working directory")
  print("usage: %s <data-file> [+data-file]" % sys.argv[0])
  sys.exit(1)

logging.info("Legend: 'x' => invalid fragments; '.' => converted correctly")

for f in sys.argv[1:]:
  istr = eformat.istream(f)
  logging.info("Working at file %s (%d fragments)" % (f, len(istr)))
  ostr = eformat.ostream()
  curr = istr.__iter__()
  try:
    while True:
      try:
        e = next(curr)
        e.check_tree()
      except RuntimeError as ex:
        sys.stdout.write('x')
        sys.stdout.write('\n')
        sys.stdout.write(str(ex))
        sys.stdout.write('\n => Fragment Ignored!\n')
        sys.stdout.flush()
      else:
        sys.stdout.write('.')
        sys.stdout.flush()
        ostr.write(e)
  except StopIteration:
    pass
  sys.stdout.write('\n')
  tmp = ostr.last_filename()
  converted = ostr.events_in_file()
  del ostr
  destination = os.path.basename(f) + '.v' + version
  if os.path.exists(destination):
    backup = destination + '~'
    if os.path.exists(backup):
      logging.info("Removing (old) backup %s" % backup)
      os.unlink(backup)
    logging.info("Back'ing-up %s at %s" % (destination, backup))
    os.rename(destination, backup)
  os.rename(tmp, destination)
  logging.info("Recorded %d fragments in the file %s" % (converted, destination))

