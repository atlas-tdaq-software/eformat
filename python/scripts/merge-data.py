#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 29-03-2007 

import os, sys

def my_conf(argv):
  """Runs the merging routines"""

  import eformat, logging 
  import EventApps.myopt as myopt

  option = {}
  option['output'] = {'short': 'o', 'arg': True,
                      'default': '',
                      'description': 'Filename of the output file'}
  option['verbosity'] = {'short': 'V', 'arg': True,
                         'default': logging.INFO,
                         'description': 'From which level to print system messag es [%d, %d]. For details please consult the documentation of python\'s "logging" module' % (logging.NOTSET, logging.CRITICAL)}
                                                       
  parser = myopt.Parser(extra_args=True)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])
  
  if len(sys.argv) == 1:
    print(parser.usage('global "%s" options:' % sys.argv[0]))
    sys.exit(1)

  #process the global options
  (kwargs, extra) = parser.parse(sys.argv[1:], prefix='global "%s" options:' % sys.argv[0])

  #now the things which require global defaults
  logging.getLogger('').setLevel(kwargs['verbosity'])

  stream = eformat.istream(extra)
  ostream = eformat.ostream()

  total = 0
  for e in stream: 
    ostream.write(e)
    sys.stdout.write('.')
    sys.stdout.flush()
    total += 1

  sys.stdout.write('\n')
  sys.stdout.flush()

  oname = ostream.last_filename()
  if len(kwargs['output']) != 0:
    del ostream
    os.rename(oname, kwargs['output'])
    oname = kwargs['output']

  logging.info('Wrote %d events in %s\n' % (total, oname))
  sys.exit(0)
 
if __name__ == "__main__":
  my_conf(sys.argv)

