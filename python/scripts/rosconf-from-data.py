#!/usr/bin/env tdaq_python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 03-Aug-2006

import sys
import os
import datetime

def random(hits, n):
  """Places all ROB hits randomly in 'n' ROSs.

  This function will take the hits found in the list given as first parameter
  and will randomly place all ROB identifiers over the 'n' available ROSs as
  defined by the second parameter (which, btw, defaults to 1).
  """
  from random import randint,seed
  
  seed(0) # make it random, but no so random!
  retval = {}
  for h in hits: retval[h] = randint(1,n)
  return retval

def my_conf(argv):
  """Runs the dumping routines"""
  import logging 
  from EventApps import myopt

  option = {}
  option['number-of-ross'] = {'short': 'n', 'arg': True,
                              'default': 1,
                              'description': 'The number of ROS in the system'}
  option['py'] = {'short': 'p', 'arg': False,
                  'default': None,
                  'description': 'Dumps as a python list/file'}
  option['ignore'] = {'short': 'v', 'arg': True,
                      'default': '0x007[3589abcde].+|0x007[67]0001',
                      'description': 'A (python) regular expression of ROB hexadecimal numbers to ignore when making the robmap list. The comparision is done on a string basis, as a result of parsing each hit in a 0x%08x printf-like format'}
  
  option['verbosity'] = {'short': 'V', 'arg': True,
                         'default': logging.INFO,
                         'description': 'From which level to print system messages [%d, %d]. For details please consult the documentation of python\'s "logging" module' % (logging.NOTSET, logging.CRITICAL)}
  
  parser = myopt.Parser(extra_args=True)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])
  
  if len(sys.argv) == 1:
    print(parser.usage('global "%s" options:' % sys.argv[0]))
    sys.exit(1)

  #process the global options
  (kwargs, extra) = parser.parse(sys.argv[1:], prefix='global "%s" options:' % sys.argv[0])

  #now the things which require global defaults
  logging.getLogger('').setLevel(kwargs['verbosity'])
  # os.environ['TDAQ_ERS_DEBUG_LEVEL'] = str(kwargs['debug'])
  import eformat

  stream = eformat.istream(extra)
  rob_list = set() 
  sys.stderr.write('Processing %s events' % len(stream))
  for event in stream:
    sys.stderr.write('.')
    sys.stderr.flush()
    for rob in event: rob_list.add(rob.source_id().code())
  sys.stderr.write('\n')

  if kwargs['py']:
    print("#!/usr/bin/env tdaq_python\n")
    
  print("# This ROB hitlist was generated automatically by %s" % \
      os.path.basename(sys.argv[0]))
  print("# It is a python application you can customize. ")
  print("# Consult the eformat wiki page. ")
  print("# Current date and time is", datetime.datetime(1,1,1).now())
  print("")

  keys = list(rob_list)
  keys.sort()

  # checks if we need to apply filtering...
  if len(kwargs['ignore'].strip()) != 0:
    import re
    ignore = re.compile(kwargs['ignore'].strip())
    keys = [k for k in keys if not ignore.match('0x%08x' % k)]
    
  if kwargs['py']:
    print("# original filename list")
    print("filename = []\n")
    for k in extra: print("filename.append('%s')" % k)
    print("")
    
    print("# uniq'fied robhit list")
    print("robhit = []\n")
    for k in keys:
      print("robhit.append(0x%08x)" % k)
    print("\nimport logging")
    print("logging.info('ROB hit list (extracted from %d file/s) contains %d unique hits' % (len(filename), len(robhit)))")
    
  else:
    print("# These unique identifiers were extracted from:")
    for k in extra: print("# %s" % k)
    print("")
    
    rob_map = random(keys, kwargs['number-of-ross'])
    for k in keys:
      print("0x%08x\tROS-%d" % (k, rob_map[k]))

if __name__ == "__main__":
  my_conf(sys.argv)

