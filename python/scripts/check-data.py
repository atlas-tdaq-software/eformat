#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 29-03-2007 
import sys
import os

def my_conf(argv):
  """Runs the checking routines"""

  import eformat, logging
  import EventApps.myopt as myopt

  option = {}
  option['verbosity'] = {'short': 'V', 'arg': True,
                         'default': logging.INFO,
                         'description': 'From which level to print system messages [%d, %d]. For details please consult the documentation of python\'s "logging" module' % (logging.NOTSET, logging.CRITICAL)}
  option['check_lvl1_ids'] = {'short': 'l', 'arg': False,
                              'default': None,
                              'description': 'Enable check that LVL1 IDs are the same in the event.'}
  option['check_source_ids'] = {'short': 's', 'arg': False,
                                'default': None,
                                'description': 'Enable check that ROB source IDs are unique for the event.'}

  
  parser = myopt.Parser(extra_args=True)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])
  
  if len(sys.argv) == 1:
    print(parser.usage('global "%s" options:' % sys.argv[0]))
    sys.exit(1)

  #process the global options
  (kwargs, extra) = parser.parse(sys.argv[1:], prefix='global "%s" options:' % sys.argv[0])

  check_source_ids = kwargs['check_source_ids']
  check_lvl1_ids = kwargs['check_lvl1_ids']

  #now the things which require global defaults
  logging.getLogger('').setLevel(kwargs['verbosity'])

  stream = eformat.istream(extra)

  #reading will check the events in the stream...
  logging.info('Checking %d fragment(s)' % len(stream))
  for e in stream:
    if not e.check_noex():
      logging.warn("Skipping event %d because it is *invalid*: %s" % \
                   (e.lvl1_id(), [str(k) for k in e.problems()]))
      continue
    sys.stdout.write('.')
    sys.stdout.flush()

    source_ids = []
    event_lvl1_id = e.lvl1_id()
    for rob in e:
      if not rob.check_noex():
        logging.warn("rob 0x%08x (%s) of event %d is *invalid*: %s" % \
                     (rob.source_id().code(), rob.source_id(), e.lvl1_id(),
                      [str(k) for k in rob.problems()]))

      if not rob.checksum(): 
        logging.warn("rob 0x%08x (%s) of event %d does not checksum" % \
                     (rob.source_id().code(),rob.source_id(),e.lvl1_id()))

      if check_source_ids and rob.source_id().code() in source_ids:
        logging.warn("Duplicate rob ID 0x%08x for LVL1 ID %d" % \
                     (rob.source_id().code(), event_lvl1_id))
      else: 
        source_ids.append(rob.source_id().code())

      if check_lvl1_ids and event_lvl1_id != rob.rod_lvl1_id():
        logging.warn("rob 0x%08x (%s) has LVL1 ID %d(0x%08x) while the event has LVL1 ID %d(0x%08x)" % \
                     (rob.source_id().code(), rob.source_id(), \
                      rob.rod_lvl1_id(), rob.rod_lvl1_id(), \
                      event_lvl1_id, event_lvl1_id))

      
  sys.stdout.write('\n')

if __name__ == "__main__":
  my_conf(sys.argv)

