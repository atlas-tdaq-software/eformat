#!/usr/bin/env tdaq_python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 22-Nov-2006

# splits the data in a data file according to user parameters

import sys
import os

def my_split(argv):
  """Runs the splitting routines"""

  import eformat, logging
  import EventApps.myopt as myopt

  option = {}
  option['start-event'] = {'short': 'a', 'arg': True,
                           'default': 0,
                           'description': 'The number of events I should skip from the begin'}
  option['max-events'] = {'short': 'n', 'arg': True,
                               'default': 0,
                               'description': 'The total maximum number of events you want in the output file. The special value 0 means up to the end.'}
  option['output'] = {'short': 'o', 'arg': True,
                      'default': '',
                      'description': 'Filename of the output file'}
  option['verbosity'] = {'short': 'd', 'arg': True,
                         'default': logging.INFO,
                         'description': 'How much I should say about the execution'}
  
  option['metadata'] = {'short': 'm', 'arg': True,
                        'default': False,
                        'description': 'Preserve metadata in the output file (from the first input file)'}

  parser = myopt.Parser(extra_args=True)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])
 
  if len(sys.argv) == 1:
    print(parser.usage('global "%s" options:' % sys.argv[0]))
    sys.exit(1)

  #process the global options
  (kwargs, extra) = parser.parse(sys.argv[1:], prefix='global "%s" options:' % sys.argv[0])

  #now the things which require global defaults
  logging.getLogger('').setLevel(kwargs['verbosity'])

  stream = eformat.istream(extra)
  if not kwargs['metadata']:
    ostream = eformat.ostream()
  else:
    import libpyevent_storage as EventStorage
    dr = EventStorage.pickDataReader(extra[0])
    ostream = eformat.ostream(run_number=dr.runNumber(), 
                              trigger_type=dr.triggerType(),
                              detector_mask=dr.detectorMask(), 
                              beam_type=dr.beamType(),
                              beam_energy=dr.beamEnergy())
    
  total = 0
  for e in stream:
    if kwargs['start-event'] > 0:
      kwargs['start-event'] -= 1
      continue

    if kwargs['max-events'] > 0 and total >= kwargs['max-events']:
      break

    ostream.write(e)
    sys.stdout.write('.')
    sys.stdout.flush()
    total += 1

  sys.stdout.write('\n')
  sys.stdout.flush()

  oname = ostream.last_filename()
  if len(kwargs['output']) != 0:
    del ostream
    os.rename(oname, kwargs['output'])
    oname = kwargs['output']
 
  sys.stdout.write('Wrote %d events in %s\n' % (total, oname))
  sys.exit(0)

if __name__ == "__main__":
  my_split(sys.argv)
