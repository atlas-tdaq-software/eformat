#!/usr/bin/env tdaq_python

##
# Split events files by ROBs, generating one file per ROB id in a given data
# file.
# @Author Rodrigo Coura Torres (Rodrigo.Torres@cern.ch)
# This script will take a set of data files, and separate their informaton by
# ROBs, so it will genertae one file per ROB id for each data file.

import os
import sys
import eformat, logging 
logging.getLogger('').setLevel(logging.INFO)
import shutil

def calculate_robset(files):
  """Given a list of input files, generate a python set of ROB identifiers.

  The generated list is like a logical union of all available ROB identifiers
  on all files.
  
  keyword parameters:
  files -- A python list of file names
  """
  
  retval = set() 
  for ife in eformat.istream(files):
    retval = retval.union([k.key() for k in ife.toc()])
    sys.stdout.write('.')
    sys.stdout.flush()
  sys.stdout.write('\n') 
  return retval

def surround_with_fullevent(rob, model):
  """Creates a full event around the given ROB fragment"""
  retval = eformat.write.FullEventFragment()
  retval.copy_header(model)
  if rob: retval.append_unchecked(rob)
  return retval

def split_data(files, robset, output_dir):
  events = eformat.istream(files)
  logging.info("Reading %d event(s) from file '%s' and dumping to %d files." % 
      (len(events), os.path.basename(files), len(robset)))

  # out will contain a stream to all possible ROB ids.
  out = {}
  for r in robset: 
    out[r] = eformat.ostream(directory=output_dir, core_name='0x%08X' % r)
    
  for ife in events:
    toc = {}
    for k in ife.toc(): toc[k.key()] = k.data()
    model = toc[list(toc.keys())[0]] #gets the first rob as a model for the others
    diff = robset.difference(list(toc.keys()))
    for source_id in diff: toc[source_id] = None 
    for source_id, ostream in out.items(): 
      ostream.write(surround_with_fullevent(toc[source_id], ife))
    sys.stdout.write('.')
    sys.stdout.flush()
      
  sys.stdout.write('\n')
  sys.stdout.flush()

  # we return a map with the list of files generated.
  retval = {}
  for rob_id, ostr in out.items(): retval[rob_id] = ostr.last_filename()
  return retval
  
def rename_files(files_map, output_dir):
  for rob_id, init_filename in files_map.items():
    output_filename = os.path.join(output_dir, '0x%08X.data' % rob_id)
    os.rename(init_filename, output_filename)
  
def main():
  
  if len(sys.argv) == 1 or (sys.argv[1] == '-h' or sys.argv[1] == '--help'):
    print("Splits event storage data files into ROB-based files.")
    print("The output is dumped in the current working directory.")
    print("usage: %s [file1] [file2] ... [fileN]" % sys.argv[0])
    sys.exit(1)

  logging.info("I will process %d file(s)" % len(sys.argv[1:]))
  
  logging.info("Calculating available unique ROB hits (considers all files)")
  robset = calculate_robset(sys.argv[1:])
  logging.info("I found %d distinct ROB source identifiers" % len(robset))

  logging.warn("To not exhaust the machine's memory, each file will be " +
      "treated individually. This is slower, but safer.")
  
  for ev_file in sys.argv[1:]:
    outdir = os.path.realpath(os.getcwd() + os.sep + 
      os.path.basename(ev_file) + '.per-rob')
    
    if os.path.exists(outdir):
      backup = outdir + '~'
      if os.path.exists(backup):
        logging.warn("Removing (old) backup at %s" % backup)
        shutil.rmtree(backup)
      logging.info("Back'ing-up %s at %s" % (outdir, backup))
      shutil.move(outdir, backup)

    os.mkdir(outdir)

    logging.info("Processing file %s" % ev_file)
    try:
      files_map = split_data(ev_file, robset, outdir)
      rename_files(files_map, outdir)
      logging.info("Saved %d file(s) to directory '%s'." % (len(files_map), outdir))
    except IOError as e:
      logging.error(e)
      logging.warn('This problem can be caused by an insufficient number of file descriptors provided by your shell (check with it either `limit`  or `ulimit -a` depending on your shell type)')
  
if __name__ == "__main__":
  main()
