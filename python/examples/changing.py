#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 03-Aug-2006

import eformat 
import sys

if len(sys.argv) == 1:
    print("usage: %s <data-file> [+data-file]" % sys.argv[0])
    sys.exit(1)

stream = eformat.istream(sys.argv[1:])
print('The total number of events available is %d' % len(stream))

new_source = eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_BEAM_CRATE, 0x1234)
new_version = eformat.helper.Version(0x0501) #only changes minor part
new_bc_time_seconds = 0x12345678
for e in stream:
    print("Source Identifier, before is", e.source_id())
    print("Version, before is", e.version())
    print("Bunch Cros. Time, before is 0x%x" % e.bc_time_seconds())
    e = eformat.write.FullEventFragment(e) #transforms in writable
    e.source_id(new_source)
    e.minor_version(new_version.minor_version())
    e.bc_time_seconds(new_bc_time_seconds)
    print("Source Identifier, after is", e.source_id())
    print("Version, after is", e.minor_version())
    print("Bunch Cros. Time, after is 0x%x" % e.bc_time_seconds())
