#!/usr/bin/env tdaq_python
# $Id$
# Created by Rodrigo TORRES <Rodrigo.Torres@cern.ch>, 05-Feb-2007

import eformat

#This example shows how to create a event using the binding to the eformat::write package.

def getFullEventFragment():
  full_event = eformat.write.FullEventFragment()

  print("Source ID = 0x%08x" % full_event.source_id().code())
  print("Bunch Crossing Time (seconds) = %d" % full_event.bc_time_seconds())
  print("Bunch Crossing Time (nanoseconds) = %d" % \
    full_event.bc_time_nanoseconds())
  print("Global ID = %d" % full_event.global_id())
  print("Run Type = %d" % full_event.run_type())
  print("Run Number = %d" % full_event.run_no())
  print("Lumi Block = %d" % full_event.lumi_block())
  print("LVL1 ID = %d" % full_event.lvl1_id())
  print("Bunch Crossing ID = %d" % full_event.bc_id())
  print("Detector Mask = %d" % full_event.detector_mask())
  print("LVL1 Trigger Type = %d" % full_event.lvl1_trigger_type())

  full_event.status([0x50,0x51,0x52])
  print("Status words are: %s" % full_event.status())

  full_event.lvl1_trigger_info([0x22, 0x23])
  print("LVL1 Info is: %s" % full_event.lvl1_trigger_info())

  full_event.lvl2_trigger_info([1,3,5,7, 9])
  print("LVL2 Info is: %s" % full_event.lvl2_trigger_info())

  full_event.event_filter_info([1,2,3])
  print("EF Info is: %s" % full_event.event_filter_info())

  st1 = eformat.helper.StreamTag()
  st1.name = 'myst'
  st1.type = 'bla'
  st1.obeys_lumiblock = False
  st2 = eformat.helper.StreamTag()
  st2.name = 'another nice name'
  st2.type = 'this can be as long as I want'
  st2.obeys_lumiblock = True
  full_event.stream_tag([st1,st2])
  print("Stream Tag/s is/are: %s" % full_event.stream_tag())

  print("Meta data size is %d words." % full_event.meta_size_word())

  print("Full fragment size is %d words." % len(full_event))

  return full_event


def getSubDetectorFragment():
  subFrag = eformat.write.SubDetectorFragment()
  
  subFrag.source_id(eformat.helper.SourceIdentifier(eformat.helper.SubDetector.FULL_SD_EVENT, 0x22))
  print("Source ID is 0x%08x" % subFrag.source_id().code())

  subFrag.minor_version(20)
  print("Minor Version is %d" % subFrag.minor_version())

  subFrag.status([0xA,0xB,0xC,0xD])
  print("Status words are: %s" % subFrag.status())

  print("Full fragment size is %d words." % len(subFrag))
  
  return subFrag


sf = getSubDetectorFragment()
ff = getFullEventFragment()
ff.append(sf)
