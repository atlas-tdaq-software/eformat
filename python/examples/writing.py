#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 15-Nov-2006

import eformat 
import sys

if len(sys.argv) == 1:
    print("usage: %s <data-file> [+data-file]" % sys.argv[0])
    sys.exit(1)
    
istr = eformat.istream(sys.argv[1:])
ostr = eformat.ostream()

for e in istr:
  print("Reading event with Lvl1 Identifer = %ld, with size %d " % \
        (e.lvl1_id(), len(e)))
  ostr.write(e)
  print("Run size is currently %d megabytes" % ostr.megabytes_in_stream())
  
print("I recorded %d events in the file %s" % (ostr.events_in_file(),
                                               ostr.current_filename()))

