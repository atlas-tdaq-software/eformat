#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 22-Nov-2006

# This examples shows how to use the dumping library to dump events
# in textual format, much like od (unix utility) can do.

import eformat
import sys

# Most of the dumping functionality is encoded inside the "dump" module
import eformat.dump as edump

# The dumping works with callbacks. You can register particular call backs for
# full events, subdetector ros and rob/rod fragments. The way to register a
# callback is by calling the appropriate callback register function. The module
# provides standard dumping functions you can use as a default.

# The more you add, the more is dumped. Remember this rule: no adding, no
# dumping! You are in absolute control. 

#edump.event_callback.append(('.+', edump.fullevent_handler))
#edump.subdetector_callback.append(('.+', edump.subdetector_handler))
#edump.ros_callback.append(('.+', edump.ros_handler))
#edump.rob_callback.append(('.+', edump.rob_handler))

# A second important rule is: if the parent fragment is not "printed" (you
# will understand why printed is quoted in a while), the children will
# not either. To print a ROB fragment, the parent ROS fragment has to be 
# printed, for that, its parent SubDetector fragment and the full event.  
# The module provides "null" handlers, that will allow you to select the 
# parent without dumping anything from it (did you get it now?).

# The next example will register callbacks for all levels, but only
# the ROB level is printed.

edump.event_callback.append(('3', edump.fullevent_handler))
edump.subdetector_callback.append(('.+', edump.null_handler))
edump.ros_callback.append(('.+', edump.null_handler))
#edump.rob_callback.append(('.+', edump.rob_handler))

# Regular expressions are used to trigger the callback call.
# You can read on python regular expressions on a python manual.
# Full event callbacks, use *decimal* "lvl1 identifier"
# Other fragment types use *hexadecimal* "source identifier" matching".

# The following lines will associate the dumping of LAR HEC A SIDE ROB
# Fragments with the default rob_handler. If you let this program to
# run, you will get a dump of LAR HEC ROB's from the ATLAS A SIDE only!
edump.rob_callback.append(('0x45.*', edump.rob_handler))

# A call to the module "dump" function trigger the dumping process. In the
# current configuration, only LAR HEC A SICE ROB fragments from event number 3
# will be printed. Make sure you can execute that before proceeding with your
# own changes.
if len(sys.argv) == 1:
    print("usage: %s <data-file> [+data-file]" % sys.argv[0])
    sys.exit(1)
    
edump.dump(eformat.istream(sys.argv[1:]))

# Now you can create your own handlers. This is their API:
# handler_name (fragment, offset)
# The fragment is an object of the type you specify in the callback. If you
# register the method in a ros_callback, you will get a ROS fragment. The
# offset parameter tells in which offset this fragment starts from. You can
# use it if you want to display absolute positions. The rest is with you.
# Explore the dump.py module to find out more things you can do!
