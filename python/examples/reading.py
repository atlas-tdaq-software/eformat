#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 03-Aug-2006

import time
import sys
import eformat

if len(sys.argv) == 1:
    print("usage: %s <data-file> [+data-file]" % sys.argv[0])
    sys.exit(1)
    
stream = eformat.istream(sys.argv[1:])
print('The total number of events available is %d' % len(stream))

volume = 0
start = time.time()
rob_list = {} #a dictionary is more efficient!
start = time.time()
for event in stream:
  volume += len(event)
  for rob in event:
      rob_list[rob.source_id().code()] = 1

rob_list = list(rob_list.keys())
total_time = time.time() - start
print('Total processing time was %f seconds' % total_time)
print('Total event data is %.2f Mb' % ((float(volume) * 4) / (1024*1024)))
print('Throughput is %.2f Mb/s' % ((float(volume) * 4) / (1024*1024*total_time)))
print('Throughput is (also) %.2f events/s' % (len(stream) / total_time))
print('There are %d unique ROB hits in these files' % len(rob_list))
