#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 17-Nov-2006

# This program replaces the C++ application with the same name.
# The code is based on python and goes a bit slower than its C++
# counterpart, but is much more flexible

from __future__ import print_function
from builtins import str
from builtins import range
import eformat, logging
import re

event_callback = []
rob_callback = []

offset_counter = 0

def pretty_print(value, description):
  """This function will create a nice output for the information you give in
  regarding its arrangement on the screen"""
  global offset_counter
  print("0x%08x 0x%08x %10lu  %s" % (offset_counter, value, value, description))
  offset_counter += 1

def pretty_print64(value, description):
  """This function will create a nice output for the information you give in
  regarding its arrangement on the screen"""
  global offset_counter
  p1 = value
  p1 >>= 32
  p2 = value
  p2 &= 0xffffffff
  print("0x%08x 0x%08x %10lu  %s" % (offset_counter, p1, p1, description + ' [1/2]'))
  print("0x%08x 0x%08x %10lu  %s" % (offset_counter+1, p2, p2, description + ' [2/2]'))
  offset_counter += 2

def null_handler(f, offset):
  """Dumps nothing.
  """
  pass

def header_handler(f, marker='fragment marker'):
  """Dumps the data of a single fragment header to the screen.

  This method can dump the information of a generic eformat header to the
  screen, returning the number of words this header contains.
  """

  pretty_print(f.marker(), marker)
  pretty_print(len(f), "fragment size (words)")
  pretty_print(f.header_size_word(), "header size (words)")
  pretty_print(f.version().code(), "version: %s" % f.version())
  pretty_print(f.source_id().code(), "source_id: %s" % f.source_id())
  status = f.status()
  pretty_print(len(status), "number of status words")
  for i in range(0, len(status)):
    pretty_print(status[i], "status[%d]" % i)
  pretty_print(f.checksum_type(), "check sum type")
  return 7 + len(status)

def fullevent_handler(f):
  """Knows how to dump full event fragments"""

  if not f.check_noex():
    logging.warn("Skipping event %d, check_noex() reported problems: %s" % 
      (f.lvl1_id(), [str(k) for k in f.problems()]))
    return

  header_handler(f, '[full event marker]')
  pretty_print(f.bc_time_seconds(), 
      "bunch cros. time in seconds")
  pretty_print(f.bc_time_nanoseconds(), 
      "bunch cros. time, additional nanoseconds")
  pretty_print(f.global_id() & 0xffff, "global event identifier LS")
  pretty_print(f.global_id() >> 32, "global event identifier MS")
  pretty_print(f.run_type(), "run type")
  pretty_print(f.run_no(), "run number")
  pretty_print(f.lumi_block(), "lumi block")
  pretty_print(f.lvl1_id(), "lvl1 identifier")
  pretty_print(f.bc_id(), "bunch cros. identifier")
  pretty_print(f.lvl1_trigger_type(), "lvl1 trigger type")
  pretty_print(f.compression_type(), "compression type")
  pretty_print(f.uncompressed_payload_size(), "uncompressed payload size")
  pretty_print(len(f.lvl1_trigger_info()), 
      "number of lvl1 trigger info words")
  for i in range(0, len(f.lvl1_trigger_info())):
    pretty_print(f.lvl1_trigger_info()[i],"lvl1 trigger info[%d]" % i)
  pretty_print(len(f.lvl2_trigger_info()), 
      "number of lvl2 trigger info words")
  for i in range(0, len(f.lvl2_trigger_info())):
    pretty_print(f.lvl2_trigger_info()[i], 
        "lvl2 trigger info[%d]" % i)
  pretty_print(len(f.event_filter_info()), 
      "number of event filter info words")
  for i in range(0, len(f.event_filter_info())):
    pretty_print(f.event_filter_info()[i], "event filter info[%d]" % i)
  for i in range(0, len(f.raw_stream_tag())):
    pretty_print(f.raw_stream_tag()[i], "stream tag word[%d]" % i)

def rob_handler(f):
  """Knows how to dump full ROS fragments"""

  if (f.check_rob_noex()): 
    header_handler(f, '[rob marker]')
  else:
    logging.warn("Skipping ROB 0x%08x (%s), check_rob_noex() reported problems: %s" % 
      (f.source_id().code(), f.source_id(), [str(k) for k in f.rob_problems()]))
    return

  if (f.check_rod_noex()):
    pretty_print(f.rod_marker(), "[rod marker]")
    pretty_print(len(f.rod_header()), "header size")
    pretty_print(f.rod_version().code(), "version: %s" % f.rod_version())
    pretty_print(f.rod_source_id().code(), "sourceid: %s" % f.rod_source_id())
    pretty_print(f.rod_run_no(), "run number")
    pretty_print(f.rod_lvl1_id(), "lvl1 identifier")
    pretty_print(f.rod_bc_id(), "bunch crossing identifier")
    pretty_print(f.rod_lvl1_trigger_type(), "lvl1 trigger type")
    pretty_print(f.rod_detev_type(), "detector event type")
    status = f.rod_status()
    i = 0
    j = 0
    if not f.status_position():
      for i in range(0, len(status)):
        pretty_print(status[i], "status[%d]" % i)
    data = f.rod_data()
    for j in range(0, len(data)):
      pretty_print(data[j], "data[%d]" % j)
    if f.status_position():
      for i in range(0, len(status)):
        pretty_print(status[i], "status[%d]" % i)
    pretty_print(len(status), "number of status words")
    pretty_print(len(data), "number of data words")
    pretty_print(f.status_position(), "status position")
    if bool(f.checksum_type()):
      pretty_print(f.checksum_value(), "rob checksum value")
  else:
    logging.warn("Skipping ROD 0x%08x (%s), check_rod_noex() reported problems: %s" % 
      (f.rod_source_id().code(), f.rod_source_id(), [str(k) for k in f.rod_problems()]))

def add_default_handlers():

  global event_callback, rob_callback
  event_callback.append(('.+', fullevent_handler))
  rob_callback.append(('.+', rob_handler))

def dump_event(event):
  """Dumps a particular FullEvent taking into consideration its relative offset
  in the stream."""

  global event_callback

  for func in event_callback:
    # we satisfy only the first registered handler that satisfy the matching
    # condition.
    if func[0].match('%lu' % event.lvl1_id()):
      func[1](event)
      event.header_size_word()
      for rob in event: dump_rob(rob)
    else:
      global offset_counter
      offset_counter ++ event.fragment_size_word()
    break
    
  if bool(event.checksum_type()):
    pretty_print(event.checksum_value(), "event checksum value")

def dump_rob(rob):
  """Dumps a particular ROB fragment taking into consideration its relative
  offset in the stream."""

  global rob_callback

  for func in rob_callback:
    # we satisfy only the first registered handler that satisfy the matching
    # condition.
    if func[0].match('0x%08x' % rob.source_id().code()):
      func[1](rob)
    else:
      global offset_counter
      offset_counter ++ rob.fragment_size_word()
    break
        
def dump(stream, skip=0, total=0):
  """Dumps fragments of type FullEvent or ROS.
  
  This method will dump in a nice, text-printable format, the event data to the
  screen. You can select to skip the first N events or print a total of event
  which is smaller than the total in the stream.
  """
  
  global event_callback, rob_callback, offset_counter

  # compile all regexps in callback first
  event_callback = [(re.compile(k[0]), k[1]) for k in event_callback]
  rob_callback = [(re.compile(k[0]), k[1]) for k in rob_callback]
  
  count = 0
  for obj in stream: 
    
    if skip > 0:
      
      offset_counter += obj.fragment_size_word()
      print(obj.lvl1_id())
      skip -= 1
      continue

    if total > 0 and count >= total: break
    
    if isinstance(obj, eformat.FullEventFragment): dump_event(obj)
    else:
      raise SyntaxError("Stream object is not a FullEvent")

    count += 1
