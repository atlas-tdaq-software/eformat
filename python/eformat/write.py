  #!/usr/bin/env tdaq_python
# $Id$
# Created by Andre Anjos <Andre.dos.Anjos@cern.ch>, 11 Feb 2007 

# This python file includes the official interface to the eformat/write
# packages at the DAQ/HLT-I release

import libpyeformat_write as cxx
import eformat as read

class FullEventFragment (cxx.FullEventFragment):
  """Describes an event format FullEventFragment.

  This class is a wrapper to eformat::write::FullEventFragment. It allows
  you to create full event fragments.
  """

  def __init__(self, *args):
    """
    Constructor call possibilities:
    __init__()
        -> Generates an empty FullEventFragment object.
        
    __init__(FullEventFragment)
        -> Generates an FullEventFragment object by reading the contents of the
        passed FullEventFragment object. With this method, the event will be
        completely broken down in smaller fragments, so the user can fully
        manipulate the object.

    __init__(FullEventFragment, regexp)
        -> Generates a FullEventFragment object by reading the contents of
        the passed FullEventFragment object. The regexp filter will *remove*
        fragments which source ID match the regular expression. The behavior
        of this method is recursive (into the fragment tree).
    """
    self.__children__ = []
    
    if len(args) == 0: 
      cxx.FullEventFragment.__init__(self)
    elif len(args) == 1 or len(args) == 2:
      remove = None
      if len(args) == 2:
        if 'match' in dir(args[1]): remove = args[1]
        elif args[1] is not None: 
          raise TypeError("Argument 2 must contain a 'match()' method")
      if isinstance(args[0], cxx.FullEventFragment) or \
         isinstance(args[0], read.FullEventFragment):
        #do a deepcopy by default
        cxx.FullEventFragment.__init__(self)
        self.copy_header(args[0])
        self.copy_payload(args[0], remove) 
      else:
        raise TypeError("Argument 1 must be a FullEventFragment") 
    else:
      raise AttributeError("Invalid number of parameters passed")
  
  def copy_payload(self, other, remove=None):
    """Copies all actual payload from the fragment given as parameter."""
    for rob in other:
      if remove is not None and \
        'match' in dir(remove) and \
        remove.match("0x%08x" % rob.source_id().code()): continue
      self.append(ROBFragment(rob))

  def lvl1_trigger_info(self, data=None):
    return block_data(super(FullEventFragment, self).lvl1_trigger_info, data)

  def lvl2_trigger_info(self, data=None):
    return block_data(super(FullEventFragment, self).lvl2_trigger_info, data)

  def event_filter_info(self, data=None):
    return block_data(super(FullEventFragment, self).event_filter_info, data)
  
  def stream_tag(self, data=None):
    if isinstance(data, list):
      tmp = read.helper.StreamTags() # try a conversion to StreamTags
      for k in data: tmp.append(k)
      data = tmp
      
    if isinstance(data, read.helper.StreamTags):
      data = data.__raw__() # convert into its raw, u32list representation
     
    if not (isinstance(data, read.u32list) or \
            isinstance(data, read.u32slice) or \
            data is None):
      raise TypeError("Expecting a (python) list of StreamTags")
    
    return block_data(super(FullEventFragment, self).stream_tag, data)

  def readonly(self):
    """Returns a read-only representation of the current fragment"""
    return read.FullEventFragment(self.__raw__())

class ROBFragment (cxx.ROBFragment):
  """Describes an event format ROBFragment.

  This class is a wrapper to eformat::write::ROBFragment. It allows
  you to create ROB fragments.
  """

  def __init__(self, *args):
    """
    Constructor call possibilities:
    __init__()
        -> Generates an empty ROBFragment object.
        
    __init__(rob_frag_obj)
        -> Generates an ROBFragment object by reading the contents of the
        passed ROBFragment object.
    """
    if len(args) == 0:
      cxx.ROBFragment.__init__(self)
    elif len(args) == 1:
      if isinstance(args[0], cxx.ROBFragment):
        cxx.ROBFragment.__init__(self)
        self.copy_header(args[0])
        self.copy_payload(args[0]) 
      elif isinstance(args[0], read.ROBFragment):
        cxx.ROBFragment.__init__(self)
        self.copy_header(args[0])
        if args[0].check_rod_noex():
          self.copy_payload(args[0]) 
        elif args[0].payload_size_word() > 9:
          self.rod_data(args[0].payload()[9:])
      else:
        raise TypeError("The input object class must be ROBFragment") 
    else:
      raise AttributeError("Invalid number of parameters passed")

  def copy_payload(self, other):
    """Copies all actual data from the fragment given as parameter."""
    self.rod_data(other.rod_data())

  def rod_status(self, data=None):
    """Sets or gets status block on the ROD fragment."""
    return block_data(super(ROBFragment, self).rod_status, data)
    
  def rod_data(self, data=None):
    """Sets or gets data block on the ROD fragment."""
    return block_data(super(ROBFragment, self).rod_data, data)
  
  def readonly(self):
    """Returns a read-only representation of the current fragment"""
    return read.ROBFragment(self.__raw__())

# some python magic to attach iteration and comparison
FullEventFragment.__eq__ = read.eq
FullEventFragment.__ne__ = read.ne
ROBFragment.__eq__ = read.eq
ROBFragment.__ne__ = read.ne

# how to iterate over children
def children(fragment):
  """Returns all registered children of this fragment."""
  return fragment.__children__ 
def iter(fragment):
  for k in fragment.__children__: yield k
FullEventFragment.children = children
FullEventFragment.__iter__ = iter

# how to append
def append(f, other):
  f.__children__.append(other)
  super(f.__class__, f).append(other)
FullEventFragment.append = append

# setup and retrieval of block data
def block_data(method, data=None):
  """Sets or gets u32 data blocks"""
  if data:
    if not (isinstance(data, read.u32list) or isinstance(data, read.u32slice)):
      data = read.u32list(data) # we try a conversion 
    method(data)
  else:
    return method()

# specific status blob for all fragments in one shot
def status_block_data(o, data=None):
  """Sets or gets status blocks on this fragment."""
  return block_data(super(o.__class__, o).status, data)

FullEventFragment.status = status_block_data
ROBFragment.status = status_block_data

