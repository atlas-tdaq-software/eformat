#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Wed 09 Jan 2008 06:10:20 PM CET 

"""Defines the types of streams we need in the eformat bindings
"""

import os, logging
from libpyeformat import FullEventFragment, convert_old 
import libpyeformat_helper as helper
import libpyevent_storage as EventStorage

class blob_istream(object):
  """Returns useless blobs of data, as defined by the EventStorage i/f."""

  def __init__(self, l):  
    """Initializes an event stream with a file or a list of files.""" 

    #make sure we are always dealing with lists 
    if type(l) is str: l = [l]

    if type(l) is not list:
      raise TypeError("stream class accepts only a string or a list of strings as parameters")

    #we leave completely to the data reader to defined "valid files".
    self.total_events = 0
    for f in l:
      dr = EventStorage.pickDataReader(f)
      if dr is None:
        raise IOError("Invalid file or format at '%s'" % f)
      self.total_events += dr.eventsInFile()

    self.filelist = list(l) # deep copy

  def __len__(self):
    """Returns the number of events available in this stream"""
    return self.total_events

  def __iter__(self):
    for f in self.filelist:
      dr = EventStorage.pickDataReader(f)
      for k in range(dr.eventsInFile()):
        yield dr.getData()

  def __getitem__(self, key):
    """Returns a specific blob in the stream. The 'key' parameter maybe
    either a single valued integer or a slice object, in which case many
    fragments are returned in one shot."""
    
    if type(key) is int or type(key) is int:
      if key >= len(self):
        raise IndexError("Index (%d) is bigger than my length (%d)" % (key, self.total_events))
      if key < 0:
        if (-key) > len(self):
          raise IndexError("Index (%d) is too small for my length (%d)" % (key, self.total_events))
        key = len(self) + key
        
      f = None
      for f in self:
        key -= 1
        if key < 0: break
      return f

    elif type(key) is slice:
      (start, stop, stride) = key.indices(self.total_events)
      valid = list(range(start, stop, stride))
      retval = []
      counter = 0
      for f in self:
        if counter in valid:
          retval.append(f)
        counter += 1
      return retval

class istream(blob_istream):
  """The istream class allows the user to read file streams.

  This class enpowers the user to open and read seamlessly EventStorage files,
  one after the other, transparently. The user may specify a single file or
  EventStorage set, or a list of files and sets mixed. The events will flow
  like in a normal python file. 
  
  As you read, this class is iterable, which means you can say something like:

  for event in istream('myfile.data'):
    ... #do something with the event

  Or even, for the longer runs:

  for event in istream(['file1.data', 'fileset2', 'file3.data']):
    ... #do something with the event

  Any checks for consistency are on your control
  """

  def __init__(self, l):
    blob_istream.__init__(self, l)

  def __iter__(self):
    for f in self.filelist:
      dr = EventStorage.pickDataReader(f)
      for k in range(dr.eventsInFile()):
        blob = dr.getData()

        # check for people trying old versions and convert it on the spot
        fragment_version = helper.Version(blob[3])
        if fragment_version.major_version() != helper.MAJOR_DEFAULT_VERSION:
          current_version = helper.Version()
          logging.debug("Converting from version %s to %s" % \
              (fragment_version.human_major(), current_version.human_major()))
          blob = convert_old(blob)
        
        if blob[0] == helper.HeaderMarker.FULL_EVENT:
          yield FullEventFragment(blob)
        else:
          raise SyntaxError("Expecting event marker, not 0x%08x" % blob[0])

  def iter_raw(self):
    for f in self.filelist:
      dr = EventStorage.pickDataReader(f)
      for k in range(dr.eventsInFile()):
        yield dr.getData()

class ostream(object):
  """The stream class allows the user to write file streams.
  
  This class enpowers the user to open and write seamlessly EventStorage files
  from python. The system accepts blocks of data as any eformat fragment.
  
  To use this class just use the 'write()' method with the fragment you want to
  write to the stream.
  """

  def __init__(self,
               directory='.',
               core_name="python_script",
               run_number=0,
               trigger_type=0,
               detector_mask=0,
               beam_type=0,
               beam_energy=0,
               meta_data_strings=[],
               start_index=1,
               compression=EventStorage.CompressionType.NONE,
               complevel=1):
    """Initializes an event stream with the required parameters. This
    constructor will instantiate an EventStorage data file that will contain a
    record honouring the parameters you gave as input. Most of them are useless
    for testbed purposes, but you may still want to play around."""
    parameters = EventStorage.run_parameters_record()
    #parameters.marker = 
    #parameters.record_size =
    parameters.run_number = run_number
    #parameters.max_events = 
    #parameters.rec_enable = 
    parameters.trigger_type = trigger_type
    parameters.detector_mask_LS = detector_mask & 0xFFFFFFFFFFFFFFFF
    parameters.detector_mask_MS = (detector_mask >>64) & 0xFFFFFFFFFFFFFFFF
    parameters.beam_type = beam_type
    parameters.beam_energy = beam_energy
    self.writer = EventStorage.DataWriter(directory, core_name, parameters, \
    meta_data_strings, start_index, compression, complevel)
    if not self.writer.good():
      raise IOError('Internal DataWriter cannot open output file!')

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):
    self.close()

  def current_filename(self):
    """Returns the current file that is being written"""
    return self.writer.nameFile(EventStorage.FileStatus.UNFINISHED)

  def last_filename(self):
    """Returns the last filename that was written"""
    return self.writer.nameFile(EventStorage.FileStatus.FINISHED)

  def next(self):
    """Start with the next file writing, closing the current file."""
    return self.writer.nextFile()

  def events_in_file(self):
    """Returns the total number of events in the current file."""
    return self.writer.eventsInFile()

  def events_in_stream(self):
    """Returns the total number of events in the current sequence."""
    return self.writer.eventsInFileSequence()

  def megabytes_in_file(self):
    """Returns the total number of megabytes in the current file."""
    return self.writer.dataMB_InFile()

  def megabytes_in_stream(self):
    """Returns the total number of megabytes in the current sequence."""
    return self.writer.dataMB_InFileSequence()

  def max_events_per_file(self, v):
    """Sets the maximum number of events per single file."""
    self.writer.setMaxFileNE(v)

  def max_megabytes_per_file(self, v):
    """Sets the maximum number of megabytes per single file."""
    self.writer.setMaxFileMB(v)

  def write(self, o):
    """Writes a fragment into the bytestream."""
    self.writer.putData(o)
  
  def setguid(self, guid):
    """Set the guid of the _next_ file"""
    self.writer.setGuid(guid)

  def close(self):
    """Closes the current file"""
    self.writer.closeFile()


##Kept here for backcompatibility
##Functionality is now in ostream
class compstream(ostream):
  """The stream class allows the user to write compressed file streams.

  This class enpowers the user to open and write seamlessly EventStorage files
  from python. The system accepts blocks of data as any eformat fragment.

  To use this class just use the 'write()' method with the fragment you want to
  write to the stream.
  """
  def __init__(self,
               directory='.',
               core_name="python_script",
               run_number=0,
               trigger_type=0,
               detector_mask=0,
               beam_type=0,
               beam_energy=0,
               meta_data_strings=[],
               start_index=1,
               compression=EventStorage.CompressionType.NONE,
               complevel=1):
    super(compstream,self).__init__(directory,
                                    core_name,
                                    run_number,
                                    trigger_type,
                                    detector_mask,
                                    beam_type,
                                    beam_energy,
                                    meta_data_strings,
                                    start_index,
                                    compression,
                                    complevel)
  
