#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 08 Jan 2008 10:53:33 AM CET 

"""The base interface to eformat logging and functionality.
"""
import logging

# import the basic functionality
from .stream import * # EventStorage functionality
from libpyeformat import * 
import libpyeformat_helper as helper
from . import dummy

# Comparison
def eq(f1, f2):
  """Compares equality of two fragments."""
  l = f1.__raw__()
  k = f2.__raw__()
  if len(l) != len(k):
    return False
  for i in range(0, len(l)):
    if l[i] != k[i]: return False
  return True      
def ne(f1, f2):
  return not (f1 == f2)

from . import write

# This fixes the values of the HeaderMarkers (because Boost.Python forces them
# into a signed integer without range checking. Not valid for 64-bit builds
if helper.HeaderMarker.FULL_EVENT != 0xaa1234aa:
  helper.HeaderMarker.FULL_EVENT += 0xffffffff + 1
  helper.HeaderMarker.ROB += 0xffffffff + 1
  helper.HeaderMarker.ROD += 0xffffffff + 1

class DebugLogger(object):
  """Attacheable class to the logging system to mimic the EventApps.logger."""

  def __init__(self, level):
    self.level = logging.DEBUG - level

  def __call__(self, msg, *args, **kwargs):
    return logging.log(self.level, msg, *args, **kwargs)

for k in range(1, logging.DEBUG): 
  logging.addLevelName(k, 'DEBUG_%d' % (logging.DEBUG-k))
  exec('logging.debug%d = DebugLogger(%d)' % (k,k))

Header.__eq__ = eq
Header.__ne__ = ne

# Printability of Version and SourceIdentifier
helper.SourceIdentifier.__str__ = helper.SourceIdentifier.human 
helper.Version.__str__ = helper.Version.human

# Printability of stream tags
def print_stream_tag(t):
  return "{'name': '%s', 'type': '%s', 'obeys_lumiblock': %s}" % \
      (t.name, t.type, t.obeys_lumiblock)
helper.StreamTag.__str__ = print_stream_tag
def print_stream_tags(t):
  retval = '['
  for r in range(len(t)): retval += str(t[r]) + ', '
  return retval[0:-2] + ']'
helper.StreamTags.__str__ = print_stream_tags

# Comparison of versions, identifiers and others
def eq_code(v1, v2):
  return int(v1) == int(v2)
def ne_code(v1, v2):
  return int(v1) != int(v2)
def gt_code(v1, v2):
  return int(v1) > int(v2)
def lt_code(v1, v2):
  return int(v1) < int(v2)
def ge_code(v1, v2):
  return int(v1) >= int(v2)
def le_code(v1, v2):
  return int(v1) <= int(v2)
helper.SourceIdentifier.__eq__ = eq_code
helper.SourceIdentifier.__ne__ = ne_code
helper.SourceIdentifier.__gt__ = gt_code
helper.SourceIdentifier.__lt__ = lt_code
helper.SourceIdentifier.__ge__ = ge_code
helper.SourceIdentifier.__le__ = le_code
helper.Version.__eq__ = eq_code
helper.Version.__ne__ = ne_code
helper.Version.__gt__ = gt_code
helper.Version.__lt__ = lt_code
helper.Version.__ge__ = ge_code
helper.Version.__le__ = le_code

def iterate_u32blob(o):
  """Allows iteration on u32list or u32slice objects"""
  for k in range(len(o)): yield o[k]
u32list.__iter__ = iterate_u32blob
u32slice.__iter__ = iterate_u32blob

# attach printability to uint32_t blobs
def print_u32blob(o):
  """Returns a string representation of the u32list or u32slice object"""
  return '{u32}[' + ', '.join(['0x%08x' % k for k in o]) + ']'
u32list.__str__ = print_u32blob
u32list.__repr__ = print_u32blob
u32slice.__str__ = print_u32blob
u32slice.__repr__ = print_u32blob

def eq_u32blob(b1, b2):
  """Compares two u32 blobs for equality, word after word."""
  if len(b1) != len(b2): return False
  for i in range(len(b1)):
    if b1[i] != b2[i]: return False
  return True
u32list.__eq__ = eq_u32blob
u32slice.__eq__ = eq_u32blob


def safe_convert_old(blob):
  """
  This format conversion function can be safely called with any fragment. Differently from the convert_old function, it will not raise an exception if the provided fragment is already aligned with the current version. In this case, the same fragment will be returned.

  """

  import libpyeformat
  fragment_version = helper.Version(blob[3])
  if fragment_version.major_version() == helper.MAJOR_DEFAULT_VERSION:
    return blob
  else:
    return libpyeformat.convert_old(blob)


class EformatDeprecationWarning(UserWarning):
    """
    Python builtin DeprecationWarning is ignored in Python2.7 while UserWarning
    is not. Hence this class (inspired by Matplotlib).
    https://docs.python.org/dev/whatsnew/2.7.html#the-future-for-python-2-x
    """
    pass


_deprecated_FErobs = FullEventFragment.robs


def _wrapped_FErobs(self):
    import warnings
    warnings.warn('"FullEventFragment.robs" may crash with compressed data, please use an iterator.',
                  EformatDeprecationWarning, stacklevel=1)
    return _deprecated_FErobs(self)


FullEventFragment.robs = _wrapped_FErobs
