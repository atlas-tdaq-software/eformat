#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 22 Jan 2008 03:47:39 PM CET 

"""Implements constructions to create dummy events and data
"""

import random
import eformat

def make_rob(source_id, payload=-1,
             lvl1_id=0, bc_id=0, status=[0x0]):
  """Generates a ROB fragment with random content.

  This method will generate a eformat.write.ROBFragment with random content.

  Parameters:

  source_id -- This is the full source identifier to be used for the created
  ROBFragment.

  payload -- This can be either an integer number, indicating the number of
  random words to generate as the payload of the (ROD) fragment or a list,
  indicating the precise contents of the fragment. If this number is smaller
  than zero, I'll will decide how much data to put in the ROB (between 1 and
  500 words).

  lvl1_id -- This is to set the Level-1 identifier of the ROB.
  
  bc_id -- This is good to set the Bunch Crossing identifier of this ROB.

  status -- Set here a status list, if you want to have anything different than
  [0x0] (the default).

  Returns a eformat.write.ROBFragment
  """
  retval = eformat.write.ROBFragment()
  if type(payload) not in [list, eformat.u32list, eformat.u32slice]: 
    if payload < 0: payload = random.randint(1, 500)
    payload = [random.randint(0, 0xffffffff) for k in range(payload)]
  retval.rod_data(payload)
  retval.rod_lvl1_id(lvl1_id)
  retval.rod_bc_id(bc_id)
  retval.status(status)
  retval.source_id(source_id)
  return retval
 
def make_fe(payload=-1, lvl1_id=0, bc_id=0, status=[0x0]):
  """Generates a FullEvent fragment with random content.

  This method will generate a eformat.write.FullEventFragment with random
  content.

  Parameters:

  payload -- This can be either an integer number, indicating the number of
  random ROBs to generate or a list, indicating the precise contents of
  the fragment in terms of eformat.write.ROBFragment's. If the value is
  set to a value smaller than zero, I will choose how many subfragments this
  event will have.

  lvl1_id -- This is to set the Level-1 identifier of itself and the underlying
  fragments.
  
  bc_id -- This is good to set the Bunch Crossing identifier for the underlying
  fragments.

  status -- Set here a status list, if you want to have anything different than
  [0x0] (the default).

  Returns a eformat.write.FullEventFragment
  """
  retval = eformat.write.FullEventFragment()
  if type(payload) not in [list]:
    if payload < 0: payload = random.randint(0, 1600)
    sid = [eformat.helper.SourceIdentifier(random.choice\
        (list(eformat.helper.SubDetector.values.values())), \
        random.choice(range(0xffff))) for k in range(payload)]
    payload = [make_rob(source_id=sid[k], lvl1_id=lvl1_id, bc_id=bc_id) \
               for k in range(payload)]
  for k in payload: retval.append(k)
  retval.status(status)
  retval.source_id(eformat.helper.SourceIdentifier(eformat.helper.SubDetector.FULL_SD_EVENT, 0x0))
  return retval

def make_file(filename, events, verbose=False):
  """Generates an EventStorage file with as many (dummy) events as you want.

  This method will generate a number of dummy events and store them in the
  file you designate.

  Parameters:

  filename -- This is the name of the output file you want to have
 
  events -- This is the number of random events to be generated. Level-1
  identifiers will be sequential starting from zero. Bunch Crossing identifier
  will be set to zero in all events. The same is valid for the RunNumber.

  verbose -- If this flag is set to True, this function will print a '.' (dot)
  for every generated fragment.
  """
  import os, sys
  f = eformat.ostream()
  if verbose: 
    sys.stdout.write('Generating %d events => \'%s\'\n' % (events, filename))
    sys.stdout.flush()
  for k in range(events): 
    f.write(make_fe(lvl1_id=k))
    if verbose: 
      sys.stdout.write('.')
      sys.stdout.flush()
  if verbose: 
    sys.stdout.write('\n')
    sys.stdout.flush()
  tmp = f.last_filename()
  del f
  os.rename(tmp, filename)

