Fri May 20 12:58:00 CEST 2005 Andre DOS ANJOS  <Andre.dos.Anjos@cern.ch>
	* Release v2r0p0
	* Total revamp of eformat

Thu Sep 25 11:34:27 CEST 2003 Andre DOS ANJOS  <Andre.dos.Anjos@cern.ch>
 	* Release v1r8p5
	* Fixes to IOVecStorage and ROBSpecific implementations
	* Changes in the building policy (cmt/requirements)
	* Removal of obsolete documentation
	* Improvements to the Fragment API
	
Mon Aug 11 06:03:51 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r8p1
	* Reduced dynamic dependencies to shared libraries (you may need to
	load more manually)
	* Removed obsolete standalone.mk makefile

Thu Aug  7 12:47:55 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r8p0
	* Merged eformat and eformat_dc (Giovanna guarantees DF releases to
	exist at all times)
	* Simplified Allocator loading (no more dynamic binding)
	* Dropped "ad hoc" dll implementation
	* Included linking options to libeformat.so. Users now get all 
	dependencies for free (or at least part of them)

Wed Jul  2 16:42:45 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r7p4
	* Removed compounds() method that copied pointers. Fixed test 
	programs to the old interface
	* Templated headers and fragments w.r.t the STL container allocator
	they use.

Wed Jun 25 20:29:50 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r7p2
	* Back with compounds() method avoid copying of pointers
	* Documentation updates

Wed Jun 25 09:59:21 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r7p0
	* Changes to the interface due to the use of new DF_ALLOCATOR
	* Documentation update

Fri May  9 10:15:21 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r6p1
	* Moved File class to FileImplementation to avoid name conflicts
	with namespace EventFormat::File
	* Reflect the changes to GzImplementation and SimpleImplementation

Tue Apr 29 15:27:51 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r6p0
	* Removed differences between file storage types. Now there is one
	single FileStorage that instantiates the right implementation to
	use based on the filename to open.
	* Introduced an Allocator abstract interface that all interfaces
	have to conform to. The allocation is not anymore templated on the
	fragment, but chosen at application level.
	* It was introduced a new Storage base class. All storages
	inherit from that interface, w/o exceptions.
	* Introduced a new type of fragments, the ones that don't know the
	storage type they are in. This fragments can be typically used
	when no "efficient appending" of data is required in the
	application and removes from the user the necessity to write
	templated code in generic libraries.

Thu Apr 17 14:57:17 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r5p0
	* Added optional error reporting functionality
	* Fixed bugs
	* Updated comments

Fri Apr  4 20:17:14 CEST 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r4p0
	* Removed templating on storage type from Headers
	* Specific header parts are now normal (non-templated) classes
	* Implemented automatic endianness checking and byte-swapping
	* Removed obstack storage types (legacy code)
	* Removed obstack releated tests and producer/consumer test
	* Removed byte-swapping on file reading/writing so we get what we
	get in the end, it doesn't matter anymore.
	* Included a new "diff" utility to verify files on their ROB level
	* Provided Header level equality/inequality operators

Tue Mar 11 14:37:22 CET 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r3p4 
	* Now the CMT and standalone releases have the same number
	* Did some small changes to the debug policy
	* Fixed some compilation problems under gcc-3.2
	* Fixed a bug when appending memory blocks on RawMemory and
	Obstack storages.

Tue Feb 25 13:46:13 CET 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r3p2 (standalone 1.0.3)
	* Increases the compressed buffer size to 50Mbytes

Seg Fev 24 11:13:50 CET 2003 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r3p1 (standalone 1.0.2)
	* Small change in test/config_rose.cxx to output a configuration
	file that can serve to configure multiple ROS emulators (ROSE).

Tue Feb 03 12:54:00 CET 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r2p2 (standalone 1.0.1)
	* Changed the allocator interface to conform to the STL and ROS
	allocators. 
	* Improved the implementation of the FastAllocator in speed. This
	implementation is based on the equivalent implementation on the
	ROS side. This move aims integration in the medium timescale
	* The size() methods on fragments now have the possibility to 
	trust the serialised fragment specifications. This shall increase
	the speed when processing fragments that are partially serialised
	in some media.
	* The extend() method on fragments has be finalised and should
	work properly on storages that support it.

Fri Jan 24 16:51:23 CET 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r2p0 (standalone 1.0.0)
	* Added possibility to define offset and size of storages to be
	copied into other storages. This way you can partially copy
	storages now.
	* Changed the fragment "smart" append policy not to take into
	account fragment ownership. Now, fragments may be appended to
	others even if the fragment owner doesn't own the storage being
	appended.
	* Fixed bug on debug printouts
	* Fixed bug on GzFileStorage, added more debug printouts
	* Included more types of fragments RawMemory/SimpleAllocator,
	File/SimpleAllocator and GzFile/SimpleAllocator for offline users.

Wed Dec 11 15:35:07 CET 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r1p2 (standalone 0.8.1)
	* Implemented specific method for SubDetector search in FullEvent
	  fragments
	* Fixed possible deletion of "NULL" in FastAllocator
	* Removed excessive "inline'ing" of code in SimpleAllocator
	* Fixed bug in ROBHeader and Header for setting both Version and
	  Source identifiers when reading from storage
	* Fixed bug in retrieving the parts of the Source identifier when
	  reading data from the storage
	* Updated test programs to reflect these changes

Tue Nov  5 13:29:27 CET 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r1p1 (standalone 0.8) 
	* Implemented non-member methods for minimal construction of
	fragments.
	* Fixed FullEvent specific header to read/write EF v2.2
	* Fixed writing of offset map entries (now the entries also
	include the module identifier (instead of simple numbering). This
	modification allows some search algorithms to be implemented int
	the Fragment.
	* Included more test programs (one for the minimal construction of
	fragments)
	* Included new Fragment method that allows to search for a
	sub-level fragment with its module identifier.
	* Fixed debugging output when to write uint8_t in hexa format
	* Improved test program "raw_build" to use constructors based on
	source identifier components (that should be the way to do it!)
	* Improved comments

Tue Oct 29 11:30:00 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r1p0 (standalone 0.7)
	* Moved to Event Format v2.2
	* Introduced a specific type to handle source identifier coding and
	  decoding
	* Introduced a specific type to handle formatting versions
	* This release is *maybe* not rh61 (gcc-2.91) compatible anymore
	* Introduced a test program for coding and decoding source identifiers
	* Fixed compilation problems that only appeared in debug level 3

Fri Oct 25 17:24:34 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p15 (standalone 0.6)
	* Fixed '-pendantic' compilation flag problems. The package can now
	be compiled also with that flag.
	* Debug info is compiled out by default, 'pedantic' flag is in
	* Included new memory management scheme that should be faster when
	making new fragments on the heap.
	* Included simple (for compatibility) a simpler memory manager that
	can be used for tests
	* Fixed bugs concerning the RawMemoryStorage (now it writes only
	its logical size to the other storage)
	* Compounds, inside the fragment are not anymore a std::vector<>*,
	but a std::vector<>. This should increase performance for online
	runs. 
	* A consequence of the previous enhacement is that you no longer
	get a std::vector<>* when you release_compounds()
	* Fragments gained a new template parameter: the allocator type.
	
Mon Aug 12 12:35:32 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p12 (standalone 0.5)
	* Added methods in Fragment (and RODFragment) that allows smart
	serilialisation even with different storage types. The storages
	have to support it or a compilation-time error is generated.
	* Moved the old Raw Memory storage code into a new file (name)
	named after "obstack".
	* Created a new storage based on new/delete, and called it
	RawMemory, replacing the old storage with the same name. It will
	be possible to concatenate data from this storage into a
	DC::Buffer.
	
Wed Aug 05 17:47:20 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p11 (standalone 0.4.1)
	* Only cosmetic fixes. Latest release before changing storage
	types.

Wed Jul 24 12:25:00 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p10 (standalone 0.4)
	* Improved headers to read and write the break-down structure of
	the source identifiers
	* Included configuration utility for ROSE
	* Removed L2_source utility (was of no use)
	* Improved compressed file read-in routines (it should be much
	faster now).

Thu Jul 18 09:48:10 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p9
	* Removed all traces of the _register_ keyword

Thu Jun 27 10:16:25 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p8
	* Fixed leak on RODFragment regarding header instantiation and
	ownerships.

Wed Jun 12 09:33:50 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p7
	* Fixed global declarations

Tue Jun 11 19:28:10 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p6
	* Added scheme for setting the ownership policy at the library.
	From now on you select the library not to automatically delete  
	the parts you stick-in by saying ownership_policy(false).
	* Turned-off builtin debug statements by default. If you want
	to enable them now, you should change the macro manually under
	the "cmt" directory (file: "requirements", on the private
	section). 
	* Updated comments on header files to reflect the changes
	regarding the ownership policy.
	* Updated the use-case/test program "raw_build.cxx" to use the new
	ownership policy scheme.
	* Inserted new use-case/test program under eformat_dc that
	explains how to build messages and how to serialise them
	into data collection buffers.
	* Fix DCBufferStorage bug on "seek()".
	
Mon Jun 10 11:42:39 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p4
	* Added methods for acquiring resources both in Fragment and
	RODFragment types
	* Improved resource releasing methods, they should work better
	from now on.
	* Added the old (standalone release) documents to the the 'doc'
	subdirectory.
	* Added a 'standalone.mk' Makefile that enables the user to
	compile a standalone version of the eformat package that does
	not depend on the data-collection software, still using the
	data collection repository. You can also make a new tar.gz file
	out of the checked-out version that will be just like the 
	out-dated standalone releases available at the web. For that,
	just issue: 'make -f standalone pack' at your prompt.
	* Removed some CMT global macro dependencies.

Wed Jun  5 17:01:12 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release v1r0p3
	* No more standalone releases!	
	* Fixed some compilation problems with RH-6.1 (gcc-2.91), so the
	package should compile under RH-6.1 now.
	* Smashed some bugs concerning fragment construction from scratch
	(out of high-level objects, not from storages), header-updating
	and validity checks. The library should work more consistently in
	those aspects now.
	* Simplified the Fragment class, reducing the number of required
	template parameters.
	* Enhanced the typedef's for fragments and improved the naming
	conventions for Fragments and Headers. I hope things are clearer
	now.
	* Improved the comments on many parts of the library, so the
	doxygen output should be better and more consistent.
	* Split the packages into two: 
	1) eformat - all eformat functionality, but the DC::buffer storage 
	2) eformat_dc - all eformat functionality, includding DC::buffer
	storage. 
	Depending on your application you might use 1) or 2). Notice that
	the use of 2) includes the use of 1), of course. This split was
	required by the off-line people who want to 'glue' their
	repository to datacollection's.
	* Included an example of how to build fragments from scratch
	(test_raw_build.exe) with lots of comments. This should help
	people that have to do it, like in the ROS(E), SFI, L2PU among
	others.
	* Fixed offset-map read/write for the fragment header, it was not
	beingdone consistenly with the fragment description on the DAQ
	note, now it is. 

Tue May 28 12:59:31 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release 0.2.1
	* Improved comments on Fragment and RODHeader constructors
	* Fixed some compilation problems with gcc-2.91, available on
	redhat release 6.1
	
Thu May 23 16:38:43 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release 0.2
	* Headers and Fragments are now templated with respect to the
	header marker and the storage type. This allows the instantiated 
	class to check for the correct marker and to be used for different
	storage types
	* GenericHeader's are gone. Now the Header is a separate, 
	self-contained entity that contains the generic and specific parts.
	* A better append notification scheme is implemented and test. This
	scheme works within an arbitrary appending depth.
	* More test programs
	* A few bugs were smashed
	* More comments were added
	* More debugging output is now generate by the library, if the user
	switches them on.
	* Created two new storage types. One is based on compressed files and
	is able to read and write compressed files on-the-fly. The second
	one is based on a raw memory allocation scheme. It can be used to
	hold fragments in memory temporarily
	* Implemented "data export" capability on the whole library. Now, 
	fragments contained in a storage (or instantiated from objects
	that would normally use a storage type) can be serialised on
	other storage types by using an equivalent scheme (the serialise()
	method).
	* Some cosmetic changes to enhace the doxygen output were made

Thu May 15 20:43:37 CEST 2002 Andre dos ANJOS <Andre.dos.Anjos@cern.ch>
	* Release 0.1
	* Initial Release
