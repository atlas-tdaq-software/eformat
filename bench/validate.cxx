//Dear emacs, this is -*- c++ -*-

/**
 * @file validate.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Describes a test that benchmarks validation of full events.
 */

#include "eformat/eformat.h"
#include "EventStorage/pickDataReader.h"
#include <iostream>
#include <chrono>

int main (int argc, char** argv)
{
  using namespace eformat;

  if ( argc != 2 ) {
    std::cerr << "usage: " << argv[0] << " <test-file>"
	      << std::endl;
    std::exit(1);
  }

  //time accounting
  double cpu_time_used = 0;
  uint32_t events_read = 0;
  uint32_t components_instantiated = 0;
  uint32_t event_size = 0;

  //open the EventStorage file
  EventStorage::DataReader* d = pickDataReader(argv[1]);

  while (d->good()) {
    uint32_t size_bytes = 0;
    uint32_t* event = 0;

    DRError ecode = d->getData(size_bytes, (char**)&event); //new[] called

    while(ecode == DRWAIT) { 
      usleep(500000);
      std::cout << "[WARNING] Waiting data..." << std::endl << std::flush;
      ecode = d->getData(size_bytes, (char**)&event); //new[] called
    }

    if(DRNOOK == ecode) {
      std::cout << "[ERROR] No more data. Exit." << std::endl << std::flush;
      break; 
    }

    try {
      FullEventFragment<const uint32_t*> fe(event);
      event_size += 4*fe.fragment_size_word();

      auto start = std::chrono::high_resolution_clock::now();
      fe.check_tree();
      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double,std::milli> diff = end - start;

      //calculate the number of event components:
      components_instantiated += 1 + fe.nchildren();

      cpu_time_used += diff.count();
      ++events_read;
    }

    catch (eformat::Issue& ex) {
      std::cerr << std::endl
        << "Uncaught eformat exception: " << ex.what() << std::endl;
    }

    catch (ers::Issue& ex) {
      std::cerr << std::endl
        << "Uncaught ERS exception: " << ex.what() << std::endl;
    }

    catch (std::exception& ex) {
      std::cerr << std::endl
        << "Uncaught std exception: " << ex.what() << std::endl;
    }

    catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }
    
    delete[] event;

  }

  std::cout << std::endl;
  std::cout << " Statistics for Event Validation:" << std::endl;
  std::cout << " --------------------------------" << std::endl;
  std::cout << "  - Total reading time: " << cpu_time_used << " millisecs"
    << std::endl;
  std::cout << "  - Validation time per event ("
    << events_read << "): " << cpu_time_used/events_read
                               << " millisecs" << std::endl;
  std::cout << "  - Average event size: "
    << event_size/(1024.0*1024*events_read) << " megabytes" << std::endl;
  std::cout << "  - Validation time per component ("
    << components_instantiated << "): " 
      << 1e3*cpu_time_used/components_instantiated
         << " microsecs" << std::endl;
  std::exit(0);
}

