//Dear emacs, this is -*- c++ -*-

/**
 * @file header_access.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Describes a test that benchmarks header information access. The test tries
 * to read some words of ROBHeaders, starting from full events.
 */

#include "eformat/eformat.h"
#include "EventStorage/pickDataReader.h"
#include <chrono>
#include <iostream>

int main (int argc, char** argv) {
  using namespace eformat;

  if ( argc != 2 ) {
    std::cerr << "usage: " << argv[0] << " <test-file>"
      << std::endl;
    std::exit(1);
  }

  //time accounting
  double cpu_time_used = 0;
  double validation_cpu_time = 0;
  uint32_t dummy = 0;
  uint32_t robs_read = 0;
  uint32_t event_counter = 0;

  //open normally a file
  std::fstream in(argv[1], std::ios::in|std::ios::binary);
  if (!in) {
    std::cerr << "File `" << argv[1] << "' does not exist?!" << std::endl;
    std::exit(1);
  }

  //open the EventStorage file
  EventStorage::DataReader* d = pickDataReader(argv[1]);

  while (d->good()) {
    uint32_t size_bytes = 0;
    uint32_t* event = 0;

    DRError ecode = d->getData(size_bytes, (char**)&event); //new[] called

    while(ecode == DRWAIT) { 
      usleep(500000);
      std::cout << "[WARNING] Waiting data..." << std::endl << std::flush;
      ecode = d->getData(size_bytes, (char**)&event); //new[] called
    }

    if(DRNOOK == ecode) {
      std::cout << "[ERROR] No more data. Exit." << std::endl << std::flush;
      break; 
    }

    try {
      FullEventFragment<const uint32_t*> fe(event);

      fe.check_tree();
      ++event_counter;

      auto start = std::chrono::high_resolution_clock::now();

      const uint32_t* robp[2048];
      uint32_t nrob = fe.children(robp, 2048);
      for (size_t k=0; k<nrob; ++k) {
        ROBFragment<const uint32_t*> rob(robp[k]);
        dummy += rob.rod_lvl1_id(); //access
        ++robs_read;
      }

      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::nano> diff = end - start;
      cpu_time_used += diff.count();
      start = std::chrono::high_resolution_clock::now();
      fe.check_tree();
      end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> mdiff = end - start;
      validation_cpu_time += mdiff.count();
    }

    catch (eformat::Issue& ex) {
      std::cerr << std::endl
        << "Uncaught eformat exception: " << ex.what() << std::endl;
    }

    catch (ers::Issue& ex) {
      std::cerr << std::endl
        << "Uncaught ERS exception: " << ex.what() << std::endl;
    }

    catch (std::exception& ex) {
      std::cerr << std::endl
        << "Uncaught std exception: " << ex.what() << std::endl;
    }

    catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

    delete[] event;

  } ///event loop

  std::cout << " Statistics for ROB Header access:" << std::endl;
  std::cout << " ---------------------------------" << std::endl;
  std::cout << "  - Total reading time: " << cpu_time_used/1e6 << " milisecs"
    << std::endl;
  std::cout << "  - Reading time per Event ("
    << event_counter << "): " << cpu_time_used/(event_counter*1e3)
                                 << " usecs" << std::endl;
  std::cout << "  - Reading time per ROB ("
    << robs_read << "): " << cpu_time_used/(robs_read)
                             << " nanosecs" << std::endl;
  std::cout << "  - Validation per event (after header access): " 
    << validation_cpu_time/(event_counter)
       << " microseconds" << std::endl;

  std::exit(0);
}


