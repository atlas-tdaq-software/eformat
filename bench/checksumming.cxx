//Dear emacs, this is -*- c++ -*-

/**
 * @file validate.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Describes a test that benchmarks validation of full events.
 */

#include "eformat/checksum.h"
#include <iostream>
#include <cstdlib>
#include <chrono>

double benchmark(uint32_t size, uint32_t times, eformat::CheckSum& type) {
  size /= 4; //we get in words
  uint32_t* data = new uint32_t[size];
  for (uint32_t i=0; i<size; ++i) data[i] = i;
  auto start = std::chrono::high_resolution_clock::now();
  for (uint32_t i=0; i< times; ++i) eformat::helper::checksum(type, data, size);
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double,std::milli> diff = end - start;
  delete[] data;
  return diff.count();
}

int main (int argc, char** argv) {

  if ( argc != 2 ) {
    std::cerr << "usage: " << argv[0] << " <N>" << std::endl;
    std::exit(1);
  }
  eformat::CheckSum types[2] = {eformat::CRC16_CCITT, eformat::ADLER32};
  uint32_t sizes[18] = {
                        0, 40, 80,
                        200, 400, 800, 
                        2000, 4000, 8000, 
                        20000, 40000, 80000, 
                        200000, 400000, 800000, 
                        2000000, 4000000, 8000000
                       };
  
  const uint32_t N = std::strtoul(argv[1], 0, 0);

  std::cout << "Type, Iterations, Size (bytes), Total Time (ms), Average Time (us)"
            << std::endl;
  for (size_t i=0; i<2; ++i) {
    for (size_t j=0; j<18; ++j) {
      double ms = benchmark(sizes[j], N, types[i]);
      std::cout << types[i] << ", "
                << N << ", " << sizes[j] << ", "
                << ms << ", "
                << 1e3*ms/N << std::endl;
    }
  }

  std::exit(0);
}

