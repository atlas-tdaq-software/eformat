//Dear emacs, this is -*- c++ -*-

/**
 * @file data_read.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * Describes a test that benchmarks ROB reading. Each ROB is read once and all 
 * of its ROD fragment data is touched.
 */

#include "eformat/eformat.h"
#include "EventStorage/pickDataReader.h"
#include <iostream>
#include <vector>
#include <chrono>

int main (int argc, char** argv) {
  using namespace eformat;

  if ( argc != 2 ) {
    std::cerr << "usage: " << argv[0] << " <test-file>" << std::endl;
    std::exit(1);
  }

  //time accounting
  double cpu_time_used = 0;
  uint32_t elements_read = 0;
  uint32_t robs_read = 0;

  //open the EventStorage file
  EventStorage::DataReader* d = pickDataReader(argv[1]);

  while (d->good()) {
    uint32_t size_bytes = 0;
    uint32_t* event = 0;

    DRError ecode = d->getData(size_bytes, (char**)&event); //new[] called

    while(ecode == DRWAIT) { 
      usleep(500000);
      std::cout << "[WARNING] Waiting data..." << std::endl << std::flush;
      ecode = d->getData(size_bytes, (char**)&event); //new[] called
    }

    if(DRNOOK == ecode) {
      std::cout << "[ERROR] No more data. Exit." << std::endl << std::flush;
      break; 
    }

    try {
      FullEventFragment<const uint32_t*> fe(event);
      std::vector<const uint32_t*> robs;
      const uint32_t* robp[2048];
      uint32_t nrob = fe.children(robp, 2048);
      uint32_t counter = 0;
      robs.insert(robs.end(), robp, &robp[nrob]);
      auto start = std::chrono::high_resolution_clock::now();;
      for (std::vector<const uint32_t*>::const_iterator it = robs.begin();
          it != robs.end(); ++it) {
        ROBFragment<const uint32_t*> rob(*it);
        const uint32_t* my;
        rob.rod_data(my);
        size_t ndata = rob.rod_ndata();
        for (size_t m=0; m<ndata; ++m) {
          counter += my[m];
          ++elements_read;
        }
        ++robs_read;
      }
      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> diff = end - start;
      cpu_time_used += diff.count();
    }

    catch (eformat::Issue& ex) {
      std::cerr << std::endl
        << "Uncaught eformat exception: " << ex.what() << std::endl;
    }

    catch (ers::Issue& ex) {
      std::cerr << std::endl
        << "Uncaught ERS exception: " << ex.what() << std::endl;
    }

    catch (std::exception& ex) {
      std::cerr << std::endl
        << "Uncaught std exception: " << ex.what() << std::endl;
    }

    catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

    delete[] event;

  }

  std::cout << " Statistics for ROB data access:" << std::endl;
  std::cout << " -------------------------------" << std::endl;
  std::cout << "  - Total reading time: " << cpu_time_used << " millisecs"
    << std::endl;
  std::cout << "  - Reading time per ROB ("
    << robs_read << "): " << 1e3*cpu_time_used/robs_read
                             << " microsecs" << std::endl;
  std::cout << "  - Reading time per data word in a ROB (" 
    << elements_read << "): "
      << 1e6*cpu_time_used/elements_read << " nanosecs" << std::endl;

  std::exit(0);
}

