#!/bin/bash
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Fri 23 Nov 2007 12:24:50 PM CET
export PYTHONPATH=${TDAQ_PYTHONPATH}
export PYTHONPATH=$PWD/../../installed/$CMTCONFIG/lib:$PWD/../../installed/share/lib/python/$PYTHONPATH
tdaq_python ../bench/streamtag_timing.py $@
exit $?
