#!/usr/bin/env tdaq_python


from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import eformat
import timeit
import sys
  
def prepare_list(n):
    st = eformat.helper.StreamTag()
    st.name = "First"
    st.type = "physics"
    for i in range(n):
        st.robs.append(i)
    

    sts = eformat.helper.StreamTags()
    sts.append(st)
    return sts


def encode(list):
    return list.__raw__()


def decode(blob, list):
    list.__fromraw__(blob)



if __name__ == "__main__":
    enc = []
    dec = []
    
    for r in range(0,201,10):

        basic="""\
from __main__ import prepare_list, encode, decode
sts = prepare_list(%d)
""" % r

        encode_st="""\
l = encode(sts)
"""
       
        decode_st="""\
decode(l,sts)    
"""        
        t_enc = timeit.Timer(stmt=encode_st,setup=basic)
        t_dec = timeit.Timer(stmt=decode_st,setup=basic+encode_st)

        nloop = int(sys.argv[1])
        nrep = 3  
        enc.append((r, old_div(sum(t_enc.repeat(repeat = nrep, number = nloop)),(nloop*nrep))))
        dec.append((r, old_div(sum(t_dec.repeat(repeat = nrep, number = nloop)),(nloop*nrep))))

    print("StreamTag encoding result")
    print("#ROB\ttime(us)")
    for a,b in enc:
        print("%d\t%.1f" % (a,b*1000000))

    print()
    print("StreamTag decoding result")
    print("#ROB\ttime(us)")
    for a,b in dec:
        print("%d\t%.1f" % (a,b*1000000))
        
#x,y = zip(*res)
#pickle.dump((x,y),open(sys.argv[1],'w'),protocol=-1)




